import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewBulkTemplateComponent } from './view-bulk-template.component';

describe('ViewBulkTemplateComponent', () => {
  let component: ViewBulkTemplateComponent;
  let fixture: ComponentFixture<ViewBulkTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBulkTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBulkTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
