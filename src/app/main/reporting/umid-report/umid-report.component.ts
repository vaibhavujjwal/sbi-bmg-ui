import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from "../../../common/common.service";
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { ReportingService } from '../reporting.service';
import { MatTableDataSource, MatSort, MatPaginator } from '../../../../../node_modules/@angular/material';

@Component({
  selector: 'app-umid-report',
  templateUrl: './umid-report.component.html',
  styleUrls: ['./umid-report.component.scss']
})
export class UmidReportComponent implements OnInit {
  reportingForm: FormGroup;
  reportType:boolean;
  showReport:boolean;
  formattedFromDate: any;
  dlt_TEMPLATE_ID:any;
  now = moment(new Date());
  minDateTo: any;
  maxDateTo = this.now.format('YYYY-MM-DD');
  maxDate = this.now.format('YYYY-MM-DD');
  displayData = "user data";
  submittedTime : any;
  aggregatorName : any;
  submit_ID : any;
  deliveryStatus : any;
  reason : any;
  in_flag : any;
  campaign_ID : any;
  campaign_NAME : any;
  department_User : any;
  mobileNumber : any;
  senderCli : any;
  sms_type : any;
  dept_NAME : any;
  category : any;
  channel : any;
  sms_Content : any;
  delivery_TIME : any;
  scheduled_DATE : any;
  teleCircle : any;
  operator : any;
  pdfResponse : any;
  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService, 
    private reportingService: ReportingService, public commonService: CommonService) { 
    }

  ngOnInit() {
    this.initializeForm();
    this.reportType = true;
  }
  initializeForm(){
    this.reportingForm = this.fb.group({
      date: [new Date().toISOString().substr(0, 10)],
      umid: ['', [Validators.required, Validators.minLength(25), Validators.maxLength(31),Validators.pattern(this.commonService.patterns.numberOnly)]]
      
    });
  };

  submitFormUmid(obj){
    if(this.reportingForm.invalid){
      Object.keys(this.reportingForm.controls).forEach((key,index)=>{
        console.log(key)
        if(this.reportingForm.controls[key].invalid){
          this.reportingForm.controls[key].markAsTouched();
        }
      })
      return;
    }

    let umidDate = new Date(this.reportingForm.controls['date'].value);
    this.formattedFromDate = moment(umidDate).format("YYYY-MM-DD");
    let req = {
        umid : this.reportingForm.controls['umid'].value,
        date : this.formattedFromDate,
    }
    console.log(req);
    this.spinner.show();
    let serviceName = "getDetailReportsUmid";
    if(obj=='download'){
      serviceName = "generateDLRPDFUmid";
    }
    this.reportingService[serviceName](req).subscribe(
      res => {
        this.spinner.hide();
        if ((!obj || obj!="download")&& res && res['data'] && res['data'].reportResponseList.length) {
          this.showReport = true;
          let resp = res['data'].reportResponseList[0];
          this.submittedTime = resp["submit_TIME"];
          this.aggregatorName = resp["aggregator"]?resp["aggregator"]:'NA';
          this.submit_ID = resp["umid"];
          //this.deliveryStatus = resp["submit_STATE"];

          if (resp["submit_STATE"] == 504) {
            this.deliveryStatus = "Delivered";
          }
          else if (resp["submit_STATE"] == 502) {
            this.deliveryStatus = "Delivery awaited";
          }else if (resp['submit_STATE'] == 506 && resp['error_DESC']==="MSG_DELIVERY_TIMEOUT") {
            resp['submit_STATE'] = "Pending";
            resp['error_DESC']="Message delivery awaited"
          }else {
            this.deliveryStatus = "Failed";
          }
          this.reason = resp["error_DESC"];
          this.in_flag = "National";
          if (resp["in_FLAG"] == 1) {
            this.in_flag = "International";
          }
          this.campaign_ID = resp["campaign_ID"];
          if (resp["campaign_ID"] == 0) {
            this.channel = "API";
          } else if (resp["campaign_ID"] > 0) {
            this.channel = "Campaign";
          } else if (resp["campaign_ID"] == null) {
            this.channel = null;
          }
          this.campaign_NAME = resp["campaign_NAME"]?resp["campaign_NAME"]:"NA";
          this.department_User = resp["username"];
          this.mobileNumber = resp["msisdn"];
          this.senderCli = resp["cli"];
          this.sms_type = resp["dcs"];
          if (resp["dcs"] == 0) {
            this.sms_type = "Text";
          } else if (resp["dcs"] == 1 || resp["dcs"] == 8) {
            this.sms_type = "Unicode";
          } 
          this.dlt_TEMPLATE_ID = resp["dlt_TEMPLATE_ID"]?resp["dlt_TEMPLATE_ID"]:"NA"
          this.sms_Content = resp["msg_TXT"];
          if (this.sms_type == "Unicode") {
            try {
              this.sms_Content = this.commonService.unicodeToText(resp["msg_TXT"]);
            }
            catch (e) {
              console.log("comes in catch block...");
              this.sms_Content = resp["msg_TXT"];
            }
          }        
          this.dept_NAME = resp["dept_NAME"];
          this.category = resp["category"];

          if (resp["category"] == 0) {
            this.category = "NA";
          }
          else if (resp["category"] == 1) {
            this.category = "OTP";
          }
          else if (resp["category"] == 2) {
            this.category = "Transactional";
          }
          else if (resp["category"] == 3) {
            this.category = "Informational";
          }
          else if (resp["category"] == 4) {
            this.category = "Promotional";
          }
          //this.channel = resp["channel"];
          this.delivery_TIME = resp["delivery_TIME"];
          this.scheduled_DATE = resp["scheduled_DATE"];
          this.teleCircle = resp["circle"];
          this.operator = resp["operator"];
        }else if(obj=="download" && res){
            this.pdfResponse = res['data'];
            console.log("PDF API response........" + this.pdfResponse);
            let date = new Date();
            console.log("Date for PDF....:" + moment(date).format('YYYYMMDDHHMMSS'))
            const fileName = "SMS_UMID_DLR_REPORT" + moment(date).format('YYYYMMDDHHMMSS') + ".pdf";
            this.downloadFileUmid(this.pdfResponse, fileName);
        } 
        
        
        else {
          this.showReport = false;
          this.commonService.showErrorToast("No Data Found");
        }
      });

      
  }

  downloadFileUmid(pdfBase64, fileName) {
    var res = pdfBase64.substr(28, (pdfBase64.length - 28));
    pdfBase64 = res;
    console.log("substr..." + pdfBase64);
    // IE doesn't allow using a blob object directly as link href
    // instead it is necessary to use msSaveOrOpenBlob
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      var byteCharacters = atob(pdfBase64);
      var byteNumbers = new Array(byteCharacters.length);
      for (var i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      var blob = new Blob([byteArray], { type: 'application/pdf' });
      window.navigator.msSaveOrOpenBlob(blob, fileName);
      return;
    }

    // For Other Browsers
    const linkSource = 'data:application/pdf;base64,' + pdfBase64;
    const link = document.createElement('a');
    link.href = linkSource;
    link.download = fileName;
    document.body.appendChild(link);
    link.click();
    link.remove();

    window.addEventListener("focus", function () {
      URL.revokeObjectURL(link.href);
    }, { once: true });
  }

  getType(type){
    this.reportType = true;
    if(type=="detailed"){
      this.reportType = false;
    }
  }
  restrictKeywordUmid(event: Event) {
    event.preventDefault();
  }
}
