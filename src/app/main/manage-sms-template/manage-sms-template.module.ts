import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ManageSmsTemplateComponent} from './manage-sms-template.component';
import {ApprovedSmsTemplateComponent} from './approved-sms-template/approved-sms-template.component';
import {TemplatePendingComponent} from './template-pending/template-pending.component';
import {BulkUploadTemplateComponent} from './bulk-upload-template/bulk-upload-template.component';
import { routing } from './manage-sms-template.routing';
import { ManageSmsTemplateService } from "./manage-sms-template.service";
import { SharedModule } from '../../common/shared.modules';
import {MatDatepickerModule,MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule} from '@angular/material';
import { CreateSmsTemplateComponent } from './create-sms-template/create-sms-template.component';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from '../../validators/format-datepicker';

@NgModule({
  imports: [
    CommonModule,
    routing,
    SharedModule,
    MatDatepickerModule,
    MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
    MatSortModule, MatTableModule

  ],
  declarations: [ManageSmsTemplateComponent, ApprovedSmsTemplateComponent, TemplatePendingComponent, BulkUploadTemplateComponent,
     CreateSmsTemplateComponent],
  providers: [ManageSmsTemplateService,
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class ManageSmsTemplateModule { }



