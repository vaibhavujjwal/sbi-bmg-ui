import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Router } from "@angular/router";
import { DashboardComponent } from "./dashboard.component";
import { ChartsModule } from 'ng2-charts';
import { DashboardService } from "./dashboard.service";
import { SharedModule } from "../../common/shared.modules";
import { ModuleGuardService } from '../../guards';

export const routes: Routes = [
  {
    path: "",
    component: DashboardComponent,
    canActivate : [ModuleGuardService]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ChartsModule,
    SharedModule
  ],
  declarations: [DashboardComponent],
  providers: [DashboardService]
})
export class DashboardModule { }
