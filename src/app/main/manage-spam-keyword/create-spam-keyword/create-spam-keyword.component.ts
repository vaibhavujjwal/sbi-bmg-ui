import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from '../../../common/common.service';
import { ManageSpamKeywordService } from '../manage-spam-keyword.service';
import { RESPONSE } from "../../../common/common.const";
import { Router } from "@angular/router";

@Component({
  selector: 'app-create-spam-keyword',
  templateUrl: './create-spam-keyword.component.html',
  styleUrls: ['./create-spam-keyword.component.scss']
})

export class CreateSpamKeywordComponent implements OnInit {
  spamKeywordForm: FormGroup;
  formSubmitted: boolean = false;
  isEdit: boolean = false;
  caseFlag:any;
  keywordNAme:any;
  private modalRef: NgbModalRef;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };

  constructor(private fb: FormBuilder, private router: Router,
    private modalService: NgbModal, private spinner: NgxSpinnerService,
    private spamService: ManageSpamKeywordService, private commonService: CommonService, ) { }

  ngOnInit() {
    this.initialiseForms();
  }

  initialiseForms() {
    this.spamKeywordForm = this.fb.group({
      level: ['', [Validators.required]],
      keyword: ['', [Validators.required, Validators.pattern(this.commonService.patterns.tenCommaSeparatedKeywords)]]
    });
  }

  caseSensitive(){
    this.formSubmitted = true;
    if(this.spamKeywordForm.controls["keyword"].valid) {
    this.keywordNAme = this.spamKeywordForm.controls["keyword"].value.trim();
      this.spinner.show();
      this.spamService.getsensitiveFlag(this.keywordNAme).subscribe(
        res => {
        this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.caseFlag = res["data"];
            if(this.caseFlag == false){
                this.commonService.showErrorToast("This Spam Keyword already exists. Please enter new Spam Keyword.");
                this.spamKeywordForm.controls["keyword"].setValue(" ");
            }
        } else {
            this.caseFlag = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
    this.formSubmitted=false;
    }
  }


  submitForm() {
    this.formSubmitted = true;
    if(this.spamKeywordForm.valid) {
      let reqObj = {
          "loginId": this.commonService.getUser(),
          "loginUsername": this.commonService.getUserName(),
          "keyword": this.spamKeywordForm.controls.keyword.value.trim(),
          "level": this.spamKeywordForm.controls.level.value
      }
      this.spinner.show();
      this.spamService.createSpamKeywords(reqObj).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast(reqObj['keyword'] +' added succesfully');
            this.router.navigate(["/main/manage-spam-keyword/list-spam-keyword"]);
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
          this.formSubmitted = false;
      });
    }
  }
}
