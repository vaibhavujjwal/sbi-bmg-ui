import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMoRouteComponent } from './create-mo-route.component';

describe('CreateMoRouteComponent', () => {
  let component: CreateMoRouteComponent;
  let fixture: ComponentFixture<CreateMoRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMoRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMoRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
