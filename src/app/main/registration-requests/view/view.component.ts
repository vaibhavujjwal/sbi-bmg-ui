import { Component, OnInit } from "@angular/core";
import { RegistrationRequestsService } from "../registration-requests.service";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {
  NgbModal,
  NgbModalRef,
  NgbModalOptions,
  ModalDismissReasons
} from "@ng-bootstrap/ng-bootstrap";
import {
  RESPONSE,
  USER_REG_STATUS,
  MODULE,
  USER_ROLE
} from "src/app/common/common.const";
import { CommonService } from "src/app/common/common.service";
import * as FileSaver from "file-saver";

@Component({
  selector: "app-view",
  templateUrl: "./view.component.html",
  styleUrls: ["./view.component.scss"],
  providers: [RegistrationRequestsService]
})
export class ViewComponent implements OnInit {
  MODULES = MODULE;
  ROLES = USER_ROLE;
  APPROVED = USER_REG_STATUS.APPROVED;
  PENDING = USER_REG_STATUS.PENDING;
  REJECTED = USER_REG_STATUS.REJECTED;

  NEW_REQUEST = "N";
  EXISTING_REQUEST = "E";

  private modalRef: NgbModalRef;
  modalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;

  rows = [];
  temp = [];
  columns = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  table: DatatableComponent;
  searchText: string = "";

  statusList: Array<object> = [];

  selectedRequest: object = {};
  p: number = 1;

  updateStatusForm: FormGroup;
  isFormSubmitted: boolean = false;

  txnIdSorted: boolean = false;
  usernameSorted: boolean = false;
  emailSorted: boolean = false;
  statusSorted: boolean = false;
  dateSorted: boolean = false;

  constructor(
    private _service: RegistrationRequestsService,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    public commonService: CommonService
  ) {}

  ngOnInit() {
    this.pageLimit = this.commonService.recordsPerPage[0];
    // this.getRequestsList();
    this.getStatusList();
  }

  // getRequestsList() {
  //   this.spinner.show();
  //   this._service
  //     .getRequestsList(this.commonService.getUserRoleId())
  //     .subscribe(data => {
  //       if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
  //         let tmp = data["data"];
  //         tmp.forEach(element => {
  //           if (element.l1Status["id"] == USER_REG_STATUS.PENDING) {
  //             element["overallStatus"] = "Pending at L1";
  //           } else if (element.l1Status["id"] == USER_REG_STATUS.REJECTED) {
  //             element["overallStatus"] = "Rejected at L1";
  //           } else if (element.l2Status["id"] == USER_REG_STATUS.PENDING) {
  //             element["overallStatus"] = "Pending at L2";
  //           } else if (element.l2Status["id"] == USER_REG_STATUS.REJECTED) {
  //             element["overallStatus"] = "Rejected at L2";
  //           } else if (element.l3Status["id"] == USER_REG_STATUS.PENDING) {
  //             element["overallStatus"] = "Pending at L3";
  //           } else if (element.l3Status["id"] == USER_REG_STATUS.REJECTED) {
  //             element["overallStatus"] = "Rejected at L3";
  //           } else if (element.l3Status["id"] == USER_REG_STATUS.APPROVED) {
  //             element["overallStatus"] = "Approved";
  //           }
  //         });
  //         this.rows = this.temp = tmp;
  //       } else {
  //         this.rows = this.temp = [];
  //         this.commonService.showErrorToast(data['result']['userMsg']);
  //       }
  //       this.spinner.hide();
  //     });
  // }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
    this.rows = this.commonService.sortArray(isAssending, sortBy, this.rows, isNumber);
  }


  // applySearch() {
  //   let self = this;
  //   this.rows = this.temp.filter(function(item) {
  //     return JSON.stringify(Object.values(item))
  //       .toLowerCase()
  //       .includes(self.searchText.toLowerCase());
  //   });
  // }

  viewRequestDetailsModal(request, content, btn) {
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();

    this.selectedRequest = request;

    this.modalRef = this.modalService.open(content, this.modalOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  getStatusList() {
    this.statusList = this._service.getStatusList();
  }

  prepareStatusUpdateForm() {
    this.updateStatusForm = this.formBuilder.group({
      status: ["", [Validators.required]],
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.remarksDescription)
        ]
      ]
    });
  }

  openStatusUpdateModal(request, content) {
    // btn &&
    //   btn.parentElement &&
    //   btn.parentElement.parentElement &&
    //   btn.parentElement.parentElement.parentElement &&
    //   btn.parentElement.parentElement.parentElement.blur();

    this.selectedRequest = request;
    this.isFormSubmitted = false;
    this.prepareStatusUpdateForm();

    this.modalRef = this.modalService.open(content, this.modalOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  updateStatus() {
    this.isFormSubmitted = true;
    if (this.updateStatusForm.valid) {
      this.spinner.show();
      let statusInfo = {
        remarks: this.updateStatusForm.controls.remarks.value,
        roleId: this.commonService.getUserRoleId(),
        status: this.updateStatusForm.controls.status.value
      };
      this._service
        .updateRequestStatus(this.selectedRequest["txnId"], statusInfo)
        .subscribe(data => {
          this.spinner.hide();
          if (data) {
            this.commonService.showSuccessToast('update');
          } else {
            this.commonService.showErrorToast(data['result']['userMsg']);
            this.modalRef.close();
            // this.getRequestsList();
          }
        });
    }
  }

  downloadFile(id) {
    this.spinner.show();
    this._service.downloadFile(id).subscribe(
      data => {
        const blob = new Blob([data], { type: "application/pdf" });
        this.spinner.hide();
        FileSaver.saveAs(data, id + "_" + new Date().getTime() + ".pdf");
      },
      error => {
        this.commonService.showErrorAlertMessage("File not found");
      }
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
