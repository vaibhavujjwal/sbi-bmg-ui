import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {
  constructor() { }

  transform(items: any, filter: any, defaultFilter: boolean): any {

    if (!filter) {
      return items;
    }

    if (!Array.isArray(items)) {
      return items;
    }

    if (filter && Array.isArray(items)) {
      let filterKeys = Object.keys(filter);

      if (defaultFilter) {
        return items.filter(item => filterKeys.reduce((x, keyName) =>
          (x && new RegExp(filter[keyName], 'gi').test(item[keyName])) || filter[keyName] === '', true));
      } else {
        return items.filter(item => {
          return filterKeys.some((keyName) => {
            let result: any;
            result = new RegExp(filter[keyName], 'gi').test(item[keyName]) || filter[keyName] === '';

            if (result === false && typeof (item[keyName]) === 'object') {
              // check if the filter property is present as an internal object
              const inneOobjectToFilter: object = item[keyName];
              if (inneOobjectToFilter !== undefined) {

                for (let key in inneOobjectToFilter) {
                  if (new RegExp(filter[keyName], 'gi').test(inneOobjectToFilter[key])) {
                    result = true;
                    break;
                  } else {
                    result = false;
                  }
                }
              }
              // if (item['primaryRouteId'].hasOwnProperty(keyName) &&
              //     new RegExp(item['primaryRouteId'][keyName], 'gi').test(filter[keyName])) {
              //   result = true;
              // } else if (item['secondaryRouteId'].hasOwnProperty(keyName) && item['secondaryRouteId'][keyName] === filter[keyName]) {
              //   result = true;
              // } else if (item['retryRouteId'].hasOwnProperty(keyName) && item['retryRouteId'][keyName] === filter[keyName]) {
              //   result = true;
              // } else if (item['retryDetails'][0].hasOwnProperty(keyName) && item['retryDetails'][0][keyName] === filter[keyName]) {
              //   result = true;
              // } else {
              //   result = false;
              // }
            }
            return result;
          });
        });
      }
    }
  }
}