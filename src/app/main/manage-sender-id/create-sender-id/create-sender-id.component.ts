import { Component, OnInit } from '@angular/core';
import { SenderIdService } from '../manage-sender-id.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";
import { Router } from "@angular/router";
import { stringValidators } from '../../../validators/number.validator';

@Component({
  selector: 'app-create-sender-id',
  templateUrl: './create-sender-id.component.html',
  styleUrls: ['./create-sender-id.component.scss']
})

export class CreateSenderIdComponent implements OnInit {
  users = [];
  senderIdForm: FormGroup;
  formSubmitted: boolean = false;
  isEdit: boolean = false;
  uniqueFlag: any;
  senderLength : any;
  showLengthError : any;

  senderIDReq = {};
  userSearchType : number = 1;
  userSetting = {};
  constructor(private senderIdService: SenderIdService, private router: Router,
  private fb: FormBuilder, private spinner: NgxSpinnerService, private commonService: CommonService) { }

  ngOnInit() {
    this.getAllUsers();
    this.initialiseForms();
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
    this.users = this.commonService.sortArray(isAssending, sortBy, this.users, isNumber );
  }

  initialiseForms() {
    this.senderIdForm = this.fb.group({
      user: ['', [Validators.required]],
      senderId: ['', { validators: [Validators.required ,  stringValidators , Validators.pattern(this.commonService.patterns.cliPattern)] }]
    });
  }

  getAllUsers() {
    this.spinner.show();
    this.senderIdService.getUsersList(this.userSearchType).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          debugger;
          this.users = res["data"].filter(
            x => x["roleId"] == 4 || x["roleId"] == 5
          );
          this.sortTable('userName', true);

          this.userSetting = {
            singleSelection: true,
            idField: 'userId',
            textField: 'userName',
            itemsShowLimit: 3,
            allowSearchFilter: true,
            defaultOpen: false
          }

        } else {
          this.users = [];
        }
      });
  }


  uniqueCheck() {
    this.formSubmitted = true;
    if (this.senderIdForm.controls["senderId"].valid) {

   if(this.senderIdForm.controls["senderId"].value){
    if (this.senderIdForm.controls["senderId"].value.indexOf(",") == -1) {
      if(this.senderIdForm.controls["senderId"].value != null && this.senderIdForm.controls["senderId"].value != "" ){
            this.spinner.show();
            this.senderIDReq = {
              "senderID": this.senderIdForm.controls["senderId"].value.trim()
            }
            this.senderIdService.getUniqueFlag(this.senderIDReq).subscribe(
            res => {
              this.spinner.hide();
              if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
                this.uniqueFlag = res["data"];
                if (this.uniqueFlag == false) {
                  this.commonService.showErrorToast("This CLI already exists. Please enter new CLI.");
            }
      } else {
           this.uniqueFlag = [];
           this.commonService.showErrorToast(res['result']['userMsg']);
         }
       });
     }
    }
   }
   this.formSubmitted=false;
  }
  }



  submitForm(){
    debugger;
    this.formSubmitted = true;
    if (this.senderIdForm.valid) {
      var userArray=this.senderIdForm.controls["user"].value;
      for(let i=0;i<userArray.length;i++){
             var userID = userArray[i].userId;
      }
      let reqObj = {
        "userId": userID,
        "senderid": this.senderIdForm.controls["senderId"].value.trim()
      }
      debugger;
      this.spinner.show();
      this.senderIdService.saveSenderId(reqObj).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast(reqObj['senderid'] + ' CLI created successfully');
            this.router.navigate(["/main/manage-sender-id/list-sender-id"]);
            this.initialiseForms();
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
          this.formSubmitted = false;
        });
    }
  }
}
