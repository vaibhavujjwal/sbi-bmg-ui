import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { AddComponent } from "./add/add.component";
import { ViewComponent } from "./view/view.component";
import { ManageAggregatorComponent } from "./manage-aggregator.component";
import { MODULE } from "src/app/common/common.const";

export const routes: Routes = [
  {
    path: "",
    component: ManageAggregatorComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/manage-aggregator/view",
        pathMatch: "full"
      },
      {
        path: "view",
        component: ViewComponent,
        data: { moduleName: MODULE.AGGREGATOR, permissions: ["R", "RW"] }
      },
      {
        path: "add",
        component: AddComponent,
        data: { moduleName: MODULE.AGGREGATOR, permissions: ["RW"] }
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
