export interface FailedReportResponse {
    submitTime: string,
    aggregator: string,
    state: number,
    errorDesc: string,
    rcount: number,
    }