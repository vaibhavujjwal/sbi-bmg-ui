import { ManageBulkTemplateModule } from './manage-bulk-template.module';

describe('ManageBulkTemplateModule', () => {
  let manageBulkTemplateModule: ManageBulkTemplateModule;

  beforeEach(() => {
    manageBulkTemplateModule = new ManageBulkTemplateModule();
  });

  it('should create an instance', () => {
    expect(manageBulkTemplateModule).toBeTruthy();
  });
});
