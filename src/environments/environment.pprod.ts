export const environment = {
  production: false,
  backendApiURL: "https://smsguipprod.sbi.co.in:8443/sbi/config/v1/",
  backendApiURLMG: "https://smsguipprod.sbi.co.in:8443/sbi/config/v1/",
  backendLogin: "https://smsguipprod.sbi.co.in:8443/",
  backendApiURLNonEncrytion: "https://smsguipprod.sbi.co.in:8443/sbi/config/v2/",
};