import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '../../../../../node_modules/@angular/forms';
import { NgxSpinnerService } from '../../../../../node_modules/ngx-spinner';
import { ReportingService } from '../reporting.service';
import { ExcelService } from '../../../common/excel.service';
import { CommonService } from '../../../common/common.service';
import * as moment from 'moment';
import { RESPONSE } from '../../../common/common.const';
import { MatTableDataSource, MatPaginator, MatSort } from '../../../../../node_modules/@angular/material';
import { FailedReportResponse } from './failed-response';

@Component({
  selector: 'app-failed-sms',
  templateUrl: './failed-sms.component.html',
  styleUrls: ['./failed-sms.component.scss']
})
export class FailedSMSComponent implements OnInit {

  reportingForm: FormGroup;
  formSubmitted: boolean = false;
  searchText: string = "";
  pageLimit: number;
  // p: number = 1;
  sDate: any;
  then: any;
  endDte: any;
  edate: any;
  eDate: any;
  now = moment(new Date());
  minDate1: any;
  minDate: any;
  maxDate = this.now.format('YYYY-MM-DD');
  maxDate1 = this.now.format('YYYY-MM-DD');
  formattedFromDate: any;
  formattedToDate: any;
  reportList = [];
  aggregatorList: Array<any> = [];
  aggSettings: object = {};
  selectedAggregatorList: string = "";
  dataSource: MatTableDataSource<FailedReportResponse>;
  dataSourceLength: number;
  @ViewChild('input') input: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['submitTime', 'aggregator', 'state', 'errorDesc', 'rcount'];

  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
    private reportingService: ReportingService,
    private excelService: ExcelService, public commonService: CommonService) { }

  ngOnInit() {
    this.initializeForm();
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.setEndDAte();
    this.minDate = new Date('Jan 01, 2019');
    this.getAggregatorList();
  }

  initializeForm() {
    this.reportingForm = this.fb.group({
      startDate: [new Date().toISOString().substr(0, 10)],
      endDate: [new Date().toISOString().substr(0, 10)],
      aggregator: ['', Validators.required]
    });
  }

  getAggregatorList() {
    this.spinner.show();
    this.reportingService.getAggregatorList().subscribe(res => {
      this.spinner.hide();
      this.aggregatorList = res['data'];
    });
    this.aggSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'aggregatorName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      defaultOpen: false
    };
  }

  submitForm() {
    this.formSubmitted = true;
    let fromDate = new Date(this.reportingForm.controls['startDate'].value);
    let toDate = new Date(this.reportingForm.controls['endDate'].value);
    this.formattedFromDate = moment(fromDate).format("YYYY-MM-DD");
    this.formattedToDate = moment(toDate).format("YYYY-MM-DD");
    this.reportingForm.controls['startDate'].setValue(this.formattedFromDate);
    this.reportingForm.controls['endDate'].setValue(this.formattedToDate);
    if (this.reportingForm.valid) {
      let aggregatorSelected = this.reportingForm.controls['aggregator'].value;
      if (aggregatorSelected.length > 0) {
        this.selectedAggregatorList = aggregatorSelected.map(aggregatorItem => aggregatorItem['aggregatorName']).toString();
      }
      else {
        this.selectedAggregatorList = this.reportingForm.controls['aggregator'].value;
      }

      let req = {
        startDate: this.reportingForm.controls['startDate'].value,
        endDate: this.reportingForm.controls['endDate'].value,
        aggregator: this.selectedAggregatorList.split(',').length < this.aggregatorList.length ? this.selectedAggregatorList : 'all'//this.selectedAggregatorList
      }
      this.spinner.show();

      this.reportingService.getFailedSMSAggregatorWise(req).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.reportList = res['data'];
            this.reportList.forEach(item => { if (!item['aggregator']) item['aggregator'] = 'NA' })
            this.dataSource = new MatTableDataSource(this.reportList);
            if (this.dataSource.data.length == 0) {
              this.dataSourceLength = 0;
            } else {
              this.dataSourceLength = this.dataSource.data.length;
            }
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;

          } else {
            this.reportList = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length == 0) {
      this.dataSourceLength = 0;
    }
    else {
      this.dataSourceLength = this.dataSource.filteredData.length;
    }
  }

  
  setEndDAte() {
    this.sDate = this.reportingForm.controls['startDate'].value,
      this.then = moment(new Date(this.sDate));
    this.minDate1 = this.then.format('YYYY-MM-DD');
    var isDateBeforeCurrent = moment(this.then.add(1, 'M'), 'YYYY-MM-DD').isBefore(moment());
    moment(this.then.subtract(1, 'M'));
    if (isDateBeforeCurrent == false) {
      this.maxDate1 = moment(new Date()).format('YYYY-MM-DD');
    } else {
      this.maxDate1 = this.then.add(1, 'M').format('YYYY-MM-DD');
    }
    this.reportingForm.controls['endDate'].setValue(this.sDate);
  }

  setStartDAte() {
    this.maxDate = moment(new Date()).format('YYYY-MM-DD');
  }

  restrictKeyword(event: Event) {
    event.preventDefault();
  }
  
  exportAs(fileType: string) {
    console.log(" xnkjasnjkajksjkdnke" + this.reportList)
    let data = [];
    this.reportList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Submit Date"] = new Date(element.submitTime).toLocaleDateString('en-GB');
      excelDataObject["Aggregator Name"] = element.aggregator;
      excelDataObject["Count"] = element.rcount;
      excelDataObject["Error Description"] = element.errorDesc;
      excelDataObject["State"] = element.state;
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'Failed-SMS-Report');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'Failed-SMS-Report');
        break;
      default:
        this.excelService.exportAsCsvFile(data, 'Failed-SMS-Report');
        break;
    }
  }
}
// sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
  //   this.reportList = this.commonService.sortArray(isAssending, sortBy, this.reportList, isNumber);
  // }
