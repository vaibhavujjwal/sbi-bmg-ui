import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ManageCampaignService } from '../manage-campaign.service';
import { ExcelService } from "../../../common/excel.service";
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";
import * as moment from 'moment';
import {
  NgbModal,
  NgbModalRef,
  NgbModalOptions,
  ModalDismissReasons
} from '@ng-bootstrap/ng-bootstrap';
import { MODULE } from "../../../common/common.const";
import { MatTableDataSource, MatSort, MatPaginator } from '../../../../../node_modules/@angular/material';
import { ResponseView } from '../ResponseView';

@Component({
  selector: 'app-pending-approval',
  templateUrl: './pending-approval.component.html',
  styleUrls: ['./pending-approval.component.scss']
})
export class PendingApprovalComponent implements OnInit {
  MODULES = MODULE;
  largeModalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false,
    size: "lg"
  };
  campaignForm: FormGroup;
  formSubmitted: boolean = false;
  campaignList = [];
  searchText: string = "";
  pageLimit: number;
  p: number = 1;
  // campIdSorted: boolean = false;
  // statusSorted: boolean = false;
  // scheduleTimeSorted: boolean = false;
  // campNameSorted: boolean = false;
  // createdTimeSorted: boolean = false;
  // messageTypeSorted: boolean = false;
  // typeFlagIdSorted: boolean = false;
  // messageTextSorted: boolean = false;
  // senderIdSorted: boolean = false;
  // totalRecordCountSorted: boolean = false;
  // successfulRecordCountSorted: boolean = false;
  // failedRecordCountSorted: boolean = false;
  // expiredRecordCountSorted: boolean = false;
  // retriedRecordCountSorted: boolean = false;
  // deliveryExpiredCountSorted: boolean = false;
  // invalidRecordCountSorted: boolean = false;
  private modalRef: NgbModalRef;
  statusForm: FormGroup;
  changeStatusCamp: boolean = false;
  userDetails: object;
  closeResult: string;
  updateUserStatusEvent: string;
  dataSource: MatTableDataSource<ResponseView>;
  // isDataAvail: boolean = false;
  @ViewChild('input') input: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['actions', 'templateIdDlt','id', 'campaignName', 'statusDesc', 'scheduleTime', 'createdOn',
    'messageType', 'messageText', 'senderId', 'typeFlag', 'totalRecordCount'];

  constructor(private modalService: NgbModal, private fb: FormBuilder,
    private spinner: NgxSpinnerService, private manageCampaignService: ManageCampaignService,
    private excelService: ExcelService, private formBuilder: FormBuilder,
    public commonService: CommonService) { }

  ngOnInit() {
    this.initializeForm();
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.setEndDAte(true);
    this.submitForm();
  }

  initializeForm() {
    var today = new Date();
    var priorDate = new Date().setDate(today.getDate()-7);
    this.campaignForm = this.fb.group({
      startDate: [new Date(priorDate).toISOString().substr(0, 10)],
      endDate: [new Date().toISOString().substr(0, 10)]
    });
  }
  formattedFromDate: any;
  formattedToDate: any;
  submitForm() {
    this.formSubmitted = true;
    let fromDate = new Date(this.campaignForm.controls['startDate'].value);
    let toDate = new Date(this.campaignForm.controls['endDate'].value);

    this.formattedFromDate = moment(fromDate).format("YYYY-MM-DD");
    this.formattedToDate = moment(toDate).format("YYYY-MM-DD");

    this.campaignForm.controls['startDate'].setValue(this.formattedFromDate);
    this.campaignForm.controls['endDate'].setValue(this.formattedToDate);

    if (this.campaignForm.valid) {
      let req = {
        "startDate": this.campaignForm.controls['startDate'].value,
        "endDate": this.campaignForm.controls['endDate'].value,
        "type": this.commonService.getUserType(true)
        // "userId": this.commonService.getUser()
      };
      this.spinner.show();
      this.manageCampaignService.getCampaignList(req).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.campaignList = res['data'];
            this.campaignList.forEach(element => {
              if (element['messageType'] == 2) {
                try {
                  element['messageText'] = this.commonService.unicodeToText(element['messageText']);
                }
                catch (err) {
                  element['messageText'] = element['messageText'];
                  console.log("error while coverting unicode ........" + err);
                }
              }

              if (element['messageType'] == 2) {
                element['messageType'] = "Unicode";
              } else if (element['messageType'] == 1) {
                element['messageType'] = "Text";
              }

              if (element['typeFlag'] == 'D') {
                element['typeFlag'] = "National";
              } else {
                element['typeFlag'] = "International";
              }
            });
            this.dataSource = new MatTableDataSource(this.campaignList);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
          } else {
            this.campaignList = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
          // if (this.campaignList.length > 0) {
          //   this.isDataAvail = true;
          // }
          // else{
          //   this.isDataAvail=false;
          // }

        }
      );
    }
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
    this.campaignList = this.commonService.sortArray(isAssending, sortBy, this.campaignList, isNumber);
  }

  exportAs(fileType: string) {
    let data = [];
    this.campaignList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Channel"] = new Date(element.date).toLocaleDateString();
      excelDataObject["Campaign ID"] = element.id;
      excelDataObject["Status"] = element.statusDesc;
      excelDataObject["Scheduled On"] = new Date(element.scheduleTime).toLocaleDateString();
      excelDataObject["Submittted On"] = new Date(element.createdOn).toLocaleDateString();
      excelDataObject["SMS Type"] = element.messageType;
      excelDataObject["SMS Content"] = element.messageText;
      excelDataObject["Sender CLI"] = element.senderId;
      excelDataObject["Total SMS"] = element.totalRecordCount;
      excelDataObject["Successful SMS"] = element.successfulRecordCount;
      excelDataObject["Failed SMS"] = element.failedRecordCount;
      excelDataObject["Expired SMS"] = element.expiredRecordCount;
      excelDataObject["SMS Re-Tried"] = element.retriedRecordCount;
      excelDataObject["Delivery Expired"] = element.deliveryExpiredCount;
      excelDataObject["Invalid SMS"] = element.invalidRecordCount;
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'Detailed-Report');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'Detailed-Report');
        break;
      default:
        this.excelService.exportAsCsvFile(data, 'Detailed-Report');
        break;
    }
  }
  prepareApprovalForm() {
    this.statusForm = this.formBuilder.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)
        ]
      ]
    });
  }
  approvalStatus(user, content, event, btn) {
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();

    this.prepareApprovalForm();
    this.changeStatusCamp = false;
    this.userDetails = user;
    this.modalRef = this.modalService.open(content, this.largeModalOptions);
    this.updateUserStatusEvent = event;
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  updateCampStatus() {
    if (this.updateUserStatusEvent == "Approve") {
      this.statusForm.controls.remarks.setValue("approved");
    }
    this.changeStatusCamp = true;
    if (this.statusForm.valid) {
      this.spinner.show();
      let req = {
        "campaignId": this.userDetails["id"],
        // "userId": this.commonService.getUser(),
        "remarks": this.statusForm.controls.remarks.value,
        "status": (this.updateUserStatusEvent == "Approve") ? 1 : 2,
        "username": this.commonService.getUserName(),
      };
      let userType = req.username;
      this.manageCampaignService.updateSattus(req).subscribe(
        res => {
          this.spinner.hide();
          console.log(JSON.stringify(res));
          // if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.spinner.hide();
          this.modalRef.close();
          if (userType == 'headofdept') {
            if (this.updateUserStatusEvent == "Approve") {
              this.commonService.showSuccessToast("'" + this.userDetails["campaignName"] + "' campaign has been sent for final approval");
            } else {
              this.commonService.showSuccessToast("'" + this.userDetails["campaignName"] + "' campaign has been rejected");
            }
          } else {
            if (this.updateUserStatusEvent == "Approve") {
              this.commonService.showSuccessToast("'" + this.userDetails["campaignName"] + "' campaign has been approved");
            } else {
              this.commonService.showSuccessToast("'" + this.userDetails["campaignName"] + "' campaign has been rejected");
            }
          }
          this.submitForm();
          // } else {
          //   this.commonService.showErrorToast(res['result']['userMsg']);
          // }
        },
        err => {
          this.modalRef.close();
          this.submitForm();
        });
    }
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  sDate: any;
  then: any;
  endDte: any;
  edate: any;
  eDate: any;
  minDate: any;
  now = moment(new Date());
  maxDate = this.now.format('YYYY-MM-DD');

  setEndDAte(flag: boolean) {
    this.sDate = this.campaignForm.controls['startDate'].value;
    this.then = moment(new Date(this.sDate));
    this.minDate = this.then.format('YYYY-MM-DD');
    if(!flag){
      //this.campaignForm.controls['endDate'].setValue(this.sDate); 
    }
  }

  setStartDAte() {
   // this.eDate = this.campaignForm.controls['endDate'].value,
     // this.edate = moment(new Date(this.eDate));
    //this.endDte = this.edate.format('YYYY-MM-DD');

  }
}