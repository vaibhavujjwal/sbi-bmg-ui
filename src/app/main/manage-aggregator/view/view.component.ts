import { Component, OnInit,ViewChild } from '@angular/core';
import { NgbModal, NgbModalOptions, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { Router } from "@angular/router";
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { ManageAggregatorService } from "../manage-aggregator.service";
import Swal from "sweetalert2";
import { NgxSpinnerService } from 'ngx-spinner';
import { ExcelService } from "../../../common/excel.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";
import { MODULE } from "../../../common/common.const";
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

export interface AggregatorListTypes {
  Actions: any;
  aggregatorName: string;
  aggregatorProtocol: any;
  aggregatorType: any;
  ip: any;
  port: any;
  tps: any;
  status: any;
  topic: any;
  textPayload: any;
  unicodePayload: any;
  category: any;
  emailAddr: any;
  emailCcAddr: any;
  responseType: any;
}

@Component({
  selector: "app-view",
  templateUrl: "./view.component.html",
  styleUrls: ["./view.component.scss"],
  providers: [ManageAggregatorService]
})

export class ViewComponent implements OnInit {
  MODULES = MODULE;
  deleteForm: FormGroup;
  pageLimit: number;
  topicList = [];
  recordId = [];
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  searchText: string = "";
  formSubmitted: boolean = false;
  deleteFormSubmitted: boolean = false;
  deleteAggregatorList: Array<string> = [];
  closeResult: string;
  p: number = 1;
  aggregatorList = [];
  cliSorted: boolean = false;
  departmentUserSorted: boolean = false;
  statusSorted: boolean = false;
  payloadSorted: boolean = false;
  unicodePayloadSorted: boolean = false;
  categorySorted: boolean = false;
  EmailSorted: boolean = false;
  secondaryEmailSorted: boolean = false;
  responseTypeSorted: boolean = false;
  showError: boolean = false;
  modalRef: any;
  topicName: any;
  queueDtls: any;
  dataSourceLength: number;
  description: any;
  displayedColumns: any[] = ['Actions',
    'aggregatorName',
    'aggregatorProtocol',
    'aggregatorType',
    'ip',
    'port',
    'tps',
    'status',
    'topic',
    'textPayload',
    'unicodePayload',
    'category',
    'emailAddr',
    'emailCcAddr',
    'responseType'
  ];

  dataSource: MatTableDataSource<AggregatorListTypes>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private modalService: NgbModal, private manageAggService: ManageAggregatorService,
    private fb: FormBuilder, private spinner: NgxSpinnerService, private excelService: ExcelService, public commonService: CommonService) {
  }

  ngOnInit() {
    this.getAggregatorList();
    this.pageLimit = this.commonService.recordsPerPage[0];
  }

  getAggregatorList() {
    this.spinner.show();
    let reqObj = {
      "id": "",
      "status": ""
    }
    this.manageAggService.getAggregatorById(reqObj).subscribe(
      res => {
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.aggregatorList = res["data"];
          this.aggregatorList.forEach(aggList => {
            aggList['aggregatorType'] == 'D' ? aggList['aggregatorType'] = 'Domestic' : aggList['aggregatorType'] = 'International'
            aggList['status'] == 1 ? aggList['status'] = 'Enabled' : aggList['status'] = 'Disabled'
          })
          this.dataSource = new MatTableDataSource(this.aggregatorList) ;
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          console.log("Datasource length...." + this.dataSource.data.length);
            if(this.dataSource.data.length==0){
              this.dataSourceLength =0;
            } else
            {
              this.dataSourceLength =this.dataSource.data.length;
            }
          this.spinner.hide();

        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.aggregatorList = [];
        }
      });
  }

  viewTopicDetails(TopicDetails: any, record: any) {
    this.topicName = record['topicId']['topic'];
    this.queueDtls = record['topicId']['queueDetails'];
    this.description = record['topicId']['description'];
    this.modalRef = this.modalService.open(TopicDetails, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {

    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.showError=false;
    if(this.dataSource.filteredData.length==0){
      this.showError=true;
    }
  }


  changeStatus(record: any) {
    let req = {
      "aggregatorName": record['aggregatorName'],
      "aggregatorProtocol": record['aggregatorProtocol'],
      "aggregatorType": record['aggregatorType'] == 'Domestic' ? 'D' : 'I',
      "id": record['id'],
      "ip": record['ip'],
      "port": record['port'],
      "status": record['status'] == 'Enabled' ? 0 : 1,
      "tps": record['tps']
    };
    this.spinner.show();
    this.manageAggService.updateAggregator(req).subscribe(
      res => {
        this.spinner.hide();
        if (record['status'] == 'Enabled') {
          this.commonService.showSuccessToast(record['aggregatorName'] + ' aggregator disabled');
        } else if (record['status'] == 'Disabled') {
          this.commonService.showSuccessToast(record['aggregatorName'] + ' aggregator enabled');
        }
        this.getAggregatorList();
      },
      err => {
        this.getAggregatorList();
      });
  }

  changeStatusAggregatorConfirmation(record: any, id) {
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    if (record['status'] == 'Enabled') {
      modalRef.componentInstance.content = 'Are you sure you want to disable ' + record['aggregatorName'] + ' Aggregator?';
    } else {
      modalRef.componentInstance.content = 'Are you sure you want to enable ' + record['aggregatorName'] + ' Aggregator?';
    }
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.changeStatus(record);
        } else {
          document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
        }
      },
      (reason: any) => {
        document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
      });
  }

  prepareDeleteAggregatorForm() {
    this.deleteForm = this.fb.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)
        ]
      ]
    });
  }

  viewDeleteAggregatorModal(agg, content, btn) {
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();

    this.prepareDeleteAggregatorForm();
    this.deleteFormSubmitted = false;
    this.deleteAggregatorList = agg;
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  deleteAggregator() {
    this.deleteFormSubmitted = true;

    if (this.deleteForm.valid) {
      Swal.fire({
        title: "Are you sure?",
        text: "Do you really want to permanently delete " + this.deleteAggregatorList["aggregatorName"] + " aggregator?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ok, Proceed"
      }).then(result => {
        if (result.value) {
          this.spinner.show();
          let req = {
            "aggId": this.deleteAggregatorList['id'],
            "loginId": this.commonService.getUser(),
            "remarks": this.deleteForm.controls.remarks.value
          };
          this.manageAggService.deleteAggregator(req)
            .subscribe(data => {
              this.spinner.hide();
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.commonService.showSuccessToast(this.deleteAggregatorList["aggregatorName"] + ' aggregator  has been deleted');
                this.modalRef.close();
                this.getAggregatorList();
              } else {
                this.commonService.showErrorToast(data['result']['userMsg']);
              }
            });
        }
      });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
