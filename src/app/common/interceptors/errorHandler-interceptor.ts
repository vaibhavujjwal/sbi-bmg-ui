import { Injectable, Input } from "@angular/core";
import { finalize, tap } from "rxjs/operators";
import { HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { CommonService } from "../../common/common.service";
import { NgbModalRef, NgbModal, ModalDismissReasons } from "../../../../node_modules/@ng-bootstrap/ng-bootstrap";
declare var AesUtil: any;
@Injectable()

export class ErrorHandlerInterceptor implements HttpInterceptor {
  constructor(private router: Router, private spinner: NgxSpinnerService,
    private commonService: CommonService, private modalService: NgbModal) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(
      tap(
        event => {
          /*try {
            let authToken = event["headers"].get("x-auth-token");
            if (authToken) sessionStorage.setItem("_bearerTkn", authToken);
          } catch (err) { }*/
          if (
            event instanceof HttpResponse &&
            event["body"] &&
            event["body"]["result"] &&
            event["body"]["result"]["statusCode"] == 401
          ) {
            this.modalService.dismissAll();
            this.router.navigate(["/auth/login"]);
          }
          this.onSuccess(event);
        },
        error => {
          this.onError(error);
        }
      ),
      finalize(() => { this.spinner.hide() })
    );
  }

  private onSuccess(event: any) {
  }
  private onError(error: any) {

    if (error != null || error != undefined) {
      if (error['error']['result'] != null || error['error']['result'] != undefined) {
        if (error['error']['result']['userMsg']) {
          this.spinner.hide();
          this.commonService.showErrorToast(error['error']['result']['userMsg']);

          if (error['error']['result']['userMsg'].toLowerCase() == "Unauthorized access".toLowerCase() || error['error']['result']['statusCode'] == 401) {
            this.modalService.dismissAll();
            sessionStorage.clear();
            this.router.navigate([""]);
          }
        }
      }
      else if (error.error instanceof Blob && error.error.type === "application/json") {
        // https://github.com//issues/19888
        // When request of type Blob, the error is also in Blob instead of object of the json data
        return new Promise<any>((resolve, reject) => {
          let reader = new FileReader();
          reader.onload = (e: Event) => {
            try {
              const errmsg = JSON.parse((<any>e.target).result);
              this.spinner.hide();
              console.log("Response of error..." + errmsg.result.userMsg);
              this.commonService.showErrorToast(errmsg.result.userMsg);
            } catch (e) {
              console.log("catch error..." + e);
            }
          };
          reader.onerror = (e) => {
            console.log("erorrrrr..." + e);
          };
          reader.readAsText(error.error);
        });
      } else {
        var iterationCount = 1000;
        var keySize = 128;
        var aesUtil = new AesUtil(keySize, iterationCount);
        console.log("error... ", error.error);
        console.log("error text... ", error.error.text);
        if (error.error.text == "This session has been expired (possibly due to multiple concurrent logins being attempted as the same user).") {
          this.commonService.showErrorToast("This session has expired due to multiple concurrent login attempts");
          this.spinner.hide();
          sessionStorage.clear();
          this.router.navigate([""]);
        }
        console.log("error.qwertyuiopasdfghjklzxcvbnm... ", error.error.qwertyuiopasdfghjklzxcvbnm);
        var ciphertext = aesUtil.decrypt("8bc82ffe516ea1f1b89dcb529a201d56", "ebb7947b5feabdc0b60f2a694fa259c4", "Gr33nL!ght#D91", error.error.qwertyuiopasdfghjklzxcvbnm);
        console.log('ciphertext of error is...' + JSON.stringify(JSON.parse(ciphertext)));
        this.spinner.hide();

        if ((JSON.parse(ciphertext)['result'] && JSON.parse(ciphertext)['result']['userMsg']) || JSON.parse(ciphertext)['result']['statusCode'] == 401) {
          this.modalService.dismissAll();
          this.commonService.showErrorToast(JSON.parse(ciphertext)['result']['userMsg']);
          if (JSON.parse(ciphertext)['result']['userMsg'].toLowerCase() == "Unauthorized access".toLowerCase()) {
            sessionStorage.clear();
            this.router.navigate([""]);
          }
        }
      }
    }
  }
}
