import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { MoKeywordsService } from "../manage-mo-keywords.service";
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";
import { Router } from "@angular/router";

@Component({
  selector: "app-create-mo-keywords",
  templateUrl: "./create-mo-keywords.component.html",
  styleUrls: ["./create-mo-keywords.component.scss"]
})
export class CreateMoKeywordsComponent implements OnInit {
  moKeywordForm: FormGroup;
  formSubmitted: boolean = false;

  primaryDropdownList = [];
  secondaryDropdownList = [];
  dropdownSettings = {};
  departments = [];
  routes = [];
  caseFlagExistence: boolean = false;
  caseRequired: boolean = false;
  uniqueKeyword: boolean = true;
  retryAggregatorList: Array<object> = [];
  retryCountList: Array<number> = [];
  commonList: Array<object> = [];


  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
    private moKeywordsService: MoKeywordsService, private router: Router,
    private commonService: CommonService) { }

  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      placeholder: 'Select Filter',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true,
      enableCheckAll: false,
      defaultOpen: false
    };
    this.getRouteList();
    this.getDepartmentList();

    this.initialiseForms();
    this.caseRequired = true;

    this.getRetryAggregatorList();
    this.retryCountList = [1, 2, 3];
  }

  getRetryAggregatorList() {
    this.retryAggregatorList = this.moKeywordsService.getRetryAggregatorList();
      this.retryAggregatorList = this.filterList(
        this.retryAggregatorList,
        "S"
        );
      this.retryAggregatorList = this.filterList(
        this.retryAggregatorList,
        "B"
      );
    
  }

  initialiseForms() {
    this.moKeywordForm = this.fb.group({
      caseSensitivityFlag: [''],
      code: ['', [Validators.required, Validators.pattern(this.commonService.patterns.numberOnly)]],
      keywords: ['', [Validators.pattern(this.commonService.patterns.keywordPattern)]],
      primaryRouteId: ['', [Validators.required]],
      secondaryRouteId: [''],
      department: ['', [Validators.required]],
      status: [''],
      url: [''],
      retryFlag: [false],
      retryRoute: new FormControl({ disabled: true, value: "" }, [
        Validators.required
      ]),
      retryCount: new FormControl({ disabled: true, value: "" }, [
        Validators.required,
        Validators.pattern(this.commonService.patterns.numberOnly)
      ]),
      retryIntervalArray: this.fb.array([])
    });
  }

  resetFrom() {
    this.moKeywordForm.controls['caseSensitivityFlag'].setValue('');
    this.moKeywordForm.controls['code'].setValue('');
    this.moKeywordForm.controls['keywords'].setValue('');
    this.moKeywordForm.controls['primaryRouteId'].setValue('');
    this.moKeywordForm.controls['secondaryRouteId'].setValue('');
    this.moKeywordForm.controls['department'].setValue('');
  }

  getRouteList() {
    this.spinner.show();
    this.moKeywordsService.getRouteList('false').subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.primaryDropdownList = res['data'];
          this.secondaryDropdownList = JSON.parse(JSON.stringify(this.primaryDropdownList));
          this.commonList = res['data']
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.primaryDropdownList = [];
          this.secondaryDropdownList = [];
        }
      });
  }

  filterList(list, value) {
    return list.filter(x => x["id"] != value);
  }

  routeChanged(field) {
    let primaryVal = this.moKeywordForm.controls["primaryRouteId"].value;
    let secondaryVal = this.moKeywordForm.controls["secondaryRouteId"].value;
    if (field == "primaryRouteId") {
      this.secondaryDropdownList = this.filterList(
        this.commonList,
        primaryVal
      );

    } else if (field == "secondaryRouteId") {
      this.primaryDropdownList = this.filterList(
        this.commonList,
        secondaryVal
      );
    } 
    if(!secondaryVal){
      this.retryAggregatorList = this.filterList(
        this.retryAggregatorList,
        "S"
      );
      this.retryAggregatorList = this.filterList(
        this.retryAggregatorList,
        "B"
      );
    }else if(secondaryVal){
      this.retryAggregatorList = this.moKeywordsService.getRetryAggregatorList();
    }
  }





  getDepartmentList() {
    this.spinner.show();
    this.moKeywordsService.getDepartmentList().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.departments = res['data'];
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.departments = [];
        }
      });
  }

  updateRequiredCase() {
    debugger;
    if (this.moKeywordForm.controls['keywords'].value == "") {
      this.caseRequired = true;
      this.moKeywordForm.controls['caseSensitivityFlag'].clearValidators();
      this.moKeywordForm.controls['caseSensitivityFlag'].updateValueAndValidity();
    } else {
      this.caseRequired = false;
      this.moKeywordForm.controls['caseSensitivityFlag'].clearValidators();
      this.moKeywordForm.controls['caseSensitivityFlag'].setValidators(Validators.required);
      this.moKeywordForm.controls['caseSensitivityFlag'].updateValueAndValidity();
    }
  }





  keywordExistance() {
    this.updateRequiredCase();
    this.uniqueKeyword = true;
    if (this.moKeywordForm.controls['code'].valid && this.moKeywordForm.controls['keywords'].valid) {
      this.moKeywordForm.controls['caseSensitivityFlag'].value == "Y" ? this.caseFlagExistence = true : this.caseFlagExistence = false;
      let req = {
        "caseSensitivityFlag": this.caseFlagExistence,
        "keyword": this.moKeywordForm.controls['keywords'].value,
        "code": this.moKeywordForm.controls['code'].value
      };
      this.spinner.show();
      if (this.moKeywordForm.controls['code'] || this.moKeywordForm.controls['keywords']) {

        this.moKeywordsService.keywordExists(req).subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
              if (res['data'] == "false") {
              } else {
                if (req['keyword']) {
                  this.commonService.showErrorToast(req['keyword'] + ' already exist against ' + req['code']);
                } else {
                  this.commonService.showErrorToast('Empty Keyword already exist against ' + req['code']);
                }
              }
            } else {
              this.uniqueKeyword = true;
              this.commonService.showErrorToast(res['result']['userMsg']);
            }
          });
      }
    }
  }

  flagChanged(flagName, fields) {
    if (this.moKeywordForm.controls[flagName].value) {
      for (let x = 0; x < fields.length; x++)
        this.moKeywordForm.controls[fields[x]].enable();
    } else {
      for (let x = 0; x < fields.length; x++) {
        this.moKeywordForm.controls[fields[x]].disable();
      }
    }
  }

  retryCountChanged() {

    let retryCount = this.moKeywordForm.controls.retryCount.value;
    let retryIntervalControl = <FormArray>(this.moKeywordForm.controls.retryIntervalArray);

    for (let i = retryIntervalControl.length - 1; i >= 0; i--) {
      retryIntervalControl.removeAt(i);
    }

    for (let i = 0; i < retryCount; i++) {
      retryIntervalControl.push(
        this.fb.group({
          retryInterval: [
            '',
            [
              Validators.required,
              Validators.min(1),
              Validators.max(900),
              Validators.pattern(this.commonService.patterns.numberOnly)
            ]
          ]

        })
      )
    }
  }

  manipulateRetryInterval() {
    if (this.moKeywordForm.controls.retryCount.value > 1) {
      for (let x = 0; x < this.moKeywordForm.controls.retryCount.value - 1; x++) {
        let y = x + 1;
        if (Number(this.moKeywordForm.controls.retryIntervalArray.value[x].retryInterval) > Number(this.moKeywordForm.controls.retryIntervalArray.value[y].retryInterval)) {
          this.commonService.showErrorToast("Duration of retry interval " + ++y + " should be greater than retry interval " + ++x);
          return;
        }
      }

    }

    var retryIntervalValues = [];
    for (let x = 0; x < this.moKeywordForm.controls.retryCount.value; x++) {
      retryIntervalValues.push(this.moKeywordForm.controls.retryIntervalArray.value[x].retryInterval);
    }
    return retryIntervalValues;
  }

  submitForm() {

    this.formSubmitted = true;
    if (this.moKeywordForm.valid) {
      if (this.moKeywordForm.controls.retryFlag.value) {
        var retryIntervalCommaSeparatedValue = this.manipulateRetryInterval();
        if (!retryIntervalCommaSeparatedValue) { return }
      }
      let req = {
        "caseSensitivityFlag": this.moKeywordForm.controls['caseSensitivityFlag'].value,
        "code": this.moKeywordForm.controls['code'].value,
        "keyword": this.moKeywordForm.controls['keywords'].value,
        "primaryRouteId": this.moKeywordForm.controls['primaryRouteId'].value,
        "secondaryRouteId": this.moKeywordForm.controls['secondaryRouteId'].value,
        "department": this.moKeywordForm.controls['department'].value,
        "retryIntervals": this.moKeywordForm.controls['retryFlag'].value && retryIntervalCommaSeparatedValue ? retryIntervalCommaSeparatedValue.join() : 0,
        "retryFlag": this.moKeywordForm.controls['retryFlag'].value ? 'Y' : 'N',
        "maxRetryCount": this.moKeywordForm.controls['retryFlag'].value && this.moKeywordForm.controls['retryCount'].value ? Number(this.moKeywordForm.controls['retryCount'].value) : 0,
        "retryRoute": this.moKeywordForm.controls['retryFlag'].value && this.moKeywordForm.controls['retryRoute'].value ? this.moKeywordForm.controls['retryRoute'].value : "B"
      };
      this.spinner.show();
      this.moKeywordsService.saveMoKeyword(req).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast(req['keyword'] + ' MO Keyword added successfully');
            this.initialiseForms();
            this.router.navigate(["/main/manage-mo-keywords/list-mo-keywords"]);
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
          this.formSubmitted = false;
        });
    }
  }
  onItemSelect(event) { }
  OnItemDeSelect(event) { }
}
