import { Component, OnInit } from '@angular/core';
import { ManageMoRouteService } from '../manage-mo-route.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from '../../../common/common.service';
import { RESPONSE } from '../../../common/common.const';
import { Router } from "@angular/router";

@Component({
  selector: 'app-create-mo-route',
  templateUrl: './create-mo-route.component.html',
  styleUrls: ['./create-mo-route.component.scss']
})
export class CreateMoRouteComponent implements OnInit {
  users = [];
  moRouteForm: FormGroup;
  formSubmitted = false;
  routName : any;
  isEdit = false;
  uniqueRoute : boolean = true;
  caseFlag : any;

  constructor(private  manageMoRouteService: ManageMoRouteService ,
    private fb: FormBuilder,private router: Router, private spinner: NgxSpinnerService,
    public commonService: CommonService) { }

  ngOnInit() {
    this.initialiseForms();
  }

  caseSensitive(){
    this.uniqueRoute = true;
    this.formSubmitted = true;
    if (this.moRouteForm.controls['routeName'].valid) {
    this.spinner.show();
    this.routName = this.moRouteForm.controls["routeName"].value.trim();
    this.manageMoRouteService.getsensitiveFlag(this.routName).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.caseFlag = res["data"];
          if(this.caseFlag == 'true'){
            this.uniqueRoute = false;
          }
        } else {
          this.uniqueRoute = true;
          this.caseFlag = [];
        }
        this.formSubmitted = false;
      });
    }
}

  initialiseForms() {
    this.moRouteForm = this.fb.group({
      routeName: ['', [Validators.required , Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)]],
      protocol: ['', [Validators.required]],
      host: [''],
      port: ['', [Validators.required,Validators.max(65535),Validators.min(1024)]],
      uri: ['', [Validators.required , Validators.pattern(this.commonService.patterns.uriValidation)]],
      });
  }

dynamicValidation(){
  this.moRouteForm.controls.host.clearValidators();
  this.moRouteForm.controls.host.updateValueAndValidity();
  let contents = this.moRouteForm.controls['host'].value.trim();
  for (var i = 0, n = contents.length; i < n; i++) {
    if (contents.charCodeAt(i) >= 48 && contents.charCodeAt(i) <= 57) {
      this.moRouteForm.controls.host.setValidators([Validators.required,Validators.minLength(5) ,Validators.maxLength(100),Validators.pattern(this.commonService.patterns.ip)]);
      this.moRouteForm.controls.host.updateValueAndValidity();
  }else{
      this.moRouteForm.controls.host.setValidators([Validators.required, Validators.minLength(5) ,Validators.maxLength(100),Validators.pattern(this.commonService.patterns.uriValidation)]);
      this.moRouteForm.controls.host.updateValueAndValidity();
    }
  }
}

  submitForm() {
    this.formSubmitted = true;
    this.dynamicValidation();
    if (this.moRouteForm.valid && this.uniqueRoute) {
      let reqObj = {
        "uri": this.moRouteForm.controls["uri"].value.trim(),
        "protocol": this.moRouteForm.controls["protocol"].value,
        "port": this.moRouteForm.controls["port"].value,
        "name": this.moRouteForm.controls["routeName"].value.trim(),
        "host": this.moRouteForm.controls["host"].value
      }
      this.spinner.show();
      this.manageMoRouteService.saveMoRoute(reqObj).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast(reqObj['name'] +' MO Route added succesfully.Please configure topic at solace before enable it');
            this.router.navigate(["/main/manage-mo-route/view-mo-route"]);
            this.uniqueRoute = true;
            this.initialiseForms();
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
          this.formSubmitted = false;
        });
    }
  }
}


