import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";
import { CommonService } from "../common/common.service";
@Injectable()
export class NetworkService {
  private backendApiURL = environment.backendApiURL;
  private backendApiURLMG = environment.backendApiURLMG;
  private backendLogin = environment.backendLogin;
  private backendApiURLNonEncrytion = environment.backendApiURLNonEncrytion;

  constructor(
    private http: HttpClient,
    private _commonService: CommonService
  ) { }

  getMG(url: any, head?: any, auth?: any): Observable<HttpResponse<any>> {
    return this.http.get<HttpResponse<any>>(
      this.backendApiURLMG + url,
      this._commonService.getToken(auth)
    );
  }

  get(url: any, head?: any, auth?: any, token?: any): Observable<HttpResponse<any>> {
    return this.http.get<HttpResponse<any>>(
      this.backendApiURL + url,
      this._commonService.getToken(auth, token)
    );
  }

  getLogout(url: any, head?: any, auth?: any, token?: any): Observable<HttpResponse<any>> {
    return this.http.get<HttpResponse<any>>(
      this.backendLogin + url,
      this._commonService.getToken(auth)
    );
  }

  // getFile(url: any, head?: any, auth?: any): Observable<any> {
  //   return this.http.get(this.backendApiURL + url, {
  //     headers: this._commonService.getToken(auth),
  //     responseType: "blob" as "json"
  //   });
  // }

  getFile(url: any, head?: any, auth?: any): Observable<any> {
    if (auth) {
      return this.http.get(this.backendApiURLNonEncrytion+ url, {
        headers: new HttpHeaders({
          // Authorization: "bearer " + sessionStorage.getItem("_bearerTkn")
          'X-Auth-Token': sessionStorage.getItem("_bearerTkn"),
        }),
        responseType: "blob" as "json"
      });
    } else {
      return this.http.get(this.backendApiURLNonEncrytion + url, {
        headers: this._commonService.getToken(auth)
        ,responseType: "blob" as "json"
      });
    }

  }
  getFileDownload(url: any, head?: any, auth?: any): Observable<any> {
    if (auth) {
      return this.http.get(this.backendApiURLNonEncrytion + url, {
        headers: new HttpHeaders({
          // Authorization: "bearer " + sessionStorage.getItem("_bearerTkn")
          'X-Auth-Token': sessionStorage.getItem("_bearerTkn"),
        }),
        responseType: "blob" as "json"
      });
    } else {
      return this.http.get(this.backendApiURLNonEncrytion + url, {
        headers: this._commonService.getToken(auth),
        responseType: "blob" as "json"
      });
    }
        // responseType: "blob" as "json" | "text" as "json" | "json" 

  }

  post(url: any, param?: any, head?: any, auth?: any): Observable<any> {
    return this.http.post(
      this.backendApiURL + url,
      param,
      this._commonService.getToken(auth)
    );
  }

  postLogin(url: any, param?: any): Observable<HttpResponse<Object>> {
    return this.http.post<HttpResponse<Object>>(
      this.backendLogin + url,
      param,
      {observe: 'response'}
    );
  }

  getNonEncryption(url: any, param?: any, head?: any, auth?: any): Observable<any> {
    return this.http.get(
      this.backendApiURLNonEncrytion + url
    );
  }

  getNonEncryption1(url: any, param?: any, head?: any, auth?: any): Observable<any> {
    return this.http.get(
      this.backendApiURLNonEncrytion + url,
      this._commonService.getToken(auth)
    );
  }
  

  postNonEncrypt(url: any, param?: any, head?: any, auth?: any): Observable<any> {
    return this.http.post(
      this.backendApiURLNonEncrytion + url,param,this._commonService.getToken(auth)
    );
  }

  put(url: any, param?: any, head?: any, auth?: any): Observable<any> {
    return this.http.put(
      this.backendApiURL + url,
      param,
      this._commonService.getToken(auth)
    );
  }

  delete(url: any, param?: any, head?: any, auth?: any): Observable<any> {
    let options = this._commonService.getToken(auth);
    if (param) {
      options['params'] = param;
      return this.http.delete(
        this.backendApiURL + url,
        options
      );
    } else {
      return this.http.delete(
        this.backendApiURL + url,
        options
      );
    }

  }
  // postLogin(url: any, param?: any, head?: any, auth?: any): Observable<any> {
  //   return this.http.post(
  //     this.backendLogin + url,
  //     param
  //     //this._commonService.getToken(auth)
  //   );
  // }
  uploadFile(url: any, param?: any, head?: any, auth?: any): Observable<any> {
    
    if (auth == "bearer") {
      return this.http.post(
        this.backendApiURLNonEncrytion + url,
        param,
        {
          headers: new HttpHeaders({
            //Authorization: "bearer " + sessionStorage.getItem("_bearerTkn"),
            'X-Auth-Token': sessionStorage.getItem("_bearerTkn"),
            observe: "response",
            responseType: "text"
          })
        }
      );
    } else {
      return this.http.post(
        this.backendApiURLNonEncrytion + url,
        param,
        this._commonService.getToken(auth)
      );
    }

  }
}

const fileUploadHeaders = {
  "Cache-Control": "no-cache",
  Pragma: "no-cache",
  "Process-Data": false
};
