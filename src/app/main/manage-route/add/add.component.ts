import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormArray
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from "../../../common/common.service";
import { ManageUserService } from "../../manage-user/manage-user.service";
import { ManageRouteService } from "../manage-route.service";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE } from "src/app/common/common.const";

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.scss"],
  providers: [ManageRouteService, ManageUserService]
})
export class AddComponent implements OnInit {
  DOMESTIC: string = "domestic";
  INTERNATIONAL: string = "international";

  routeForm: FormGroup;
  isFormSubmitted: boolean = false;

  selectedUser: object = {};
  userRoute: object;
  userList: any;
  domesticAggregatorList: Array<object> = [];
  domesticPrimaryAggregatorList: Array<object> = [];
  domesticSecondaryAggregatorList: Array<object> = [];
  domesticRetryAggregatorList: Array<object> = [];

  internationalAggregatorList: Array<object> = [];
  internationalPrimaryAggregatorList: Array<object> = [];
  internationalSecondaryAggregatorList: Array<object> = [];
  internationalRetryAggregatorList: Array<object> = [];
  applicationCategoryList: Array<object> = [];

  primaryTrafficPercentageList: Array<object> = [];
  topologyList: Array<object> = [];
  retryCountList: Array<object> = [];
  userRouteDetailsData: Array<object> = [];
  aggregatorList: Array<object> = [];

  isUpdate: boolean = false;
  selectedAppCategoryName: any;
  isUserDisabled: boolean = false;

  userIdQueryParam: any;
  appQueryParam: any;
  categoryQueryParam: any;

  dropdownList: any;
  selectedItems: any;
  dropdownSettings: any;

  defaultRouteDetails = {
    routeType: "",
    primaryRouteId: {
      id: ""
    },
    secondaryRouteId: {
      id: ""
    },
    retryRouteId: {
      id: ""
    },
    topologyMode: "",
    primaryTrafficPer: "",
    retryFlag: "N",
    maxRetryCount: "",
    retryDetails: []
  };
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private manageRouteService: ManageRouteService,
    private manageUserService: ManageUserService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params["userId"]) {
        this.userIdQueryParam = params["userId"];
        this.appQueryParam = params["app"];
        this.categoryQueryParam = params["category"]
      }
    });
    //this.internationalFlag = 'N';
    this.prepareRouteForm();
    this.getUserList();
    this.getStaticDropdownLists();

    this.dropdownSettings = {
      singleSelection: true,
      idField: 'userId',
      textField: 'userName',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: true,
      defaultOpen: false
    };
  }

  prepareRouteForm() {
    this.routeForm = this.formBuilder.group({
      userId: ["", [Validators.required]],
      applicationCategoryName: ["", Validators.required],
      routeType: ["", Validators.required],
      routeDetails: this.formBuilder.group({
        topology: ["", [Validators.required]],
        primaryRoute: ["", [Validators.required]],
        secondaryRoute: [""],
        primaryTrafficPercentage: ["", [Validators.required]],
        retryFlag: [false],
        retryRoute: new FormControl({ disabled: true, value: "" }, [
          Validators.required
        ]),
        retryCount: new FormControl({ disabled: true, value: "" }, [
          Validators.required,
          Validators.pattern(this.commonService.patterns.numberOnly)
        ]),
        retryIntervalArray: this.formBuilder.array([])
      }),
      international: this.formBuilder.group({
        internationalRoute: [false],
        topology: new FormControl({ disabled: true, value: "" }, [
          Validators.required
        ]),
        primaryRoute: new FormControl({ disabled: true, value: "" }, [
          Validators.required
        ]),
        secondaryRoute: new FormControl({ disabled: true, value: "" }),
        primaryTrafficPercentage: new FormControl(
          { disabled: true, value: "" },
          [Validators.required]
        ),
        retryFlag: [false],
        retryRoute: new FormControl({ disabled: true, value: "" }, [
          Validators.required
        ]),
        retryCount: new FormControl({ disabled: true, value: "" }, [
          Validators.required,
          Validators.pattern(this.commonService.patterns.numberOnly)
        ]),
        retryIntervalArray: this.formBuilder.array([])
      })
    });
  }

  getUserList() {
    this.spinner.show();
    this.manageRouteService.getSpecificUsersList().subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        // let tmpList = data["data"];
        // this.userList = tmpList.filter(function (item) {
        //   return (
        //     item["status"] != 5 &&
        //     item["status"] != 7 &&
        //     (item["roleId"] == 4 || item["roleId"] == 5)
        //   );
        // });
        this.userList = data["data"];
        if(!this.userIdQueryParam){
          this.userList = this.userList.filter(element => 
          element['pending'] > 0)
        }

        this.sortTable('userName', true);

        if (this.userIdQueryParam) {
          this.selectedItems = this.userList.filter(element => element['userId'] == this.userIdQueryParam)
          this.routeForm.controls.userId.setValue(this.selectedItems);
          this.isUserDisabled = true;
          this.userSelectionChanged();
        }
      } else {
        this.commonService.showErrorAlertMessage(data["result"]["userMsg"]);
      }
      if (!this.userIdQueryParam)
        this.spinner.hide();
    });
  }

  
  getStaticDropdownLists() {
    this.topologyList = this.manageRouteService.getTopologyList();
    this.primaryTrafficPercentageList = this.manageRouteService.getPrimaryTrafficPercentageList();
    this.retryCountList = this.manageRouteService.getRetryCountList();
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
    this.userList = this.commonService.sortArray(isAssending, sortBy, this.userList, isNumber);
  }

  getUserRouteDetails() {
    this.spinner.show();
    this.userRouteDetailsData = [];
    let appCategoryName = this.routeForm.controls.applicationCategoryName.value.split(' ');
    let req = {
      "userid": this.selectedUser['userId'],
      "appName": appCategoryName[0],
      "category": appCategoryName[1]
    }
    this.manageRouteService
      .getRoute(req)
      .subscribe(data => {
        this.spinner.hide();
        if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
          this.userRouteDetailsData = data["data"];
          this.userRouteDetailsData.length > 0 ? this.isUpdate = true : this.isUpdate = false;
          if (!this.isUpdate) {
            this.updateControlsDynamically(false);
          }
          this.getAggListCategoryAndRegionWise(false);
        }
      });
  }

  updateFormDetailsRoutewise(type, data) {
    this.routeForm.controls.routeType.setValue(
      data["routeType"]
    );
    if (!this.isUpdate)
      this.RouteTypeChanged();
    this.routeForm.controls.routeDetails["controls"]["topology"].setValue(
      data["topologyMode"]
    );
    this.topologyChanged();
    this.routeForm.controls.routeDetails["controls"]["primaryRoute"].setValue(
      data["primaryRouteId"]["id"]
    );
    this.routeChanged("primaryRoute", type);

    if (data["secondaryRouteId"]) {
      this.routeForm.controls.routeDetails["controls"]["secondaryRoute"].setValue(
        data["secondaryRouteId"]["id"]
      );
    }
    this.routeChanged("secondaryRoute", type);
    this.routeForm.controls.routeDetails["controls"][
      "primaryTrafficPercentage"
    ].setValue(data["primaryTrafficPer"]);

    this.routeForm.controls.routeDetails["controls"]["retryFlag"].setValue(
      data["retryFlag"] == "Y"
    );
    this.flagChanged(type, "retryFlag", ["retryRoute", "retryCount"]);
    this.routeForm.controls.routeDetails["controls"]["retryRoute"].setValue(
      data["retryFlag"] == "Y" ? data["retryRouteId"]["id"] : ""
    );
    this.routeChanged("retryRoute", type);
    this.routeForm.controls.routeDetails["controls"]["retryCount"].setValue(
      data["retryFlag"] == "Y" ? data["maxRetryCount"] : ""
    );

    let retryIntervalControls = <FormArray>(
      this.routeForm.controls.routeDetails["controls"]["retryIntervalArray"]
    );

    for (let i = retryIntervalControls.length - 1; i >= 0; i--) {
      retryIntervalControls.removeAt(i);
    }

    if (data["retryFlag"] == "Y") {
      for (let i = 1; i <= data["retryDetails"].length; i++) {
        let retryIntervalObj = data["retryDetails"].find(
          x => x["retryCountNo"] == i
        );
        retryIntervalControls.push(
          this.formBuilder.group({
            retryInterval: [
              retryIntervalObj["retryInterval"],
              [
                Validators.required,
                Validators.min(1),
                Validators.max(900),
                Validators.pattern(this.commonService.patterns.numberOnly)
              ]
            ]
          })
        );
      }
    }
  }

  resetFormDetails() {
    this.isFormSubmitted = false;
    this.selectedUser = {};
    this.updateFormDetailsRoutewise(this.DOMESTIC, this.defaultRouteDetails);
    // this.updateFormDetailsRoutewise(
    //   this.INTERNATIONAL,
    //   this.defaultRouteDetails
    // );
  }

  saveRoute() {
    this.isFormSubmitted = true;
    if (this.routeForm.valid) {
      let routeObj = this.routeForm.value;
      let finalArr = [];
      let domesticObj = this.getRouteDetailsFormInfo(
        this.DOMESTIC,
        routeObj
      );

      if (domesticObj == undefined) {
        return;
      }
      domesticObj['retryDetails'] ? domesticObj['retryDetails'] = domesticObj['retryDetails'] : domesticObj['retryDetails'] = null;
      domesticObj['maxRetryCount'] ? domesticObj['maxRetryCount'] = domesticObj['maxRetryCount'] : domesticObj['maxRetryCount'] = 0;
      domesticObj['retryRouteId'] ? domesticObj['retryRouteId'] = domesticObj['retryRouteId'] : domesticObj['retryRouteId'] = 0;
      finalArr.push(domesticObj);

      // if (routeObj[this.INTERNATIONAL]["internationalRoute"]) {
      //   let internationalObj = this.getRouteDetailsFormInfo(
      //     this.INTERNATIONAL,
      //     routeObj,
      //     "I"
      //   );
      //   if (internationalObj == undefined) {
      //     return;
      //   }
      //   internationalObj['retryDetails'] ? internationalObj['retryDetails'] = internationalObj['retryDetails'] : internationalObj['retryDetails'] = null;
      //   internationalObj['maxRetryCount'] ? internationalObj['maxRetryCount'] = internationalObj['maxRetryCount'] : internationalObj['maxRetryCount'] = 0;
      //   internationalObj['retryRouteId'] ? internationalObj['retryRouteId'] = internationalObj['retryRouteId'] : internationalObj['retryRouteId'] = 0;
      //   finalArr.push(internationalObj);
      // }


      let usrId = this.routeForm.controls.userId.value[0].userId;
      this.spinner.show();
      if (this.isUpdate) {
        this.manageRouteService.updateRoute(usrId, finalArr).subscribe(
          data => {
            this.spinner.hide();
            if (data == null) {
              this.commonService.showSuccessToast(
                "Route details updated successfully"
              );
              this.isUpdate = false;
              this.router.navigate(['/main/manage-route/list']);
            } else {
              this.commonService.showErrorAlertMessage(
                data["result"]["userMsg"]
              );
            }
          },
          error => {
            this.commonService.showErrorAlertMessage("Something went wrong");
          }
        );
      } else {
        this.manageRouteService.saveRoute(usrId, finalArr).subscribe(data => {
          this.spinner.hide();
          if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast(
              "Route details saved successfully"
            );
            this.routeForm.controls.userId.setValue([]);
            this.routeForm.controls.applicationCategoryName.setValue("");
            this.resetFormDetails();
          } else {
            this.commonService.showErrorAlertMessage(data["result"]["userMsg"]);
          }
        });
      }
      //this.internationalFlag = 'N';
    }
  }

  getRouteDetailsFormInfo(type, routeObj) {
    let retryDetails = null;
    if (routeObj.routeDetails["retryFlag"]) {
      retryDetails = [];
      for (let x = 0; x < routeObj.routeDetails["retryIntervalArray"].length; x++) {
        let y = x + 1;
        if (y < routeObj.routeDetails["retryIntervalArray"].length) {
          if (Number(routeObj.routeDetails["retryIntervalArray"][y]["retryInterval"]) <= Number(routeObj.routeDetails["retryIntervalArray"][x]["retryInterval"])) {
            this.commonService.showErrorToast(
              "Duration of retry interval " + ++y + " should be greater than retry interval " + ++x
            );
            return;
          }
        }
        retryDetails.push({
          retryInterval:
            Number(routeObj.routeDetails["retryIntervalArray"][x]["retryInterval"]),
          retryCountNo: x + 1
        });
      }
    }

    let obj = {
      dlrTimeoutRetryInterval: 0,
      dlrTimeoutRetryRequried: "Y",
      maxRetryCount: routeObj.routeDetails["retryFlag"]
        ? Number(routeObj.routeDetails["retryCount"])
        : null,
      primaryRouteId: Number(routeObj.routeDetails["primaryRoute"]),
      primaryTrafficPer: routeObj.routeDetails["primaryTrafficPercentage"],
      retryDetails: retryDetails,
      retryFlag: routeObj.routeDetails["retryFlag"] ? "Y" : "N",
      retryRouteId: routeObj.routeDetails["retryFlag"]
        ? Number(routeObj.routeDetails["retryRoute"])
        : null,
      routeType: routeObj["routeType"],
      secondaryRouteId: routeObj.routeDetails["secondaryRoute"]
        ? Number(routeObj.routeDetails["secondaryRoute"])
        : null,
      topologyMode: Number(routeObj.routeDetails["topology"]),
      applicationName: this.routeForm.controls["applicationCategoryName"].value.split(' ')[0],
      category: this.routeForm.controls["applicationCategoryName"].value.split(' ')[1]
    };

    return obj;
  }

  updateControlsDynamically(flag) {
    if (flag) {
      this.routeForm.controls.applicationCategoryName.setValue("");
      this.routeForm.controls.routeType.setValue("");
      this.applicationCategoryList = [];
      this.userRouteDetailsData = [];
      this.domesticAggregatorList = [];
      this.domesticPrimaryAggregatorList = [];
      this.domesticSecondaryAggregatorList = [];
      this.domesticRetryAggregatorList = [];
    }
    this.routeForm.controls.routeDetails['controls']['topology'].setValue("");
    this.routeForm.controls.routeDetails['controls']['primaryRoute'].setValue("");
    this.routeForm.controls.routeDetails['controls']['secondaryRoute'].setValue("");
    this.routeForm.controls.routeDetails['controls']['primaryTrafficPercentage'].setValue("");
    this.routeForm.controls.routeDetails['controls']['retryFlag'].setValue(false);
    this.routeForm.controls.routeDetails['controls']['retryRoute'].disable();
    this.routeForm.controls.routeDetails['controls']['retryRoute'].setValue("");
    this.routeForm.controls.routeDetails['controls']['retryCount'].disable();
    this.routeForm.controls.routeDetails['controls']['retryCount'].setValue("");
    let retryIntervalControl = <FormArray>(
      this.routeForm.controls.routeDetails["controls"]["retryIntervalArray"]
    );

    for (let x = retryIntervalControl.length - 1; x >= 0; x--) {
      retryIntervalControl.removeAt(x);
    }
  }


  getAggListCategoryAndRegionWise(flag) {

    if ((this.routeForm.controls.userId.valid && (!this.routeForm.controls.applicationCategoryName.invalid) && this.isUpdate) || (this.routeForm.controls.userId.valid && this.routeForm.controls.applicationCategoryName.valid && this.routeForm.controls.routeType.valid)) {
      this.spinner.show();
      let appCategoryName = this.routeForm.controls.applicationCategoryName.value.split(' ');
      let routeType = this.routeForm.controls.routeType.value ? this.routeForm.controls.routeType.value : this.userRouteDetailsData[0]['routeType'];
      this.manageRouteService.getAggListParamWise(routeType, appCategoryName[1]).subscribe(res => {
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.spinner.hide();
          this.domesticAggregatorList = res['data'];
          this.domesticPrimaryAggregatorList = res["data"];
          this.domesticSecondaryAggregatorList = res["data"];
          this.domesticRetryAggregatorList = res["data"];
          if (this.isUpdate && !flag)
            this.updateFormDetailsRoutewise("D", this.userRouteDetailsData[0]);
          if (res["data"].length == 0)
            this.commonService.showErrorToast("Aggregators not configured for selected category and route type.");
        }
      });
    }
  }
  userSelectionChanged() {
    let val = this.routeForm.controls.userId.value[0]['userId'];
    this.isFormSubmitted = false;
    this.selectedUser = this.userList.find(x => x["userId"] == val);
    // if (this.selectedUser['status'] != 4) {
    //   this.commonService.showSuccessToast("Route Details have already been configured.");
    // }
    //this.internationalFlag = this.selectedUser['internationalRouteFlag'] ? this.selectedUser['internationalRouteFlag'] : 'N';
    this.updateControlsDynamically(true);
    // this.applicationCategoryList = [];
    // this.userRouteDetailsData = [];
    // this.domesticAggregatorList = [];
    // this.domesticPrimaryAggregatorList = [];
    // this.domesticSecondaryAggregatorList = [];
    // this.domesticRetryAggregatorList = [];
    this.getApplicationCategoryList(this.selectedUser['userId']);
  }

  onItemDeSelect() {
    this.updateControlsDynamically(true);
  }

  appCategoryNameChanged() {
    //this.userRouteDetailsData = [];
    this.domesticAggregatorList = [];
    this.domesticPrimaryAggregatorList = [];
    this.domesticSecondaryAggregatorList = [];
    this.domesticRetryAggregatorList = [];
    if (this.userIdQueryParam) {
      this.selectedAppCategoryName = this.applicationCategoryList.filter(element =>
        element['appName'] == this.appQueryParam && element['category'] == this.categoryQueryParam)
      this.routeForm.controls.applicationCategoryName.setValue(this.selectedAppCategoryName[0]['appName'] + " " + this.selectedAppCategoryName[0]['category'])
      this.routeForm.controls.applicationCategoryName.disable();
    } else {
      let appCategoryName = this.routeForm.controls.applicationCategoryName.value.split(' ');
      this.selectedAppCategoryName = this.applicationCategoryList.filter(element =>
        element['appName'] == appCategoryName[0] && element['category'] == appCategoryName[1]);
    }


    if (this.userIdQueryParam)
      this.getUserRouteDetails();
    else if(this.selectedAppCategoryName[0]['appFrmRoute'] != null ) {
      this.commonService.showErrorToast("Route is already configured for this application category.");
    }
  }

  RouteTypeChanged() {
    console.log("route type changed.........." + this.routeForm.controls.routeType.value);
    this.routeForm.controls.routeDetails['controls']['primaryRoute'].setValue("");
    this.routeForm.controls.routeDetails['controls']['secondaryRoute'].setValue("");
    this.routeForm.controls.routeDetails['controls']['retryRoute'].setValue("");
    this.domesticAggregatorList = [];
    this.domesticPrimaryAggregatorList = [];
    this.domesticSecondaryAggregatorList = [];
    this.domesticRetryAggregatorList = [];
    this.getAggListCategoryAndRegionWise(true);
  }
  getApplicationCategoryList(userId) {
    this.spinner.show();
    this.manageRouteService.getApplicationCategoryList(userId).subscribe(res => {
      this.spinner.hide();
      if (res["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        this.applicationCategoryList = res["data"];
        if (this.appQueryParam && this.categoryQueryParam) {
          this.appCategoryNameChanged();
        }
      }
    })
  }

  topologyChanged() {
    let value = this.routeForm.controls.routeDetails["controls"]["topology"].value;
    if (value == 1) {
      this.routeForm.controls.routeDetails["controls"][
        "secondaryRoute"
      ].clearValidators();
      this.routeForm.controls.routeDetails["controls"]["secondaryRoute"].setValidators(
        [Validators.required]
      );
      this.routeForm.controls.routeDetails["controls"][
        "secondaryRoute"
      ].updateValueAndValidity();
    } else {
      this.routeForm.controls.routeDetails["controls"][
        "secondaryRoute"
      ].clearValidators();
      this.routeForm.controls.routeDetails["controls"][
        "secondaryRoute"
      ].updateValueAndValidity();
    }
  }

  flagChanged(type, flagName, fields) {
    if (this.routeForm.controls.routeDetails["controls"][flagName].value) {
      for (let x = 0; x < fields.length; x++)
        this.routeForm.controls.routeDetails["controls"][fields[x]].enable();
    } else {
      for (let x = 0; x < fields.length; x++)
        this.routeForm.controls.routeDetails["controls"][fields[x]].disable();
    }
  }

  filterRouteList(list, value) {
    return list.filter(x => x["id"] != value);
  }

  routeChanged(field, type) {
    let primaryVal = this.routeForm.controls.routeDetails["controls"]["primaryRoute"]
      .value;
    let secondaryVal = this.routeForm.controls.routeDetails["controls"][
      "secondaryRoute"
    ].value;
    let retryVal = this.routeForm.controls.routeDetails["controls"]["retryRoute"]
      .value;

    if (field == "primaryRoute") {
      this.domesticSecondaryAggregatorList = this.filterRouteList(
        this.domesticAggregatorList,
        primaryVal
      );

      if (retryVal)
        this.domesticSecondaryAggregatorList = this.filterRouteList(
          this.domesticSecondaryAggregatorList,
          retryVal
        );

      this.domesticRetryAggregatorList = this.filterRouteList(
        this.domesticAggregatorList,
        primaryVal
      );

      if (secondaryVal)
        this.domesticRetryAggregatorList = this.filterRouteList(
          this.domesticRetryAggregatorList,
          secondaryVal
        );
    } else if (field == "secondaryRoute") {
      this.domesticPrimaryAggregatorList = this.filterRouteList(
        this.domesticAggregatorList,
        secondaryVal
      );

      this.domesticRetryAggregatorList = this.filterRouteList(
        this.domesticPrimaryAggregatorList,
        primaryVal
      );

      if (retryVal)
        this.domesticPrimaryAggregatorList = this.filterRouteList(
          this.domesticPrimaryAggregatorList,
          retryVal
        );
    } else if (field == "retryRoute") {
      this.domesticPrimaryAggregatorList = this.filterRouteList(
        this.domesticAggregatorList,
        retryVal
      );

      this.domesticSecondaryAggregatorList = this.filterRouteList(
        this.domesticPrimaryAggregatorList,
        primaryVal
      );

      if (secondaryVal) {
        this.domesticPrimaryAggregatorList = this.filterRouteList(
          this.domesticPrimaryAggregatorList,
          secondaryVal
        );
      }
    }
  }

  retryCountChanged(type) {
    let retryCount = this.routeForm.controls.routeDetails["controls"]["retryCount"]
      .value;
    let retryIntervalControl = <FormArray>(
      this.routeForm.controls.routeDetails["controls"]["retryIntervalArray"]
    );

    for (let x = retryIntervalControl.length - 1; x >= 0; x--) {
      retryIntervalControl.removeAt(x);
    }

    for (let x = 0; x < retryCount; x++) {
      retryIntervalControl.push(
        this.formBuilder.group({
          retryInterval: [
            "",
            [
              Validators.required,
              Validators.min(1),
              Validators.max(900),
              Validators.pattern(this.commonService.patterns.numberOnly)
            ]
          ]
        })
      );
    }
  }

  get RouteForm() {
    let x = <FormGroup>this.routeForm.controls.routeDetails;
    return x.controls;
  }

  get internationalRouteForm() {
    let x = <FormGroup>this.routeForm.controls.international;
    return x.controls;
  }
}