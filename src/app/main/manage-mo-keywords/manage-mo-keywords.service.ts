
import { Injectable } from '@angular/core';
import { NetworkService } from '../../common/network.service';

@Injectable()
export class MoKeywordsService {

  constructor(private _networkService: NetworkService) { }

  getkeywordsList(id: any) {
    return this._networkService.get('honcho/mo/route?epoch=' + id, null, 'bearer');
  }

  // getAllUsers() {
  //   return this._networkService.get('user', null, 'basic');
  // }

  // getAllAccountTypes() {
  //   return this._networkService.get('types', null, 'basic');
  // }



  getDepartmentList() {
    return this._networkService.get('honcho/master/department', null, 'bearer');
  }
  getRouteList(req: any) {
    return this._networkService.get('honcho/mo/server?showAll=' + req, null, 'bearer');
  }

  saveMoKeyword(req: any) {
    return this._networkService.post('honcho/mo/route', req, null, 'bearer');
  }

  keywordExists(req: any) {
    return this._networkService.get('honcho/mo/keyword/exists/'+req['code']+'/'+req['keyword'], null, 'bearer');
  }

  // getPendingSenderIdList(id: any){
  //   return this._networkService.get('senderid/info/'+ id +'/pending', null, 'basic');
  // }

  deleteMoKeyword(req: any) {
    return this._networkService.post('honcho/mo/route/' + req['id'] + '?remarks=' + req['remarks'], null, null, 'bearer');
  }

  updateMOKeywrd(req: any) {
    return this._networkService.post('honcho/update/mo/route', req, null, 'bearer');
  }


  getRetryAggregatorList() {
    let retryAggregator = [{ "id": "P", "aggregatorName": "Primary Route" },
    { "id": "S", "aggregatorName": "Secondary Route" },
    { "id": "B", "aggregatorName": "Both" }];
    return retryAggregator;
  }

  // updateSattus(req: any){
  //   return this._networkService.put('senderid/approve/'+ req["senderid"] +'?approvedBy='+ req["approvedBy"] +'&status=' + req["status"], null, null, 'basic');
  // }

}

