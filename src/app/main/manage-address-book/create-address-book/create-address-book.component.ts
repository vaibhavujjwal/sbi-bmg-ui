import { Component, OnInit, ViewChild } from '@angular/core';
import { AddressBookService } from '../manage-address-book.service';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from "../../../common/common.service";
import { ExcelService } from "../../../common/excel.service";
import { RESPONSE } from "../../../common/common.const";
import { Router } from '../../../../../node_modules/@angular/router';
import { RxwebValidators } from '../../../../../node_modules/@rxweb/reactive-form-validators';

@Component({
  selector: 'app-create-address-book',
  templateUrl: './create-address-book.component.html',
  styleUrls: ['./create-address-book.component.scss']
})
export class CreateAddressBookComponent implements OnInit {
  addressBookForm: FormGroup;
  formSubmitted: boolean = false;
  isEdit: boolean = false;
  private modalRef: NgbModalRef;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  groupList = [];
  fileUploaded: boolean = false;
  uploadedFileId: any;

  constructor(private addressBookService: AddressBookService, private fb: FormBuilder,
    private modalService: NgbModal, private spinner: NgxSpinnerService, private commonService: CommonService,
    private toastr: ToastrService, private excelService: ExcelService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getAddressBookList();
    this.initialiseForms();
  }

  getAddressBookList() {
    let userId = this.commonService.getUser();
    this.spinner.show();
    this.addressBookService.getAddressBookList().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.groupList = res['data'];
          let recordFor = this.getForm.addressBookType.value;
          if (recordFor == "N") {
            this.groupList = this.groupList.filter(word => word.type == "N");
            console.log(this.groupList);
          } else {
            this.groupList = this.groupList.filter(word => word.type == "P");
            console.log(this.groupList);
          }
        }
        else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  initialiseForms() {
    this.addressBookForm = this.fb.group({
      addressBookType: ['N', [Validators.required]],
      uploadType: ['1', [Validators.required]],
      records: this.fb.array([]),
      requestType: ['1'],
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30), Validators.pattern(this.commonService.patterns.addressBookName)]]
    });
    // Validators.compose([Validators.required, this.commonService.nospaceValidator])
    let items = this.addressBookForm.get('records') as FormArray;
    items.push(this.createRecord());
  }

  createRecord(): FormGroup {
    let recordFor = this.getForm.addressBookType.value;
    if (recordFor === "N") {
      return this.fb.group({
        mobileNumber: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern(this.commonService.patterns.phone), RxwebValidators.unique()]],
        name: [''],
        company: [''],
        email: [''],
        location: ['']
      });
    } else {
      return this.fb.group({
        mobileNumber: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern(this.commonService.patterns.phone), RxwebValidators.unique()]],
        name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50), Validators.pattern(this.commonService.patterns.addressName)]],
        company: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50), Validators.pattern(this.commonService.patterns.alphaNumericStartWithOnlyAlphabetWithSpace)]],
        email: ['', [Validators.required, Validators.pattern(this.commonService.patterns.email)]],
        location: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(256), Validators.pattern(this.commonService.patterns.address)]]
      });
    }
  }

  addNewRecord() {
    let items = this.addressBookForm.get('records') as FormArray;
    items.push(this.createRecord());
  }

  deleteRecord(index: any) {
    let control = <FormArray>this.addressBookForm.controls.records;
    control.removeAt(index);
  }

  get getForm() { return this.addressBookForm.controls; }

  contactTypeChanged(event: any) {
    this.getAddressBookList();
    let addressBookType = this.getForm.addressBookType.value;
    let uploadType = this.getForm.uploadType.value;

    let controls = <FormArray>this.addressBookForm.controls.records;
    if (addressBookType == "N" && uploadType != "2") {
      controls.controls.forEach(element => {
        element["controls"]["mobileNumber"].setValidators([Validators.required, Validators.maxLength(10), Validators.minLength(10)]);
        element["controls"]["mobileNumber"].updateValueAndValidity();
        element["controls"]["name"].clearValidators();
        element["controls"]["name"].updateValueAndValidity();
        element["controls"]["email"].clearValidators();
        element["controls"]["email"].updateValueAndValidity();
        element["controls"]["location"].clearValidators();
        element["controls"]["location"].updateValueAndValidity();
      });
    } else if (addressBookType == "P" && uploadType != "2") {
      controls.controls.forEach(element => {
        element["controls"]["mobileNumber"].setValidators([Validators.required, Validators.maxLength(10), Validators.minLength(10)]);
        element["controls"]["mobileNumber"].updateValueAndValidity();
        element["controls"]["name"].setValidators([Validators.required, Validators.minLength(2), Validators.maxLength(50), Validators.pattern(this.commonService.patterns.addressName)]);
        element["controls"]["name"].updateValueAndValidity();
        element["controls"]["email"].setValidators([Validators.required, Validators.pattern(this.commonService.patterns.email)]);
        element["controls"]["email"].updateValueAndValidity();
        element["controls"]["company"].setValidators([Validators.required, Validators.minLength(2), Validators.maxLength(50), Validators.pattern(this.commonService.patterns.alphaNumericStartWithOnlyAlphabetWithSpace)]);
        element["controls"]["company"].updateValueAndValidity();
        element["controls"]["location"].setValidators([Validators.required, Validators.minLength(5), Validators.maxLength(256), Validators.pattern(this.commonService.patterns.address)]);
        element["controls"]["location"].updateValueAndValidity();
      });
    }
  }

  uploadTypeChanged(enent: any) {
    let uploadType = this.getForm.uploadType.value;
    let contactType = this.getForm.addressBookType.value;

    if (uploadType == "2") {
      this.fileUploaded = false;
      const control = <FormArray>this.getForm.records;
      for (let i = control.length - 1; i >= 0; i--) {
        control.removeAt(i)
      }
    } else {
      let items = this.addressBookForm.get('records') as FormArray;
      items.push(this.createRecord());
    }

  }

  fileDownload(fileType: string) {

    if (fileType === "text") {

    } else {

    }
  }

  groupFlagChanged(groupFalg: any) {
    this.getForm.name.setValue("");
  }

  // @ViewChild('fileInput') fileInput: any;

  onFileSelect(files) {

    let self = this;
    // self.fileInput.nativeElement.value = '';
    const file = files[0];
    if (file) {
      const filemb = file.size / (1024 * 1024);
      const fileExt = file.name.split(".").pop();
      if (fileExt.toLowerCase() == "xlsx" || fileExt.toLowerCase() == "xls" || fileExt.toLowerCase() == "csv") {
        let fromDataReq = new FormData();
        fromDataReq.append("fileData", files[0]);
        fromDataReq.append("module", "1");
        fromDataReq.append("userId", this.commonService.getUser());
        fromDataReq.append("username ", this.commonService.getUserName());
        this.spinner.show();
        this.addressBookService.uploadFile(fromDataReq).subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
              this.uploadedFileId = res['data'];
              this.fileUploaded = true;
              this.commonService.showSuccessToast("File uploaded successfully");
            } else {
              this.fileUploaded = false;
              this.commonService.showErrorToast(res['result']['userMsg']);
            }
          },
          err => {
            this.fileUploaded = false;
          });
      } else {
        this.commonService.showErrorToast("Invalid file");
        this.fileUploaded = false;
      }
    }
  }
  numArr = [];
  hasDuplicateNum: boolean;
  fetchRecordsFromForm() {
    let items = this.addressBookForm.get('records') as FormArray;
    let dataArr = [];
    items.controls.forEach(element => {
      let rowObj = {
        "mobileNumber": element.get('mobileNumber').value,
        "name": element.get('name').value,
        "company": element.get('company').value,
        "email": element.get('email').value,
        "location": element.get('location').value
      };
      dataArr.push(rowObj);
      this.numArr.push(JSON.stringify(rowObj['mobileNumber']));
    });
    return dataArr;
  }

  hasDuplicates(array) {
    return (new Set(array)).size !== array.length;
  }

  submitForm() {
    this.formSubmitted = true;
    console.log(this.addressBookForm.value);
    if (this.addressBookForm.valid) {
      let req = {
        "name": this.getForm.name.value,
        "addressBookType": this.getForm.addressBookType.value,
        "requestType": this.getForm.requestType.value,
        "uploadType": this.getForm.uploadType.value,
        "fileId": this.fileUploaded ? this.uploadedFileId : null,
        "existingAddressBookId": this.getForm.requestType.value == 2 ? this.getForm.name.value : "",
        "addressBookInput": this.getForm.uploadType.value == 1 ? this.fetchRecordsFromForm() : null
        // "userId": this.commonService.getUser()
      };
      // this.hasDuplicateNum = this.hasDuplicates(this.numArr);
      // if (this.hasDuplicateNum) {
      //   this.commonService.showErrorToast("Enter unique mobile number");
      // }
      // else {
        this.spinner.show();
        this.addressBookService.createAddressBook(req).subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
              this.commonService.showSuccessToast('"' + res['data']['name'] + '" address book has been created successfully');
              this.router.navigate(["/main/manage-address-book/view-address-book"]);
              this.formSubmitted = false;
              this.initialiseForms();
            } else {
              this.commonService.showErrorToast(res['result']['userMsg']);
            }
          });
      // }

    }
  }

  sampleFileDownload(fileType: string) {
    if (fileType === "txt") {
      let txtData = "TEXT\r\nSAMPLE";
      this.excelService.exportAsTxtFile(txtData, 'Sample_TXT');
    } else if (fileType === "excel") {
      let excelData;
      if (this.getForm.addressBookType.value == "N") {
        excelData = [
          {
            "Mobile Number": "XXXXXXXX12"
          },
          {
            "Mobile Number": "XXXXXXXX23"
          }
        ];
      } else {
        excelData = [
          {
            "Mobile Number": "XXXXXXXX12",
            "Name": "test name 01",
            "Email": "test01@email.com",
            "Address": "test address 01",
            "Company": "test comapny 01"
          },
          {
            "Mobile Number": "XXXXXXXX12",
            "Name": "test name 02",
            "Email": "test02@email.com",
            "Address": "test address 02",
            "Company": "test comapny 02"
          }
        ];
      }

      this.excelService.exportAsExcelFile(excelData, 'Sample_Excel');
    } else if (fileType === "csv") {
      let csvData;
      if (this.getForm.addressBookType.value == "N") {
        csvData = [
          {
            "Mobile Number": "XXXXXXXX12"
          },
          {
            "Mobile Number": "XXXXXXXX23"
          }
        ];
      } else {
        csvData = [
          {
            "Mobile Number": "XXXXXXXX12",
            "Name": "test name 01",
            "Email": "test01@email.com",
            "Address": "test address 01",
            "Company": "test comapny 01"
          },
          {
            "Mobile Number": "XXXXXXXX12",
            "Name": "test name 02",
            "Email": "test02@email.com",
            "Address": "test address 02",
            "Company": "test comapny 02"
          }
        ];
      }
      this.excelService.exportAsCsvFile(csvData, 'Sample_CSV');
    }
  }

  formControlValueChanged() {
    this.addressBookForm.get('addressBookType').valueChanges.subscribe(
      (mode: string) => {
      });
  }
  // trimming_fn(x) {
  //   return x ? x.replace(/\\/g, '') : '';
  // }
}