import { Component, OnInit, ViewChild } from '@angular/core';
import { ManageDepartmentService } from '../manage-department.service';
import { NgbModal, NgbModalRef, NgbModalOptions,ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { NgxSpinnerService } from 'ngx-spinner';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../../../common/common.service';
import { RESPONSE } from '../../../common/common.const';
import Swal from "sweetalert2";

export interface departmentListTypes {
  Actions: any;
  id: number;
  department: string;
  show: any;
}
@Component({
  selector: 'app-view-department',
  templateUrl: './view-department.component.html',
  styleUrls: ['./view-department.component.scss']
})
export class ViewDepartmentComponent implements OnInit {
  departmentList = [];
  departmentForm: FormGroup;
  deleteForm: FormGroup;
  pageLimit: number;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  searchText: string = "";
  p: number = 1;
  formSubmitted: boolean = false;
  deleteFormSubmitted: boolean = false;
  updateFormSubmitted: boolean = false;
  modalRef: any;
  closeResult: string;
  deleteDepartmentList : Array<string>;
  updateDepartmentList = [];
  showError: boolean = false;
  displayedColumns: string[] = ['actions',	'department',	'id',	'show'];
  dataSource: MatTableDataSource<departmentListTypes>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private modalService: NgbModal,
    private spinner: NgxSpinnerService, private fb: FormBuilder, public commonService: CommonService, private deptService: ManageDepartmentService) {
    }

    ngOnInit() {
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.getDepartmentList();
    debugger;
    }

    getDepartmentList() {
          this.spinner.show();
          let request ={
            "id" : "",
            "status":""
          }
          this.deptService.getAllDepartmentList(request).subscribe(
            res => {
              this.spinner.hide();
              if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
                this.departmentList = res["data"];
                this.departmentList.forEach(deptList => {
                  deptList['show'] == 1? deptList['show'] = 'Enabled' : deptList['show'] ='Disabled';
                });
                this.dataSource= new MatTableDataSource(this.departmentList) ;
                this.dataSource.sort = this.sort;
                this.dataSource.paginator = this.paginator;
              } else {
                this.commonService.showErrorToast(res['result']['userMsg']);
                this.departmentList = [];
              }
      });
    }

    applyFilter(event: Event) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
      this.showError=false;
      if(this.dataSource.filteredData.length==0){
        this.showError=true;
      }
    }

    prepareDeleteDeaprtmentForm() {
    this.deleteForm = this.fb.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)
        ]
      ]
    });
  }

  viewDeleteDepartmentModal(dept, content, btn){
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();
    this.prepareDeleteDeaprtmentForm();
    this.deleteFormSubmitted = false;
     this.deleteDepartmentList = dept;
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  deleteDepartment(){
    this.deleteFormSubmitted = true;
    if (this.deleteForm.valid) {
      Swal.fire({
        title: "Are you sure?",
        text: "Do you really want to permanently delete "+ this.deleteDepartmentList["department"] +" department?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ok, Proceed"
      }).then(result => {
        if (result.value) {
          this.spinner.show();
          let req = {
            "deptId": this.deleteDepartmentList['id'],
            "loginId": this.commonService.getUser(),
            "remarks": this.deleteForm.controls.remarks.value
          };
          this.deptService.deleteDepartmentById(req)
            .subscribe(data => {
              this.spinner.hide();
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.commonService.showSuccessToast(this.deleteDepartmentList["department"] +' department  has been deleted');
                this.modalRef.close();
              } else {
                this.commonService.showErrorToast(data['result']['userMsg']);
              }
              this.getDepartmentList();
            });
        }
      });
    }
  }

  changeStatus(record: any){
    let req = {
      "deptId": record['id'],
      "status": record['show'] == 'Enabled' ? 0 : 1
    };
    this.spinner.show();
    this.deptService.updateStatusById(req).subscribe(
    res => {
      this.spinner.hide();
      if(record['show']== 'Enabled'){
        this.commonService.showSuccessToast(record['department'] + ' department disabled');
      }else if(record['show']== 'Disabled'){
        this.commonService.showSuccessToast(record['department'] + ' department enabled');
      }
      this.getDepartmentList();

    },
    err => {
      this.getDepartmentList();
    });
  }

  changeStatusDepartmentConfirmation(record : any,id){
    debugger;
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    if(record['show'] == 'Enabled'){
      modalRef.componentInstance.content = 'Are you sure you want to disable '+ record['department'] +' department?';
    }else{
      modalRef.componentInstance.content = 'Are you sure you want to enable ' + record['department'] + ' department?';
    }
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.changeStatus(record);
        }else{
          document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];

        }
      },
      (reason: any) => {
        document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];


      });
  }

  populateValues(item) {
    this.departmentForm.controls['deptName'].setValue(item.department);
  }

  prepareUpdateUserForm() {
    this.departmentForm = this.fb.group({
      deptName: [
        "",
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
          Validators.pattern(this.commonService.patterns.alphaNumaric)
        ]
      ]
    });
  }

  updateRecord(updatemodal: any, record: any) {
    this.prepareUpdateUserForm();
    this.populateValues(record);
    this.updateDepartmentList = record;
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {

    });
  }

  updateDepartment(){
    this.updateFormSubmitted = true;
    if (this.departmentForm.valid) {
      let reqObj = {
        "deptName": this.departmentForm.controls["deptName"].value,
        "deptId": this.updateDepartmentList['id']
      }
      this.spinner.show();
      this.deptService.updateDepartmentById(reqObj).subscribe(
        res => {
          this.commonService.showSuccessToast(this.updateDepartmentList['department'] + ' department has been updated');
                  this.updateFormSubmitted = false;
                  this.modalRef.close();
                  this.getDepartmentList();
        });
    }
  }


  confirmUpdateRecoed(){
    this.formSubmitted = true;
    if(this.departmentForm.valid){
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    modalRef.componentInstance.content = 'Are you sure you want to update details?';
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.updateDepartment();
        }
      },
      (reason: any) => {

      });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

}
