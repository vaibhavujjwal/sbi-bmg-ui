import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ManageSmsTemplateComponent } from "./manage-sms-template.component";
import { ApprovedSmsTemplateComponent } from "./approved-sms-template/approved-sms-template.component";
import { TemplatePendingComponent } from "./template-pending/template-pending.component";
import { BulkUploadTemplateComponent } from "./bulk-upload-template/bulk-upload-template.component";
import { CreateSmsTemplateComponent } from "./create-sms-template/create-sms-template.component";
import { ModuleGuardService, AuthGuardService } from "src/app/guards";
import { MODULE } from "src/app/common/common.const";

export const routes: Routes = [
  {
    path: "",
    component: ManageSmsTemplateComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/manage-sms-template/approved-sms-template",
        pathMatch: "full"
      },
      {
        path: "approved-sms-template",
        component: ApprovedSmsTemplateComponent,
        data: { moduleName: MODULE.SMS_TEMPLATE, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "create-sms-template",
        component: CreateSmsTemplateComponent,
        data: { moduleName: MODULE.SMS_TEMPLATE_BULK_UPLOAD, permissions: ["RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "template-pending",
        component: TemplatePendingComponent,
        data: {
          moduleName: MODULE.SMS_TEMPLATE_APPROVAL,
          permissions: ["R", "RW"]
        },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "bulk-upload-template",
        component: BulkUploadTemplateComponent,
        data: {
          moduleName: MODULE.SMS_TEMPLATE_BULK_UPLOAD,
          permissions: ["RW"]
        },
        canActivate: [AuthGuardService, ModuleGuardService]
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
