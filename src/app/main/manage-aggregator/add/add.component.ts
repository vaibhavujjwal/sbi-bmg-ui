import { Component, OnInit } from "@angular/core";
import Stepper from "bs-stepper";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from "../../../common/common.service";
import { ManageAggregatorService } from "../manage-aggregator.service";
import Swal from "sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE } from "src/app/common/common.const";

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.scss"],
  providers: [ManageAggregatorService]
})
export class AddComponent implements OnInit {
  INTERNATIONAL: string = "I";
  DOMESTIC: string = "D";

  HTTP: string = "HTTP";
  SMPP: string = "SMPP";

  aggregatorForm: FormGroup;
  formSubmitted: boolean = false;

  aggregatorTypeList: Array<object> = [];
  aggregatorProtocolList: Array<object> = [];
  aggregatorResponseType: Array<object> = [];

  categoryList: Array<object> = [];
  isEdit: boolean = false;
  aggregatorId = -1;
  aggregatorInfo: object = {};

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public commonService: CommonService,
    private manageAggregatorService: ManageAggregatorService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params["id"]) this.aggregatorId = params["id"];
    });

    if (this.aggregatorId != -1) {
      this.isEdit = true;
      //this.fetchExistingAggregatorDetails();
    }
    this.prepareAggregatorForm();
    this.loadAggregatorTypeList();
    this.loadAggregatorProtocolList();
    this.loadAggregatorResponseList();
    this.loadCategoryList();
    if (this.aggregatorId != -1) {
      //this.loadDepartmentList();
      this.fetchExistingUserDetails();
    }
  }

  prepareAggregatorForm() {
    this.aggregatorForm = this.formBuilder.group({
      aggregatorName: [
        "",
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
          Validators.pattern(this.commonService.patterns.alphaNumaricHyphen)
        ]
      ],
      aggregatorType: ["", [Validators.required]],
      tps: [
        "",
        [
          Validators.required,
          Validators.min(1),
          Validators.max(50000),
          Validators.pattern(this.commonService.patterns.numberOnly)
        ]
      ],
      aggregatorProtocol: ["", [Validators.required]],
      port: ["", [Validators.required, Validators.max(65535), Validators.min(100), Validators.pattern(this.commonService.patterns.numberOnly)]],
      ip: ["", [Validators.required, Validators.pattern(this.commonService.patterns.ip)]],
      payload: ["", [Validators.pattern(this.commonService.patterns.queryString)]],
      unicodePayload: ["", [Validators.pattern(this.commonService.patterns.queryString)]],
      aggregatorResponse: ["", Validators.required],
      category: ["", Validators.required],
      email: ["", [Validators.required, Validators.pattern(this.commonService.patterns.email)]],
      emailSecondary: ["", Validators.pattern(this.commonService.patterns.email)]

      /*topic: [
        "",
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
          Validators.pattern(
            this.commonService.patterns.alphaNumericStartWithOnlyAlphabet
          )
        ]
      ],

      url: new FormControl({ disabled: true, value: "" }, [
        Validators.required,
        Validators.pattern(this.commonService.patterns.urlRegex)
      ]),
      sourceIp: new FormControl({ disabled: true, value: "" }, [
        Validators.required,
        Validators.pattern(this.commonService.patterns.anyThing)
      ]),
      sourcePort: new FormControl({ disabled: true, value: "" }, [
        Validators.required,
        Validators.pattern(this.commonService.patterns.numberOnly)
      ]),
      userName: [
        "",
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
          Validators.pattern(
            this.commonService.patterns.alphaNumericStartWithOnlyAlphabet
          )
        ]
      ],
      passWord: [
        "",
        [Validators.required, Validators.minLength(2), Validators.maxLength(50)]
      ]*/
    });
  }

  loadAggregatorTypeList() {
    this.aggregatorTypeList = this.manageAggregatorService.getAggregatorTypeList();
  }

  loadAggregatorProtocolList() {
    this.aggregatorProtocolList = this.manageAggregatorService.getAggregatorProtocolList();
  }

  loadAggregatorResponseList() {
    this.spinner.show();
    this.manageAggregatorService.getAggregatorResponseList().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.aggregatorResponseType = res['data'];
        } else {
          this.aggregatorResponseType = [];
        }
      });
  }

  loadCategoryList() {
    this.spinner.show();
    this.manageAggregatorService.getCategoryList().subscribe(data => {
      this.spinner.hide();
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        this.categoryList = data["data"];
      }
    });
  }

  loadExistingAggregatorDetails() {
    this.aggregatorForm["controls"]["aggregatorName"].setValue(this.aggregatorInfo["aggregatorName"]);
    this.aggregatorForm["controls"]["aggregatorType"].setValue(this.aggregatorInfo["aggregatorType"]);
    this.aggregatorForm["controls"]["tps"].setValue(this.aggregatorInfo["tps"]);
    this.aggregatorForm["controls"]["aggregatorProtocol"].setValue(this.aggregatorInfo["aggregatorProtocol"]);
    this.aggregatorForm["controls"]["port"].setValue(this.aggregatorInfo["port"]);
    this.aggregatorForm["controls"]["ip"].setValue(this.aggregatorInfo["ip"]);
    this.aggregatorForm["controls"]["payload"].setValue(this.aggregatorInfo["textPayload"]);
    this.aggregatorForm["controls"]["unicodePayload"].setValue(this.aggregatorInfo["unicodePayload"]);
    this.aggregatorForm["controls"]["aggregatorResponse"].setValue(this.aggregatorInfo["responseType"]);
    this.aggregatorForm["controls"]["category"].setValue(this.aggregatorInfo["category"]["id"]);
    this.aggregatorForm["controls"]["email"].setValue(this.aggregatorInfo["emailAddr"]);
    this.aggregatorForm["controls"]["emailSecondary"].setValue(this.aggregatorInfo["emailCcAddr"]);
    this.spinner.hide();
  }

  fetchExistingUserDetails() {
    if (this.aggregatorId != -1) {
      this.spinner.show();
      let reqObj = {
        "id": this.aggregatorId,
        "status": ""
      }
      this.manageAggregatorService.getAggregatorById(reqObj).subscribe(data => {
        this.spinner.hide();
        if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
          this.aggregatorInfo = data["data"][0];
          this.loadExistingAggregatorDetails();
        } else {
          this.commonService.showErrorToast(data['result']['userMsg']);
          this.router.navigate(["/main/manage-aggregator/view"]);
        }
      });
    }
  }

  saveAggregator() {
    this.formSubmitted = true;
    if (this.aggregatorForm.valid) {
      let reqObj = {
        "aggregatorName": this.aggregatorForm.controls.aggregatorName.value.trim(),
        "aggregatorProtocol": this.aggregatorForm.controls.aggregatorProtocol.value.trim(),
        "aggregatorType": this.aggregatorForm.controls.aggregatorType.value.trim(),
        "ip": this.aggregatorForm.controls.ip.value.trim(),
        "id": this.aggregatorId == -1 ? "" : this.aggregatorInfo['id'],
        "port": this.aggregatorForm.controls.port.value,
        "tps": this.aggregatorForm.controls.tps.value,
        "textPayload": this.aggregatorForm.controls.payload.value.trim(),
        "unicodePayload": this.aggregatorForm.controls.unicodePayload.value.trim(),
        "responseType": this.aggregatorForm.controls.aggregatorResponse.value,
        "status": this.aggregatorId == -1 ? 1 : this.aggregatorInfo['status'],
        "categoryid": Number(this.aggregatorForm.controls.category.value),
        "emailAddr": this.aggregatorForm.controls.email.value.trim(),
        "emailCcAddr": this.aggregatorForm.controls.emailSecondary.value ? this.aggregatorForm.controls.emailSecondary.value.trim() : "",
      }
      this.spinner.show();
      if (this.aggregatorId == -1) {
        this.manageAggregatorService.createAggregator(reqObj).subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
              this.commonService.showSuccessToast(reqObj['aggregatorName'] + ' aggregator added succesfully');
              this.router.navigate(["/main/manage-aggregator/view"]);
            } else {
              if (res['result']['userMsg'].startsWith("Duplicate entry")) {
                this.commonService.showErrorToast("This aggregator name already exists. Please enter new aggregator name");
              } else
                this.commonService.showErrorToast(res['result']['userMsg']);
            }
          }
        );
      } else {
        this.manageAggregatorService.updateAggregator(reqObj).subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
              this.commonService.showSuccessToast(this.aggregatorInfo["aggregatorName"] + ' aggregator updated succesfully');
              this.router.navigate(["/main/manage-aggregator/view"]);
            } else {
              this.commonService.showErrorToast(res['result']['userMsg']);
            }

          });
      }
    }
  }
  
  cancel() {
    Swal.fire({
      title: "Are you sure?",
      text: "All information will be discarded!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ok, Left this page"
    }).then(result => {
      if (result.value) {
        this.router.navigate(["/main/manage-aggregator/view"]);
      }
    });
  }

  // fetchExistingAggregatorDetails() {
  //   if (this.aggregatorId != -1) {
  //     this.spinner.show();
  //     this.manageAggregatorService
  //       .getAggregator(this.aggregatorId)
  //       .subscribe(data => {
  //         if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
  //           this.aggregatorInfo = data["data"];
  //           this.loadExistingAggregatorDetails();
  //         } else {
  //           this.commonService.showErrorToast(data['result']['userMsg']);
  //           this.router.navigate(["/main/manage-user/view-user"]);
  //         }
  //       });
  //   }
  // }

  /*aggregatorProtocolChanged() {
    if (this.aggregatorForm.controls.aggregatorProtocol.value == this.HTTP) {
      this.aggregatorForm.controls.url.enable();
      this.aggregatorForm.controls.sourceIp.disable();
      this.aggregatorForm.controls.sourcePort.disable();
    } else if (
      this.aggregatorForm.controls.aggregatorProtocol.value == this.SMPP
    ) {
      this.aggregatorForm.controls.url.disable();
      this.aggregatorForm.controls.sourceIp.enable();
      this.aggregatorForm.controls.sourcePort.enable();
    }
  }*/
}