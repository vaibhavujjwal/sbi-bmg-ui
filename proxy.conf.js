const PROXY_CONFIG = [
    {
        context: [
            "/api",
            "/many",
            "/endpoints",
            "/i",
            "/need",
            "/to",
            "/proxy",
            "/access",
            "/payment",
            "/patron"
        ],
        target: "http://localhost:8090/sbi/config/v1/",
        secure: false,
        logLevel: "info",
        changeOrigin: true
    }
]

module.exports = PROXY_CONFIG;