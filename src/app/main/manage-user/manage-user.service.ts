import { Injectable } from "@angular/core";
import { NetworkService } from "../../common/network.service";
import { CommonService } from "../../common/common.service";
import { MODULE } from "src/app/common/common.const";
import { forkJoin } from 'rxjs';

@Injectable()
export class ManageUserService {
  NO_HEADER = null;
  SYSTEM_DEPT_ID = 2000;
  ROLE_ID = 3;
  loginId = "loginId=";
  constructor(
    private _networkService: NetworkService,
    private _commonService: CommonService
  ) {
    this.loginId = this.loginId + this._commonService.getUser();
  }

  getData(req: any) {
    let roles = this._networkService.get("honcho/master/role", this.NO_HEADER, this._commonService.bearer);
    let useryRoles = this._networkService.get("honcho/user/byrole/" + req['roleId'], this.NO_HEADER, this._commonService.bearer);
    let departments = this._networkService.get("honcho/master/department", this.NO_HEADER, this._commonService.bearer);
    // let existingUser = this._networkService.post("honcho/user" + "?userId=" + req['userId'], this.NO_HEADER, this._commonService.bearer);
    let existingUser = this._networkService.post("honcho/user/" + req['userId'], this.NO_HEADER, this._commonService.bearer);
    + req['userId']
    let forkJoinResponseArray = [];
    if (req['loadDepartment'] && req['fetExistUser']) {
      forkJoinResponseArray = [roles, useryRoles, departments, existingUser];
    } else if (req['loadDepartment']) {
      forkJoinResponseArray = [roles, useryRoles, departments];
    } else if (req['fetExistUser']) {
      forkJoinResponseArray = [roles, useryRoles, existingUser];
    } else {
      forkJoinResponseArray = [roles, useryRoles];
    }
    return forkJoin(forkJoinResponseArray);

  }

  checkUserType(obj) {
    return this._networkService.get("honcho/check/users/" + obj.searchFor + "/" +obj.type,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }
  
  validateUsername(obj) {
    return this._networkService.post(
      "honcho/user/validate/username" +
      "?username=" +
      obj.value +
      "&userId=" +
      obj.id,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  validateEmailAddress(obj) {
    return this._networkService.post(
      "honcho/user/validate/email" +
      "?email=" +
      obj.value +
      "&userId=" +
      obj.id,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  validateMobileNumber(obj) {
    return this._networkService.post(
      "honcho/user/validate/msisdn" +
      "?msisdn=" +
      obj.mobileNumber +
      "&userId=" +
      obj.userId,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  saveUser(user, txnId) {
    return this._networkService.post(
      "honcho/user/save" + "?transactionId=" + txnId,
      user,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getUser(id) {
    return this._networkService.post("honcho/user/" + id, this.NO_HEADER, this._commonService.bearer);
  }

  // updateUser(userId, user, txnId) {
  //   return this._networkService.put(
  //     "honcho/user/" + userId,
  //     user,
  //     this.NO_HEADER,
  //     this._commonService.bearer
  //   );
  // }

  updateUser(user, txnId) {
    return this._networkService.post(
      "honcho/update/user",
      user,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getFinalApproverList(req) {
    return this._networkService.get("honcho/user/byrole/" + req['roleId'], this.NO_HEADER, this._commonService.bearer);
  }

  updateUserStatus(obj) {
    return this._networkService.post(
      "honcho/user/status/update",
      obj,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  deleteUser(userId, loginUserId, remarks) {
    return this._networkService.post(
      "honcho/delete/user" + "?userId=" + userId + "&remarks=" + remarks,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getUsersList(type) {
    return this._networkService.get(
      "honcho/user/list/" + type,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }
  // /sbi/config/v1/honcho/user/list?type={type}&pageNo={pageNo}&pageSize={pageSize}
  getUsersListOnFly(req) {
    return this._networkService.get(
      "honcho/user/list?type=" + req['type'] + '&pageNo=' + req['pageNo'] + '&pageSize=' + req['pageSize'],
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getSearchedRecord(req) {
    return this._networkService.get(
      "honcho/user/search?type=" + req['type'] + '&pageNo=' + req['pageNo'] + '&pageSize=' + req['pageSize']
      + '&searchBy=' + req['searchBy'] + '&searchFor=' + req['searchFor'],
      this.NO_HEADER,
      this._commonService.bearer
    );
  }


  getUserByRoleId(roleId) {
    return this._networkService.get(
      "honcho/user/byrole/" + roleId,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getApproverList(dept, role) {
    return this._networkService.get(
      "honcho/user/approver/list?department=" +
      dept +
      "&role=" +
      role,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getDepartmentList() {
    return this._networkService.get(
      "honcho/master/department",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getTopicList() {
    return this._networkService.get(
      "honcho/master/topic",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getRoleList() {
    return this._networkService.get(
      "honcho/master/role",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getCategoryList() {
    return this._networkService.get(
      "honcho/master/category",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getChannelList() {
    return this._networkService.get(
      "honcho/master/channel",
      this.NO_HEADER,
      this._commonService.bearer
    );
    //return [{ id: 1, name: "HTTP" }, { id: 2, name: "Bulk" }];
  }

  getStatusList() {
    return this._networkService.get(
      "honcho/master/status",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getLockUserList(){
    return this._networkService.get("honcho/user/getLockedUsers",
    this.NO_HEADER,
    this._commonService.bearer);
  }

  unlockUser(req: Object){
    return this._networkService.post(
      "honcho/user/unlockUsers",req,
      this.NO_HEADER,
      this._commonService.bearer
    )
  }

  checkApplicationNameUniqueness(req: any) {
    return this._networkService.post(
      "honcho/user/validate/appName?appName=" + req['applicationName'] + "&id=" + req['id'],
      this.NO_HEADER,
      this._commonService.bearer
    )
  }

  getPrimaryRouteList() {
    //return this._networkService.get("department", this.NO_HEADER, this._commonService.bearer);

    return [
      { id: 1, name: "RouteOne" },
      { id: 2, name: "RouteTwo" },
      { id: 3, name: "RouteThree" }
    ];
  }

  getInternationalRoutesList() {
    //return this._networkService.get("department", null, this._commonService.bearer);

    return [
      { id: 1, name: "RouteOne" },
      { id: 2, name: "RouteTwo" },
      { id: 3, name: "RouteThree" }
    ];
  }

  getRetryRoutesList() {
    //return this._networkService.get("department", null, this._commonService.bearer);

    return [
      { id: 1, name: "RouteOne" },
      { id: 2, name: "RouteTwo" },
      { id: 3, name: "RouteThree" }
    ];
  }

  getReportPrivilegesList() {
    return [
      { id: 1, name: "Self" },
      { id: 2, name: "Department" },
      { id: 3, name: "All" }
    ];
  }

  getTopologyList() {
    return [
      { id: 1, name: "Active/Active" },
      { id: 2, name: "Active/Passive" }
    ];
  }

  // alterDepartmentList(userRole, departmentList) {
  //   var marketingUserDepartmentList = departmentList.filter(dept => {
  //     return dept['department'] == "Marketing and Communication"
  //   })
  //   var nonMarketingUserDepartmentList = departmentList.filter(dept => {
  //     return dept['department'] != "Marketing and Communication"
  //   })
  //   if (userRole['name'] == 'ROLE_MARKETING') return marketingUserDepartmentList
  //   else return nonMarketingUserDepartmentList
  // }

  getPermissionsViewList() {
    let list = [];

    list.push({
      sno: "1",
      view: "Dashboard",
      formControlName: MODULE.DASHBOARD,
      isSubModule: false
    });
    // list.push({
    //   sno: "2",
    //   view: "Registration Requests",
    //   formControlName: MODULE.REGISTRATION_REQUEST,
    //   isSubModule: false
    // });

    list.push({
      sno: "2",
      view: "Manage Department Configuration",
      formControlName: MODULE.DEPARTMENT_CONFIGURATION,
      isSubModule: false
    });

    list.push({
      sno: "3",
      view: "Manage User",
      formControlName: MODULE.USER,
      isSubModule: false
    });

    // var userManagementSubModules = [];
    // userManagementSubModules.push({
    //   sno: "3.1",
    //   view: "Approval Requests",
    //   formControlName: MODULE.USER_APPROVAL
    // });

    // list.push({
    //   sno: "3",
    //   view: "Manage User",
    //   formControlName: MODULE.USER,
    //   isSubModule: true,
    //   subModuleList: userManagementSubModules
    // });

    list.push({
      sno: "4",
      view: "Manage Aggregator",
      formControlName: MODULE.AGGREGATOR,
      isSubModule: false
    });

    list.push({
      sno: "5",
      view: "Manage Aggregator Routes",
      formControlName: MODULE.ROUTES,
      isSubModule: false
    });

    list.push({
      sno: "6",
      view: "Manage Campaign",
      formControlName: MODULE.CAMPAIGN,
      isSubModule: false
    });

    var campaignManagementSubModules = [];
    campaignManagementSubModules.push({
      sno: "6.1",
      view: "Approval Requests",
      formControlName: MODULE.CAMPAIGN_APPROVAL
    });

    list.push({
      sno: "6",
      view: "Manage Campaign",
      formControlName: MODULE.CAMPAIGN,
      isSubModule: true,
      subModuleList: campaignManagementSubModules
    });

    // list.push({
    //   sno: "4",
    //   view: "Manage Aggregator",
    //   formControlName: MODULE.AGGREGATOR,
    //   isSubModule: false
    // });

    list.push({
      sno: "7",
      view: "Manage Address Book",
      formControlName: MODULE.ADDRESS_BOOK,
      isSubModule: false
    });

    list.push({
      sno: "8",
      view: "Manage Sender CLI",
      formControlName: MODULE.SENDERID,
      isSubModule: false
    });

    var senderIdManagementSubModules = [];

    // senderIdManagementSubModules.push({
    //   sno: "8.1",
    //   view: "Approval Requests",
    //   formControlName: MODULE.SENDERID_APPROVAL
    // });

    senderIdManagementSubModules.push({
      sno: "8.1",
      view: "Bulk Upload",
      formControlName: MODULE.SENDERID_BULKUPLOAD
    });

    list.push({
      sno: "8",
      view: "Manage Sender CLI",
      formControlName: MODULE.SENDERID,
      isSubModule: true,
      subModuleList: senderIdManagementSubModules
    });

    list.push({
      sno: "9",
      view: "Manage Template",
      formControlName: MODULE.SMS_TEMPLATE,
      isSubModule: false
    });

    var smsTemplateManagementSubModules = [];

    smsTemplateManagementSubModules.push({
      sno: "9.1",
      view: "Approval Requests",
      formControlName: MODULE.SMS_TEMPLATE_APPROVAL
    });

    smsTemplateManagementSubModules.push({
      sno: "9.2",
      view: "Bulk Upload",
      formControlName: MODULE.SMS_TEMPLATE_BULK_UPLOAD
    });

    list.push({
      sno: "9",
      view: "Manage Template",
      formControlName: MODULE.SMS_TEMPLATE,
      isSubModule: true,
      subModuleList: smsTemplateManagementSubModules
    });

    list.push({
      sno: "10",
      view: "Manage Bulk Template",
      formControlName: MODULE.Bulk_TEMPLATE,
      isSubModule: false
    });

    var bulkTemplateManagementSubModules = [];

    bulkTemplateManagementSubModules.push({
      sno: "10.1",
      view: "Approval Requests",
      formControlName: MODULE.BULK_TEMPLATE_APPROVAL
    });

    bulkTemplateManagementSubModules.push({
      sno: "10.2",
      view: "Create_Template",
      formControlName: MODULE.BULK_TEMPLATE_BULK_UPLOAD
    });

    list.push({
      sno: "10",
      view: "Manage Bulk Template",
      formControlName: MODULE.Bulk_TEMPLATE,
      isSubModule: true,
      subModuleList: bulkTemplateManagementSubModules
    });

    list.push({
      sno: "11",
      view: "Manage Spam Keywords",
      formControlName: MODULE.SPAM_KEYWORD,
      isSubModule: false
    });

    list.push({
      sno: "12",
      view: "Manage MO Route",
      formControlName: MODULE.MO_ROUTE,
      isSubModule: false
    });

    list.push({
      sno: "13",
      view: "Manage MO Keywords",
      formControlName: MODULE.MO_KEYWORD,
      isSubModule: false
    });

    // list.push({
    //   sno: "7",
    //   view: "Manage Spam Keywords",
    //   formControlName: MODULE.SPAM_KEYWORD,
    //   isSubModule: false
    // });

    // list.push({
    //   sno: "9",
    //   view: "Department Configuration",
    //   formControlName: MODULE.DEPARTMENT_CONFIGURATION,
    //   isSubModule: false
    // });
    // list.push({
    //   sno: "11",
    //   view: "System Configuration",
    //   formControlName: MODULE.SYSTEM_CONFIGURATION,
    //   isSubModule: false
    // });

    list.push({
      sno: "14",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: false
    });

    var reportingSubModules = [];

    reportingSubModules.push({
      sno: "14.1",
      view: "Summary Reports",
      formControlName: MODULE.SUMMARY_REPORT
    });

    reportingSubModules.push({
      sno: "14.2",
      view: "Detailed Report",
      formControlName: MODULE.DETAILED_REPORT
    });

    reportingSubModules.push({
      sno: "14.3",
      view: "MO Summary Reports",
      formControlName: MODULE.MO_SUMMARY_REPORT
    });

    reportingSubModules.push({
      sno: "14.4",
      view: "MO Detailed Report",
      formControlName: MODULE.MO_DETAILED_REPORT
    });

    reportingSubModules.push({
      sno: "14.5",
      view: "OTT Summary Report",
      formControlName: MODULE.PUSH_NOTIFICATION_REPORT
    });

    reportingSubModules.push({
      sno: "14.5",
      view: "Failed SMS Report",
      formControlName: MODULE.FAILED_SMS_REPORT
    });


    list.push({
      sno: "14",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: true,
      subModuleList: reportingSubModules
    });
    return list;
  }


  getPermissionsViewListForCRM() {
    let list = [];
    list.push({
      sno: "1",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: false
    });

    var reportingSubModules = [];

    // reportingSubModules.push({
    //   sno: "1.1",
    //   view: "Summary Reports",
    //   formControlName: MODULE.SUMMARY_REPORT
    // });

    reportingSubModules.push({
      sno: "1.2",
      view: "Detailed Report",
      formControlName: MODULE.DETAILED_REPORT
    });

    // reportingSubModules.push({
    //   sno: "1.3",
    //   view: "MO Summary Reports",
    //   formControlName: MODULE.MO_SUMMARY_REPORT
    // });

    // reportingSubModules.push({
    //   sno: "1.4",
    //   view: "MO Detailed Report",
    //   formControlName: MODULE.MO_DETAILED_REPORT
    // });

    // reportingSubModules.push({
    //   sno: "1.5",
    //   view: "OTT Summary Report",
    //   formControlName: MODULE.PUSH_NOTIFICATION_REPORT
    // });

    list.push({
      sno: "1",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: true,
      subModuleList: reportingSubModules
    });
    return list;
  }

  getPermissionsViewListCampaign() {
    let list = [];

    list.push({
      sno: "1",
      view: "Dashboard",
      formControlName: MODULE.DASHBOARD,
      isSubModule: false
    });

    list.push({
      sno: "2",
      view: "Manage Campaign",
      formControlName: MODULE.CAMPAIGN,
      isSubModule: false
    });

    list.push({
      sno: "3",
      view: "Manage Address Book",
      formControlName: MODULE.ADDRESS_BOOK,
      isSubModule: false
    });

    list.push({
      sno: "4",
      view: "Manage Sender CLI",
      formControlName: MODULE.SENDERID,
      isSubModule: false
    });

    list.push({
      sno: "5",
      view: "Manage Template",
      formControlName: MODULE.SMS_TEMPLATE,
      isSubModule: false
    });
    //var bulkTemplateManagementSubModules = [];

    /*bulkTemplateManagementSubModules.push({
      sno: "5.1",
      view: "Create Template",
      formControlName: MODULE.BULK_TEMPLATE_BULK_UPLOAD
    });

    list.push({
      sno: "5",
      view: "Manage Template",
      formControlName: MODULE.SMS_TEMPLATE,
      isSubModule: true,
      subModuleList: bulkTemplateManagementSubModules
    });*/

    list.push({
      sno: "6",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: false
    });

    var reportingSubModules = [];

    reportingSubModules.push({
      sno: "6.1",
      view: "Summary Reports",
      formControlName: MODULE.SUMMARY_REPORT
    });

    reportingSubModules.push({
      sno: "6.2",
      view: "Detailed Report",
      formControlName: MODULE.DETAILED_REPORT
    });

    reportingSubModules.push({
      sno: "6.3",
      view: "MO Summary Reports",
      formControlName: MODULE.MO_SUMMARY_REPORT
    });

    reportingSubModules.push({
      sno: "6.4",
      view: "MO Detailed Report",
      formControlName: MODULE.MO_DETAILED_REPORT
    });

    reportingSubModules.push({
      sno: "6.5",
      view: "OTT Summary Report",
      formControlName: MODULE.PUSH_NOTIFICATION_REPORT
    });

    list.push({
      sno: "6",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: true,
      subModuleList: reportingSubModules
    });
    return list;
  }

  getPermissionsViewListAdmin() {
    let list = [];

    list.push({
      sno: "1",
      view: "Dashboard",
      formControlName: MODULE.DASHBOARD,
      isSubModule: false
    });
    list.push({
      sno: "2",
      view: "Manage Department Configuration",
      formControlName: MODULE.DEPARTMENT_CONFIGURATION,
      isSubModule: false
    });

    list.push({
      sno: "3",
      view: "Manage User",
      formControlName: MODULE.USER,
      isSubModule: false
    });

    list.push({
      sno: "4",
      view: "Manage Aggregator",
      formControlName: MODULE.AGGREGATOR,
      isSubModule: false
    });

    list.push({
      sno: "5",
      view: "Manage Aggregator Routes",
      formControlName: MODULE.ROUTES,
      isSubModule: false
    });

    list.push({
      sno: "6",
      view: "Manage Campaign",
      formControlName: MODULE.CAMPAIGN,
      isSubModule: false
    });

    list.push({
      sno: "7",
      view: "Manage Sender CLI",
      formControlName: MODULE.SENDERID,
      isSubModule: false
    });

    var senderIdManagementSubModules = [];

    senderIdManagementSubModules.push({
      sno: "7.1",
      view: "Bulk Upload",
      formControlName: MODULE.SENDERID_BULKUPLOAD
    });

    list.push({
      sno: "7",
      view: "Manage Sender CLI",
      formControlName: MODULE.SENDERID,
      isSubModule: true,
      subModuleList: senderIdManagementSubModules
    });

    list.push({
      sno: "8",
      view: "Manage Template",
      formControlName: MODULE.SMS_TEMPLATE,
      isSubModule: false
    });

    /*list.push({
      sno: "9",
      view: "Manage Bulk Template",
      formControlName: MODULE.Bulk_TEMPLATE,
      isSubModule: false
    });*/

    list.push({
      sno: "9",
      view: "Manage Spam Keywords",
      formControlName: MODULE.SPAM_KEYWORD,
      isSubModule: false
    });

    list.push({
      sno: "10",
      view: "Manage MO Route",
      formControlName: MODULE.MO_ROUTE,
      isSubModule: false
    });

    list.push({
      sno: "11",
      view: "Manage MO Keywords",
      formControlName: MODULE.MO_KEYWORD,
      isSubModule: false
    });

    list.push({
      sno: "12",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: false
    });

    var reportingSubModules = [];

    reportingSubModules.push({
      sno: "12.1",
      view: "Summary Reports",
      formControlName: MODULE.SUMMARY_REPORT
    });

    reportingSubModules.push({
      sno: "12.2",
      view: "Detailed Report",
      formControlName: MODULE.DETAILED_REPORT
    });

    reportingSubModules.push({
      sno: "12.3",
      view: "MO Summary Reports",
      formControlName: MODULE.MO_SUMMARY_REPORT
    });

    reportingSubModules.push({
      sno: "12.4",
      view: "MO Detailed Report",
      formControlName: MODULE.MO_DETAILED_REPORT
    });

    reportingSubModules.push({
      sno: "12.5",
      view: "OTT Summary Report",
      formControlName: MODULE.PUSH_NOTIFICATION_REPORT
    });

    reportingSubModules.push({
      sno: "12.6",
      view: "Failed SMS Report",
      formControlName: MODULE.FAILED_SMS_REPORT
    });

    list.push({
      sno: "12",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: true,
      subModuleList: reportingSubModules
    });
    return list;
  }

  getPermissionsViewListForMKT() {
    let list = [];
    list.push({
      sno: "1",
      view: "Manage Campaign",
      formControlName: MODULE.CAMPAIGN,
      isSubModule: false
    });

    var campaignManagementSubModules = [];
    campaignManagementSubModules.push({
      sno: "1.1",
      view: "Approval Requests",
      formControlName: MODULE.CAMPAIGN_APPROVAL
    });

    list.push({
      sno: "1",
      view: "Manage Campaign",
      formControlName: MODULE.CAMPAIGN,
      isSubModule: true,
      subModuleList: campaignManagementSubModules
    });
    return list;
  }

  getPermissionsViewListAPI() {
    let list = [];

    list.push({
      sno: "1",
      view: "Dashboard",
      formControlName: MODULE.DASHBOARD,
      isSubModule: false
    });

    list.push({
      sno: "2",
      view: "Manage Sender CLI",
      formControlName: MODULE.SENDERID,
      isSubModule: false
    });

    list.push({
      sno: "3",
      view: "Manage Template",
      formControlName: MODULE.SMS_TEMPLATE,
      isSubModule: false
    });

    var smsTemplateManagementSubModules = [];

    /*smsTemplateManagementSubModules.push({
      sno: "3.1",
      view: "Bulk Upload",
      formControlName: MODULE.SMS_TEMPLATE_BULK_UPLOAD
    });

    list.push({
      sno: "3",
      view: "Manage Template",
      formControlName: MODULE.SMS_TEMPLATE,
      isSubModule: true,
      subModuleList: smsTemplateManagementSubModules
    });*/

    list.push({
      sno: "4",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: false
    });

    var reportingSubModules = [];

    reportingSubModules.push({
      sno: "4.1",
      view: "Summary Reports",
      formControlName: MODULE.SUMMARY_REPORT
    });

    reportingSubModules.push({
      sno: "4.2",
      view: "Detailed Report",
      formControlName: MODULE.DETAILED_REPORT
    });

    reportingSubModules.push({
      sno: "4.3",
      view: "MO Summary Reports",
      formControlName: MODULE.MO_SUMMARY_REPORT
    });

    reportingSubModules.push({
      sno: "4.4",
      view: "MO Detailed Report",
      formControlName: MODULE.MO_DETAILED_REPORT
    });

    reportingSubModules.push({
      sno: "4.5",
      view: "OTT Summary Report",
      formControlName: MODULE.PUSH_NOTIFICATION_REPORT
    });

    list.push({
      sno: "4",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: true,
      subModuleList: reportingSubModules
    });
    return list;
  }

  getPermissionsViewListHOD() {
    let list = [];

    list.push({
      sno: "1",
      view: "Dashboard",
      formControlName: MODULE.DASHBOARD,
      isSubModule: false
    });

    list.push({
      sno: "2",
      view: "Manage Campaign",
      formControlName: MODULE.CAMPAIGN,
      isSubModule: false
    });

    var campaignManagementSubModules = [];
    campaignManagementSubModules.push({
      sno: "2.1",
      view: "Approval Requests",
      formControlName: MODULE.CAMPAIGN_APPROVAL
    });

    list.push({
      sno: "2",
      view: "Manage Campaign",
      formControlName: MODULE.CAMPAIGN,
      isSubModule: true,
      subModuleList: campaignManagementSubModules
    });

    list.push({
      sno: "3",
      view: "Manage Sender CLI",
      formControlName: MODULE.SENDERID,
      isSubModule: false
    });

    list.push({
      sno: "4",
      view: "Manage Template",
      formControlName: MODULE.SMS_TEMPLATE,
      isSubModule: false
    });

    var smsTemplateManagementSubModules = [];

    smsTemplateManagementSubModules.push({
      sno: "4.1",
      view: "Approval Requests",
      formControlName: MODULE.SMS_TEMPLATE_APPROVAL
    });

    list.push({
      sno: "4",
      view: "Manage Template",
      formControlName: MODULE.SMS_TEMPLATE,
      isSubModule: true,
      subModuleList: smsTemplateManagementSubModules
    });

    /*list.push({
      sno: "5",
      view: "Manage Bulk Template",
      formControlName: MODULE.Bulk_TEMPLATE,
      isSubModule: false
    });

    var bulkTemplateManagementSubModules = [];

    bulkTemplateManagementSubModules.push({
      sno: "5.1",
      view: "Approval Requests",
      formControlName: MODULE.BULK_TEMPLATE_APPROVAL
    });

    list.push({
      sno: "5",
      view: "Manage Bulk Template",
      formControlName: MODULE.Bulk_TEMPLATE,
      isSubModule: true,
      subModuleList: bulkTemplateManagementSubModules
    });*/

    list.push({
      sno: "5",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: false
    });

    var reportingSubModules = [];

    reportingSubModules.push({
      sno: "5.1",
      view: "Summary Reports",
      formControlName: MODULE.SUMMARY_REPORT
    });

    reportingSubModules.push({
      sno: "5.2",
      view: "Detailed Report",
      formControlName: MODULE.DETAILED_REPORT
    });

    reportingSubModules.push({
      sno: "5.3",
      view: "MO Summary Reports",
      formControlName: MODULE.MO_SUMMARY_REPORT
    });

    reportingSubModules.push({
      sno: "5.4",
      view: "MO Detailed Report",
      formControlName: MODULE.MO_DETAILED_REPORT
    });

    reportingSubModules.push({
      sno: "5.5",
      view: "OTT Summary Report",
      formControlName: MODULE.PUSH_NOTIFICATION_REPORT
    });

    list.push({
      sno: "5",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: true,
      subModuleList: reportingSubModules
    });
    return list;
  }

  getPermissionsViewListDLT() {
    let list = [];
    list.push({
      sno: "1",
      view: "Manage Template",
      formControlName: MODULE.SMS_TEMPLATE,
      isSubModule: false
    });

    /*list.push({
      sno: "2",
      view: "Manage Bulk Template",
      formControlName: MODULE.Bulk_TEMPLATE,
      isSubModule: false
    });*/
    return list;
  }

  getRetryCountList() {
    return [{ id: 1, name: 1 }, { id: 2, name: 2 }, { id: 3, name: 3 }];
  }
}
