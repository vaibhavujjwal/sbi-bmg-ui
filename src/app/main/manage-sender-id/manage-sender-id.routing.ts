import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ManageSenderIdComponent } from "./manage-sender-id.component";
import { ListSenderIdComponent } from "./list-sender-id/list-sender-id.component";
import { CreateSenderIdComponent } from "./create-sender-id/create-sender-id.component";
import { PendingSenderIdComponent } from "./pending-sender-id/pending-sender-id.component";
import { UploadBulkSenderIdComponent } from "./upload-bulk-sender-id/upload-bulk-sender-id.component";
import { ModuleGuardService, AuthGuardService } from "src/app/guards";
import { MODULE } from "src/app/common/common.const";

export const routes: Routes = [
  {
    path: "",
    component: ManageSenderIdComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/manage-sender-id/list-sender-id",
        pathMatch: "full"
      },
      {
        path: "list-sender-id",
        component: ListSenderIdComponent,
        data: { moduleName: MODULE.SENDERID, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "create-sender-id",
        component: CreateSenderIdComponent,
        data: { moduleName: MODULE.SENDERID, permissions: ["RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "pending-sender-id",
        component: PendingSenderIdComponent,
        data: {
          moduleName: MODULE.SENDERID_APPROVAL,
          permissions: ["R", "RW"]
        },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "upload-bulk-sender-id",
        component: UploadBulkSenderIdComponent,
        data: {
          moduleName: MODULE.SENDERID_BULKUPLOAD,
          permissions: ["RW"]
        },
        canActivate: [AuthGuardService, ModuleGuardService]
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
