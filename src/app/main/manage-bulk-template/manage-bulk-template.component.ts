import {Component, OnInit } from '@angular/core';
import { MODULE } from 'src/app/common/common.const';
import { CommonService } from 'src/app/common/common.service';

@Component({
  selector: 'app-manage-bulk-template',
  templateUrl: './manage-bulk-template.component.html',
  styleUrls: ['./manage-bulk-template.component.scss']
})
export class ManageBulkTemplateComponent implements OnInit {
  MODULES = MODULE;
  constructor(public commonService: CommonService) {}

  ngOnInit() {
  }

}
