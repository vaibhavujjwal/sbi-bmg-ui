import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ReportingService } from '../reporting.service';
import { ExcelService } from "../../../common/excel.service";
import { CommonService } from "../../../common/common.service";
import * as moment from 'moment';
import { RESPONSE } from "../../../common/common.const";
import { MatTableDataSource, MatSort, MatPaginator } from '../../../../../node_modules/@angular/material';
import { DetailedReportResponse } from './detailed-response';

@Component({
  selector: 'app-detailed-report',
  templateUrl: './detailed-report.component.html',
  styleUrls: ['./detailed-report.component.scss']
})
export class DetailedReportComponent implements OnInit {

  reportingForm: FormGroup;
  formSubmitted: boolean = false;
  dateExist: boolean = false;
  reportList = [];
  searchText: string = "";
  pageLimit: number;
  p: number = 1;
  channelSorted: boolean = false;
  aggregatorNameSorted: boolean = false;
  campaignIDSorted: boolean = false;
  campaignNameSorted: boolean = false;
  departmentNameSorted: boolean = false;
  departmentUserSorted: boolean = false;
  operatorSorted: boolean = false;
  circleSorted: boolean = false;
  smsTypeSorted: boolean = false;
  smsContentSorted: boolean = false;
  submitIDSorted: boolean = false;
  prioritySorted: boolean = false;
  senderIdSorted: boolean = false;
  internationalSorted: boolean = false;
  scheduledOnSorted: boolean = false;
  submittedOnSorted: boolean = false;
  deliveredOnSorted: boolean = false;
  deliveryStatusSorted: boolean = false;
  msisdnSorted: boolean = false;
  sDate: any;
  then: any;
  endDte: any;
  edate: any;
  eDate: any;
  now = moment(new Date());
  minDateTo: any;
  maxDateTo = this.now.format('YYYY-MM-DD');
  maxDate = this.now.format('YYYY-MM-DD');
  reportType: string;
  formattedFromDate: any;
  formattedToDate: any;
  dataSource: MatTableDataSource<DetailedReportResponse>;
  dataSourceLength: number;
  @ViewChild('input') input: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['msisdn','dlt_TEMPLATE_ID', 'cli', 'submit_TIME', 'delivery_TIME', 'submit_STATE',
    'error_DESC', 'msg_TXT'];

  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService, private reportingService: ReportingService,
    private excelService: ExcelService, public commonService: CommonService) { }

  ngOnInit() {
    this.initializeForm();
    this.dataSourceLength=0; 
    this.reportType = 'B';
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.setEndDAte();
    this.maxDate = moment(new Date()).format('YYYY-MM-DD');
  }

  initializeForm() {
    this.reportingForm = this.fb.group({
      msdin: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(16), Validators.pattern(this.commonService.patterns.phone)]],
      startDate: [new Date().toISOString().substr(0, 10)],
      endDate: [new Date().toISOString().substr(0, 10)],
    });
  }

  submitForm() {
    this.formSubmitted = true;
    this.reportList = [];
    let fromDate = new Date(this.reportingForm.controls['startDate'].value);
    let toDate = new Date(this.reportingForm.controls['endDate'].value);
    this.formattedFromDate = moment(fromDate).format("YYYY-MM-DD");
    this.formattedToDate = moment(toDate).format("YYYY-MM-DD");
    this.reportingForm.controls['startDate'].setValue(this.formattedFromDate);
    this.reportingForm.controls['endDate'].setValue(this.formattedToDate);
    if (this.reportingForm.valid) {
      let req = {
        msdin: this.reportingForm.controls['msdin'].value,
        startDate: this.reportingForm.controls['startDate'].value,
        endDate: this.reportingForm.controls['endDate'].value
      };
      this.spinner.show();
      this.reportingService.getDetailReports(req).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.reportList = res['data']['reportResponseList'];
            console.log("Detailed Reports...." + JSON.stringify(this.reportList));

            if (this.reportList) {
              this.reportList.forEach(element => {
                if (element['submit_STATE'] == 504) {
                  element['submit_STATE'] = "Delivered";
                }
                else if (element['submit_STATE'] == 502) {
                  element['submit_STATE'] = "Delivery awaited";
                }else if (element['submit_STATE'] == 506 && element['error_DESC']==="MSG_DELIVERY_TIMEOUT") {
                  element['submit_STATE'] = "Pending";
                  element['error_DESC'] = "Message delivery awaited"
                }else {
                  element['submit_STATE'] = "Failed";
                }
                if (element['campaign_ID'] == 0) {
                  element['channel'] = "API";
                } else if (element['campaign_ID'] > 0) {
                  element['channel'] = "Campaign";
                } else if (element['campaign_ID'] == null) {
                  element['channel'] = null;
                }

                if (!element['aggregator']) {
                  element['aggregator'] = "NA"
                }
                if (element['in_FLAG'] == 0 || element['in_FLAG'] == null || element['in_FLAG'] == "") {
                  element['in_FLAG'] = "National";
                } else if (element['in_FLAG'] == 1) {
                  element['in_FLAG'] = "International";
                }
                if (element['category'] == 0) {
                  element['category'] = "NA";
                }
                else if (element['category'] == 1) {
                  element['category'] = "OTP";
                }
                else if (element['category'] == 2) {
                  element['category'] = "Transactional";
                }
                else if (element['category'] == 3) {
                  element['category'] = "Informational";
                }
                else if (element['category'] == 4) {
                  element['category'] = "Promotional";
                }
                else {
                  element['category'] = element['category'];
                }
                if (element['dcs'] == 0) {
                  element['dcs'] = "Text";
                } else if (element['dcs'] == 1 || element['dcs'] == 8) {
                  element['dcs'] = "Unicode";
                } else {
                  element['dcs'] = "Null";
                }
                if (element['dcs'] == "Unicode") {
                  try {
                    element['msg_TXT'] = this.commonService.unicodeToText(element['msg_TXT']);
                  }
                  catch (e) {
                    console.log("comes in catch block...");
                    element['msg_TXT'] = element['msg_TXT'];
                  }
                }
              });
            }
            this.dataSource = new MatTableDataSource(this.reportList);
            if (this.dataSource.data.length == 0) {
              this.dataSourceLength = 0;
            } else {
              this.dataSourceLength = this.dataSource.data.length;
            }
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.dataSource.paginator.firstPage();
          } else {
            this.reportList = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
    }
    else {
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length == 0) {
      this.dataSourceLength = 0;
    }
    else {
      this.dataSourceLength = this.dataSource.filteredData.length;
    }
  }

  setEndDAte() {
    this.sDate = this.reportingForm.controls['startDate'].value,
      this.then = moment(new Date(this.sDate));
    this.minDateTo = this.then.format('YYYY-MM-DD');
    var isDateBeforeCurrent = moment(this.then.add(1, 'M'), 'YYYY-MM-DD').isBefore(moment());
    moment(this.then.subtract(1, 'M'));
    if (isDateBeforeCurrent == false) {
      this.maxDateTo = moment(new Date()).format('YYYY-MM-DD');
      //this.reportingForm.controls['endDate'].setValue(this.maxDateTo);
    } else {
      this.maxDateTo = this.then.add(1, 'M').format('YYYY-MM-DD');
      //this.reportingForm.controls['endDate'].setValue(this.maxDateTo);
    }
    //   if(currentDate.date() != futureMonth.date() && futureMonth.isSame(futureMonthEnd.format('YYYY-MM-DD'))) {
    //     futureMonth = futureMonth.add(1, 'd');
    // }
    this.reportingForm.controls['endDate'].setValue(this.sDate);
  }


  restrictKeyword(event: Event) {
    event.preventDefault();
  }
  setStartDAte() {
    this.maxDate = moment(new Date()).format('YYYY-MM-DD');
  }


  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
    this.reportList = this.commonService.sortArray(isAssending, sortBy, this.reportList, isNumber);
  }

  exportAs(fileType: string) {
    let data = [];
    this.reportList.forEach(element => {
      let excelDataObject = {};
      if (element.campaign_ID == 0) {
        element.channel = "API";
      } else if (element.campaign_ID > 0) {
        element.channel = "Campaign";
      } else if (element.campaign_ID == null) {
        element.channel = null;
      }
      excelDataObject["msisdn"] = element.msisdn;
      if (this.reportType == 'D') {
        excelDataObject["Channel"] = element.channel;
        excelDataObject["Aggregator Name"] = element.aggregator;
        excelDataObject["Campaign ID"] = element.campaign_ID;
        excelDataObject["Campaign Name"] = element.campaign_NAME;
        excelDataObject["Department Name"] = element.dept_NAME;
        excelDataObject["Department Users"] = element.username;
        excelDataObject["Operator"] = element.operator;
        excelDataObject["Telecom Circle"] = element.circle;
      }
      excelDataObject["Sender CLI"] = element.cli;
      if (this.reportType == 'D') {
        excelDataObject["Priority"] = element.category;
        excelDataObject["National/ International"] = element.in_FLAG;
        excelDataObject["Scheduled On"] = new Date(element.scheduled_DATE).toLocaleString('en-GB').replace(",", "");
      }
      excelDataObject["Submitted On"] = new Date(element.submit_TIME).toLocaleString('en-GB').replace(",", "");
      excelDataObject["Delivered On"] = element.delivery_TIME ? new Date(element.delivery_TIME).toLocaleString('en-GB').replace(",", "") : "NA";
      excelDataObject["Delivered Status"] = element.submit_STATE;
      excelDataObject["Reason"] = element.error_DESC;
      if (this.reportType == 'D') {
        excelDataObject["SMS Type"] = element.dcs;
      }
      excelDataObject["SMS Content"] = element.msg_TXT;
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'Detailed-Report');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'Detailed-Report');
        break;
      default:
        this.excelService.exportAsCsvFile(data, 'Detailed-Report');
        break;
    }
  }

  getType(type: any) {
    this.reportType = type;
    this.spinner.show();
    if (this.reportType == 'D') {
      this.displayedColumns = [];
      this.displayedColumns = ['msisdn', 'channel', 'aggregator', 'campaign_ID', 'campaign_NAME', 'dept_NAME', 'username', 'operator',
        'circle', 'cli', 'category', 'in_FLAG', 'scheduled_DATE', 'submit_TIME', 'delivery_TIME', 'submit_STATE', 'error_DESC', 'dcs',
        'msg_TXT', 'amid'];
      this.spinner.hide();
    }
    else if (this.reportType == 'B') {
      this.displayedColumns = [];
      this.displayedColumns = ['msisdn', 'cli', 'submit_TIME', 'delivery_TIME', 'submit_STATE', 'error_DESC', 'msg_TXT'];
      this.spinner.hide();
    }
  }

  pdfResponse: string = '';
  showPdf() {
    this.formSubmitted = true;
    let fromDate = new Date(this.reportingForm.controls['startDate'].value);
    let toDate = new Date(this.reportingForm.controls['endDate'].value);
    this.formattedFromDate = moment(fromDate).format("YYYY-MM-DD");
    this.formattedToDate = moment(toDate).format("YYYY-MM-DD");
    this.reportingForm.controls['startDate'].setValue(this.formattedFromDate);
    this.reportingForm.controls['endDate'].setValue(this.formattedToDate);
    if (this.reportingForm.valid) {
      let req = {
        msdin: this.reportingForm.controls['msdin'].value,
        startDate: this.reportingForm.controls['startDate'].value,
        endDate: this.reportingForm.controls['endDate'].value
      };
      this.spinner.show();
      this.reportingService.generateDLRPDF(req).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.pdfResponse = res['data'];
            console.log("PDF API response........" + this.pdfResponse);
            let date = new Date();
            console.log("Date for PDF....:" + moment(date).format('YYYYMMDDHHMMSS'))
            const fileName = "SMS_DLR_REPORT_" + moment(date).format('YYYYMMDDHHMMSS') + ".pdf";
            this.downloadFile(this.pdfResponse, fileName);
            // const linkSource = this.pdfResponse;
            // const downloadLink = document.createElement("a");
            // let date = new Date();
            // console.log("Date for PDF name" + moment(date).format('YYYYMMDDHHMMSS'))
            // const fileName = "SMS_DLR_REPORT_" + moment(date).format('YYYYMMDDHHMMSS') + ".pdf";
            // downloadLink.href = linkSource;
            // downloadLink.download = fileName;
            // downloadLink.click();
          } else {
            this.pdfResponse = '';
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });

    }
  }
  downloadFile(pdfBase64, fileName) {
    var res = pdfBase64.substr(28, (pdfBase64.length - 28));
    pdfBase64 = res;
    console.log("substr..." + pdfBase64);
    // IE doesn't allow using a blob object directly as link href
    // instead it is necessary to use msSaveOrOpenBlob
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      var byteCharacters = atob(pdfBase64);
      var byteNumbers = new Array(byteCharacters.length);
      for (var i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      var blob = new Blob([byteArray], { type: 'application/pdf' });
      window.navigator.msSaveOrOpenBlob(blob, fileName);
      return;
    }

    // For Other Browsers
    const linkSource = 'data:application/pdf;base64,' + pdfBase64;
    const link = document.createElement('a');
    link.href = linkSource;
    link.download = fileName;
    document.body.appendChild(link);
    link.click();
    link.remove();

    window.addEventListener("focus", function () {
      URL.revokeObjectURL(link.href);
    }, { once: true });
  }
}
