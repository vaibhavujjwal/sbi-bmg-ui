import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ManageCampaignService } from '../manage-campaign.service';
import { ExcelService } from "../../../common/excel.service";
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { MODULE } from "../../../common/common.const";
import * as moment from 'moment';
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';
import { MatTableDataSource, MatPaginator, MatSort } from '../../../../../node_modules/@angular/material';
import { ResponseView } from '../ResponseView';
@Component({
  selector: 'app-view-scheduled-campaign',
  templateUrl: './view-scheduled-campaign.component.html',
  styleUrls: ['./view-scheduled-campaign.component.scss']
})
export class ViewScheduledCampaignComponent implements OnInit {
  MODULES = MODULE;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  campaignForm: FormGroup;
  response: ResponseView
  formSubmitted: boolean = false;
  campaignList = [];
  searchText: string = "";
  pageLimit: number;
  dataSourceLength: number;
  // p: number = 1;
  // campIdSorted: boolean = false;
  // campNameSorted: boolean = false;
  // campCatgSorted: boolean = false;
  // messageCountSorted: boolean = false;
  // messageCountUserSorted: boolean = false;
  // totalRecordCountSorted: boolean = false;
  // statusSorted: boolean = false;
  // scheduleTimeSorted: boolean = false;
  // createdTimeSorted: boolean = false;
  // messageTypeSorted: boolean = false;
  // messageTextSorted: boolean = false;
  // senderIdSorted: boolean = false;
  // typeFlagIdSorted: boolean = false;
  // totalRecordCountSorted1: boolean = false;
  // successfulRecordCountSorted: boolean = false;
  // failedRecordCountSorted: boolean = false;
  // expiredRecordCountSorted: boolean = false;
  // retriedRecordCountSorted: boolean = false;
  // deliveryExpiredCountSorted: boolean = false;
  // invalidRecordCountSorted: boolean = false;
  unicodeToTextValue: any = "";
  sDate: any;
  then: any;
  endDte: any;
  edate: any;
  eDate: any;
  now = moment(new Date());
  minDate: any;
  maxDate = this.now.format('YYYY-MM-DD');
  maxDate1 = this.now.format('YYYY-MM-DD');
  role: string;
  formattedFromDate: any;
  formattedToDate: any;
  dataSource: MatTableDataSource<ResponseView>;
  // isDataAvail:boolean=false;
  @ViewChild('input') input: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['templateIdDlt','id', 'campaignName', 'category', 'statusDesc', 'scheduleTime', 'createdOn',
    'messageType', 'messageText', 'senderId', 'typeFlag','remarks', 'totalRecordCount', 'messageCountPerUser', 'messageCount'];

  constructor(private modalService: NgbModal, private fb: FormBuilder, private spinner: NgxSpinnerService, private manageCampaignService: ManageCampaignService,
    private excelService: ExcelService, public commonService: CommonService) { }

  ngOnInit() {
    this.role = this.commonService.getUserByRole();
    if (this.commonService.hasWriteAccess(this.MODULES.CAMPAIGN) == true) {
      this.displayedColumns[0] = "actions";
      if(this.role=="ROLE_CAMPAIGN"){
        this.displayedColumns = ['actions','templateIdDlt','id', 'campaignName', 'category', 'statusDesc', 'scheduleTime', 'createdOn',
        'messageType', 'messageText', 'senderId', 'typeFlag','remarks', 'totalRecordCount', 'messageCountPerUser', 'messageCount'];
      }else if(this.role=="ROLE_ADMIN"){
        this.displayedColumns = ['actions','templateIdDlt','id', 'campaignName','pfid', 'category', 'statusDesc', 'scheduleTime', 'createdOn',
        'messageType', 'messageText', 'senderId', 'typeFlag','remarks', 'totalRecordCount', 'messageCountPerUser', 'messageCount'];
      }
    }else if(this.role=="ROLE_ADMIN" && !this.commonService.hasWriteAccess(this.MODULES.CAMPAIGN)){
      this.displayedColumns = ['templateIdDlt','id', 'campaignName','pfid', 'category', 'statusDesc', 'scheduleTime', 'createdOn',
        'messageType', 'messageText', 'senderId', 'typeFlag','remarks', 'totalRecordCount', 'messageCountPerUser', 'messageCount'];
    }
    this.initializeForm();
    this.pageLimit = this.commonService.recordsPerPage[0];


    this.submitForm();
    this.setEndDAte();
  }

  initializeForm() {
    this.campaignForm = this.fb.group({
      startDate: [new Date().toISOString().substr(0, 10)],
      endDate: [new Date().toISOString().substr(0, 10)]
    });
  }

  submitForm() {
    let roleId;
    let type;
    this.formSubmitted = true;
    roleId = this.commonService.getUserRoleId();
    if (roleId == 5) {
      type = 2;
    } else if (roleId == 3) {
      type = 3;
    } else if (roleId == 2) {
      type = 1;
    } else if (roleId == 6) {
      type = 6;
    }

    let fromDate = new Date(this.campaignForm.controls['startDate'].value);
    let toDate = new Date(this.campaignForm.controls['endDate'].value);

    this.formattedFromDate = moment(fromDate).format("YYYY-MM-DD");
    this.formattedToDate = moment(toDate).format("YYYY-MM-DD");

    this.campaignForm.controls['startDate'].setValue(this.formattedFromDate);
    this.campaignForm.controls['endDate'].setValue(this.formattedToDate);

    if (this.campaignForm.valid) {
      let req = {
        "startDate": this.campaignForm.controls['startDate'].value,
        "endDate": this.campaignForm.controls['endDate'].value,
        "type": type
      };
      this.spinner.show();
      this.manageCampaignService.getCampaignList(req).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.campaignList = res['data'];
            this.campaignList.forEach(element => {
              if (element['messageType'] == 2) {
                try {
                  element['messageText'] = this.commonService.unicodeToText(element['messageText']);
                }
                catch (err) {
                  element['messageText'] = element['messageText'];
                  console.log("error while coverting unicode ........" + err);
                }
              }

              if (element['messageType'] == 2) {
                element['messageType'] = "Unicode";
              } else if (element['messageType'] == 1) {
                element['messageType'] = "Text";
              }
              if (element['typeFlag'] == 'D') {
                element['typeFlag'] = "National";
              } else {
                element['typeFlag'] = "International";
              }
            });

            this.dataSource = new MatTableDataSource(this.campaignList);
            console.log("Datasource length...." + this.dataSource.data.length);
            if (this.dataSource.data.length == 0) {
              this.dataSourceLength = 0;
            } else {
              this.dataSourceLength = this.dataSource.data.length;
            }
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
          } else {
            this.campaignList = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
          // if (this.campaignList.length > 0) {
          //   this.isDataAvail = true;
          // }
          // else{
          //   this.isDataAvail=false;
          // }
        });
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length == 0) {
      this.dataSourceLength = 0;
    }
    else {
      this.dataSourceLength = this.dataSource.filteredData.length;
    }
  }
  getUnicodeToTextConversion(unicode: any) {
    this.unicodeToTextValue = this.commonService.unicodeToText(unicode);
  }

  setEndDAte() {
    this.sDate = this.campaignForm.controls['startDate'].value,
      this.then = moment(new Date(this.sDate));
    this.minDate = this.then.format('YYYY-MM-DD');
    this.campaignForm.controls['endDate'].setValue(this.sDate);
  }

  setStartDAte() {
    this.maxDate = moment(new Date()).format('YYYY-MM-DD');
  }

  confirmDeleteRecord(record: any) {
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    modalRef.componentInstance.content = 'Are you sure you want to delete this campaign?';
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.deleteRecord(record);
        }
      },
      (reason: any) => {
      });
  }

  restrictKeyword(event: Event) {
    event.preventDefault();
  }

  deleteRecord(record: any) {
    let req = {
      "campaignId": record['id'],
      "remarks": "Record Deleted"
    };
    this.spinner.show();
    this.manageCampaignService.deleteCampaign(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.commonService.showSuccessToast("delete");
          this.submitForm();
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  confirmCancelCampaign(record: any) {
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    modalRef.componentInstance.content = 'Are you sure you want to cancel this campaign?';
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.cancelCampaign(record);
        }
      },
      (reason: any) => {
      });
  }

  cancelCampaign(record: any) {
    let req = {
      "campaignId": record['id'],
      "remarks": "Cancel Campaign",
      "campaignStatus": 1
    };
    this.spinner.show();
    this.manageCampaignService.cancelCampaign(req).subscribe(
      res => {
        this.spinner.hide();
        this.commonService.showSuccessToast("Campaign is successfully cancelled");
      });
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
    this.campaignList = this.commonService.sortArray(isAssending, sortBy, this.campaignList, isNumber);
  }

  exportAs(fileType: string) {
    let data = [];
    this.campaignList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Channel"] = new Date(element.date).toLocaleDateString();
      excelDataObject["Campaign ID"] = element.id;
      excelDataObject["Status"] = element.statusDesc;
      excelDataObject["Scheduled On"] = new Date(element.scheduleTime).toLocaleDateString();
      excelDataObject["Submittted On"] = new Date(element.createdOn).toLocaleDateString();
      excelDataObject["SMS Type"] = element.messageType;
      excelDataObject["SMS Content"] = element.messageText;
      excelDataObject["Sender CLI"] = element.senderId;
      excelDataObject["Total SMS"] = element.totalRecordCount;
      excelDataObject["Successful SMS"] = element.successfulRecordCount;
      excelDataObject["Failed SMS"] = element.failedRecordCount;
      excelDataObject["Expired SMS"] = element.expiredRecordCount;
      excelDataObject["SMS Re-Tried"] = element.retriedRecordCount;
      excelDataObject["Delivery Expired"] = element.deliveryExpiredCount;
      excelDataObject["Invalid SMS"] = element.invalidRecordCount;
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'Detailed-Report');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'Detailed-Report');
        break;
      default:
        this.excelService.exportAsCsvFile(data, 'Detailed-Report');
        break;
    }
  }

  getFile(id: number, name: string) {
    let req = {
      campaignId: id,
      type: 3
      // type: status
    }
    this.spinner.show();
    this.manageCampaignService.fileDownload(req).subscribe(
      res => {
        this.spinner.hide();
        if (res) {
          this.commonService.downloadFile(res, req.campaignId + "_Terminated", "application/csv", ".csv")
        } else {
          this.commonService.showErrorToast("No data found");
        }
      });
  }
}