import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewBulkTemplateComponent } from './view-bulk-template/view-bulk-template.component';
import { CreateBulkTemplateComponent } from './create-bulk-template/create-bulk-template.component';
import { PendingBulkTemplateComponent } from './pending-bulk-template/pending-bulk-template.component';
import { routing } from "./manage-bulk-template.route";
import { ManageBulkTemplateComponent } from "./manage-bulk-template.component";
import { FormsModule } from "@angular/forms";
import { SharedModule } from "../../common/shared.modules";
import { EscapeHtmlPipe } from '../../validators/pipes/keep-html.pipe';
import {MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule} from '@angular/material';

import { SafePipe} from '../../safe.pipe';

import { ManageBulkTemplateService } from "./manage-bulk-template.service";

@NgModule({
  imports: [
    CommonModule,
    routing,
    FormsModule,
    SharedModule,MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
    MatSortModule, MatTableModule

  ],
  declarations: [ManageBulkTemplateComponent, ViewBulkTemplateComponent,
    CreateBulkTemplateComponent, PendingBulkTemplateComponent, EscapeHtmlPipe,SafePipe],
  exports: [EscapeHtmlPipe],
  providers: [ManageBulkTemplateService]
})
export class ManageBulkTemplateModule { }
