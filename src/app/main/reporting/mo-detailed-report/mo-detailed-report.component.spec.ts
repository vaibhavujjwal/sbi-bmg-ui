import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoDetailedReportComponent } from './mo-detailed-report.component';

describe('MoDetailedReportComponent', () => {
  let component: MoDetailedReportComponent;
  let fixture: ComponentFixture<MoDetailedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoDetailedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoDetailedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
