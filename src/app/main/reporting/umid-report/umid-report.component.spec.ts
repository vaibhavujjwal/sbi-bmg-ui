import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmidReportComponent } from './umid-report.component';

describe('UmidReportComponent', () => {
  let component: UmidReportComponent;
  let fixture: ComponentFixture<UmidReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmidReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmidReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
