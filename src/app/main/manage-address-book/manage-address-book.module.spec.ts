import { ManageAddressBookModule } from './manage-address-book.module';

describe('ManageAddressBookModule', () => {
  let manageAddressBookModule: ManageAddressBookModule;

  beforeEach(() => {
    manageAddressBookModule = new ManageAddressBookModule();
  });

  it('should create an instance', () => {
    expect(manageAddressBookModule).toBeTruthy();
  });
});
