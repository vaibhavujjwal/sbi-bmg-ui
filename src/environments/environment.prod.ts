export const environment = {
  production: true,
  backendApiURL: "https://bulksmshub.sbi.co.in:8443/sbi/config/v1/",
  backendApiURLMG: "https://bulksmshub.sbi.co.in:8443/sbi/config/v1/",
  backendLogin: "https://bulksmshub.sbi.co.in:8443/",
  backendApiURLNonEncrytion: "https://bulksmshub.sbi.co.in:8443/sbi/config/v2/",
};
