import { Component, OnInit, ViewChild } from '@angular/core';
import { ManageBulkTemplateService } from "../manage-bulk-template.service";
import { NgxSpinnerService } from "ngx-spinner";
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { NgbModal, NgbModalOptions, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { CommonService } from "../../../common/common.service";
import { RESPONSE, MODULE } from "../../../common/common.const";
import { ExcelService } from "../../../common/excel.service";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

export interface templateListTypes {
  Actions: any;
  name: string;
  contentType: any;
  content: any;
  username: string;
  creationDate: any;
  approvedBy: any;
  status: any;
}

@Component({
  selector: 'app-pending-bulk-template',
  templateUrl: './pending-bulk-template.component.html',
  styleUrls: ['./pending-bulk-template.component.scss']
})

export class PendingBulkTemplateComponent implements OnInit {

  MODULES = MODULE;
  dltApproveForm: FormGroup;
  largeModalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false,
    size: "lg"
  };
  modalRef: any;
  templateList = [];
  approvalReq = {};
  formSubmitted: any;
  dltIdFlag: boolean;
  math = Math;
  templateID: any;
  changeStatusCamp: boolean = false;
  showError: boolean = false;
  userDetails: object;
  smsTemplateForm: FormGroup;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  pageLimit: number;
  searchText: string = "";
  p: number = 1;
  // unicodeToTextValue : any ="";
  closeResult: string;
  statusForm: FormGroup;
  updateUserStatusEvent: string;

  displayedColumns: any[] = ['actions',
    'spiceTemplateid',
    'templateIdDlt',
    'name',
    'templateType',
    'contentType',
    'content',
    'username',
    'creationDate',
    'approvedBy',
    'status'];

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private manageBulkTemplateService: ManageBulkTemplateService, private spinner: NgxSpinnerService,
    private modalService: NgbModal, private fb: FormBuilder, private formBuilder: FormBuilder, public commonService: CommonService, private excelService: ExcelService) { }

  ngOnInit() {
    this.getTemplateList();
    this.initialiseForms();
    this.pageLimit = this.commonService.recordsPerPage[0];
  }

  initialiseForms() {
    this.smsTemplateForm = this.fb.group({
      contentType: ['', [Validators.required]],
      name: ['', [Validators.required]],
      language: [''],
      maskFlag: ['', [Validators.required]],
      maskText: [''],
      content: ['', [Validators.required]],
      user: ['', [Validators.required]],
      requestId: ['', [Validators.required]],
      placeHolder: ['']
    });
  }

  getTemplateList() {
    let req = {
      "id": 0,
      "channelId": this.commonService.getChannelId(),
      "userRole": this.commonService.getUserByRole()
    }
    this.spinner.show();
    this.manageBulkTemplateService.getTemplateListPending(req).subscribe(
      res => {
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.templateList = res["data"];

          this.templateList.forEach(element => {
            if (element['contentType'] == '2') {
              try {
                element['content'] = this.commonService.unicodeToText(element['content']);
              }
              catch (err) {
                element['content'] = element['content'];
                console.log("error while coverting unicode ........" + err);
              }
            }
          });
          this.templateList.forEach(element => {
            element['contentType'] == 1 ? element['contentType'] = "Text" : element['contentType'] = "Unicode";

          });
          this.templateList.forEach(element => {
            if (!element['templateIdDlt'] || element['templateIdDlt']==0) {
              element['templateIdDlt'] = 'NA';
            }
          });

          this.templateList.forEach(element => {
            if (element['templateType']=='inf') {
              element['templateType'] = 'Informational';
            }else if (element['templateType']=='otp') {
              element['templateType'] = 'OTP';
            }else if (element['templateType']=='promo') {
              element['templateType'] = 'Promotional';
            }else if (element['templateType']=='txn') {
              element['templateType'] = 'Transactional';
            }
          });
        
          this.dataSource = new MatTableDataSource(this.templateList);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.spinner.hide();

        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.templateList = [];
        }
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.showError = false;
    if (this.dataSource.filteredData.length == 0) {
      this.showError = true;
    }
  }

  confirmDeleteRecord(record: any) {
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    modalRef.componentInstance.content = 'Are you sure you want to permanently delete this ' + record['templateName'] + ' template?';
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.componentInstance.remarks = true;
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.deleteRecord(record);
        }
      },
      (reason: any) => {
      });
  }

  updateRecord(updatemodal: any, record: any) {
    this.populateValues(record);
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  populateValues(item) {
    if (item.language == '1') {
      this.smsTemplateForm.controls['contentType'].setValue('1');
    } else {
      this.smsTemplateForm.controls['contentType'].setValue('2');
    }
    this.smsTemplateForm.controls['name'].setValue(item.templateName);
    this.smsTemplateForm.controls['maskFlag'].setValue(item.maskFlag);
    this.smsTemplateForm.controls['maskText'].setValue(item.maskText);
    this.smsTemplateForm.controls['content'].setValue(item.template);
    this.templateID = item.id;
  }

  updateCampStatus() {
    if(this.commonService.getUserByRole()=='ROLE_DLT' && this.dltApproveForm.invalid){
      Object.keys(this.dltApproveForm.controls).forEach((key,index)=>{
        console.log(key)
        if(this.dltApproveForm.controls[key].invalid){
          this.dltApproveForm.controls[key].markAsTouched();
        }
      })
      return;
    }
    if (this.updateUserStatusEvent == "Approve") {
      this.statusForm.controls.remarks.setValue("approved");
    }
    this.approvalReq['remarks'] = this.statusForm.controls.remarks.value;
    this.approvalReq['userID'] = this.commonService.getUser();
    this.approvalReq['approvedBy'] = this.commonService.getUser();
    this.approvalReq['dltTemplateId'] =  this.dltApproveForm && this.dltApproveForm.controls['dltTemplateId']?this.dltApproveForm.controls['dltTemplateId'].value:'';
    this.approvalReq['dltTelemarketer'] = this.dltApproveForm && this.dltApproveForm.controls['telemarketer']?this.dltApproveForm.controls['telemarketer'].value:'';
    this.approvalReq['dltTemplateName'] = this.dltApproveForm && this.dltApproveForm.controls['dltTemplateName']?this.dltApproveForm.controls['dltTemplateName'].value:'';
    this.approvalReq['dltType'] = this.dltApproveForm && this.dltApproveForm.controls['type']?this.dltApproveForm.controls['type'].value:'';
    this.approvalReq['dltHeader'] = this.dltApproveForm && this.dltApproveForm.controls['header']?this.dltApproveForm.controls['header'].value:'';
    this.approvalReq['dltCategory'] = this.dltApproveForm && this.dltApproveForm.controls['category']?this.dltApproveForm.controls['category'].value:'';
    this.approvalReq['registeredDlt'] =  this.dltApproveForm && this.dltApproveForm.controls['registeredDLT']?this.dltApproveForm.controls['registeredDLT'].value:'';
    this.approvalReq['dltRequestedOn'] = null;
    this.approvalReq['dltStatusDate'] = new Date();//"2021-01-20";
    this.approvalReq['dltApprovalStatus'] = this.dltApproveForm && this.dltApproveForm.controls['approvalStatus']?this.dltApproveForm.controls['approvalStatus'].value:'';
    this.approvalReq['dltStatus'] = this.dltApproveForm && this.dltApproveForm.controls['status']?this.dltApproveForm.controls['status'].value:'';
    this.approvalReq['dltConsentType'] = this.dltApproveForm && this.dltApproveForm.controls['consentType']?this.dltApproveForm.controls['consentType'].value:'';
    this.approvalReq['dltRejectionReason'] = this.dltApproveForm && this.dltApproveForm.controls['rejectionReason']?this.dltApproveForm.controls['rejectionReason'].value:''
    this.changeStatusCamp = true;
    if (this.statusForm.valid) {
      this.spinner.show();
      let approvalAPI = '';
      if(this.commonService.getUserByRole()=="ROLE_DLT"){
        if(this.approvalReq['remarks']=='approved'){
          this.approvalReq['status'] = 0;
        }//approvalAPI = 'updateDltBUlkTemplate';
         approvalAPI = 'updateDltTemplate';
      }else{//approvalAPI = 'updateSattus';
        approvalAPI = 'updateHodTemplate';
      }
      this.manageBulkTemplateService[approvalAPI](this.approvalReq)
        .subscribe(res => {
          this.spinner.hide();
          this.modalRef.close();
          if (this.updateUserStatusEvent == "Approve") {
            let userMessage = this.commonService.getUserByRole()=="ROLE_DLT"?" has been sent to HOD for approval":"template has been approved";
            this.commonService.showSuccessToast(this.approvalReq['templateNme'] + userMessage);
          } else {
            this.commonService.showSuccessToast(this.approvalReq['templateNme'] + " template has been rejected");
          }
          this.getTemplateList();
        },
          err => {
            this.commonService.showErrorToast(err['error']['userMsg']);
          });
    }
  }

  confirmStatusRecord(record: any) {
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    if (record['status'] == 2) {
      modalRef.componentInstance.content = 'Are you sure you want to enable ' + record['templateName'] + ' template?';
    } else if (record['status'] == 1) {
      modalRef.componentInstance.content = 'Are you sure you want to disable ' + record['templateName'] + ' template?';
    }
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.changeStatus(record);
        } else {
          this.getTemplateList();
        }
      },
      (reason: any) => {

      });
  }

  confirmUpdateRecoed() {
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    modalRef.componentInstance.content = 'Are you sure you want to update this template?';
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.updateApiTemplateForm();
        }
      },
      (reason: any) => {
      });
  }

  updateApiTemplateForm() {
    let reqObj = {
      "smsContentType": this.smsTemplateForm.controls["contentType"].value,
      "templateName": this.smsTemplateForm.controls["name"].value,
      "maskFlag": this.smsTemplateForm.controls["maskFlag"].value,
      "maskText": this.smsTemplateForm.controls["maskText"].value,
      "templateContent": this.smsTemplateForm.controls["content"].value,
      "templateID": this.templateID
    }
    this.spinner.show();
    this.manageBulkTemplateService.updateAPITemplate(reqObj).subscribe(
      res => {
        this.commonService.showSuccessToast(reqObj['templateName'] + ' template has been updated');
        this.formSubmitted = false;
        this.modalRef.close();
        this.getTemplateList();
      });
  }

  deleteRecord(record: any) {
    let req = {
      "templateId": record['id'],
      "remarks": "delete template",
      "templateNme": record['templateName'],
      "loginId": this.commonService.getUser(),
    };
    this.spinner.show();
    this.manageBulkTemplateService.deleteTemplate(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.commonService.showSuccessToast(record['templateName'] + ' template has been deleted');
          this.getTemplateList();
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  prepareApprovalForm() {
    this.statusForm = this.formBuilder.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)
        ]
      ]
    });
  }

  prepareDltForm(){
    this.dltApproveForm = this.fb.group({
      dltTemplateId: ['', [Validators.required]],
      telemarketer: ['', [Validators.required]],
      dltTemplateName: ['', [Validators.required]],
      type: ['', [Validators.required]],
      header: ['', [Validators.required]],
      category : ['', [Validators.required]],
      registeredDLT : ['', [Validators.required]],
      requestedOn : ['', [Validators.required]],
      statusDate : ['', [Validators.required]],
      approvalStatus : ['', [Validators.required]],
      status : ['', [Validators.required]],
      consentType : ['', [Validators.required]],
      rejectionReason : ['', [Validators.required]]
    });
  }

  approvalStatus(user, content, event, btn) {
    let getuserRole = this.commonService.getUserByRole();
    if(getuserRole=='ROLE_DLT'){
      this.prepareDltForm();
      this.dltApproveForm.controls['approvalStatus'].setValue(event);
    }
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();
      var statusArpproveCode = getuserRole =="ROLE_DLT"?10:1;
   /* this.approvalReq = {
      "id": user['id'],
      "approvedBy": this.commonService.getUser(),
      "templateNme": user['name'],
      "status": event == 'Approve' || this.commonService.getUserByRole()=="ROLE_DLT"? statusArpproveCode : 7,
      "dltId": user["templateIdDlt"]
    };*/
    let userIDlist = [];
    userIDlist.push(user['id']);
    this.approvalReq = {
        "templateId": userIDlist, //result,
        "status": event == 'Approve' || this.commonService.getUserByRole()=="ROLE_DLT"? statusArpproveCode : 7,
        "dltTemplateMessage":user['content'],
        "templateNme": user['name'],
        "fileId":0
    };
    this.prepareApprovalForm();
    this.changeStatusCamp = false;
    this.userDetails = user;
    this.modalRef = this.modalService.open(content, this.largeModalOptions);
    this.updateUserStatusEvent = event;
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  changeStatus(record: any) {
    let req = {
      "id": record['id'],
      "approvedBy": record['approvedBy'],
      "status": record['status'] == 1 ? 2 : 1
    };
    this.spinner.show();
    this.manageBulkTemplateService.updateSattus(req).subscribe(
      res => {
        this.spinner.hide();
        if (req['status'] == 1) {
          this.commonService.showSuccessToast(record['templateName'] + ' template enabled');
        } else {
          this.commonService.showSuccessToast(record['templateName'] + ' template disabled');
        }
        this.getTemplateList();
      });
  }

  exportAs(fileType: string) {
    let data = [];
    this.templateList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Template Name"] = element.templateName;
      excelDataObject["Template Content Type"] = element.templateContentType;
      excelDataObject["Template  Text"] = element.template;
      excelDataObject["Assigned To"] = element.username;
      excelDataObject["Created By"] = element.createdBy;
      excelDataObject["Approved By"] = element.approvedBy;
      excelDataObject["Status"] = element.status == 1 ? 'Enable' : 'Disable';
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'SMS-Template-List');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'SMS-Template-List');
        break;
      default:
        this.excelService.exportAsExcelFile(data, 'SMS-Template-List');
        break;
    }
  }
}