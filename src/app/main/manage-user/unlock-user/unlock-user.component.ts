import { Component, OnInit } from '@angular/core';
import { RESPONSE, MODULE } from '../../../common/common.const';
import { CommonService } from '../../../common/common.service';
import { ManageUserService } from '../manage-user.service';
import { NgxSpinnerService } from '../../../../../node_modules/ngx-spinner';
import { NgbModal, NgbModalOptions, NgbModalRef, ModalDismissReasons } from '../../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { MatTableDataSource, MatPaginator, MatSort } from '../../../../../node_modules/@angular/material';
export interface unlockResponse{
        name: string
}

@Component({
  selector: 'app-unlock-user',
  templateUrl: './unlock-user.component.html',
  styleUrls: ['./unlock-user.component.scss'],
  providers: [ManageUserService]
})
export class UnlockUserComponent implements OnInit {
  displayedColumns: string[] = ['action', 'pfid'];
  dataSource : MatTableDataSource<unlockResponse>;;
  userList: any;
  MODULES = MODULE;
  userDetails: object;
  searchText: string;
  checkornot: boolean;
  dataSourceLength: number;
  index: number;
  userListReq:Array<string> = [];
  private modalRef: NgbModalRef;
  modalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  constructor(public commonService: CommonService,
    private manageUserService: ManageUserService,
    private spinner: NgxSpinnerService, private modalService: NgbModal) { }

  ngOnInit() {
    this.lockedUserList();
  }
  lockedUserList() {
    this.spinner.show();
    this.manageUserService.getLockUserList().subscribe(data => {
      this.spinner.show();
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        let userL = data["data"];
        let lockList = [];
        this.userDetails = {};
        for (var x = 0; x < userL.length; x++) {
          this.userDetails = {
            "name": userL[x]
          }
          lockList.push(this.userDetails)
        }
        this.dataSource = new MatTableDataSource(lockList);
        if(lockList.length==0){
          this.dataSourceLength=0;
        }
        this.spinner.hide();
        console.log(lockList);
      }else{
        this.spinner.hide();
        this.commonService.showErrorToast(data['result']['userMsg']);
      }
    });
  }

  unlockUser(element, index, content) {
    this.userDetails = element;
    this.index = index;
    this.modalRef = this.modalService.open(content, this.modalOptions);
  }
  unlockFunctionCall(flag) {
     if (flag == 'Y') {
      this.callUnlockFunction(this.userDetails);
    }
    if(flag == 'N'){
      document.getElementById(this.index+"")['checked']=false;
    }
    this.modalRef.close();
  }

  callUnlockFunction(unlockName) {
    let loginUserName = this.commonService.getUserName();
    this.spinner.show();
    this.userListReq.push(unlockName.name);
    let req = {
      "userList": this.userListReq,
      "unlockedBy":loginUserName
    }
    console.log(req);
    this.manageUserService.unlockUser(req).subscribe(data => {
      this.spinner.hide();
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        let userL = data["data"];
        document.getElementById(this.index+"")['disabled']=true;
        this.commonService.showSuccessToast(unlockName.name + " unlocked successfully");
        this.lockedUserList();
      }else{
        document.getElementById(this.index+"")['checked']=false;
        this.commonService.showErrorToast(data['result']['userMsg']);
      }
      this.spinner.hide();
    });
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length == 0) {
      this.dataSourceLength = 0;
    }
    else {
      this.dataSourceLength = this.dataSource.filteredData.length;
    }
  }
}