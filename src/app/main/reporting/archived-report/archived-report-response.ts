export interface ArchivedReportResponse {
    date: string,
    keyword: string,
    department: string,
    shortLongCode: string,
    countTotal: number,
    countSuccess: number,
    countFailed: number,
    countInactive: number,
    countUnmatched: number
}