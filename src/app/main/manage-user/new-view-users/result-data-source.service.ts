import { Injectable } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { Result } from './result';
import { BehaviorSubject, Observable } from 'rxjs';
import { ManageUserService } from '../manage-user.service';
import { catchError, finalize } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { CollectionViewer } from '@angular/cdk/collections';
import { NewViewUsersComponent } from './new-view-users.component';
import { RESPONSE } from '../../../common/common.const';
import { NgxSpinnerService } from '../../../../../node_modules/ngx-spinner';

@Injectable()
export class ResultDataSourceService implements DataSource<Result> {
    public lessonsSubject = new BehaviorSubject<Result[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    private countSubject = new BehaviorSubject<Number>(0);
    public loading$ = this.loadingSubject.asObservable();
    public count$ = this.countSubject.asObservable();
    response: any;
    constructor(private user_service: ManageUserService,
        private spinner: NgxSpinnerService,
    ) {
    }

    loadUsers(data) {
        this.loadingSubject.next(true);
        this.spinner.show();
        this.user_service.getUsersListOnFly(data).pipe(
            catchError(() => of([])),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe((res) => {
            if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
                this.response = res["data"]["userDataList"];
                console.log("Response for dataSource...." + JSON.stringify(this.response));
                this.countSubject.next(res["data"]["totalRecords"]);
                this.lessonsSubject.next(res["data"]["userDataList"]);
                res["data"]["userDataList"].forEach(element => {
                    if (element["hod"])
                        element["hod"] = element["hod"].substring(1, element["hod"].length - 1);
                    if (element["finalApprover"])
                        element["finalApprover"] = element["finalApprover"].substring(1, element["finalApprover"].length - 1);
                });
            }
            else {
            }
            this.spinner.hide();
        });
    }

    getUser(data) {
        this.loadingSubject.next(true);
        this.spinner.show();
        this.user_service.getSearchedRecord(data).pipe(
            catchError(() => of([])),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe((res) => {
            if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
                this.countSubject.next(res["data"]["totalRecords"]);
                this.lessonsSubject.next(res["data"]["userDataList"]);
                res["data"]["userDataList"].forEach(element => {
                    if (element["hod"])
                        element["hod"] = element["hod"].substring(1, element["hod"].length - 1);
                    if (element["finalApprover"])
                        element["finalApprover"] = element["finalApprover"].substring(1, element["finalApprover"].length - 1);
                })
            }
            else {
                }
            this.spinner.hide();
        });
    }

    connect(collectionViewer: CollectionViewer): Observable<Result[]> {
        return this.lessonsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.lessonsSubject.complete();
        this.loadingSubject.complete();
        this.countSubject.complete();
    }
}