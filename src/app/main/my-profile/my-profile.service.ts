import { Injectable } from "@angular/core";
import { NetworkService } from '../../common/network.service';
import { CommonService } from "../../common/common.service";

@Injectable()
export class MyProfileService {
    roleName: string;
    constructor(private _networkService: NetworkService, public commonService: CommonService) { }

    getProfile() {
        this.roleName = this.commonService.getUserByRole();
        if (this.roleName == "ROLE_ADMIN") {
            return this._networkService.get('honcho/account', null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {    //HOD
            return this._networkService.get('provost/account', null, 'bearer');
        }
        else if (this.roleName == "ROLE_API") {    //API
            return this._networkService.get('txns/account', null, 'bearer');
        }
        else if (this.roleName == "ROLE_CAMPAIGN") {      //Campaign
            return this._networkService.get('hype/account', null, 'bearer');
        }
        else if (this.roleName == "ROLE_CRM") {      //CRM
            return this._networkService.get('msisdn/account', null, 'bearer');
        }
        else if (this.roleName == "ROLE_MARKETING") {
            return this._networkService.get('peddle/account', null, 'bearer');
        }
    }
}