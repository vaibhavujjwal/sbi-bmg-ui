import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormArray,
  AbstractControl,
  AsyncValidatorFn,
  ValidationErrors,
  ValidatorFn
} from "@angular/forms";
import { RxwebValidators } from '../../../../../node_modules/@rxweb/reactive-form-validators';
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from "../../../common/common.service";
import { ManageUserService } from "../manage-user.service";
import Swal from "sweetalert2";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE } from "src/app/common/common.const";
import { Observable } from "../../../../../node_modules/rxjs";
import { promise } from "../../../../../node_modules/protractor";
import { map } from 'rxjs/operators';
import {
  AsyncValidator,
  NG_ASYNC_VALIDATORS,
  Validator
} from '@angular/forms';
import { Directive } from '@angular/core';
import { of } from 'rxjs';

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.scss"],
  providers: [ManageUserService]
})
export class AddUserComponent implements OnInit {
  currentStep: number = 1;
  FLAG_YES: string = "Y";
  isUniqueAppName: boolean = true;
  mainForm: FormGroup;


  HTTP: number = 1;
  BULK: number = 2;
  skipConfigurations: boolean = false;
  tempcheckDis: boolean = false;;
  disableSubmit: boolean = false;
  stepOne = { formSubmitted: false, formCompleted: false };
  stepTwo = { formSubmitted: false, formCompleted: false };
  stepThree = { formSubmitted: false, formCompleted: false };
  stepFour = { formSubmitted: false, formCompleted: false };
  topologyList: Array<object> = [];
  exitingFlag:boolean=false;
  categoryList: Array<object> = [];
  roleList: Array<object> = [];
  finalApproverList: Array<object> = [];
  constDepartmentList: Array<object> = [];

  userByRoles = [];
  existingApprovals: String = "";
  existingCategoryForCamp: String = "";
  userObject = {};
  userHod = [];
  userNames = [];
  selectedApproval = [];
  selectedDepartment = [];
  selectedCateForCampUser: any;
  userByRoleSetting = {};
  userDepartmentSetting = {};
  channelList: Array<object> = [];
  userByRoleList: Array<object> = [];
  allDepartmentList: Array<object> = [];
  departmentList: Array<object> = [];
  accessPermissionViewList: Array<object> = [];
  accessPermissionViewListCRM: Array<object> = [];
  accessPermissionViewListAPI: Array<object> = [];
  accessPermissionViewListCampaign: Array<object> = [];
  accessPermissionViewListAdmin: Array<object> = [];
  accessPermissionViewListMkt: Array<object> = [];
  accessPermissionViewListHOD: Array<object> = [];
  accessPermissionViewListDLT: Array<object> = [];
  selectedRole: object = {};
  retryCountList: Array<object> = [];
  retryDetailsString: String = "";
  finalApprovers = [];
  finalCategoryForCampUser = [];
  hodsArrays: String = "";
  existingHods = [];
  userId = -1;
  userData1 = [];
  userInfo: object = {};
  finalApproverSetting = {};
  categoryForCampSetting = {};
  records: FormArray;
  finalApproverReq: Object = {
    "roleId": 6
  }
  isUniqueUsernameEmailMob: boolean = false;
  isUserUpdated: boolean = true;
  selectedU: string;

  categoryListForCampUser: Array<object> = [];
  dropdownList: any;
  selectedItems: any;
  dropdownSettings: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public commonService: CommonService,
    private manageUserService: ManageUserService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params["userId"]) this.userId = params["userId"];
    });
    this.prepareAccessPermissionViewList();
    this.userRegistrationFormSetup();
    if (this.userId != -1) {
      this.getData(true);
    } else {
      this.getData();
    }
  }

  getData(all?: any) {
    if (this.departmentList.length != 0) {
      this.departmentList = [];
      let sysDeptId = this.manageUserService.SYSTEM_DEPT_ID;
      if (this.selectedRole["isSystemRole"] == this.FLAG_YES) {
        let dept = this.allDepartmentList.find(x => x["id"] == sysDeptId);
        this.departmentList.push(dept);
      } else {
        this.departmentList = this.allDepartmentList.filter(
          x => x["id"] != sysDeptId
        );
      }
    }

    let req = {
      loadAll: all ? true : false,
      roleId: this.manageUserService.ROLE_ID,
      loadDepartment: this.departmentList.length == 0 ? true : false,
      fetExistUser: this.userId != -1 ? true : false,
      userId: this.userId
    };
    this.spinner.show();
    this.manageUserService.getData(req).subscribe(responseList => {
      this.spinner.hide();
      if (responseList[0]['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.roleList = responseList[0]["data"];
        console.log("Role List....." + JSON.stringify(this.roleList));
      } else {
        this.roleList = [];
      }
      if (responseList[1]['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.userByRoles = responseList[1]["data"];
        for (var i = 0; i < this.userByRoles.length; i++) {
          this.userHod.push(this.userByRoles[i]['username'] + '-' + this.userByRoles[i]['department']['department']);
          this.userNames.push(this.userByRoles[i]['username']);
        }

        var array = [];
        for (var i = 0; i < this.userByRoles.length; i++) {
          array.push({ "combinedName": this.userHod[i], "name": this.userNames[i] });
        };

        //Sorting of array
        array.sort((a,b) => a.combinedName.localeCompare(b.combinedName));
        this.userData1 = array;
        console.log("Department HOD List....." + JSON.stringify(this.userData1));

        this.userByRoleSetting = {
          singleSelection: false,
          idField: 'name',
          textField: 'combinedName',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          itemsShowLimit: 3,
          allowSearchFilter: true,
          defaultOpen: false
        }
      } else {
        this.userByRoles = [];
      }
      if (req['loadDepartment']) {
        if (responseList[2]['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.constDepartmentList = this.departmentList = responseList[2]["data"];
          console.log("Department List....." + JSON.stringify(this.constDepartmentList));
        } else {
          this.departmentList = [];
        }
      } else if (req['fetExistUser'] && !req['loadDepartment']) {
        if (responseList[2]['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.userInfo = responseList[2]["data"];
          console.log("Fetch User Data....." + JSON.stringify(this.userInfo));
        } else {
          this.userInfo = [];
        }
      }
      if (req['loadDepartment'] && req['fetExistUser']) {
        if (responseList[3]['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.userInfo = responseList[3]["data"];
          console.log("Load Data for update user...." + JSON.stringify(this.userInfo));
          this.exitingFlag = true;
          this.loadExistingUserDetails();
        } else {
          this.userInfo = [];
        }
      }
    });
  }

  prepareAccessPermissionViewList() {
    this.accessPermissionViewList = this.manageUserService.getPermissionsViewList();
    this.accessPermissionViewListCRM = this.manageUserService.getPermissionsViewListForCRM();
    this.accessPermissionViewListCampaign = this.manageUserService.getPermissionsViewListCampaign();
    this.accessPermissionViewListAdmin = this.manageUserService.getPermissionsViewListAdmin();
    this.accessPermissionViewListMkt = this.manageUserService.getPermissionsViewListForMKT();
    this.accessPermissionViewListAPI = this.manageUserService.getPermissionsViewListAPI();
    this.accessPermissionViewListHOD = this.manageUserService.getPermissionsViewListHOD();
    this.accessPermissionViewListDLT = this.manageUserService.getPermissionsViewListDLT();
  }

  userRegistrationFormSetup() {
    this.mainForm = this.formBuilder.group({
      personalInfo: this.formBuilder.group(
        {
          firstName: [
            "",
            [
              Validators.required,
              Validators.minLength(1),
              Validators.maxLength(50),
              Validators.pattern(this.commonService.patterns.nameWithOnlyAlphabetsSpace)
            ]
          ],
          lastName: [
            "",
            [
              Validators.required,
              Validators.minLength(2),
              Validators.maxLength(50),
              Validators.pattern(this.commonService.patterns.nameWithOnlyAlphabetsSpace)
            ]
          ],
          userName: [
            "",
            [
              Validators.required,
              Validators.minLength(5),
              Validators.maxLength(20),
              Validators.pattern(
                this.commonService.patterns.alphaNumericPFID
              )
            ],
            this.usernameValidator()
          ],
          emailAddress: [
            "",
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.email)
            ],
            this.emailValidator()
          ],
          alternativeEmailAddress: [
            "",
            [Validators.pattern(this.commonService.patterns.email)]
          ],
          mobileNumber: [
            "",
            [
              Validators.required,
              Validators.minLength(10),
              Validators.maxLength(10),
              Validators.pattern(this.commonService.patterns.indianMobileNumber)
            ],
            this.mobileValidator()
          ],
          department: ["", [Validators.required]],
          role: ["", [Validators.required]],
          departmentHod: ["", [Validators.required]],
          finalApprover: ["", [Validators.required]],
          category: ["", [Validators.required]],
          showMsg: [false],
          showFullMsg: [false],
          // applicationName: ["NA"],
          ipWhitelist: [""],
          campApprovalReq:[true]
        },
        { validator: this.matchPrimaryAndAlternateEmailAddress }
      ),

      appCategory: this.formBuilder.group({
        records: this.formBuilder.array([
          this.createRecord()
        ])
      }),
      configuration: this.formBuilder.group(
        {
          category: ["0"],
          channel: ["", [Validators.required]],
          tps: ["", [Validators.required, Validators.min(100), Validators.max(50000), Validators.pattern(this.commonService.patterns.numberOnly)]],
          spamFilterLevel: ["", [Validators.required]],
          maxMessagePartLength_bulk: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.numberOnly),
              Validators.min(1),
              Validators.max(7)
            ]
          ),
          maxSenderId_bulk: ["", [
            Validators.required,
            Validators.pattern(this.commonService.patterns.numberOnly),
            Validators.min(1),
            Validators.max(50)
          ]],
          maxTemplate_bulk: ["", [
            Validators.required,
            Validators.pattern(this.commonService.patterns.numberOnly),
            Validators.min(1),
            Validators.max(5000)
          ]],
          duplicateCheckFlag: [false],
          templateCheckFlag: [false],
          fcmCheckFlag: [false],
          senderIdCheckFlag: [false],
          DLRReportRequestFlag: [false],
          internationalRouteFlag: [false],
          longMessageSupportFlag: [false],
          unicodeMessageSupportFlag: [false],
          maxFileSize_bulk: new FormControl({ disabled: true, value: "" }, [
            Validators.required,
            Validators.pattern(this.commonService.patterns.numberOnly),
            Validators.max(450)
          ]),
          maxRecipientCount_bulk: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.numberOnly),
              Validators.min(1),
              Validators.max(5000000)
            ]
          ),
          maxDailyThresholdLimit_bulk: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.numberOnly),
              Validators.min(1),
              Validators.max(400000000)
            ]
          ),
          maxHttpConnectionCount: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required,
              Validators.min(1),
              Validators.max(20),
              Validators.pattern(this.commonService.patterns.numberOnly)
            ]
          ),
          DLRCallbackTopology: new FormControl({ disabled: true, value: "" }, [
            Validators.required
          ]),
          DLRCallbackPrimaryURL: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.url)
            ]
          ),
          DLRCallbackSecondaryURL: new FormControl(
            { disabled: true, value: "" },
            [Validators.pattern(this.commonService.patterns.url)]
          ),
          DLRCallbackMaxConnectionCount: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.numberOnly),
              Validators.min(1),
              Validators.max(20)
            ]
          ),
          DLRCallbackRetryFlag: [false],
          DLRCallbackRetryCount: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required
            ]
          ),
          DLRCallbackRetryIntervalArray: this.formBuilder.array([])
        },
        { validator: [this.matchPrimaryAndSecondaryURL, this.matchThresholdAndRecepientValue] }
      ),
      authorization: this.formBuilder.group({
      })
    });

    let authFormGroup = <FormGroup>this.mainForm.controls.authorization;
    this.accessPermissionViewList.forEach(element => {
      if (!element["isSubModule"]) {
        authFormGroup.addControl(
          element["formControlName"],
          new FormControl("H", Validators.required)
        );
      } else {
        let subModuleList = element["subModuleList"];
        subModuleList.forEach(subElement => {
          authFormGroup.addControl(
            subElement["formControlName"],
            new FormControl("H", Validators.required)
          );
        });
      }
    });
    this.accessPermissionViewListCampaign.forEach(element => {
      if (!element["isSubModule"]) {
        authFormGroup.addControl(
          element["formControlName"],
          new FormControl("H", Validators.required)
        );
      } else {
        let subModuleList = element["subModuleList"];
        subModuleList.forEach(subElement => {
          authFormGroup.addControl(
            subElement["formControlName"],
            new FormControl("H", Validators.required)
          );
        });
      }
    });

    this.accessPermissionViewListCRM.forEach(element => {
      if (!element["isSubModule"]) {
        authFormGroup.addControl(
          element["formControlName"],
          new FormControl("H", Validators.required)
        );
      } else {
        let subModuleList = element["subModuleList"];
        subModuleList.forEach(subElement => {
          authFormGroup.addControl(
            subElement["formControlName"],
            new FormControl("H", Validators.required)
          );
        });
      }
    });
    this.accessPermissionViewListHOD.forEach(element => {
      if (!element["isSubModule"]) {
        authFormGroup.addControl(
          element["formControlName"],
          new FormControl("H", Validators.required)
        );
      } else {
        let subModuleList = element["subModuleList"];
        subModuleList.forEach(subElement => {
          authFormGroup.addControl(
            subElement["formControlName"],
            new FormControl("H", Validators.required)
          );
        });
      }
    });
    this.accessPermissionViewListAPI.forEach(element => {
      if (!element["isSubModule"]) {
        authFormGroup.addControl(
          element["formControlName"],
          new FormControl("H", Validators.required)
        );
      } else {
        let subModuleList = element["subModuleList"];
        subModuleList.forEach(subElement => {
          authFormGroup.addControl(
            subElement["formControlName"],
            new FormControl("H", Validators.required)
          );
        });
      }
    });
    this.accessPermissionViewListAdmin.forEach(element => {
      if (!element["isSubModule"]) {
        authFormGroup.addControl(
          element["formControlName"],
          new FormControl("H", Validators.required)
        );
      } else {
        let subModuleList = element["subModuleList"];
        subModuleList.forEach(subElement => {
          authFormGroup.addControl(
            subElement["formControlName"],
            new FormControl("H", Validators.required)
          );
        });
      }
    });

    this.accessPermissionViewListMkt.forEach(element => {
      if (!element["isSubModule"]) {
        authFormGroup.addControl(
          element["formControlName"],
          new FormControl("H", Validators.required)
        );
      } else {
        let subModuleList = element["subModuleList"];
        subModuleList.forEach(subElement => {
          authFormGroup.addControl(
            subElement["formControlName"],
            new FormControl("H", Validators.required)
          );
        });
      }
    });
  }

  createRecord(): FormGroup {
    return this.formBuilder.group({
      appName: ["", [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
        Validators.pattern(this.commonService.patterns.alphaNumericOnly),
        RxwebValidators.unique()
      ]
        , this.appNameValidator.bind(this, 0)
      ],
      cate: ["", [Validators.required]],
      id: [""]
    }
    )
  }

  get personalInfoForm() {
    let x = <FormGroup>this.mainForm.controls.personalInfo;
    return x.controls;
  }

  get configurationForm() {
    let x = <FormGroup>this.mainForm.controls.configuration;
    return x.controls;
  }

  get appCategory() {
    let x = <FormGroup>this.mainForm.controls.appCategory;
    return x.controls;
  }

  get authorizationForm() {
    let x = <FormGroup>this.mainForm.controls.authorization;
    return x.controls;
  }

  addNewRecord() {
    this.records = this.mainForm.controls.appCategory.get('records') as FormArray;
    this.records.push(this.createRecord());
  }

  deleteRecord(index: any) {
    let control = <FormArray>this.appCategory["records"];
    control.removeAt(index);
  }

  get getForm() {
    return this.mainForm.controls.appCategory.get('records') as FormArray;
  }

  appNameValidator(control: AbstractControl, id: any): Observable<{ [key: string]: any } | null> {
    let obj = {
      applicationName: id.value,
      id: control
    };
    return this.manageUserService.checkApplicationNameUniqueness(obj)
      .pipe(debounceTime(10000),
        map(res => {
          if (res['data'] == false) {
            return { 'isNotUnique': true };
          } else {
            return null;
          }
        })
      );
  }

  emailValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      let obj = {
        id: this.userId != -1 ? this.userId : 0,
        value: control.value
      };
      return this.manageUserService.validateEmailAddress(obj).pipe(debounceTime(1000),
        map(res => {
          if (res["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            if (res['data'] == false) {
              return { 'isEmailNotUnique': true };
            } else {
              return null;
            }
          }
          else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        })
      );
    }
  }

  mobileValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      let obj = {
        userId: this.userId != -1 ? this.userId : 0,
        mobileNumber: control.value
      };
      return this.manageUserService.validateMobileNumber(obj).pipe(debounceTime(1000),
        map(res => {
          if (res["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            if (res['data'] == false) {
              return { 'isMobileNotUnique': true };
            } else {
              return null;
            }
          }
          else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        })
      );
    }
  }

  usernameValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      let obj = {
        id: this.userId != -1 ? this.userId : 0,
        value: control.value
      };
      return this.manageUserService.validateUsername(obj).pipe(debounceTime(1000),
        map(res => {
          if (res["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            if (res['data'] == false) {
              return { 'isUsernameNotUnique': true };
            } else {
              return null;
            }
          }
          else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        })
      );
    }
  }

  matchPrimaryAndAlternateEmailAddress(group: FormGroup) {
    let primaryEmailAddress = group.controls["emailAddress"].value.trim();
    let alternativeEmailAddress = group.controls[
      "alternativeEmailAddress"
    ].value.trim();
    if (
      primaryEmailAddress.length > 0 &&
      alternativeEmailAddress.length > 0 &&
      primaryEmailAddress == alternativeEmailAddress
    ) {
      return { emailAddressMatched: true };
    }
    return null;
  }

  matchPrimaryAndSecondaryURL(group: FormGroup) {
    if (group.controls["DLRReportRequestFlag"].value) {
      if (group.controls["DLRCallbackPrimaryURL"].value && group.controls["DLRCallbackSecondaryURL"].value) {
        let primaryURL = group.controls["DLRCallbackPrimaryURL"].value.trim();
        let secondaryURL = group.controls["DLRCallbackSecondaryURL"].value.trim();
        if (
          primaryURL.length > 0 &&
          secondaryURL.length > 0 &&
          primaryURL == secondaryURL
        ) {
          return { DLRCallbackURLsMatched: true };
        }
      }
    }
    return null;
  }

  matchThresholdAndRecepientValue(group: FormGroup) {
    if (group.controls["maxDailyThresholdLimit_bulk"].value && group.controls["maxRecipientCount_bulk"].value) {
      let thresholdLimit = Number(group.controls["maxDailyThresholdLimit_bulk"].value);
      let recepientCount = Number(group.controls["maxRecipientCount_bulk"].value);

      if (thresholdLimit &&
        recepientCount &&
        thresholdLimit < recepientCount) {
        return { BulkThresholdMoreThanRecepientCount: true };
      }
    }
    return null;
  }

  loadExistingUserDetails() {
    this.personalInfoForm["userName"].setValue(this.userInfo["username"]);
    this.personalInfoForm["userName"].disable();
    this.personalInfoForm["firstName"].setValue(this.userInfo["firstName"]);
    this.personalInfoForm["lastName"].setValue(this.userInfo["lastName"]);
    this.personalInfoForm["emailAddress"].setValue(this.userInfo["email"]);
    this.personalInfoForm["alternativeEmailAddress"].setValue(
      this.userInfo["alternateEmails"] ? this.userInfo["alternateEmails"] : ""
    );
    this.personalInfoForm["mobileNumber"].setValue(this.userInfo["msisdn"]);
    this.selectedDepartment.push(this.userInfo["deptId"]);
    this.personalInfoForm["department"].setValue(this.selectedDepartment);
    this.personalInfoForm["role"].setValue(this.userInfo["roleId"]["id"] + "");
    //SELECTED HOD DATA POPULATE
    this.hodsArrays = this.userInfo["hod"];
    this.hodsArrays = this.hodsArrays.substring(1, this.hodsArrays.length - 1);
    this.existingHods = this.hodsArrays.split(',');
    if (this.hodsArrays != "NA" && this.hodsArrays != "")
      this.personalInfoForm["departmentHod"].setValue(this.existingHods);
    else
      this.personalInfoForm["departmentHod"].setValue([]);
    this.personalInfoForm["ipWhitelist"].setValue(this.userInfo["ipWhitelist"]);

    if (this.userInfo['roleId']['name'] == "ROLE_CAMPAIGN" || this.userInfo['roleId']['name'] == "ROLE_API") {
      this.personalInfoForm["departmentHod"].setValidators([Validators.required]);
      this.personalInfoForm["departmentHod"].updateValueAndValidity();
    }

    if (this.userInfo['roleId']['name'] == "ROLE_API") {
      //POPULATE FORMARRAY DATA WHILE UPDATING USER
      let existingCatAppList = [];
      this.userInfo["categoryAppNameList"].forEach(element => {
        let rowObj = {
          "appName": element.appName,
          "cate": element.categoryId.id,
          "id": element.id
        };
        existingCatAppList.push(rowObj);
      })
      let control = <FormArray>this.appCategory["records"];
      if (this.userInfo['roleId']['name'] == "ROLE_API" || this.userInfo['roleId']['name'] == "ROLE_CAMPAIGN") {
        control.removeAt(0);
      }
      existingCatAppList.forEach(x => {
        control.push(
          this.formBuilder.group({
            appName: [x.appName, [
              Validators.required,
              Validators.minLength(2),
              Validators.maxLength(30),
              Validators.pattern(this.commonService.patterns.alphaNumaric),
              RxwebValidators.unique()
            ]
              , this.appNameValidator.bind(this, x.id)
            ],
            cate: [x.cate, [Validators.required]],
            id: [x.id]
          }
          )
        )
      })
    }
    if (this.userInfo['roleId']['name'] != "ROLE_MARKETING") {
      this.personalInfoForm["showMsg"].setValue(
        this.userInfo["showMsg"] == "Y"
      );
      this.personalInfoForm["showFullMsg"].setValue(
        this.userInfo["showFullMsg"] == "Y"
      );
    }

    if (this.userInfo['roleId']['name'] == "ROLE_MARKETING") {
      this.personalInfoForm["finalApprover"].setValue(this.userInfo["finalApprover"]);
    }
    this.selectedRole = this.userInfo["roleId"];

    if (this.userInfo["roleId"]["configurationFlag"] == "R" ||
      (this.userInfo["roleId"]["configurationFlag"] == "N")
    ) {
      this.configurationForm["channel"].setValue(
        this.userInfo["channelId"]["id"]
      );
      this.configurationForm["spamFilterLevel"].setValue(
        this.userInfo["spamKeywordLevel"]
      );
      this.configurationForm["tps"].setValue(this.userInfo["tps"]);
      this.configurationForm["duplicateCheckFlag"].setValue(
        this.userInfo["duplicateCheck"] == "Y"
      );
      this.configurationForm["templateCheckFlag"].setValue(
        this.userInfo["templateCheck"] == "Y"
      );
      this.configurationForm["internationalRouteFlag"].setValue(
        this.userInfo["internationalRouteFlag"] == "Y"
      );
      this.configurationForm["longMessageSupportFlag"].setValue(
        this.userInfo["longMessageAllowed"] == "Y"
      );
      this.configurationForm["unicodeMessageSupportFlag"].setValue(
        this.userInfo["unicodeSupport"] == "Y"
      );
      this.configurationForm["fcmCheckFlag"].setValue(
        this.userInfo["fcmFlag"] == "Y"
      );
      this.configurationForm["senderIdCheckFlag"].setValue(
        this.userInfo["senderidCheck"] == "Y"
      );
      this.configurationForm["DLRReportRequestFlag"].setValue(
        this.userInfo["dlrReportsReq"] == "Y"
      );
      this.configurationForm["maxHttpConnectionCount"].setValue(
        this.configurationForm["channel"].value == this.HTTP
          ? this.userInfo["maxConnection"]
          : ""
      );
      this.configurationForm["maxSenderId_bulk"].setValue(
        this.userInfo["maxSenderid"]
      );
      this.configurationForm["maxTemplate_bulk"].setValue(
        this.userInfo["maxTemplate"]
      );
      if (this.configurationForm.longMessageSupportFlag.value == true) {
        this.configurationForm["maxMessagePartLength_bulk"].enable();
        this.configurationForm["maxMessagePartLength_bulk"].setValue(
          this.userInfo["maxMessageParts"]
        );
      } else {
        this.configurationForm["maxMessagePartLength_bulk"].disable();
        this.configurationForm["maxMessagePartLength_bulk"].setValue(
          ""
        );
      }

      if (this.configurationForm["channel"].value == this.BULK) {
        this.configurationForm["maxFileSize_bulk"].setValue(
          this.userInfo["maxFilesize"]
        );
        this.configurationForm["maxRecipientCount_bulk"].setValue(
          this.userInfo["maxRecipientCount"]
        );
        this.configurationForm["maxDailyThresholdLimit_bulk"].setValue(
          this.userInfo["maxDailyThresholdLimit"]
        );
      }
      if (this.userInfo["userDlrCallback"] != null) {
        // if (this.userInfo["dlrReportsReq"] == "Y") {
        this.configurationFormFlagChanged("DLRReportRequestFlag", [
          "DLRCallbackMaxConnectionCount",
          "DLRCallbackTopology",
          "DLRCallbackPrimaryURL",
          "DLRCallbackSecondaryURL",
          "DLRCallbackRetryFlag",
          "DLRCallbackRetryIntervalArray"
        ]);
        this.configurationForm["DLRCallbackPrimaryURL"].setValue(
          this.userInfo["userDlrCallback"]["primaryUrl"]
        );
        this.configurationForm["DLRCallbackSecondaryURL"].setValue(
          this.userInfo["userDlrCallback"]["secondaryUrl"]
            ? this.userInfo["userDlrCallback"]["secondaryUrl"]
            : ""
        );
        this.configurationForm["DLRCallbackTopology"].setValue(
          this.userInfo["userDlrCallback"]["topologyMode"]
        );
        this.configurationForm["DLRCallbackMaxConnectionCount"].setValue(
          this.userInfo["userDlrCallback"]["maxConnection"]
        );
        this.configurationFormFlagChanged('DLRCallbackRetryFlag', ['DLRCallbackRetryCount', 'DLRCallbackRetryIntervalArray']);
        if (this.userInfo["userDlrCallback"]["retryFlag"] == "Y") {
          this.configurationForm.DLRCallbackRetryFlag.setValue(
            true
          );

          this.configurationForm["DLRCallbackRetryCount"].setValue(
            this.userInfo["userDlrCallback"]["maxRetryCount"]
          );

          let retryIntervalControls = <FormArray>(
            this.configurationForm["DLRCallbackRetryIntervalArray"]
          );

          for (let i = retryIntervalControls.length - 1; i >= 0; i--) {
            retryIntervalControls.removeAt(i);
          }

          let existingDLRCallabackRetryDetails = this.userInfo["userDlrCallback"]["retryDetails"].split(',')
          for (let i = 0; i < existingDLRCallabackRetryDetails.length; i++) {
            retryIntervalControls.push(
              this.formBuilder.group({
                DLRCallbackRetryInterval: [
                  existingDLRCallabackRetryDetails[i],
                  [
                    Validators.required,
                    Validators.min(1),
                    Validators.max(900),
                    Validators.pattern(this.commonService.patterns.numberOnly)
                  ]
                ]
              })
            );
          }
        }
        // }
      }
      this.channelChanged(this.configurationForm["channel"].value);
    }
    // let pJson = JSON.parse(this.userInfo["permissionJson"]);
    // console.log("Permission JSON line 906.."+JSON.stringify(pJson));
    // if (this.userInfo['roleId']['name'] == 'ROLE_CRM') {
    //   this.accessPermissionViewListCRM.forEach(element => {
    //     let val = "H";
    //     if (pJson[element["formControlName"]] && !element["isSubModule"]) {
    //       val = pJson[element["formControlName"]];
    //       this.authorizationForm[element["formControlName"]].setValue(val);
    //     } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
    //       element['subModuleList'].forEach(ele => {
    //         val = "H";
    //         if (pJson[ele["formControlName"]]) {
    //           val = pJson[ele["formControlName"]];
    //         }
    //         this.authorizationForm[ele["formControlName"]].setValue(val);
    //       });
    //     }
    //   });
    // } else if (this.userInfo['roleId']['name'] == 'ROLE_CAMPAIGN') {
    //   this.accessPermissionViewListCampaign.forEach(element => {
    //     let val = "H";
    //     if (pJson[element["formControlName"]] && !element["isSubModule"]) {
    //       val = pJson[element["formControlName"]];
    //       this.authorizationForm[element["formControlName"]].setValue(val);
    //     } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
    //       element['subModuleList'].forEach(ele => {
    //         val = "H";
    //         if (pJson[ele["formControlName"]]) {
    //           val = pJson[ele["formControlName"]];
    //         }
    //         this.authorizationForm[ele["formControlName"]].setValue(val);
    //       });
    //     }
    //   });
    // }
    // else if (this.userInfo['roleId']['name'] == 'ROLE_ADMIN') {
    //   this.accessPermissionViewListAdmin.forEach(element => {
    //     let val = "H";
    //     if (pJson[element["formControlName"]] && !element["isSubModule"]) {
    //       val = pJson[element["formControlName"]];
    //       this.authorizationForm[element["formControlName"]].setValue(val);
    //     } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
    //       element['subModuleList'].forEach(ele => {
    //         val = "H";
    //         if (pJson[ele["formControlName"]]) {
    //           val = pJson[ele["formControlName"]];
    //         }
    //         this.authorizationForm[ele["formControlName"]].setValue(val);
    //       });
    //     }
    //   });
    // }
    // else if (this.userInfo['roleId']['name'] == 'ROLE_MARKETING') {
    //   this.accessPermissionViewListMkt.forEach(element => {
    //     let val = "H";
    //     if (pJson[element["formControlName"]] && !element["isSubModule"]) {
    //       val = pJson[element["formControlName"]];
    //       this.authorizationForm[element["formControlName"]].setValue(val);
    //     } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
    //       element['subModuleList'].forEach(ele => {
    //         val = "H";
    //         if (pJson[ele["formControlName"]]) {
    //           val = pJson[ele["formControlName"]];
    //         }
    //         this.authorizationForm[ele["formControlName"]].setValue(val);
    //       });
    //     }
    //   });
    // }
    // else if (this.userInfo['roleId']['name'] == 'ROLE_API') {
    //   this.accessPermissionViewListAPI.forEach(element => {
    //     let val = "H";
    //     if (pJson[element["formControlName"]] && !element["isSubModule"]) {
    //       val = pJson[element["formControlName"]];
    //       this.authorizationForm[element["formControlName"]].setValue(val);
    //     } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
    //       element['subModuleList'].forEach(ele => {
    //         val = "H";
    //         if (pJson[ele["formControlName"]]) {
    //           val = pJson[ele["formControlName"]];
    //         }
    //         this.authorizationForm[ele["formControlName"]].setValue(val);
    //       });
    //     }
    //   });
    // }
    // else if (this.userInfo['roleId']['name'] == 'ROLE_HOD') {
    //   this.accessPermissionViewListHOD.forEach(element => {
    //     let val = "H";
    //     if (pJson[element["formControlName"]] && !element["isSubModule"]) {
    //       val = pJson[element["formControlName"]];
    //       this.authorizationForm[element["formControlName"]].setValue(val);
    //     } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
    //       element['subModuleList'].forEach(ele => {
    //         val = "H";
    //         if (pJson[ele["formControlName"]]) {
    //           val = pJson[ele["formControlName"]];
    //         }
    //         this.authorizationForm[ele["formControlName"]].setValue(val);
    //       });
    //     }
    //   });
    // }
    // else {
    //   this.accessPermissionViewList.forEach(element => {
    //     let val = "H";
    //     if (pJson[element["formControlName"]] && !element["isSubModule"]) {
    //       val = pJson[element["formControlName"]];
    //       this.authorizationForm[element["formControlName"]].setValue(val);
    //     } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
    //       element['subModuleList'].forEach(ele => {
    //         val = "H";
    //         if (pJson[ele["formControlName"]]) {
    //           val = pJson[ele["formControlName"]];
    //         }
    //         this.authorizationForm[ele["formControlName"]].setValue(val);
    //       });
    //     }
    //   });
    // }
  }

  fetchRecordsFromForm() {
    let items = this.mainForm.controls.appCategory.get('records') as FormArray;
    let dataArr = [];
    items.controls.forEach(element => {
      let rowObj = {
        "appName": element.get('appName').value,
        "categoryId": element.get('cate').value
      };
      dataArr.push(rowObj);
    });
    return dataArr;
  }

  longMessageSupportChanged(isChecked) {
    if (isChecked) {
      this.configurationForm["maxMessagePartLength_bulk"].enable();
    } else {
      this.configurationForm["maxMessagePartLength_bulk"].disable();
      this.configurationForm["maxMessagePartLength_bulk"].setValue("");
    }
  }

  saveUser() {
    this.stepThree.formSubmitted = true;
    if (!this.mainForm.controls.authorization.valid) {
      return;
    }

    if (this.mainForm.valid ||
      ((this.selectedRole["configurationFlag"] == "S" ||
        this.selectedRole["configurationFlag"] == "N") &&
        this.mainForm.controls["personalInfo"].valid &&
        this.mainForm.controls["authorization"].valid) ||
      (this.selectedRole['name'] == "ROLE_CAMPAIGN" &&
        this.mainForm.controls["personalInfo"].valid &&
        this.mainForm.controls["authorization"].valid &&
        this.mainForm.controls["configuration"].valid
      )
    ) {
      this.spinner.show();
      this.disableSubmit = true;
      let permissionJson = {};
      if (this.selectedU == 'ROLE_CRM') {
        this.accessPermissionViewListCRM.forEach(element => {
          if (element["isSubModule"]) {
            let subModuleList = element["subModuleList"];
            subModuleList.forEach(ele => {
              permissionJson[ele["formControlName"]] = this.authorizationForm[
                ele["formControlName"]
              ].value;
            });
          } else {
            permissionJson[element["formControlName"]] = this.authorizationForm[
              element["formControlName"]
            ].value;
          }
        });
      }
      else if (this.selectedU == 'ROLE_CAMPAIGN') {
        this.accessPermissionViewListCampaign.forEach(element => {
          if (element["isSubModule"]) {
            let subModuleList = element["subModuleList"];
            subModuleList.forEach(ele => {
              permissionJson[ele["formControlName"]] = this.authorizationForm[
                ele["formControlName"]
              ].value;
            });
          } else {
            permissionJson[element["formControlName"]] = this.authorizationForm[
              element["formControlName"]
            ].value;
          }
        });
      }
      else if (this.selectedU == 'ROLE_ADMIN') {
        this.accessPermissionViewListAdmin.forEach(element => {
          if (element["isSubModule"]) {
            let subModuleList = element["subModuleList"];
            subModuleList.forEach(ele => {
              permissionJson[ele["formControlName"]] = this.authorizationForm[
                ele["formControlName"]
              ].value;
            });
          } else {
            permissionJson[element["formControlName"]] = this.authorizationForm[
              element["formControlName"]
            ].value;
          }
        });
      }
      else if (this.selectedU == 'ROLE_MARKETING') {
        this.accessPermissionViewListMkt.forEach(element => {
          if (element["isSubModule"]) {
            let subModuleList = element["subModuleList"];
            subModuleList.forEach(ele => {
              permissionJson[ele["formControlName"]] = this.authorizationForm[
                ele["formControlName"]
              ].value;
            });
          } else {
            permissionJson[element["formControlName"]] = this.authorizationForm[
              element["formControlName"]
            ].value;
          }
        });
      }
      else if (this.selectedU == 'ROLE_API') {
        this.accessPermissionViewListAPI.forEach(element => {
          if (element["isSubModule"]) {
            let subModuleList = element["subModuleList"];
            subModuleList.forEach(ele => {
              permissionJson[ele["formControlName"]] = this.authorizationForm[
                ele["formControlName"]
              ].value;
            });
          } else {
            permissionJson[element["formControlName"]] = this.authorizationForm[
              element["formControlName"]
            ].value;
          }
        });
      }else if (this.selectedU == 'ROLE_HOD') {
        this.accessPermissionViewListHOD.forEach(element => {
          if (element["isSubModule"]) {
            let subModuleList = element["subModuleList"];
            subModuleList.forEach(ele => {
              permissionJson[ele["formControlName"]] = this.authorizationForm[
                ele["formControlName"]
              ].value;
            });
          } else {
            permissionJson[element["formControlName"]] = this.authorizationForm[
              element["formControlName"]
            ].value;
          }
        });
      }else if (this.selectedU == 'ROLE_DLT') {
        for(var x=0;x<this.roleList.length; x++){
          if(this.roleList[x]['id']=='7'){
            permissionJson = JSON.parse(this.roleList[x]["permissionJson"]);
            console.log("Permission..."+ permissionJson);
          }
        }
        /*this.accessPermissionViewListDLT.forEach(element => {
          if (element["isSubModule"]) {
            let subModuleList = element["subModuleList"];
            subModuleList.forEach(ele => {
              permissionJson[ele["formControlName"]] = this.authorizationForm[
                ele["formControlName"]
              ].value;
            });
          } else {
            permissionJson[element["formControlName"]] = this.authorizationForm[
              element["formControlName"]
            ].value;
          }
        });*/
      }
      else {
        this.accessPermissionViewList.forEach(element => {
          if (element["isSubModule"]) {
            let subModuleList = element["subModuleList"];
            subModuleList.forEach(ele => {
              permissionJson[ele["formControlName"]] = this.authorizationForm[
                ele["formControlName"]
              ].value;
            });
          } else {
            permissionJson[element["formControlName"]] = this.authorizationForm[
              element["formControlName"]
            ].value;
          }
        });
      }

      if (this.userId == -1) {
        if (this.selectedU == 'ROLE_CAMPAIGN') {
          var finalApproval = [];
          var finalApprovalName = this.personalInfoForm['finalApprover'].value;
          if (finalApprovalName.length > 0) {
            for (var i = 0; i < finalApprovalName.length; i++) {
              finalApproval.push(finalApprovalName[i].username?finalApprovalName[i].username:finalApprovalName[i])
            }
            console.log("my String", finalApproval.toString());
            // finalApproval = finalApprovalName.map(a => a);
          } else {
            finalApproval = this.personalInfoForm['finalApprover'].value;
          }

          var hodNames;
          var hods = this.personalInfoForm['departmentHod'].value;
          var deptHods = [];
          for (var i = 0; i < hods.length; i++) {
            deptHods.push(hods[i]['name']);
          }
          if (deptHods.length > 0) {
            hodNames = deptHods.map(a => a);
          } else {
            hodNames = this.personalInfoForm['departmentHod'].value;
          }

          var categoryForCamp;
          var cateCampUser = this.personalInfoForm['category'].value;
          var categoryCampAray = [];
          for (var i = 0; i < cateCampUser.length; i++) {
            categoryCampAray.push(cateCampUser[i]['id']);
          }

          if (categoryCampAray.length > 0) {
            categoryForCamp = categoryCampAray.map(a => a);
          } else {
            categoryForCamp = this.personalInfoForm['category'].value;
          }
        } else if (this.selectedU == 'ROLE_API') {
          finalApproval = ['NA'];
          categoryForCamp = ['NA']
          var hodNames;
          var hods = this.personalInfoForm['departmentHod'].value;
          var deptHods = [];
          for (var i = 0; i < hods.length; i++) {
            deptHods.push(hods[i]['name']);
          }
          if (deptHods.length > 0) {
            hodNames = deptHods.map(a => a);
          } else {
            hodNames = this.personalInfoForm['departmentHod'].value;
          }
        } else {
          hodNames = ['NA'];
          finalApproval = ['NA'];
          categoryForCamp = ['NA'];
        }
      }
      if (this.userId != -1) {
        var selectedCategoryReq = [];
        var newHods = [];
        var finalApprovalUpdate = [];

        if (this.selectedU == 'ROLE_CAMPAIGN') {
          //CATEGORY REQ FOR CAMP
          var selectedCategorys = this.personalInfoForm['category'].value;
          for (var i = 0; i < selectedCategorys.length; i++) {
            if (selectedCategorys[i].id) {
              selectedCategoryReq.push(selectedCategorys[i].id);
            } else {
              selectedCategoryReq.push(selectedCategorys[i]);
            }
          }
          //HOD REQ FOR CAMP
          var editHod = this.personalInfoForm['departmentHod'].value;
          console.log("selected HOD..." + editHod);
          for (var i = 0; i < editHod.length; i++) {
            if (editHod[i].name) {
              newHods.push(editHod[i].name);
            } else {
              newHods.push(editHod[i]);
            }
          }
          //FINAL APPROVAL REQ FOR CAMP
          var finalApprovalName = this.personalInfoForm['finalApprover'].value;
          if (finalApprovalName.length > 0) {
            for (var i = 0; i < finalApprovalName.length; i++) {
              if (finalApprovalName[i].username) {
                finalApprovalUpdate.push(finalApprovalName[i].username);
              } else {
                finalApprovalUpdate.push(finalApprovalName[i]);
              }
            }
            console.log("my String", finalApprovalUpdate.toString());
          } else {
            finalApprovalUpdate = this.personalInfoForm['finalApprover'].value;
          }

          // var finalApprovalName = this.personalInfoForm['finalApprover'].value;
          // console.log("selected Final Approver..." + finalApprovalName);
          // if (finalApprovalName.length > 0) {
          //   finalApprovalUpdate = finalApprovalName.map(a => a);
          // } else {
          //   finalApprovalUpdate = this.personalInfoForm['finalApprover'].value;
          // }
        } else if (this.selectedU == 'ROLE_API') {
          //HOD REQ FOR CAMP
          var editHod = this.personalInfoForm['departmentHod'].value;
          console.log("selected HOD..." + editHod);
          for (var i = 0; i < editHod.length; i++) {
            if (editHod[i].name) {
              newHods.push(editHod[i].name);
            } else {
              newHods.push(editHod[i]);
            }
          }
          selectedCategoryReq = ['NA'];
          finalApprovalUpdate = ['NA'];
        } else {      //FOR REST USERS
          selectedCategoryReq = ['NA'];
          newHods = ['NA'];
          finalApprovalUpdate = ['NA'];
        }
        console.log("Category for Update..." + selectedCategoryReq);
        console.log("HODS for Update..." + newHods);
        console.log("FA for Update..." + finalApprovalUpdate);

      }
      let appCat;
      appCat = this.fetchRecordsFromForm();
      // console.log("APP CATEGORY........." + appCat);

      let deparment_id = this.personalInfoForm['department'].value?this.personalInfoForm['department'].value[0]["id"]:"";
      let campApprovalRe = this.personalInfoForm['campApprovalReq'].value?"Y":"N"
      delete permissionJson['bulkTemplateManagement'];
      if(this.selectedU == 'ROLE_CAMPAIGN' || this.selectedU == 'ROLE_API'){
        permissionJson['smsTemplateManagement_bulkUpload'] = permissionJson['smsTemplateManagement'];
      }
      let finalObj = {
        firstName: this.personalInfoForm["firstName"].value,
        lastName: this.personalInfoForm["lastName"].value,
        username: this.personalInfoForm["userName"].value,
        email: this.personalInfoForm["emailAddress"].value,
        alternateEmails:
          this.personalInfoForm["alternativeEmailAddress"].value.trim().length >
            0
            ? this.personalInfoForm["alternativeEmailAddress"].value
            : null,
        msisdn: this.personalInfoForm["mobileNumber"].value,
        deptId: Number(deparment_id),
        roleId: Number(this.personalInfoForm["role"].value),
        categoryAppNameList: (this.selectedU == 'ROLE_API') ? appCat : [],
        applicationName: "NA",
        hod: this.userId == -1 ? hodNames.toString() : newHods.toString(),
        showMsg: this.personalInfoForm["role"].value != 6
          ? (this.personalInfoForm["showMsg"].value ? "Y" : "N")
          : "N",
        showFullMsg: this.personalInfoForm["role"].value != 6
          ? (this.personalInfoForm["showFullMsg"].value ? "Y" : "N")
          : "N",
        finalApprover: this.userId == -1 ? finalApproval.toString() : finalApprovalUpdate.toString(),
        campaignUserCategories: this.userId == -1 ? categoryForCamp.toString() : selectedCategoryReq.toString(),
        // categoryId: Number(this.configurationForm["category"].value),
        spamKeywordLevel: this.configurationForm["spamFilterLevel"].value ? this.configurationForm["spamFilterLevel"].value : "L",
        channelId: Number(this.configurationForm["channel"].value),
        maxConnection: this.configurationForm["maxHttpConnectionCount"].value
          ? Number(this.configurationForm["maxHttpConnectionCount"].value)
          : 0,
        maxMessageParts: this.configurationForm["maxMessagePartLength_bulk"]
          .value
          ? Number(this.configurationForm["maxMessagePartLength_bulk"].value)
          : 1,
        maxFilesize: this.configurationForm["maxFileSize_bulk"].value
          ? Number(this.configurationForm["maxFileSize_bulk"].value)
          : 0,
        maxRecipientCount: this.configurationForm["maxRecipientCount_bulk"]
          .value
          ? Number(this.configurationForm["maxRecipientCount_bulk"].value)
          : 0,
        maxSenderid: this.configurationForm["maxSenderId_bulk"].value
          ? Number(this.configurationForm["maxSenderId_bulk"].value)
          : 0,
        maxTemplate: this.configurationForm["maxTemplate_bulk"].value
          ? Number(this.configurationForm["maxTemplate_bulk"].value)
          : 0,
        maxDailyThresholdLimit: this.configurationForm[
          "maxDailyThresholdLimit_bulk"
        ].value
          ? Number(this.configurationForm["maxDailyThresholdLimit_bulk"].value)
          : 0,
        tps: Number(this.configurationForm["tps"].value),
        ipWhitelistDetails: this.personalInfoForm["ipWhitelist"].value,
        duplicateCheck: this.configurationForm["duplicateCheckFlag"].value
          ? "Y"
          : "N",
        internationalRouteFlag: this.configurationForm[
          "internationalRouteFlag"
        ].value
          ? "Y"
          : "N",
        internationalRouteAllowed: this.configurationForm[
          "internationalRouteFlag"
        ].value
          ? "Y"
          : "N",
        templateCheck: this.configurationForm["templateCheckFlag"].value
          ? "Y"
          : "N",
        fcmFlag: this.configurationForm["fcmCheckFlag"].value
          ? "Y"
          : "N",
        templateApprovalReq: "Y",
        senderidCheck: this.configurationForm["senderIdCheckFlag"].value
          ? "Y"
          : "N",
        senderidApprovalReq: "N",
        longMessageAllowed: this.configurationForm["longMessageSupportFlag"]
          .value
          ? "Y"
          : "N",
        unicodeSupport: this.configurationForm["unicodeMessageSupportFlag"]
          .value
          ? "Y"
          : "N",
        dlrReportsReq: this.configurationForm["DLRReportRequestFlag"].value
          ? "Y"
          : "N",
        userDlrCallbackDetails: {
          primaryUrl:
            this.configurationForm["DLRReportRequestFlag"].value &&
              this.configurationForm["DLRCallbackPrimaryURL"].value
              ? this.configurationForm["DLRCallbackPrimaryURL"].value
              : null,
          secondaryUrl:
            this.configurationForm["DLRReportRequestFlag"].value &&
              this.configurationForm["DLRCallbackSecondaryURL"].value
              ? this.configurationForm["DLRCallbackSecondaryURL"].value
              : null,
          topologyMode:
            this.configurationForm["DLRReportRequestFlag"].value &&
              this.configurationForm["DLRCallbackTopology"].value
              ? Number(this.configurationForm["DLRCallbackTopology"].value)
              : 0,
          maxConnection:
            this.configurationForm["DLRReportRequestFlag"].value &&
              this.configurationForm["DLRCallbackMaxConnectionCount"].value
              ? Number(
                this.configurationForm["DLRCallbackMaxConnectionCount"].value
              )
              : 0,
          retryFlag: this.configurationForm["DLRReportRequestFlag"].value &&
            this.configurationForm.DLRCallbackRetryFlag.value
            ? 'Y' : 'N',
          maxRetryCount:
            this.configurationForm["DLRReportRequestFlag"].value &&
              this.configurationForm["DLRCallbackRetryFlag"].value &&
              this.configurationForm["DLRCallbackRetryCount"].value
              ? Number(
                this.configurationForm["DLRCallbackRetryCount"].value
              )
              : 0,
          retryDetails:
            this.configurationForm["DLRReportRequestFlag"].value &&
              this.configurationForm["DLRCallbackRetryFlag"].value &&
              this.retryDetailsString
              ? this.retryDetailsString
              : ""
        },
        accessJson: "1",
        permissionJson: permissionJson,
        solaceKey: null,
        createdBy: this.commonService.getUserName(),
        expiryDate: null,
        skipRouting: this.skipConfigurations ? "Y" : "N",
        loginId: this.commonService.getUser(),
        userId: this.userId == -1 ? 0 : this.userId,
        campApprovalReq: this.selectedRole['name'] != "ROLE_CAMPAIGN"? "NA":campApprovalRe,
      };
      if (this.userId == -1) {
        this.manageUserService
          .saveUser(finalObj, '0')// this.authorizationForm["transactionId"].value
          .subscribe(
            data => {
              this.disableSubmit = false;
              this.spinner.hide();
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.commonService.showSuccessToast(
                  "User created successfully"
                );
                this.router.navigate(["/main/manage-user/new-view-user"]);
              } else {
                this.commonService.showErrorToast(data['result']['userMsg']);
              }
            },
            error => {
              this.disableSubmit = false;
            }
          );
      } else {
        this.manageUserService
          .updateUser(
            finalObj,
            '0'//this.authorizationForm["transactionId"].value
          )
          .subscribe(
            data => {
              this.disableSubmit = false;
              this.spinner.hide();
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.commonService.showSuccessToast(
                  "User updated successfully"
                );
                this.router.navigate(["/main/manage-user/new-view-user"]);
              } else {
                this.commonService.showErrorToast(data['result']['userMsg']);
              }
            },
            error => {
              this.disableSubmit = false;
            }
          );
      }
    }
  }

  isValidIpOrDomain(selectedField) {
    let inputUrl;
    if (selectedField == 1) {
      this.configurationForm.DLRCallbackPrimaryURL.clearValidators();
      this.configurationForm.DLRCallbackPrimaryURL.updateValueAndValidity();
      inputUrl = this.configurationForm.DLRCallbackPrimaryURL.value;
    }
    else if (selectedField == 2) {
      this.configurationForm.DLRCallbackSecondaryURL.clearValidators();
      this.configurationForm.DLRCallbackSecondaryURL.updateValueAndValidity();
      inputUrl = this.configurationForm.DLRCallbackSecondaryURL.value;
    }

    if (inputUrl.startsWith("http"))
      inputUrl = inputUrl.split('://')[1];

    if (inputUrl.match(/[a-z]/i)) {
      if (selectedField == 1) {
        this.configurationForm.DLRCallbackPrimaryURL.setValidators([Validators.required, Validators.pattern(this.commonService.patterns.url)])
        this.configurationForm.DLRCallbackPrimaryURL.updateValueAndValidity();
      } else if (selectedField == 2) {
        this.configurationForm.DLRCallbackSecondaryURL.setValidators([Validators.required, Validators.pattern(this.commonService.patterns.url)])
        this.configurationForm.DLRCallbackSecondaryURL.updateValueAndValidity();
      }
    } else {
      if (selectedField == 1) {
        this.configurationForm.DLRCallbackPrimaryURL.setValidators([Validators.required, Validators.pattern(this.commonService.patterns.ipWithProtocol)])
        this.configurationForm.DLRCallbackPrimaryURL.updateValueAndValidity();
      } else if (selectedField == 2) {
        this.configurationForm.DLRCallbackSecondaryURL.setValidators([Validators.required, Validators.pattern(this.commonService.patterns.ipWithProtocol)])
        this.configurationForm.DLRCallbackSecondaryURL.updateValueAndValidity();
      }
    }
  }

  topologyChanged(value) {
    if (value == 1) {
      this.configurationForm["DLRCallbackSecondaryURL"].clearValidators();
      this.configurationForm["DLRCallbackSecondaryURL"].setValidators([
        Validators.required,
        Validators.pattern(this.commonService.patterns.url)
      ]);
      this.configurationForm[
        "DLRCallbackSecondaryURL"
      ].updateValueAndValidity();
    } else {
      this.configurationForm["DLRCallbackSecondaryURL"].clearValidators();
      this.configurationForm["DLRCallbackSecondaryURL"].setValidators([
        Validators.pattern(this.commonService.patterns.url)
      ]);
      this.configurationForm[
        "DLRCallbackSecondaryURL"
      ].updateValueAndValidity();
    }
  }

  configurationFormFlagChanged(flagName, fields) {
    this.retryCountList.length == 0 ? this.retryCountList = this.manageUserService.getRetryCountList() : "";
    if (this.configurationForm[flagName].value) {
      for (let x = 0; x < fields.length; x++)
        this.configurationForm[fields[x]].enable();
    } else {
      for (let x = 0; x < fields.length; x++)
        this.configurationForm[fields[x]].disable();
    }
  }

  setRouteObjectDetails() {
    let retryDetails = [];
    for (let x = 0; x < this.configurationForm['DLRCallbackRetryIntervalArray']['length']; x++) {
      let y = x + 1;
      if (y < this.configurationForm['DLRCallbackRetryIntervalArray']['length']) {
        if (Number(this.configurationForm['DLRCallbackRetryIntervalArray']['controls'][y]['controls']["DLRCallbackRetryInterval"].value) <= Number(this.configurationForm['DLRCallbackRetryIntervalArray']['controls'][x]['controls']["DLRCallbackRetryInterval"].value)) {
          this.commonService.showErrorToast(
            "Duration of retry interval " + ++y + " should be greater than retry interval " + ++x
          );
          this.currentStep = 2;
          return;
        }
      }
      retryDetails.push(
        Number(this.configurationForm['DLRCallbackRetryIntervalArray']['controls'][x]['controls']["DLRCallbackRetryInterval"].value)
      );
      this.retryDetailsString = retryDetails.join(',');
    }
  }

  roleChanged() {
    this.personalInfoForm["showMsg"].setValue(false);
    this.selectedRole = this.roleList.find(
      x => x["id"] == this.personalInfoForm["role"].value
    );
    console.log("Selected Role...." + JSON.stringify(this.selectedRole));
    this.dynamicFieldsOnRoleChanged(this.selectedRole);
    let pJson = JSON.parse(this.selectedRole["permissionJson"]);
    if (this.exitingFlag && this.userId != -1) {
      this.exitingFlag = !this.exitingFlag
      pJson = JSON.parse(this.userInfo['permissionJson']);
    }
    console.log("Permission JSON....." + JSON.stringify(pJson));
    this.selectedU = this.selectedRole['name'];

    // NOT ABLE TO CHANGE ROLE IF HOD and FINAL APPROVAL ALREADY ASSIGN TO OTHER USER.
    if (this.selectedU == "ROLE_HOD" || this.selectedU == "ROLE_MARKETING") {
      let obj = {
        searchFor: this.userInfo["username"],
        type: this.selectedU == "ROLE_HOD" ? 1 : 2
      };
      this.spinner.show();
      this.manageUserService.checkUserType(obj).subscribe(data => {
        this.spinner.hide();
        if (data["data"] > 0) {
          this.personalInfoForm["role"].disable();
          this.isUserUpdated = false;
        } else {
          this.personalInfoForm["role"].enable();
          this.isUserUpdated = true;
        }
      });
    }
    this.personalInfoForm.ipWhitelist.clearValidators();

    (this.selectedU == 'ROLE_CRM' || this.selectedU == 'ROLE_HOD')
      ? this.personalInfoForm.ipWhitelist.setValidators([Validators.pattern(this.commonService.patterns.commaSeparatedValidIP)])
      : this.personalInfoForm.ipWhitelist.setValidators([Validators.required, Validators.pattern(this.commonService.patterns.commaSeparatedValidIP)])
    this.personalInfoForm.ipWhitelist.updateValueAndValidity();

    if (this.selectedU == 'ROLE_CRM') {
      this.personalInfoForm["showMsg"].setValue(true);
      this.accessPermissionViewListCRM.forEach(element => {
        let val = "H";
        if (pJson[element["formControlName"]] && !element["isSubModule"]) {
          val = pJson[element["formControlName"]];
          this.authorizationForm[element["formControlName"]].setValue(val);
        } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
          element['subModuleList'].forEach(ele => {
            val = "H";
            if (pJson[ele["formControlName"]]) {
              val = pJson[ele["formControlName"]];
            }
            this.authorizationForm[ele["formControlName"]].setValue(val);
          });
        }
      });
    } else if (this.selectedU == 'ROLE_CAMPAIGN') {
      if(this.userInfo['campApprovalReq'] =="NA"){
        this.personalInfoForm['campApprovalReq'].setValue(true);
      }
      this.accessPermissionViewListCampaign.forEach(element => {
        let val = "H";
        if (pJson[element["formControlName"]] && !element["isSubModule"]) {
          val = pJson[element["formControlName"]];
           if(element["formControlName"]=='smsTemplateManagement'){
            //this.exitingFlag = false; 
            val = this.userId == -1?"RW":pJson[element["formControlName"]];
           }
          this.authorizationForm[element["formControlName"]].setValue(val);
        } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
          element['subModuleList'].forEach(ele => {
            val = "H";
            if (pJson[ele["formControlName"]]) {
              val = pJson[ele["formControlName"]];
            }
            this.authorizationForm[ele["formControlName"]].setValue(val);
          });
        }
      });
    }
    else if (this.selectedU == 'ROLE_ADMIN') {
      debugger
      this.accessPermissionViewListAdmin.forEach(element => {
        let val = "H";
        if (pJson[element["formControlName"]] && !element["isSubModule"]) {
          debugger
          val = pJson[element["formControlName"]];
          this.authorizationForm[element["formControlName"]].setValue(val);
        } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
          debugger
          element['subModuleList'].forEach(ele => {
            val = "H";
            if (pJson[ele["formControlName"]]) {
              val = pJson[ele["formControlName"]];
            }
            this.authorizationForm[ele["formControlName"]].setValue(val);
            debugger
          });
        }
      });
    }
    else if (this.selectedU == 'ROLE_MARKETING') {
      this.accessPermissionViewListMkt.forEach(element => {
        let val = "H";
        if (pJson[element["formControlName"]] && !element["isSubModule"]) {
          val = pJson[element["formControlName"]];
          this.authorizationForm[element["formControlName"]].setValue(val);
        } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
          element['subModuleList'].forEach(ele => {
            val = "H";
            if (pJson[ele["formControlName"]]) {
              val = pJson[ele["formControlName"]];
            }
            this.authorizationForm[ele["formControlName"]].setValue(val);
          });
        }
      });
    }
    else if (this.selectedU == 'ROLE_API') {
      this.accessPermissionViewListAPI.forEach(element => {
        let val = "H";
        if (pJson[element["formControlName"]] && !element["isSubModule"]) {
          val = pJson[element["formControlName"]];
          this.authorizationForm[element["formControlName"]].setValue(val);
        } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
          element['subModuleList'].forEach(ele => {
            val = "H";
            if (pJson[ele["formControlName"]]) {
              val = pJson[ele["formControlName"]];
            }
            this.authorizationForm[ele["formControlName"]].setValue(val);
          });
        }
      });
    }
    else if (this.selectedU == 'ROLE_HOD') {
      this.accessPermissionViewListHOD.forEach(element => {
        let val = "H";
        if (pJson[element["formControlName"]] && !element["isSubModule"]) {
          val = pJson[element["formControlName"]];
          this.authorizationForm[element["formControlName"]].setValue(val);
        } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
          element['subModuleList'].forEach(ele => {
            val = "H";
            if (pJson[ele["formControlName"]]) {
              val = pJson[ele["formControlName"]];
            }
            this.authorizationForm[ele["formControlName"]].setValue(val);
          });
        }
      });
    }
    else {
      this.accessPermissionViewList.forEach(element => {
        let val = "H";
        if (pJson[element["formControlName"]] && !element["isSubModule"]) {
          val = pJson[element["formControlName"]];
          this.authorizationForm[element["formControlName"]].setValue(val);
        } else if (pJson[element["formControlName"]] && element["isSubModule"]) {
          element['subModuleList'].forEach(ele => {
            val = "H";
            if (pJson[ele["formControlName"]]) {
              val = pJson[ele["formControlName"]];
            }
            this.authorizationForm[ele["formControlName"]].setValue(val);
          });
        }
      });
    }
    //COMMENTED ON MAR03,2020
    if (this.selectedU != 'ROLE_API' && this.selectedU != 'ROLE_CAMPAIGN') {
      if (this.stepOne.formCompleted)
        this.mainForm.controls["configuration"].reset({
          category: "",
          //channel: "",
          tps: "",
          spamFilterLevel: "",
          duplicateCheckFlag: false,
          templateCheckFlag: false,
          fcmCheckFlag: false,
          internationalRouteFlag: false,
          senderIdCheckFlag: false,
          DLRReportRequestFlag: false,
          longMessageSupportFlag: false,
          unicodeMessageSupportFlag: false,
          showMsg: false,
          showFullMsg: false,
          maxMessagePartLength_bulk: "",
          maxFileSize_bulk: "",
          maxRecipientCount_bulk: "",
          maxSenderId_bulk: "",
          maxTemplate_bulk: "",
          maxHttpConnectionCount: "",
          DLRCallbackTopology: "",
          DLRCallbackPrimaryURL: "",
          DLRCallbackSecondaryURL: "",
          DLRCallbackMaxConnectionCount: "",
          DLRCallbackRetryCount: "",
          DLRCallbackRetryIntervalArray: ""
        });
    }
  }

  channelChanged(value) {
    if (value == this.HTTP) {
      this.configurationForm["maxHttpConnectionCount"].enable();
      this.configurationForm["maxFileSize_bulk"].disable();
      this.configurationForm["maxRecipientCount_bulk"].disable();
      this.configurationForm["maxDailyThresholdLimit_bulk"].disable();
    } else if (value == this.BULK) {
      this.configurationForm["maxHttpConnectionCount"].disable();
      this.configurationForm["maxFileSize_bulk"].enable();
      this.configurationForm["maxRecipientCount_bulk"].enable();
      this.configurationForm["maxDailyThresholdLimit_bulk"].enable();
    }
  }

  dynamicFieldsOnRoleChanged(userRole) {
    if (userRole["isSystemRole"] == this.FLAG_YES) {
      this.personalInfoForm["department"].clearValidators();
      this.personalInfoForm["department"].setValue('');
      this.personalInfoForm["department"].updateValueAndValidity();
      this.personalInfoForm["departmentHod"].clearValidators();
      this.personalInfoForm["departmentHod"].setValue([]);
      this.personalInfoForm["departmentHod"].updateValueAndValidity();
      this.personalInfoForm["finalApprover"].clearValidators();
      this.personalInfoForm["finalApprover"].setValue([]);
      this.personalInfoForm["finalApprover"].updateValueAndValidity();
      this.personalInfoForm["category"].clearValidators();
      this.personalInfoForm["category"].setValue([]);
      this.personalInfoForm["category"].updateValueAndValidity();
    } else {
      if (this.userId == -1) {    //New User
        this.departmentList = this.constDepartmentList;
        this.userDepartmentSetting =       {
          singleSelection: true,
          selectAllText: "Select All",
          unSelectAllText: "UnSelect All",
          allowSearchFilter: true,
          closeDropDownOnSelection: false,
          idField: 'id',
          textField: 'department',
        }
        this.personalInfoForm["department"].setValidators([Validators.required])
        this.personalInfoForm["department"].updateValueAndValidity();
        if (userRole["name"] == 'ROLE_API' || userRole["name"] == 'ROLE_CAMPAIGN') {
          this.personalInfoForm["departmentHod"].setValidators([Validators.required]);
          this.personalInfoForm["departmentHod"].updateValueAndValidity();
          if (userRole["name"] == 'ROLE_API') {
            this.configurationForm["templateCheckFlag"].setValue('Y');
            this.configurationForm["templateCheckFlag"].setValidators([Validators.required]);
            this.configurationForm["templateCheckFlag"].updateValueAndValidity();
            this.tempcheckDis = true;
            this.configurationForm["spamFilterLevel"].clearValidators();
            this.configurationForm["spamFilterLevel"].setValue('L');
            this.configurationForm["spamFilterLevel"].updateValueAndValidity();
            this.personalInfoForm["finalApprover"].clearValidators();
            this.personalInfoForm["finalApprover"].setValue([]);
            this.personalInfoForm["finalApprover"].updateValueAndValidity();
            this.personalInfoForm["category"].clearValidators();
            this.personalInfoForm["category"].setValue([]);
            this.personalInfoForm["category"].updateValueAndValidity();
          }
          if (userRole["name"] == 'ROLE_CAMPAIGN') {
            this.configurationForm["templateCheckFlag"].clearValidators();
           //this.configurationForm["templateCheckFlag"].setValue('N');
            this.configurationForm["templateCheckFlag"].setValue('Y');
            this.tempcheckDis = true;
            this.configurationForm["templateCheckFlag"].updateValueAndValidity();
            this.configurationForm["spamFilterLevel"].setValidators([Validators.required]);
            this.configurationForm["spamFilterLevel"].updateValueAndValidity();
            this.personalInfoForm["category"].setValue("");
            this.personalInfoForm["category"].setValidators([Validators.required]);
            this.personalInfoForm["category"].updateValueAndValidity();
            this.personalInfoForm["finalApprover"].setValue("");
            this.personalInfoForm["finalApprover"].setValidators([Validators.required]);
            this.personalInfoForm["finalApprover"].updateValueAndValidity();
            this.manageUserService.getFinalApproverList(this.finalApproverReq).subscribe(response => {
              if (response["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                response["data"].sort((a,b) => a.username.localeCompare(b.username));
                this.finalApprovers = response["data"];
                this.finalApproverSetting = {
                  singleSelection: false,
                  idField: 'username',
                  textField: 'username',
                  selectAllText: 'Select All',
                  unSelectAllText: 'UnSelect All',
                  itemsShowLimit: 3,
                  allowSearchFilter: true,
                  defaultOpen: false
                };
              }
            });
            this.dropdownSettings = {
              singleSelection: false,
              idField: 'id',
              textField: 'category',
              selectAllText: 'Select All',
              unSelectAllText: 'UnSelect All',
              itemsShowLimit: 3,
              allowSearchFilter: true,
              defaultOpen: false
            };

            this.manageUserService.getCategoryList().subscribe(data => {
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.categoryList = data["data"];
                this.categoryListForCampUser = this.categoryList.filter(
                  x => x["id"] == 3 || x["id"] == 4
                );
                this.dropdownList = this.categoryListForCampUser;
              }
            });
          }
        }
        else if (userRole["name"] == 'ROLE_HOD' || userRole["name"] == 'ROLE_MARKETING') {
          this.personalInfoForm["departmentHod"].clearValidators();
          this.personalInfoForm["departmentHod"].setValue([]);
          this.personalInfoForm["departmentHod"].updateValueAndValidity();
          this.personalInfoForm["department"].setValidators([Validators.required])
          this.personalInfoForm["department"].updateValueAndValidity();
          this.personalInfoForm["finalApprover"].clearValidators();
          this.personalInfoForm["finalApprover"].setValue([]);
          this.personalInfoForm["finalApprover"].updateValueAndValidity();
          this.personalInfoForm["category"].clearValidators();
          this.personalInfoForm["category"].setValue([]);
          this.personalInfoForm["category"].updateValueAndValidity();
        }
      }
      else {                 //Update
        console.log("HOD BEFORE FILTER...." + JSON.stringify(this.userData1));
        this.userData1 = this.userData1.filter(
          x => x["name"] != this.userInfo["username"]
        );
        this.departmentList = this.constDepartmentList;
        this.userDepartmentSetting =       {
          singleSelection: true,
          selectAllText: "Select All",
          unSelectAllText: "UnSelect All",
          allowSearchFilter: true,
          closeDropDownOnSelection: false,
          idField: 'id',
          textField: 'department',
        };
        this.personalInfoForm["department"].setValidators([Validators.required])
        this.personalInfoForm["department"].updateValueAndValidity();
        if (userRole["name"] == 'ROLE_API' || userRole["name"] == 'ROLE_CAMPAIGN') {

          this.personalInfoForm["departmentHod"].setValidators([Validators.required]);
          this.personalInfoForm["departmentHod"].updateValueAndValidity();
          if (userRole["name"] == 'ROLE_API') {
            this.configurationForm["templateCheckFlag"].setValidators([Validators.required]);
            this.configurationForm["templateCheckFlag"].updateValueAndValidity();
            this.configurationForm["spamFilterLevel"].clearValidators();
            this.configurationForm["spamFilterLevel"].setValue('L');
            this.configurationForm["spamFilterLevel"].updateValueAndValidity();
            this.personalInfoForm["finalApprover"].clearValidators();
            this.personalInfoForm["finalApprover"].setValue([]);
            this.personalInfoForm["finalApprover"].updateValueAndValidity();
            this.personalInfoForm["category"].clearValidators();
            this.personalInfoForm["category"].setValue([]);
            this.personalInfoForm["category"].updateValueAndValidity();
          }
          if (userRole["name"] == 'ROLE_CAMPAIGN') {
            this.configurationForm["templateCheckFlag"].clearValidators();
            this.configurationForm["templateCheckFlag"].setValue('N');
            this.configurationForm["templateCheckFlag"].updateValueAndValidity();
            this.configurationForm["spamFilterLevel"].setValidators([Validators.required]);
            this.configurationForm["spamFilterLevel"].updateValueAndValidity();

            let a = JSON.parse(this.userInfo['campaignUserCategories']);
            this.selectedItems = a;

            this.dropdownSettings = {
              singleSelection: false,
              idField: 'id',
              textField: 'category',
              selectAllText: 'Select All',
              unSelectAllText: 'UnSelect All',
              itemsShowLimit: 3,
              allowSearchFilter: true,
              defaultOpen: false
            };

            this.manageUserService.getCategoryList().subscribe(data => {
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.categoryList = data["data"];
                this.categoryListForCampUser = this.categoryList.filter(
                  x => x["id"] == 3 || x["id"] == 4
                );
                this.dropdownList = this.categoryListForCampUser;
              }
            });
            this.personalInfoForm["category"].setValidators([Validators.required]);
            this.personalInfoForm["category"].updateValueAndValidity();
            this.existingApprovals = this.userInfo['finalApprover'];
            this.personalInfoForm['campApprovalReq'].setValue("NA");
            if(this.userInfo['campApprovalReq'] !="NA"){
              this.personalInfoForm['campApprovalReq'].setValue(this.userInfo['campApprovalReq']=="Y"?true:false);
            }
            this.existingApprovals = this.existingApprovals.substring(1, this.existingApprovals.length - 1);
            if (this.existingApprovals != "NA" && this.existingApprovals != "") {
              var existingFinal = this.existingApprovals.split(',');
            }
            if (existingFinal) {
              this.selectedApproval = existingFinal;
              this.personalInfoForm["finalApprover"].setValue(this.selectedApproval);
            } else {
              this.selectedApproval = []
              this.personalInfoForm["finalApprover"].setValue([]);
            }
            this.personalInfoForm["finalApprover"].setValidators([Validators.required]);
            this.personalInfoForm["finalApprover"].updateValueAndValidity();
            this.manageUserService.getFinalApproverList(this.finalApproverReq).subscribe(response => {
              if (response["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                response["data"].sort((a,b) => a.username.localeCompare(b.username));
                this.finalApprovers = response["data"];
                console.log("FINAL APPROVAL BEFORE FILTER...." + JSON.stringify(this.finalApprovers));
                this.finalApprovers = this.finalApprovers.filter(
                  x => x["username"] != this.userInfo["username"]
                );
                this.finalApproverSetting = {
                  singleSelection: false,
                  idField: 'username',
                  textField: 'username',
                  selectAllText: 'Select All',
                  unSelectAllText: 'UnSelect All',
                  itemsShowLimit: 3,
                  allowSearchFilter: true,
                  defaultOpen: false
                };
              }
            });
          }
        }
        else if (userRole["name"] == 'ROLE_HOD' || userRole["name"] == 'ROLE_MARKETING') {
          this.personalInfoForm["departmentHod"].clearValidators();
          this.personalInfoForm["departmentHod"].setValue([]);
          this.personalInfoForm["departmentHod"].updateValueAndValidity();
          this.personalInfoForm["department"].setValidators([Validators.required])
          this.personalInfoForm["department"].updateValueAndValidity();
          this.personalInfoForm["finalApprover"].clearValidators();
          this.personalInfoForm["finalApprover"].setValue([]);
          this.personalInfoForm["finalApprover"].updateValueAndValidity();
          this.personalInfoForm["category"].clearValidators();
          this.personalInfoForm["category"].setValue([]);
          this.personalInfoForm["category"].updateValueAndValidity();
        }
      }
    }
  }

  modulePermissionChanged(item, permission) {
    if (permission == "H") {
      let subModule;
      if (this.selectedU == 'ROLE_CRM') {
        subModule = this.accessPermissionViewListCRM.find(
          x => x["formControlName"] == item["formControlName"] && x["isSubModule"]
        );
      }
      if (this.selectedU == 'ROLE_HOD') {
        subModule = this.accessPermissionViewListHOD.find(
          x => x["formControlName"] == item["formControlName"] && x["isSubModule"]
        );
      }
      else if (this.selectedU == 'ROLE_CAMPAIGN') {
        subModule = this.accessPermissionViewListCampaign.find(
          x => x["formControlName"] == item["formControlName"] && x["isSubModule"]
        );
      }
      else if (this.selectedU == 'ROLE_ADMIN') {
        subModule = this.accessPermissionViewListAdmin.find(
          x => x["formControlName"] == item["formControlName"] && x["isSubModule"]
        );
      }
      else if (this.selectedU == 'ROLE_MARKETING') {
        subModule = this.accessPermissionViewListMkt.find(
          x => x["formControlName"] == item["formControlName"] && x["isSubModule"]
        );
      }
      else if (this.selectedU == 'ROLE_API') {
        subModule = this.accessPermissionViewListAPI.find(
          x => x["formControlName"] == item["formControlName"] && x["isSubModule"]
        );
      }else if(this.selectedU == 'ROLE_DLT'){
        subModule = this.accessPermissionViewListDLT.find(
          x => x["formControlName"] == item["formControlName"] && x["isSubModule"]
        );
      }else {
        subModule = this.accessPermissionViewList.find(
          x => x["formControlName"] == item["formControlName"] && x["isSubModule"]
        );
      }

      if (subModule) {
        for (var x = 0; x <= subModule["subModuleList"].length - 1; x++) {
          this.authorizationForm[
            subModule["subModuleList"][x]["formControlName"]
          ].setValue("H");
        }
      }
    }
  }

  subModulePermissionChanged(item, permission) {
    if (
      (permission == "RW" || permission == "R") &&
      this.authorizationForm[item.formControlName].value == "H"
    )
      this.authorizationForm[item.formControlName].setValue("R");
  }



  // skipConfigurationsMethod() {
  //   Swal.fire({
  //     title: "Are you sure?",
  //     text: "User will not able to access all features on site!",
  //     type: "warning",
  //     showCancelButton: true,
  //     confirmButtonColor: "#3085d6",
  //     cancelButtonColor: "#d33",
  //     confirmButtonText: "Ok, Proceed"
  //   }).then(result => {
  //     if (result.value) {
  //       this.skipConfigurations = true;
  //       this.stepTwo.formSubmitted = false;
  //       this.currentStep = 3;
  //       if (!this.stepTwo.formCompleted) {
  //         this.stepTwo.formCompleted = this.mainForm.controls.configuration.valid;
  //       }
  //     }
  //   });
  // }

  cancel() {
    Swal.fire({
      title: "Are you sure?",
      text: "All information will be discarded!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ok, Leave this page"
    }).then(result => {
      if (result.value) {
        this.router.navigate(["/main/manage-user/new-view-user"]);
      }
    });
  }

  next() {
    if (this.currentStep == 1) {
      if(this.selectedRole['name'] == 'ROLE_CAMPAIGN' && !this.personalInfoForm['campApprovalReq'].value){
        this.personalInfoForm["finalApprover"].setValue(["NA"])
        this.personalInfoForm["finalApprover"].setValidators([]);
        this.personalInfoForm["finalApprover"].updateValueAndValidity();
      }else if(this.selectedRole['name'] == 'ROLE_CAMPAIGN'){
        this.personalInfoForm["finalApprover"].setValidators([Validators.required]);
        this.personalInfoForm["finalApprover"].updateValueAndValidity();
      }
      this.stepOne.formSubmitted = true;
      if (this.mainForm.controls.personalInfo.valid) {
        if (
          this.selectedRole["configurationFlag"] == "R" ||
          this.selectedRole["configurationFlag"] == "N"
        ) {
          if (this.selectedRole['name'] == "ROLE_API") {
            this.stepFour.formSubmitted = false;
            this.currentStep = 4;
            this.manageUserService.getCategoryList().subscribe(data => {
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.categoryList = data["data"];
              }
            });
          } else {
            this.currentStep = 2;
            this.skipConfigurations = false;
          }
          if (this.personalInfoForm["role"].value == 4) {
            this.configurationForm["channel"].setValue(1);
          } else if (this.personalInfoForm["role"].value == 5) {
            this.configurationForm["channel"].setValue(2);
          }
          this.channelChanged(this.configurationForm["channel"].value);
          if (!this.stepOne.formCompleted) {
            this.stepOne.formCompleted = this.mainForm.controls.personalInfo.valid;
            this.topologyList = this.manageUserService.getTopologyList();
          }
        } else if (this.selectedRole["configurationFlag"] == "S") {
          this.skipConfigurations = true;
          this.currentStep = 3;
        }
      }
    } else if (this.currentStep == 4) {
      this.stepFour.formSubmitted = true;
      if (this.mainForm.controls.appCategory.valid) {
        this.currentStep = 2;
        this.skipConfigurations = false;
        if (!this.stepFour.formCompleted) {
          this.stepFour.formCompleted = this.mainForm.controls.personalInfo.valid;
        }
      }
    }
    else if (this.currentStep == 2) {
      this.stepTwo.formSubmitted = true;
      if (this.mainForm.controls.configuration.valid) {
        this.skipConfigurations = false;
        this.currentStep = 3;
        if (this.configurationForm.DLRCallbackRetryFlag.value && this.configurationForm.DLRReportRequestFlag.value) {
          this.setRouteObjectDetails();
        }
        if (!this.stepTwo.formCompleted) {
          this.stepTwo.formCompleted = this.mainForm.controls.appCategory.valid;
        }
      }
    }
  }

  previous() {
    if (this.currentStep == 3) {
      if (
        this.selectedRole["configurationFlag"] == "R" ||
        this.selectedRole["configurationFlag"] == "N"
      ) {
        this.currentStep = 2;
      }
      if (this.selectedRole["configurationFlag"] == "S") {
        this.currentStep = 1;
      }
    } else if (this.currentStep == 4) {
      this.currentStep = 1;
    } else if (this.currentStep == 2) {
      if (this.selectedRole['name'] == "ROLE_API") {
        this.currentStep = 4;
      } else {
        this.currentStep = 1;
      }
    }
  }

  retryCountChanged() {
    let retryCount = this.configurationForm.DLRCallbackRetryCount.value;
    let retryIntervalControl = <FormArray>(
      this.configurationForm.DLRCallbackRetryIntervalArray
    );

    for (let x = retryIntervalControl.length - 1; x >= 0; x--) {
      retryIntervalControl.removeAt(x);
    }

    for (let x = 0; x < retryCount; x++) {
      retryIntervalControl.push(
        this.formBuilder.group({
          DLRCallbackRetryInterval: [
            "",
            [
              Validators.required,
              Validators.min(1),
              Validators.max(900),
              Validators.pattern(this.commonService.patterns.numberOnly)
            ]
          ]
        })
      );
    }
  }
}