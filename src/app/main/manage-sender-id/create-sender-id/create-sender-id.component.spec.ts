import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSenderIdComponent } from './create-sender-id.component';

describe('CreateSenderIdComponent', () => {
  let component: CreateSenderIdComponent;
  let fixture: ComponentFixture<CreateSenderIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSenderIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSenderIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
