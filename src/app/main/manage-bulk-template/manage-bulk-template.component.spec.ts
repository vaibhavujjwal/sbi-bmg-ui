import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageBulkTemplateComponent } from './manage-bulk-template.component';

describe('ManageAggregatorComponent', () => {
  let component: ManageBulkTemplateComponent;
  let fixture: ComponentFixture<ManageBulkTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageBulkTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageBulkTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
