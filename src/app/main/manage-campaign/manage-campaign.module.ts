import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ManageCampaignComponent} from './manage-campaign.component';
import {ScheduleCampaignComponent} from './schedule-campaign/schedule-campaign.component';
import {ViewScheduledCampaignComponent} from './view-scheduled-campaign/view-scheduled-campaign.component';
import { routing } from './manage-campaign.routing';
import { SharedModule } from "../../common/shared.modules";
import { ManageCampaignService } from "../manage-campaign/manage-campaign.service";
import { PendingApprovalComponent } from './pending-approval/pending-approval.component';
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatButtonModule,
  MatNativeDateModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from '../../validators/format-datepicker';

@NgModule({
  imports: [
    CommonModule,
    routing,
    SharedModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatButtonModule,
    MatNativeDateModule,
    MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
    MatSortModule, MatTableModule

  ],
  declarations: [ManageCampaignComponent, ScheduleCampaignComponent, ViewScheduledCampaignComponent, PendingApprovalComponent],
  providers: [ManageCampaignService,
      {provide: DateAdapter, useClass: AppDateAdapter},
      {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class ManageCampaignModule { }
