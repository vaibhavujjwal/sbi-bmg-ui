import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common/common.service";
import { MODULE } from "../../common/common.const";

@Component({
  selector: 'app-manage-mo-route',
  templateUrl: './manage-mo-route.component.html',
  styleUrls: ['./manage-mo-route.component.scss']
})
export class ManageMoRouteComponent implements OnInit {

  MODULES = MODULE;
  constructor( public commonService: CommonService) { }

  ngOnInit() {
  }

}
