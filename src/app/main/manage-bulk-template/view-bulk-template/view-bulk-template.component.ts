import { Component, OnInit, ViewChild } from '@angular/core';
import { ManageBulkTemplateService } from "../manage-bulk-template.service";
import { NgxSpinnerService } from "ngx-spinner";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { CommonService } from "../../../common/common.service";
import { RESPONSE, MODULE } from "../../../common/common.const";
import { ExcelService } from "../../../common/excel.service";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { TitleCasePipe } from '@angular/common';

export interface templateListTypes {
  Actions: any;
  name: string;
  contentType: any;
  content: any;
  username: string;
  creationDate: any;
  approvedBy: any;
  status: any;
}

@Component({
  selector: 'app-view-bulk-template',
  templateUrl: './view-bulk-template.component.html',
  styleUrls: ['./view-bulk-template.component.scss']
})

export class ViewBulkTemplateComponent implements OnInit {
  MODULES = MODULE;
  modalRef: any;
  templateList = [];
  formSubmitted: boolean = false;
  math = Math;
  deleteReq = {};
  templateID: any;
  statusShow: any;
  recordName: any;
  smsTemplateForm: FormGroup;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  pageLimit: number;
  searchText: string = "";

  roleName: string = this.commonService.getUserByRole();
  p: number = 1;
  statusForm: FormGroup;
  errorMsgType: any;
  messagePart: any;
  spamKeywordsList = [];
  spamError: boolean = false;
  errorZero: boolean = false;
  errZeroMsg: any;
  errortext: any;
  errorUnicode: any;
  contentTypeVal: any;
  unicodeSupport: any;
  errorMsgSpam: any;
  UnicodeError: boolean = false;
  unicodeFlag: boolean = false;
  showError: boolean = false;
  maxlen: any;
  maxLengthSMS: any;
  categoryList: Array<object> = [];

  displayedColumns: any[] = ['actions',
    'spiceTemplateid',
    'templateIdDlt',
    'name',
    'templateType',
    'contentType',
    'content',
    'username',
    'creationDate',
    'approvedBy',
    'status'];
  displayedColumns1: any[] = [
    'spiceTemplateid',
    'templateIdDlt',
    'name',
    'templateType',
    'contentType',
    'content',
    'username',
    'creationDate',
    'approvedBy',
    'status'];

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private manageBulkTemplateService: ManageBulkTemplateService, private spinner: NgxSpinnerService,
    private modalService: NgbModal, private fb: FormBuilder, private formBuilder: FormBuilder,
     public commonService: CommonService, private excelService: ExcelService,private titlecasePipe:TitleCasePipe) { }

  ngOnInit() {
    this.spinner.show();
    this.getTemplateList();
    if (this.commonService.getUserByRole() == "ROLE_CAMPAIGN")
      this.getSpamKeyword();
    this.initialiseForms();
    this.maxLengthSMS = 160;
  }

  getTemlateCategoryList(){
    this.spinner.show();
    this.manageBulkTemplateService.getCategoryList().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.categoryList = res['data'];
        } else {
          this.categoryList = [];
        }
      });
  }

  initialiseForms() {
    this.smsTemplateForm = this.fb.group({
      contentType: ['', [Validators.required]],
      name: ['', [Validators.required]],
      language: [''],
      content: ['', [Validators.required]],
      user: [''],
      placeHolder: [''],
      temlateCategory: ['', [Validators.required]]
    });
  }

  checkUnicodeSupport() {
    this.unicodeSupport = this.commonService.getUnicodeSupport();
    if (this.unicodeSupport == 'Y') {
      this.unicodeFlag = true;
    } else {
      this.unicodeFlag = false;
    }
  }

  getSpamKeyword() {
    this.spinner.show();
    this.manageBulkTemplateService.getSpamList().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.spamKeywordsList = res['data'];
          console.log("SPAM............." + JSON.stringify(this.spamKeywordsList));
        } else {
          this.spamKeywordsList = [];
        }
      });
  }

  checkSpamKeyword() {
    let templateContent = this.smsTemplateForm.controls["content"].value;
    templateContent = templateContent.split(" ");
    templateContent = templateContent.map(function (value) {
      return value.toUpperCase();
    });
    let matchedKeywordsList = [];
    for (let i = 0; i < this.spamKeywordsList.length; i++) {
      var keywordExist = templateContent.indexOf(this.spamKeywordsList[i].keyword.toUpperCase());
      if (keywordExist != -1) {
        matchedKeywordsList.push(this.spamKeywordsList[i].keyword);
      }
    }
    if (matchedKeywordsList.length > 0) {
      this.spamError = true;
      this.errorMsgSpam = "Keyword(s) '" + matchedKeywordsList.join(",") + "' are not allowed to be used in the SMS content.";
    } else {
      this.spamError = false;
    }
    return matchedKeywordsList.length == 0 ? false : true;
  }

  getTemplateList() {
    this.spinner.show();
    let req = {
      "id": 0,
      "roleName": this.commonService.getUserByRole(),
      "channelId": this.commonService.getChannelId()
    }
    this.manageBulkTemplateService.getTemplateList(req).subscribe(
      res => {
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.spinner.hide();
          this.spinner.show();
          this.templateList = res["data"];
          this.templateList.forEach(element => {
            if (element['contentType'] == '2') {
              try {
                element['content'] = this.commonService.unicodeToText(element['content']);
              }
              catch (err) {
                element['content'] = element['content'];
                console.log("error while coverting unicode ........" + err);
              }
            }
          });
          this.templateList.forEach(element => {
            element['contentType'] == 1 ? element['contentType'] = "Text" : element['contentType'] = "Unicode";

          });

          this.templateList.forEach(element => {
            if (!element['templateIdDlt'] || element['templateIdDlt']==0) {
              element['templateIdDlt'] = 'NA';
            }
          });

          this.templateList.forEach(element => {
            if (element['status']['id'] == 1) {
              element['status']['status'] = 'Enabled';
            } else if (element['status']['id'] == 2) {
              element['status']['status'] = 'Disabled';
            } else if (element['status']['id'] == 0) {
              element['status']['status'] = 'HOD Approval pending';
            } else if (element['status']['id'] == 7) {
              element['status']['status'] = 'Rejected';
            }else{
              element['status']['status'] = element['status']['status'];
            }
          });
          this.templateList.forEach(element => {
            if (element['templateType']=='inf') {
              element['templateType'] = 'Informational';
            }else if (element['templateType']=='otp') {
              element['templateType'] = 'OTP';
            }else if (element['templateType']=='promo') {
              element['templateType'] = 'Promotional';
            }else if (element['templateType']=='txn') {
              element['templateType'] = 'Transactional';
            }
          });

          this.dataSource = new MatTableDataSource(this.templateList);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.spinner.hide();
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.templateList = [];
        }
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.showError = false;
    if (this.dataSource.filteredData.length == 0) {
      this.showError = true;
    }
  }

  confirmDeleteRecord(updatemodal: any, record: any) {
    this.recordName = record.name;
    this.formSubmitted = false;
    this.prepareApprovalForm();
    this.deleteReq = {
      "templateId": record['id'],
    }
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  prepareApprovalForm() {
    this.statusForm = this.formBuilder.group({
      remarks: ["",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)
        ]
      ]
    });
  }

  updateRecord(updatemodal: any, record: any) {
    this.checkUnicodeSupport();
    this.populateValues(record);
    this.getTemlateCategoryList();
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  populateValues(item) {
    if (item.contentType == 'Text') {
      this.smsTemplateForm.controls['contentType'].setValue('1');
    } else {
      this.smsTemplateForm.controls['contentType'].setValue('2');
    }
    this.smsTemplateForm.controls['name'].setValue(item.name);
    this.smsTemplateForm.controls['content'].setValue(item.content);
    let categoryprefile =4;
        if(item.templateType=='otp'){
          categoryprefile = 1;
        }else if(item.templateType=='txn'){
          categoryprefile = 2;
        }else if(item.templateType=='inf'){
          categoryprefile = 3;
        }
    this.smsTemplateForm.controls['temlateCategory'].setValue(categoryprefile);
    this.templateID = item.id;
    this.isDoubleByte();
  }

  confirmStatusRecord(record: any, id) {
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    if (record['status']['status'] == 'Disabled') {
      modalRef.componentInstance.content = 'Are you sure you want to enable ' + record['name'] + ' template?';
    } else if (record['status']['status'] == 'Enabled') {
      modalRef.componentInstance.content = 'Are you sure you want to disable ' + record['name'] + ' template?';
    }
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.changeStatus(record);
        } else {
          document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
        }
      },
      (reason: any) => {
        document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
      });
  }

  confirmUpdateRecoed() {
    this.formSubmitted = true;
    if (this.smsTemplateForm.valid && !this.spamError && !this.UnicodeError && !this.errorZero) {

      let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
      modalRef.componentInstance.content = 'Are you sure you want to update this template?';
      modalRef.componentInstance.leftButton = 'No';
      modalRef.componentInstance.rightButton = 'Yes';
      modalRef.result.then(
        (data: any) => {
          if (data == "yes") {
            this.updateApiTemplateForm();
          }
        },
        (reason: any) => {
        });
      this.formSubmitted = false;
    }
  }

  updateApiTemplateForm() {
    let contentBackSlash = this.smsTemplateForm.controls["content"].value.trim();
    contentBackSlash = contentBackSlash.replace(/"/g, "\&quot;");
    contentBackSlash = contentBackSlash.replace(/>/g, "&gt;");
    contentBackSlash = contentBackSlash.replace(/</g, "&lt;");
    contentBackSlash = contentBackSlash.replace(/>=/g, "&ge;");
    contentBackSlash = contentBackSlash.replace(/<=/g, "&le;");
    contentBackSlash = contentBackSlash.replace(/'/g, "&#39;");

    let tempalatetype ='promo';
        if(this.smsTemplateForm.controls['temlateCategory'].value==1){
          tempalatetype = 'otp';
        }else if(this.smsTemplateForm.controls['temlateCategory'].value==2){
          tempalatetype = 'txn';
        }else if(this.smsTemplateForm.controls['temlateCategory'].value==3){
          tempalatetype = 'inf';
        }

        let dotstarCount = contentBackSlash && contentBackSlash.match(/\.\*/g)?contentBackSlash.match(/\.\*/g).length:'';
        console.log(".* count..."+dotstarCount)

    let reqObj = {
      "contentType": this.smsTemplateForm.controls["contentType"].value,
      "templateName": this.smsTemplateForm.controls["name"].value.trim(),
      "content": contentBackSlash,
      "templateID": this.templateID,
      "templateType": tempalatetype,
      "placeholderCount":dotstarCount
    }
    this.spinner.show();
    this.manageBulkTemplateService.updateAPITemplate(reqObj).subscribe(
      res => {
        this.commonService.showSuccessToast('Your update request has been sent to the DLT User');
        this.formSubmitted = false;
        this.modalRef.close();
        this.getTemplateList();
      });
  }

  maxLengthText(contents) {
    if (this.messagePart == 1) {
      this.errorZero = false;
      this.maxlen = 160;
      this.maxLengthSMS = 160;
    } else if (this.messagePart > 1) {
      this.errorZero = false;
      this.maxlen = 153 * this.messagePart;
      contents.length > 160 ? this.maxLengthSMS = 153 : this.maxLengthSMS = 160;
    } else {
      this.errorZero = true;
      this.errZeroMsg = "Message part length is not configured. Please contact your administrator.";
    }
  }

  maxLengthUnicode(contents) {
    if (this.messagePart == 1) {
      this.errorZero = false;
      this.maxlen = 70;
      this.maxLengthSMS = 70;
    } else if (this.messagePart > 1) {
      this.errorZero = false;
      this.maxlen = 67 * this.messagePart;
      contents.length > 70 ? this.maxLengthSMS = 67 : this.maxLengthSMS = 70;
    } else {
      this.errorZero = true;
      this.errZeroMsg = "Message part length is not configured. Please contact your administrator.";
    }
  }

  isDoubleByte() {
    this.messagePart = this.commonService.getMaxMessageParts();
    let contents = this.smsTemplateForm.controls['content'].value.trim();
    for (var i = 0, n = contents.length; i < n; i++) {
      if (contents.charCodeAt(i) > 255) {
        if (this.unicodeSupport == 'Y') {
          this.smsTemplateForm.controls['contentType'].setValue('2');
          this.UnicodeError = false;
          this.maxLengthUnicode(contents);
        } else {
          this.errorMsgType = "Enter Text, as Unicode is not supported for this user";
          this.UnicodeError = true;
        }
        break;
      } else {
        this.smsTemplateForm.controls['contentType'].setValue('1');
        this.UnicodeError = false;
        this.maxLengthText(contents);
      }
    }
  }

  deleteRecord() {
    this.deleteReq['remarks'] = this.statusForm.controls['remarks'].value;
    this.formSubmitted = true;
    if (this.statusForm.valid) {
      this.spinner.show();
      this.manageBulkTemplateService.deleteTemplate(this.deleteReq).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast(this.recordName + ' template has been deleted');
            this.modalRef.close();
            this.getTemplateList();
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
          this.formSubmitted = false;
        });
    }
  }

  changeStatus(record: any) {
    let req = {
      "id": record['id'],
      "approvedBy": record['approvedBy'],
      "status": record['status']['id'] == 1 ? 2 : 1,
      "dltId":record['templateIdDlt']
    };
    this.spinner.show();
    let callStatusApi = 'updateSattus';
    if(this.commonService.getUserByRole() == "ROLE_DLT"){
      callStatusApi = 'updateDltBUlkTemplate';
    }
    this.manageBulkTemplateService[callStatusApi](req).subscribe(
      res => {
        this.spinner.hide();
        if (record['status']['id'] == 1) {
          this.commonService.showSuccessToast(record['name'] + ' template disabled');
        } else {
          this.commonService.showSuccessToast(record['name'] + ' template enabled');
        }
        this.getTemplateList();
      });
  }

  exportAs(fileType: string) {
    let data = [];
    this.templateList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Template Name"] = element.templateName;
      excelDataObject["Template Content Type"] = element.templateContentType;
      excelDataObject["Template  Text"] = element.template;
      excelDataObject["Assigned To"] = element.username;
      excelDataObject["Created By"] = element.createdBy;
      excelDataObject["Approved By"] = element.approvedBy;
      excelDataObject["Status"] = element.status == 'Enabled' ? 'Enable' : 'Disable';
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'SMS-Template-List');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'SMS-Template-List');
        break;
      default:
        this.excelService.exportAsExcelFile(data, 'SMS-Template-List');
        break;
    }
  }
}