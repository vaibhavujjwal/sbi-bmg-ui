export const restrictPages = ["login", "register"];

export const APPCONSTANT = {
  EMAIL: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  PASSWORD: /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/,
  EMAIL_PHONE: /(^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$)|(^[0-9]+$)/
};

export const SEARCH_FILTER = {
  BETWEEN_NUMBER_RANGE: "numberRange",
  BETWEEN_DATE_RANGE: "dateRange",
  SEARCH: "search"
};

export const RESPONSE = {
  SUCCESS: "success",
  FAILURE: "failure"
};

export const MODULE = {
  DASHBOARD: "dashboardManagement",
  USER: "userManagement",
  USER_APPROVAL: "userManagement_approvalRequest",
  CAMPAIGN: "campaignManagement",
  CAMPAIGN_APPROVAL: "campaignManagement_approvalRequest",
  AGGREGATOR: "aggregatorManagement",
  SENDERID: "senderIdManagement",
  SENDERID_BULKUPLOAD: "sednerIdManagement_bulkUpload",
  SENDERID_APPROVAL: "senderIdManagement_approvalRequest",
  Bulk_TEMPLATE: "bulkTemplateManagement",
  BULK_TEMPLATE_BULK_UPLOAD: "bulkTemplateManagement_bulkUpload",
  BULK_TEMPLATE_APPROVAL: "bulkTemplateManagement_approvalRequest",
  SMS_TEMPLATE_BULK_UPLOAD: "smsTemplateManagement_bulkUpload",
  SMS_TEMPLATE: "smsTemplateManagement",
  SMS_TEMPLATE_APPROVAL: "smsTemplateManagement_approvalRequest",
  // BULK_TEMPLATE_BULK_UPLOAD: "bulkTemplateManagement_bulkUpload",
  // BULK_TEMPLATE_APPROVAL: "bulkTemplateManagement_approvalRequest",

  SPAM_KEYWORD: "spamKeywordManagement",
  ROUTES: "routesManagement",
  DEPARTMENT_CONFIGURATION: "departmentConfigurationManagement",
  SYSTEM_CONFIGURATION: "systemConfigurationManagement",
  REPORTS: "reportingManagement",
  SUMMARY_REPORT: "summaryReport",
  DETAILED_REPORT: "detailedReport",
  MO_SUMMARY_REPORT: "moSummaryReport",
  MO_DETAILED_REPORT: "moDetailedReport",
  REGISTRATION_REQUEST: "registrationRequest",
  MO_KEYWORD: "moKeywordManagement",
  MO_ROUTE: "moRouteManagement",
  //VIEW_AGG: "aggregatorManagement",
  ADDRESS_BOOK: "addressBook",
  PUSH_NOTIFICATION_REPORT: "pushNotificationSummaryReport",
  DEPARTMENT: "departmentConfigurationManagement",
  FAILED_SMS_REPORT : "failedSMSaggregatorWiseReport"
};

export const USER_ROLE = {
  FUNCTIONAL: 1,
  CRM: 2,
  ADMIN: 3,
  HOD: 4,
  L1: 5,
  L2: 6,
  L3: 7,
  OP: 8
};

export const USER_REG_STATUS = {
  APPROVED: 6,
  PENDING: 0,
  REJECTED: 7
};

export class Constants {
  _data: any = "data";
  _token: any = "tkn";
  basic = "basic";
  bearer = "bearer";
  authToken = "Authorization";
  staticToken = "Basic UEFZUFJPX1dFQl9VU0VSOlBBWVBST19XRUJfVVNFUl9TRUNSRVQ=";
  resfreshStaticToken = "Basic d2ViQ2xpZW50OndlYkNsaWVudFNlY3JldA==";
  serverErr = "Something went wrong. Please try again after 5  minutes";
  dontHaveRights = "You don't have edit right.";
  noRecords = "No Records Found";
  loading = "loading....";
  maxUploadFileSize = 10 * 1024 * 1024;
  patterns = {
    // addressBookName: /^([A-Za-z0-9_@-\s]?)+$/,
    // templateName: /^[A-Za-z0-9@#_ -]+$/ ,
    // alphaNumericCamp: /^\d*[a-zA-Z][a-zA-Z0-9 ]*$/,
    // addressName: /^[a-zA-Z ]*$/,
    // alphaNumericStartWithOnlyAlphabetWithSpace: /^[a-zA-Z]([a-zA-Z0-9\s]?)+$/,
    // address: /^([A-Za-z0-9_#,./-\s]?)+$/,
    // cliPatternForSingle: /^[a-zA-Z0-9-_@]*$/,
    email: /^(?!\s)[a-zA-Z0-9!#$%&'*.+/=?^_`{|}~-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}$/,
    // alphaNumaric: /^[a-zA-Z0-9]*$/,
    // numberOnly: /^\d+$/,
    // alphaNumericSpaceOnly: /^[a-zA-Z0-9 ]*$/,
    // uriValidation: /^[A-Za-z0-9 .=/#_?: -]*$/,
    cliPattern: /^[a-zA-Z0-9-_@,]*$/,
    tenCommaSeparatedKeywords: /^([0-9a-zA-Z]{2,20},){0,9}([0-9a-zA-Z]{2,20})$/,
    // nameWithOnlyAlphabetsSpace: /^[a-zA-Z]+[a-zA-Z ]*$/,
    indianMobileNumber: /^[6-9][0-9]*$/,
    indianMobileNumberWithLength : /^[6-9][0-9]{9}$/,
    commaSeparatedValidIP: /^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?),){0,19}((25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?))$/,
    url: /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+[a-zA-Z\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]$/,
    ipWithProtocol : /^(?:http(s)?:\/\/)?((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01 ]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(:([1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]))?$/,




    name: /^[a-zA-Z]((?!.*[-' ]{2})[a-zA-Z '-]?)+$/,
    lastName: /^[a-zA-Z_\-]+$/,
    phone: /^[0-9]+$/,
    password: /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/,
    //email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    //latest//email: /[a-zA-Z]{1}[a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}/,
    // email: /^[A-Za-z0-9]([A-Za-z0-9._-])+@[A-Za-z0-9_-]+([\.-]?\w+)*(\.\w{1,})+$/,
    emailPhone: /(^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$)|(^[0-9]+$)/,
    alphaNumericStartWithOnlyAlphabet: /^[a-zA-Z]([a-zA-Z0-9_]?)+$/,
    alphaNumericStartWithOnlyAlphabetWithSpaceAndUnderScore: /^[a-zA-Z]([a-zA-Z0-9_\s]?)+$/,
    alphaNumericStartWithOnlyAlphabetWithSpaceAndUnderScoreAndOneSCharoneTime: /^[a-zA-Z]((?!.*[-_ ]{2})[a-zA-Z0-9-_\s]?)+$/,
    alphaNumericStartWithAlphaNumericWithSpace: /^[a-zA-Z0-9]([a-zA-Z0-9\s]?)+$/,
    startWithAlphaNumeric: /^[a-zA-Z0-9]((?!.*[-_@*#&^!%$]{2})[\s\S]?)+/,
    anyThing: /^[a-zA-Z0-9]([\s\S]?)+/,
    numberWithNegative: /^[\+\-]?\d+$/,
    numberDecimalWithNegative: /^[\+\-]?\d*\.?\d{1,2}$/,
    numberDecimalWithNegativeString: "^[\\+\\-]?\\d*\\.?\\d{1,endDecimal}$",
    realComplexQuaterNumber: /^[\+\-]?\d*\.?\d+(?:[Ee][\+\-]?\d+)?$/,
    alphaNumericOnly: /^[a-zA-Z0-9]*$/,
    remarksDescription: /^[a-zA-Z0-9]((?!.*[@!#\$\^%`~_&*()+=\-\[\]\\\';,\\/\{\}\|\":<>\?]{2})(?!.*[ ]{2})(?!.*[.]{2})[a-zA-Z0-9\s\r\n@!#\$\^%`~&_*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\?]?)+$/,
    startWithAlphaOnly: /^[a-zA-Z]([\s\S]?)+/,

    ///^[a-zA-Z0-9/?_-#:=. ]*$/,
    tenDigitCommaSeparatedNoNational: /^([6-9]{1}[0-9]{9},){0,9}([6-9]{1}[0-9]{9})$/,
    tenDigitCommaSeparatedNoInternational: /^([0-9]{5,16},){0,9}([0-9]{5,16})$/,
    domainName: /^[ a-zA-Z .]*$/,
    //  /^\d{10}(,\d{10}){0,9}/
    //((25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)(,\n|,?$))
    testRegex: /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?) | [ A-Za-z0-9 .=/#-?:_ ]*$/,
    singleSpace: /\s{1,}$/,

    // SPACE HANDLING IN BELOW REGEX

    alphaNumericCamp: /^(?!\d)[a-zA-Z\d_@-]+(?: [a-zA-Z\d_@-]+)*$/,
    templateName: /^[A-Za-z0-9@#_-]+(?: [a-zA-Z\d@#_-]+)*$/,
    addressBookName: /^[A-Za-z0-9_@-]+(?: [a-zA-Z\d_@-]+)*$/,
    addressName: /^[A-Za-z]+(?: [a-zA-Z]+)*$/,
    alphaNumericStartWithOnlyAlphabetWithSpace: /^(?!\d)[a-zA-Z\d]+(?: [a-zA-Z\d]+)*$/,
    address: /^[A-Za-z0-9_#,./-]+(?: [a-zA-Z\d_#,./-]+)*$/,
    // email: /^[a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]{1,}(?:[a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]+)@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}$/,
    cliPatternForSingle:/^[a-zA-Z\d-_@]+(?: [a-zA-Z\d-_@]+)*$/,
    alphaNumaric: /^[A-Za-z0-9]+(?: [a-zA-Z\d]+)*$/,
    alphaNumaricHyphen: /^[A-Za-z0-9_]+(?: [a-zA-Z\d]+)*$/,

    numberOnly: /^\d+$/,
    numberPFOnly: /^([\S]){5,20}$/,
    alphaNumericPFID : /^([\w])+$/,
    payload:  /^\d+$/,
    //queryString : /^(\w+(=[\w][-\s|\w]*)?(&\w+(=[\w][-\s|\w]*)?)*)?$/,
    queryString :/^(\w+(=[\w\W]|[-\s|\w\W][^&]*)(&\w+(=[\w\W]|[-\s|\w\W][^&]*))*)?[^=&]$/,
    ip: /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,
    alphaNumericSpaceOnly:/^[A-Za-z0-9]+(?: [a-zA-Z\d]+)*$/,
    keywordPattern: /^[a-zA-Z0-9]*$/,
    uriValidation: /^[A-Za-z0-9.=/#_?:-]+(?:[a-zA-Z\d.=/#_?:-]+)*$/,
    // cliPattern: /^[A-Za-z0-9-_@,]+(?:[a-zA-Z\d-_@,]+)*$/,
    nameWithOnlyAlphabetsSpace: /^[A-Za-z]+(?: [a-zA-Z]+)*$/,
    // loginPattern: /^[a-zA-Z]{3,4}[/][0-9]{5,10}$/
    // loginPattern: /^[a-zA-Z]{3,4}[/]([\S]){5,20}$/
    loginPattern: /^([\S]){5,20}$/
    // ^[a-zA-Z0-9_\x7f-\xff]{2,}/[a-zA-Z0-9_\x7f-\xff]{2,}$
  };
  dateFormat = "dd/MM/yyyy";
  dateTimeFormat = "dd/MM/yyyy HH:mm:ss";
  headerUserId = "userId";
  loginConst = {
    userId: "user_ID",
    userIdFormats: ["userId", "user_ID", "USER_ID"],
    userInfoObj: "userInfo"
  };

}
