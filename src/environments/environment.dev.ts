export const environment = {
   production: false,
   backendApiURL: "https://sbiqa.digispice.com/sbi/config/v1/",
   backendApiURLMG: "https://sbiqa.digispice.com/sbi/config/v1/",
   backendLogin : "https://sbiqa.digispice.com/",
   backendApiURLNonEncrytion: "https://sbiqa.digispice.com/sbi/config/v2/",
 };
