import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common/common.service";

@Component({
  selector: 'app-manage-department',
  templateUrl: './manage-department.component.html',
  styleUrls: ['./manage-department.component.scss']
})
export class ManageDepartmentComponent implements OnInit {

  constructor( public commonService: CommonService ) { }

  ngOnInit() {
  }

}
