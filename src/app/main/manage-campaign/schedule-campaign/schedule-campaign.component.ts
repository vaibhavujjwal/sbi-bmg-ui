import { Component, OnInit, ViewChild } from "@angular/core";
import Stepper from "bs-stepper";
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from "../../../common/common.service";
import { ManageCampaignService } from "../manage-campaign.service";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE } from "src/app/common/common.const";
import { SenderIdService } from "../../manage-sender-id/manage-sender-id.service";
import * as moment from 'moment';
import { ExcelService } from "../../../common/excel.service";
import { debug } from "util";

@Component({
  selector: 'app-schedule-campaign',
  templateUrl: './schedule-campaign.component.html',
  styleUrls: ['./schedule-campaign.component.scss'],
  providers: [SenderIdService]
})
export class ScheduleCampaignComponent implements OnInit {
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  modalRef: any;
  categoryList: any;
  selectedCategory: any;
  dynamicHeader = [];
  selectedTemplate:any;
  smsTemplateForm: FormGroup;
  private stepper: Stepper;
  isPlaceholder: boolean;
  isAddPlaceholderBtn: boolean;
  isAddEnable: boolean;
  isbtnNeed: boolean;
  categoryType:any;
  filename:any;
  templateType: number;
  containsAllElements: boolean = true;
  HTTP: number = 1;
  BULK: number = 2;
  selectTemplateSetting = {};
  FLAG_YES: string = "Y";
  currentStep: number = 1;
  math = Math;
  addressBookList = [];
  templateIDList = [];
  selectedPredefinedTemlate: any;
  predefinedTemplats = [];
  createdData: any;
  dltTemplateId: any;
  isEditable: boolean;
  fcmValue: string;
  now = moment(new Date());
  minDate = this.now.format('YYYY-MM-DD');
  maxDate: any;
  senderIdList = [];
  spamKeywordList = [];
  userId = -1;
  fileID: number;
  isValidFile: boolean = true;
  isSecondPage: boolean = false;
  mainForm: FormGroup;
  minArray = [];
  newMinArr = [];
  hoursArray: number[];
  currentDate: Date;
  currentHour: number;
  currentMin: number;
  partZero: boolean = false;
  isMatchedKey: boolean = true;
  maxLenContent: number = 60;
  singleSMSL: number;
  stepOne = { formSubmitted: false, formCompleted: false };
  stepTwo = { formSubmitted: false, formCompleted: false };
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public commonService: CommonService,
    private manageCampaignService: ManageCampaignService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
  ) { }
  @ViewChild('fileInput') fileInput: any;

  trim(value: any) {
    this.campaignInfoForm.content.setValue(value.trim());
  }
  ngOnInit() {
    debugger;
    var CurrentDate = moment(new Date());
    var futureMonth = moment(CurrentDate).add(1, 'M');
    this.maxDate = futureMonth.format("YYYY-MM-DD");
    this.getCategory();
    this.singleSMSL = 160;
    if (this.commonService.getMaxMessageParts() == 0) {
      this.maxLenContent = 0;
      this.partZero = true;
    } else if (this.commonService.getMaxMessageParts() == 1) {
      this.maxLenContent = 160;
    }
    else {
      this.maxLenContent = this.commonService.getMaxMessageParts() * 153;
    }
    this.fcmValue = this.commonService.getFCM();
    this.isEditable = false;
    this.scheduleCampaignFormSetup();
    this.getData();
  }
  getData() {
    this.spinner.show();
    this.manageCampaignService.getData().subscribe(responseList => {
      this.spinner.hide();
      if (responseList[0]['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.senderIdList = responseList[0]["data"];
      } else {
        this.senderIdList = [];
      }
      if (responseList[1]['result']['statusDesc'] == RESPONSE.SUCCESS) {

        this.spamKeywordList = responseList[1]["data"];
      } else {
        this.spamKeywordList = [];
      }

      if (responseList[0]['result']['statusDesc'] != RESPONSE.SUCCESS && responseList[1]['result']['statusDesc'] != RESPONSE.SUCCESS) {
        this.commonService.showErrorToast(responseList[0]['result']['userMsg'] + " and " + responseList[1]['result']['userMsg']);
      } else if (responseList[0]['result']['statusDesc'] != RESPONSE.SUCCESS) {
        this.commonService.showErrorToast(responseList[0]['result']['userMsg']);
      } else if (responseList[1]['result']['statusDesc'] != RESPONSE.SUCCESS) {
        this.commonService.showErrorToast(responseList[1]['result']['userMsg']);
      }
    });
  }

  getSelectedTemplate() {
    //this.isEditable = true;
    this.modalRef.close();
    this.campaignInfoForm.content.setValue(this.selectedPredefinedTemlate['content']);
    if (this.selectedPredefinedTemlate['contentType'] == 1) {
      this.campaignInfoForm.smsContentType.setValue("1");
    }
    else {
      this.campaignInfoForm.smsContentType.setValue("2");
    }
  }

  scheduleCampaignFormSetup() {
    this.createdData = undefined;
    this.stepOne = { formSubmitted: false, formCompleted: false };
    this.stepTwo = { formSubmitted: false, formCompleted: false };
    this.isbtnNeed = false;
    this.currentStep = 1;
    if (this.fileInput) {
      this.fileInput.nativeElement.value = '';
    }
    let inFlag = this.commonService.getInternationalFlag()=='Y'?'I':'D';
    this.mainForm = this.formBuilder.group({
      campaignInfo: this.formBuilder.group(
        {
          campaignName: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(30), Validators.pattern(this.commonService.patterns.alphaNumericCamp)]],
          templateType: ["0", [Validators.required]],
          smsContentType: ["1", [Validators.required]],
          content: ["", [Validators.required]],
          iNFlag: [inFlag, [Validators.required]],
          saveFlag: [false],
          templateName: [""],
          senderCli: ["", [Validators.required]],
          sendVia: ["3"],
          duplicateCheck: ["Y"],
          dynamicOption: [""],
          phone: [""],
          addressBook: [""],
          file: [null, [Validators.required]],
          compaignCategory: ["", [Validators.required]],
          scheduleType: ["S", [Validators.required]],
          dltTemplateId: ["", [Validators.required]]
        }
      ),
      scheduleInfo: this.formBuilder.group({
        sendNow: [false],
        date: ["", [Validators.required]],
        time: ["", [Validators.required]],
        min: ["", [Validators.required]]
      })
    });

    this.selectTemplateSetting = {
      singleSelection: true,
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      allowSearchFilter: true,
      closeDropDownOnSelection: false,
      idField: 'id',
      textField: 'id'
    };
  }

  get campaignInfoForm() {
    let x = <FormGroup>this.mainForm.controls.campaignInfo;
    return x.controls;
  }

  get scheduleInfoForm() {
    let x = <FormGroup>this.mainForm.controls.scheduleInfo;
    return x.controls;
  }
  saveFlagChanged(event) {
    if (event) {
      this.campaignInfoForm.templateName.setValidators([Validators.required, Validators.minLength(3), Validators.maxLength(30), Validators.pattern(this.commonService.patterns.templateName)]);
      this.campaignInfoForm.templateName.updateValueAndValidity();
    } else {
      this.campaignInfoForm.templateName.clearValidators();
      this.campaignInfoForm.templateName.updateValueAndValidity();
    }
  }

  sendNowChanged(event) {
    if (event) {
      this.scheduleInfoForm.date.clearValidators();
      this.scheduleInfoForm.date.updateValueAndValidity();
      this.scheduleInfoForm.time.clearValidators();
      this.scheduleInfoForm.time.updateValueAndValidity();
      this.scheduleInfoForm.min.clearValidators();
      this.scheduleInfoForm.min.updateValueAndValidity();
    } else {
      this.scheduleInfoForm.date.setValidators(Validators.required);
      this.scheduleInfoForm.date.updateValueAndValidity();
      this.scheduleInfoForm.time.setValidators(Validators.required);
      this.scheduleInfoForm.time.updateValueAndValidity();
      this.scheduleInfoForm.min.setValidators(Validators.required);
      this.scheduleInfoForm.min.updateValueAndValidity();
    }
  }

  showModal: boolean = false;
  toClearContent(event: any) {
    this.selectedPredefinedTemlate = undefined;
    this.isValidFile = true;
    this.campaignInfoForm.content.setValue("");
    if (this.fileInput) {
      this.fileInput.nativeElement.value = '';
      this.isbtnNeed = false;
      this.dynamicHeader = [];
    }
    this.campaignInfoForm.sendVia.setValue("3");
    this.campaignInfoForm.file.setValidators(Validators.required);
    this.campaignInfoForm.file.updateValueAndValidity();
    this.campaignInfoForm.phone.clearValidators();
    this.campaignInfoForm.phone.updateValueAndValidity();
    this.campaignInfoForm.addressBook.clearValidators();
    this.campaignInfoForm.addressBook.updateValueAndValidity();
  }

  showPredefinedTemplates(updatemodal: any) {
    this.spinner.show();
    this.manageCampaignService.getPredefinedTemplates().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
          this.predefinedTemplats = res['data'];
          this.predefinedTemplats.forEach(element => {
            if (element['contentType'] == 2) {
              try {
                element['content'] = this.commonService.unicodeToText(element['content']);
              } catch (err) {
                console.log("error while coverting unicode ........" + err);
                element['content'] = element['content'];
              }
            }
          });
          if (this.predefinedTemplats && this.predefinedTemplats.length > 0) {
            this.campaignInfoForm.content.setValue("");
            this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
            this.modalRef.result.then((result) => {
            }, (reason) => {
            });
          } else {
            this.commonService.showErrorToast("No record found!");
          }
        } else {
          this.selectedPredefinedTemlate = undefined;
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.predefinedTemplats = [];
        }
      });
  }

  closePopup() {
    this.modalRef.close();
  }
  filteredListOfCategory: any;
  isRouteNotConfigured = false;

  getCategory() {
    var loginId = this.commonService.getUser();
    this.spinner.show();
    this.manageCampaignService.getCategoryList(loginId).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.categoryList = res['data'];
          console.log("Category List Before filteration..." + JSON.stringify(this.categoryList));
          this.filteredListOfCategory = this.categoryList.filter(
            x => x["appFrmRoute"] != null
          );
          if (this.filteredListOfCategory.length == 0) {
            this.isRouteNotConfigured = true;
          }
          else {
            this.isRouteNotConfigured = false;
          }
          console.log("Category List After filteration..." + JSON.stringify(this.filteredListOfCategory));
        } else {
          this.categoryList = [];
        }
      });
  }

  restrictKeyword(event: Event) {
    event.preventDefault();
  }

  NationalInternationalFlagChanged() {
    if (this.campaignInfoForm.sendVia.value == 1) {
      this.campaignInfoForm.phone.clearValidators();
      if (this.campaignInfoForm.iNFlag.value == "D")
        this.campaignInfoForm.phone.setValidators([Validators.required, Validators.pattern(this.commonService.patterns.tenDigitCommaSeparatedNoNational)])
      else
        this.campaignInfoForm.phone.setValidators([Validators.required, Validators.pattern(this.commonService.patterns.tenDigitCommaSeparatedNoInternational)]);

      this.campaignInfoForm.phone.updateValueAndValidity();
    }
  }

  sendViaChanged(event: any) {
    let sendOption = this.campaignInfoForm.sendVia.value;
    this.campaignInfoForm.phone.clearValidators();
    this.campaignInfoForm.phone.updateValueAndValidity();
    this.campaignInfoForm.file.clearValidators();
    this.campaignInfoForm.file.updateValueAndValidity();
    this.campaignInfoForm.addressBook.clearValidators();
    this.campaignInfoForm.addressBook.updateValueAndValidity();
    switch (sendOption) {
      case "3":
        this.campaignInfoForm.file.setValidators(Validators.required);
        this.campaignInfoForm.file.updateValueAndValidity();
        break;
      case "1":
        this.NationalInternationalFlagChanged();
        break;
      case "2":
        this.campaignInfoForm.addressBook.setValidators(Validators.required);
        this.campaignInfoForm.addressBook.updateValueAndValidity();
        this.spinner.show();
        this.manageCampaignService.getAddressBookList().subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
              this.addressBookList = res['data'];
            } else {
              this.commonService.showErrorToast(res['result']['userMsg']);
              this.addressBookList = [];
            }
          });
        break;
      default:
        this.campaignInfoForm.file.setValidators(Validators.required);
        this.campaignInfoForm.file.updateValueAndValidity();
        break;
    }
  }
  getHours(date: any) {
    this.spinner.show();
    let newDate;
    newDate = moment(date).format("YYYY-MM-DD");
    this.manageCampaignService.getHours(newDate).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.hoursArray = res['data'];
        }
        else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  getMinutes(hour: any) {
    this.currentDate = new Date();
    this.currentHour = this.currentDate.getHours();
    this.currentMin = this.currentDate.getMinutes();
    if (this.currentHour == hour) {
      this.newMinArr = [];
      if (hour == this.hoursArray[(this.hoursArray.length) - 1]) {
        this.newMinArr = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];
      }
      else {
        this.minArray = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];
        this.minArray.forEach(element => {
          if (this.currentMin < element) {
            this.newMinArr.push(element);
          }
        });
      }
    }
    else {
      this.newMinArr = [];
      if (hour == this.hoursArray[(this.hoursArray.length) - 1]) {
        this.newMinArr = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];
      }
      else {
        this.newMinArr = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];
      }
    }

  }
  
  checkDynamicPlaceholderMismatch() {
    let content = this.campaignInfoForm.content.value;
    for (let i = 0; i < content.length; i++) {
      let startingIndex = content.indexOf('<!');
      let endingIndex = content.indexOf('!>');
      if (startingIndex >= 0 && endingIndex >= 0 && startingIndex < endingIndex) {
        let contentKey = content.substring(startingIndex + 2, endingIndex);
        let matchedParameter = this.dynamicHeader.filter(header => header == contentKey.trim());
        console.log("match " + matchedParameter)
        if (matchedParameter.length) {
          content = content.slice(endingIndex + 2);
        } else {
          this.commonService.showErrorToast("Dynamic placeholder key mismatched");
          return false;
        }
      } else {
        content = content.slice(endingIndex + 2);
      }
    }
    return true;
  }
  next() {
    this.isSecondPage = false;
    if (this.currentStep == 1) {
      if (this.isValidFile) {
        this.stepOne.formSubmitted = true;
        if (this.mainForm.controls.campaignInfo.valid && !this.checkSpamKeyword()) {
          if (this.campaignInfoForm.sendVia.value == "3") {
            if (this.campaignInfoForm.templateType.value == 2) {
              this.isMatchedKey = this.checkDynamicPlaceholderMismatch();
            }
            if (this.isMatchedKey)
              this.submitCreateRequest(this.fileID);
          } else {
            this.submitCreateRequest();
          }
        }
      }
    } else if (this.currentStep == 2) {
      this.stepTwo.formSubmitted = true;
      if (this.createdData['successCount'] < 1) {
        this.commonService.showErrorToast("No Valid Number to proceed further.")
        return;
      }
      if (this.mainForm.controls.scheduleInfo.valid) {
        if (this.scheduleInfoForm.min.value == 0 || this.scheduleInfoForm.min.value == 5) {
          this.scheduleInfoForm.min.setValue("0" + this.scheduleInfoForm.min.value);
        }
        if (this.scheduleInfoForm.time.value < 10) {
          this.scheduleInfoForm.time.setValue("0" + this.scheduleInfoForm.time.value);
        }
        let newDate;
        newDate = moment(this.scheduleInfoForm.date.value).format("YYYY-MM-DD");
        this.scheduleInfoForm.date.setValue(newDate);
        let req = {
          "campaignId": this.createdData['id'],
          "scheduleTime": this.scheduleInfoForm.sendNow.value ? null : this.scheduleInfoForm.date.value + " " + this.scheduleInfoForm.time.value + ":" + this.scheduleInfoForm.min.value + ":00",
          "sendNow": this.scheduleInfoForm.sendNow.value ? 'Y' : 'N',
          "totalRecords": this.createdData['totalCount'],
          "tempType":this.selectedCategory[0].category,
          "templateIdDlt": this.dltTemplateId
        };
        this.spinner.show();
        this.manageCampaignService.scheduleCampaign(req).subscribe(
          res => {
            this.spinner.hide();
            if ((req['totalRecords'] <= 10 && req['totalRecords'] != 0) || (res['result']['statusDesc'] === RESPONSE.SUCCESS && res['data']==1)) {
              this.commonService.showSuccessToast('Campaign has been scheduled');
            } else {
              this.commonService.showSuccessToast('Campaign has been sent to HOD for approval');
            }
            this.router.navigate(["/main/manage-campaign/view-schedule-campaign"]);
            this.mainForm.reset();
            this.scheduleCampaignFormSetup();
            this.stepOne.formCompleted = false;
          });
        // this.createdData['totalCount']="";
      }
    }
  }

  submitCreateRequest(fileId?: any) {
    this.selectedCategory = this.categoryList.filter(
      x => x["id"] == this.campaignInfoForm.compaignCategory.value
    );
    console.log("AppName Is...." + JSON.stringify(this.selectedCategory[0].appName));
    console.log("Category Is...." + JSON.stringify(this.selectedCategory[0].category));

    var contentBackSlash = this.campaignInfoForm.content.value.trim();
    contentBackSlash = contentBackSlash.replace(/"/g, "\&quot;");
    contentBackSlash = contentBackSlash.replace(/>/g, "&gt;");
    contentBackSlash = contentBackSlash.replace(/</g, "&lt;");
    contentBackSlash = contentBackSlash.replace(/>=/g, "&ge;");
    contentBackSlash = contentBackSlash.replace(/<=/g, "&le;");
    contentBackSlash = contentBackSlash.replace(/'/g, "&#39;");
   // let dotstarCount = contentBackSlash && contentBackSlash.match(/\.\*/g)?contentBackSlash.match(/\.\*/g).length:0;
   let dotstarCount = contentBackSlash && contentBackSlash.match(/\.\{1,30}/g)?contentBackSlash.match(/\.\{1,30}/g).length:0;
   console.log(".* count..."+dotstarCount)
    let req = {
      "campaignName": this.campaignInfoForm.campaignName.value,
      "applicationName": this.selectedCategory[0].appName,
      "category": this.selectedCategory[0].category,
      "categoryAppId": this.campaignInfoForm.compaignCategory.value,
     // "templateType": this.campaignInfoForm.templateType.value == 3 ? this.selectedPredefinedTemlate.type : this.campaignInfoForm.templateType.value,
      "templateType": 0,
      "messageType": this.isUnicode || this.campaignInfoForm.smsContentType.value==2? 2 : 1,
      "senderId": this.campaignInfoForm.senderCli.value,
      "campaignType": this.campaignInfoForm.sendVia.value,
      "typeFlag": this.campaignInfoForm.iNFlag.value,
      "fileId": fileId ? fileId : null,
      "mobileNumbers": this.campaignInfoForm.phone.value.split(","),
      "addressBookId": this.campaignInfoForm.addressBook.value,
      "messageText": this.campaignInfoForm.content.value,
      "saveTemplateFlag": this.campaignInfoForm.saveFlag.value ? 'Y' : 'N',
      "scheduleDate": this.scheduleInfoForm.date.value,
      "scheduleTime": this.scheduleInfoForm.time.value,
      "templateName": this.campaignInfoForm.templateName.value,
      "duplicateCheckFlag": this.campaignInfoForm.duplicateCheck.value,
      // "userId": this.commonService.getUser(),
      "scheduleType": this.campaignInfoForm.scheduleType.value ? this.campaignInfoForm.scheduleType.value : 'S',
      //"tempType": this.selectedCategory[0].category,
      "templateIdDlt": this.dltTemplateId, //this.dltTemplateId
      "placeholderCount": dotstarCount
    };
    this.spinner.show();
    console.log("REQ......" + JSON.stringify(req));
    this.manageCampaignService.createCampaign(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
          this.createdData = res['data'];
          if (this.createdData['contentType'] == 2) {
            try {
              this.createdData['smsText'] = this.commonService.unicodeToText(this.createdData['smsText']);
            } catch (err) {
              this.createdData['smsText'] = this.createdData['smsText'];
              console.log("error while coverting unicode ........" + err);
            }
          }
          this.currentStep = 2;
          this.stepOne.formCompleted = true;
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  checkSpamKeyword() {
    let smscontent = this.campaignInfoForm.content.value;
    smscontent = smscontent.split(" ");
    smscontent = smscontent.map(function (value) {
      return value.toUpperCase();
    });
    let matchedKeywordsList = [];
    if (this.spamKeywordList != null) {
      for (let i = 0; i < this.spamKeywordList.length; i++) {
        var keywordExist = smscontent.indexOf(this.spamKeywordList[i].keyword.toUpperCase());
        if (keywordExist != -1) {
          matchedKeywordsList.push(this.spamKeywordList[i].keyword);
        }
      }
    }

    if (matchedKeywordsList.length > 0) {
      this.commonService.showErrorToast("Keyword(s) '" + matchedKeywordsList.join(",") + "' are not allowed to be used in the SMS content.");
    }
    return matchedKeywordsList.length == 0 ? false : true;
  }

  onFileSelect(uploadedFile: any) {
    this.isValidFile = true;
    this.isAddPlaceholderBtn = false;
    this.isbtnNeed = false;
    const file: File = uploadedFile.item(0);
    let self = this;
    let headerF;
    if (this.campaignInfoForm.templateType.value == 2) {
      headerF = 'Y';
    }
    else {
      headerF = 'N';
    }
    var fileExtension = file.name.split('.').pop();
    if (fileExtension.toLowerCase() == "xlsx" || fileExtension.toLowerCase() == "xls" || fileExtension.toLowerCase() == "csv" || fileExtension.toLowerCase() == "txt") {
      let fromDataReq = new FormData();
      fromDataReq.append("fileData", file);
      fromDataReq.append("headerFlag", headerF);
      // fromDataReq.append("userId", self.commonService.getUser());
      fromDataReq.append("userName", self.commonService.getUserName());
      this.spinner.show();
      this.manageCampaignService.getHeaders(fromDataReq).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
            this.fileID = res['data']['fileId'];
            this.campaignInfoForm.file.setValue(this.fileID);
            // FOR USER DEFINED
            if (this.campaignInfoForm.templateType.value == 2) {
              if (res['data']['headers'].length > 1) {
                this.dynamicHeader = res['data']['headers'];
                this.isAddPlaceholderBtn = true;
                this.isbtnNeed = true;
                this.dynamicHeader.splice(0, 1);
                this.commonService.showSuccessToast('File uploaded successfully');
                console.log("yeahhh..." + this.dynamicHeader);
                this.validateHeaders();
              }
              else {
                this.isValidFile = false;
                this.commonService.showErrorToast('Upload personalized file');
              }
            } else {
              this.commonService.showSuccessToast('File uploaded successfully');
            }
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        },
        err => {
          console.log(this.fileInput.nativeElement.files);
          this.fileInput.nativeElement.value = "";
          console.log(this.fileInput.nativeElement.files);
        });
    }
    else {
      this.isValidFile = false;
    }
  }

  validateHeaders() {
    if (this.campaignInfoForm.templateType.value == 3 && this.selectedPredefinedTemlate && this.selectedPredefinedTemlate.type == 2) {
      let self = this;
      if (this.campaignInfoForm.templateType.value == 3) {
        let str = this.campaignInfoForm.content.value;
        const headersInText = str.match(/<!(.*?)!>/g).map(val => {
          return val.replace(/<!|!>/g, '');
        });
        console.log(this.dynamicHeader);
        self.containsAllElements = headersInText.every(function (item) {
          return self.dynamicHeader.indexOf(item) > -1;
        });
      }
    }
  }

  valueOfHeader(value: string) {
    this.isAddEnable = true;
  }
  appendPlaceholder() {
    if (this.campaignInfoForm.dynamicOption.value != "") {
      let contentValue = this.campaignInfoForm.content.value;
      let finalContent = contentValue + " <!" + this.campaignInfoForm.dynamicOption.value + "!>";
      if (finalContent.length <= this.maxLenContent) {
        this.campaignInfoForm.content.setValue(finalContent);
      }
      this.campaignInfoForm.dynamicOption.setValue("");
    }
  }
  addPlaceholder() {
    this.isPlaceholder = true;
  }
  previous() {
    this.currentStep = 1;
  }

  noOfMobileNo: string;
  lengthOfNo: number;
  arrayOfMobileNo = [];
  mobileString: any;
  checkMobileNo(mobileNo) {
    this.arrayOfMobileNo = [];
    this.lengthOfNo = this.noOfMobileNo.split(',').length;
    this.mobileString = this.noOfMobileNo.split(",", this.lengthOfNo).toString();
  }

  sampleFileDownload(fileType: string) {
    if (fileType === "txt") {
      let txtData = "Mobile Number\r\nXXXXXXXX92\r\nXXXXXXXX93";
      this.excelService.exportAsTxtFile(txtData, 'Sample_TXT');
    }
    else if (fileType === "excel") {
      let excelData;
      if (this.campaignInfoForm.templateType.value == 1) {
        excelData = [
          {
            "Mobile Number": "XXXXXXXX12"
          },
          {
            "Mobile Number": "XXXXXXXX23"
          }
        ];
        this.excelService.exportAsExcelFile(excelData, 'Sample_Excel');
      } else {
        excelData = [
          {
            "Mobile Number": "XXXXXXXX12",
            "Name": "ABC",
            "Email": "abc@xyz.com",
            "Address": "Test address 1",
            "Company": "Test comapny 1"
          },
          {
            "Mobile Number": "XXXXXXXX13",
            "Name": "DEF",
            "Email": "def@xyz.com",
            "Address": "Test address 2",
            "Company": "Test comapny 2"
          }
        ];
        this.excelService.exportAsExcelFile(excelData, 'Sample_Excel');
      }
    }
    else if (fileType === "csv") {
      let csvData;
      if (this.campaignInfoForm.templateType.value == 1) {
        csvData = [
          {
            "Mobile Number": "XXXXXXXX12"
          },
          {
            "Mobile Number": "XXXXXXXX23"
          }
        ];
        this.excelService.exportAsCsvFile(csvData, 'Sample_CSV');

      } else {
        csvData = [
          {
            "Mobile Number": "XXXXXXXX12",
            "Name": "ABC",
            "Email": "abc@xyz.com",
            "Address": "Test address 1",
            "Company": "Test comapny 1"
          },
          {
            "Mobile Number": "XXXXXXXX13",
            "Name": "DEF",
            "Email": "def@xyz.com",
            "Address": "Test address 2",
            "Company": "Test comapny 2"
          },
          {
            "": "NOTE: First column name should be same as sample file, rest you can add number of columns",
          }
        ];
        this.excelService.exportAsCsvFile(csvData, 'Sample_CSV');
      }
    }
    this.modalRef.close();
  }
  getContentType(type: any) {
    if (type == 1) {
      this.campaignInfoForm.smsContentType.setValue(1);
    }
    else if (type == 2) {
      this.campaignInfoForm.smsContentType.setValue(2);
    }
  }
  isUnicode: boolean = false;
  isUniCodeNotSupported: boolean = false;
  isWrongChar: boolean = false;
  arrayofUn = [];
  arrayOfViewUnicode = [];

  isDoubleByte(str) {
    this.arrayofUn = [];
    for (var i = 0, n = str.length; i < n; i++) {
      if (str.charCodeAt(i) > 255) {
        if (this.arrayofUn.indexOf(str[i]) === -1) {
          this.arrayofUn.push(str[i]);      //GET ARRAY OF UNICODE CHARACTERS
        }

        if (this.commonService.getUnicodeSupport() == 'N') {      //UNICODE NOT SUPPORTED
          this.isUniCodeNotSupported = true;
        } else {
          this.isUnicode = true;  //Unicode present in code
        }
      }
      else {
      }
    }
    if (this.campaignInfoForm.smsContentType.value == 1) {  //For text
      if (this.arrayofUn.length > 0) {
        if (this.isUniCodeNotSupported == false) {      // if Unicode supported
          this.isWrongChar = true;                    //Flag for alert show
        }
      } else if (this.arrayofUn.length == 0) {
        this.isUniCodeNotSupported = false;       // hide Unicode not supported after removing all unicode characters.
        this.isWrongChar = false;                 //Flag for alert hide
        this.isUnicode = false;                   //no unicode
      }
    } else if (this.campaignInfoForm.smsContentType.value == 2) {     //For Unicode
      if (this.arrayofUn.length == 0) {
        this.isWrongChar = false;                 //Flag for alert hide
        this.isUnicode = false;                   //no unicode
      }
    }

    if (str.length == 0 && this.commonService.getUnicodeSupport() == 'Y') {
      this.isUnicode = false;
      this.singleSMSL = 160;
      if (this.commonService.getMaxMessageParts() > 1) {
        this.maxLenContent = this.commonService.getMaxMessageParts() * 153;
      } else {
        this.maxLenContent = 160;
      }
    }
    if (this.isUnicode) {
      if (this.commonService.getMaxMessageParts() > 1) {
        if (str.length > 70) {
          this.singleSMSL = 67;
        }
        else {
          this.singleSMSL = 70;
        }
        this.maxLenContent = this.commonService.getMaxMessageParts() * 67;
      } else {
        this.maxLenContent = 70;
        this.singleSMSL = 70;
      }
    } else {
      if (this.commonService.getMaxMessageParts() > 1) {
        if (str.length > 160) {
          this.singleSMSL = 153;
        }
        else {
          this.singleSMSL = 160;
        }
        this.maxLenContent = this.commonService.getMaxMessageParts() * 153;
      } else {
        this.maxLenContent = 160;
        this.singleSMSL = 160;
      }
    }

    // this.arrayofUn = [];
    // for (var i = 0, n = str.length; i < n; i++) {
    //   if (str.charCodeAt(i) > 255) {
    //     if (this.commonService.getUnicodeSupport() == 'N') {
    //       this.isUniCodeNotSupported = true;
    //     } else {
    //       // console.log("Content Type......" + this.campaignInfoForm.smsContentType.value);
    //       if (this.campaignInfoForm.smsContentType.value == 1) {
    //         if (this.arrayofUn.indexOf(str[i]) === -1) {
    //           this.arrayofUn.push(str[i]);
    //         }
    //         console.log("Array of Unicode......" + this.arrayofUn);
    //       }
    //       this.isUnicode = true;
    //       if (this.commonService.getMaxMessageParts() > 1) {
    //         if (str.length > 70) {
    //           this.singleSMSL = 67;
    //         }
    //         else {
    //           this.singleSMSL = 70;
    //         }
    //         this.maxLenContent = this.commonService.getMaxMessageParts() * 67;
    //       } else {
    //         this.maxLenContent = 70;
    //         this.singleSMSL = 70;
    //       }
    //     }
    //   }
    //   else {
    //     if (this.isUnicode != true) {
    //       this.isUnicode = false;
    //     }
    //     if (this.commonService.getMaxMessageParts() > 1) {
    //       this.maxLenContent = this.commonService.getMaxMessageParts() * 153;
    //       if (str.length > 160) {
    //         this.singleSMSL = 153;
    //       }
    //       else {
    //         this.singleSMSL = 160;
    //       }
    //     } else {
    //       this.maxLenContent = 160;
    //       this.singleSMSL = 160;
    //     }
    //   }
    // }

    // if (this.campaignInfoForm.smsContentType.value == 1) {
    //   if (this.arrayofUn.length > 0) {
    //     this.isWrongChar = true;
    //     // this.campaignInfoForm.smsContentType.setValue(2);
    //   }
    //   else if (this.arrayofUn.length == 0) {
    //     this.isWrongChar = false;
    //     // this.campaignInfoForm.smsContentType.setValue(1);
    //   }
    // } else if (this.campaignInfoForm.smsContentType.value == 2) {
    //   if (this.arrayofUn.length == 0) {
    //     this.isWrongChar = false;
    //     // this.campaignInfoForm.smsContentType.setValue(1);
    //   }
    // }
  }

  getTemplateIdList(){
    this.spinner.show();
    this.campaignInfoForm['dltTemplateId'].setValue("");
    let type = this.campaignInfoForm['compaignCategory'].value;
    for(let i =0; i<this.filteredListOfCategory.length; i++){
      if(type == this.filteredListOfCategory[i].id){
        this.categoryType = this.filteredListOfCategory[i].category;
        if(this.categoryType.toLowerCase()=='informational'){
          this.categoryType = 'inf';
        }else if(this.categoryType.toLowerCase()=='promotional'){
          this.categoryType = 'promo';
        }
      }
    }
    let req = this.categoryType;
    console.log(req);
    this.manageCampaignService.getTemplateIdList(req).subscribe(
      res => {
        if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
          this.templateIDList = res['data'];
          this.spinner.hide();
        } else {
          this.spinner.hide();
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }
  getTemplatedetail(templatedetail){
    console.log(templatedetail);
    let selectedDltTemplate = this.templateIDList.filter(
      x => x["id"] == templatedetail[0].id
    );

    this.dltTemplateId = selectedDltTemplate.length>=1?selectedDltTemplate[0].templateIdDlt:0;
    console.log(selectedDltTemplate);
      try {
        selectedDltTemplate[0].content = this.commonService.unicodeToText(selectedDltTemplate[0].content);
      }
      catch (err) {
        selectedDltTemplate[0].content = selectedDltTemplate[0].content;
        console.log("error while coverting unicode ........" + err);
      }
    this.campaignInfoForm.content.setValue(selectedDltTemplate[0].content);
    this.isDoubleByte(selectedDltTemplate[0].content);
    this.trim(selectedDltTemplate[0].content);
  }
  unselectTemplate(){
    this.selectedTemplate='';
    this.campaignInfoForm.content.setValue('')
  }
  downloadSample(downloadSample,fileType:any){
    this.filename = fileType;
    this.modalRef = this.modalService.open(downloadSample, this.modelOptions);
            this.modalRef.result.then((result) => {
            }, (reason) => {
      });	
  }
}
