import { Injectable } from '@angular/core';
import { NetworkService } from '../../common/network.service';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax

@Injectable()
export class ManageMoRouteService {

  constructor( private _networkService: NetworkService) { }

  getRouteList() {
    return this._networkService.get('honcho/mo/server', null, 'bearer');
  }
  getTopicList() {
    return this._networkService.get('honcho/master/topic', null, 'bearer');
  }

  getData(){
    let routes = this._networkService.get('honcho/mo/server', null, 'bearer');
    let topics = this._networkService.get('honcho/master/topic', null, 'bearer');
    return forkJoin([routes, topics]);
  }


  saveMoRoute(req: any){
     return this._networkService.post('honcho/mo/server',req, null, 'bearer');
 }

  deleteMORoute(req: any){
    return this._networkService.post('honcho/delete/mo/server/'+ req['recordId'] + '?remarks=' + req['remarks'], null, 'bearer');
  }

  updateMORoute(req: any){
    return this._networkService.post('honcho/update/mo/server/'+ req.id  , req, null, 'bearer');
}
getsensitiveFlag(req: any){
 return this._networkService.get('honcho/mo/server/exists/'+ req +'?caseFlag=' + 'false', null, 'bearer');
}

   updateSattus(req: any){
     return this._networkService.post('honcho/mo/route/'+ req["routeId"] +'/status/'+ req["statusId"] , null, null, 'bearer');
   }

}

