import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxSpinnerModule } from 'ngx-spinner';
import {NgxPaginationModule} from 'ngx-pagination';
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ExcelService } from "./excel.service";
import { SearchFilterPipe } from "../common/filters/search.pipe";

@NgModule({
  imports: [CommonModule],
  declarations: [SearchFilterPipe],
  exports: [NgxSpinnerModule,FormsModule, ReactiveFormsModule,
    NgxDatatableModule,    
    NgxPaginationModule,
    NgMultiSelectDropDownModule,
    SearchFilterPipe
  ],
  providers: [ ExcelService ]
})
export class SharedModule {}
