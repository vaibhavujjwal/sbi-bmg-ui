import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageSpamKeywordComponent } from "./manage-spam-keyword.component";
import { ListSpamKeywordComponent } from './list-spam-keyword/list-spam-keyword.component';
import { CreateSpamKeywordComponent } from './create-spam-keyword/create-spam-keyword.component';
import { routing } from './manage-spam-keyword.routing';
import { SharedModule } from "../../common/shared.modules";
import { ManageSpamKeywordService } from './manage-spam-keyword.service';
import {MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule} from '@angular/material';



@NgModule({
  imports: [
    CommonModule,
    routing,
    SharedModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule
  ],
  declarations: [ManageSpamKeywordComponent, ListSpamKeywordComponent, CreateSpamKeywordComponent],
  providers: [ManageSpamKeywordService]

})
export class ManageSpamKeywordModule { }
