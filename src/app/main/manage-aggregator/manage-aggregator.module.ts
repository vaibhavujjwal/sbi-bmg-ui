import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ManageAggregatorComponent } from "./manage-aggregator.component";
import { ViewComponent } from "./view/view.component";
import { AddComponent } from "./add/add.component";
import { SharedModule } from "../../common/shared.modules";
import { routing } from "./manage-aggregator.route";
import {MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule} from '@angular/material';


@NgModule({
  imports: [CommonModule, SharedModule, routing,
    SharedModule,MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
    MatSortModule, MatTableModule],
  declarations: [ManageAggregatorComponent, ViewComponent, AddComponent]
})
export class ManageAggregatorModule {}
