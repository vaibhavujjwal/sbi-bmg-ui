import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageAggregatorComponent } from './manage-aggregator.component';

describe('ManageAggregatorComponent', () => {
  let component: ManageAggregatorComponent;
  let fixture: ComponentFixture<ManageAggregatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageAggregatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageAggregatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
