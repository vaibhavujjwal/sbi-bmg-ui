import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { MoKeywordsService } from '../manage-mo-keywords.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from "@angular/forms";
import { CommonService } from "../../../common/common.service";
import { RESPONSE, MODULE } from "../../../common/common.const";
import { ExcelService } from "../../../common/excel.service";
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';





export interface moKeywordTypes {
  Actions: any;
  department: number;
  code: string;
  keyword: any;
  primaryRoute : any;
  secondaryRoute : any;
  caseSensitivityFlag : any;
  retryFlag : any;
  displayRetryRoute : any;
}


@Component({
  selector: 'app-list-mo-keywords',
  templateUrl: './list-mo-keywords.component.html',
  styleUrls: ['./list-mo-keywords.component.scss']
})
export class ListMoKeywordsComponent implements OnInit {
  MODULES = MODULE;
  pageLimit: number;
  formSubmitted: boolean = false;
  moKeywordForm: FormGroup;
  modalRef: any;
  statusForm: FormGroup;
  departmentReq = {};
  recordId: any;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false,
    size: "lg"
  };
  searchText: string = "";
  moKeywordList = [];
  secondaryDropdownList = [];
  departments = [];
  commonList: Array<object> = [];

  primaryDropdownList = [];
  caseRequired:boolean=false;

  p: number = 1;
  showError:boolean=false;

  dropdownSettings = {};
  deptValue: any;
  deleteReq = {};
  primaryRouteFlag: boolean = false;
  secondaryRouteFlag: boolean = false;
  uniqueKeyword: boolean = true;
  caseFlagExistence: boolean = false;
  keywordPreviousValue: string = "";
  caseSensitivityFlagPreviousValue: string = "";
  retryRouteList: Array<object> = [];
  retryCountList: Array<Number> = [];
  displayedColumns: string[] = ['actions',	'department',	'code',	'keyword',
  'primaryRoute','secondaryRoute','caseSensitivityFlag','retryFlag','displayRetryRoute'];
  dataSource: MatTableDataSource<moKeywordTypes>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private fb: FormBuilder, private modalService: NgbModal,
    private spinner: NgxSpinnerService, private moKeywordsService: MoKeywordsService, private formBuilder: FormBuilder,
    public commonService: CommonService, private excelService: ExcelService) { }

  ngOnInit() {
    this.getMoKeywordList();
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.initialiseForms();
    this.getRouteList();
    this.getDepartmentList();
    this.retryRouteList = this.moKeywordsService.getRetryAggregatorList();
    this.retryCountList = [1, 2, 3];
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      placeholder: 'Select Filter',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true,
      enableCheckAll: false,
      defaultOpen: false
    };
  }


  initialiseForms() {
    this.moKeywordForm = this.fb.group({
      caseSensitivityFlag: [''],
      code: ['', [Validators.required, Validators.pattern(this.commonService.patterns.numberOnly)]],
      keywords: ['', [ Validators.pattern(this.commonService.patterns.keywordPattern)]],
      primaryRouteId: ['', [Validators.required]],
      secondaryRouteId: [''],
      department: ['', [Validators.required]],
      status: [''],
      url: [''],
      retryFlag: [false],
      retryRoute: new FormControl({ disabled: true, value: "" }, [
        Validators.required
      ]),
      retryCount: new FormControl({ disabled: true, value: "" }, [
        Validators.required,
        Validators.pattern(this.commonService.patterns.numberOnly)
      ]),
      retryIntervalArray: this.fb.array([])
    });
  }

  updateRequiredCase(){
    debugger;
    if(this.moKeywordForm.controls['keywords'].value == ""){
      this.caseRequired=true;
      this.moKeywordForm.controls['caseSensitivityFlag'].clearValidators();
      this.moKeywordForm.controls['caseSensitivityFlag'].updateValueAndValidity();
      }else {
      this.caseRequired=false;
      this.moKeywordForm.controls['caseSensitivityFlag'].clearValidators();
      this.moKeywordForm.controls['caseSensitivityFlag'].setValidators(Validators.required);
      this.moKeywordForm.controls['caseSensitivityFlag'].updateValueAndValidity();
    }
  }

  filterList(list, value) {
    return list.filter(x => x["id"] != value);
  }

  routeChanged(field) {
    let primaryVal = this.moKeywordForm.controls["primaryRouteId"].value;
    let secondaryVal = this.moKeywordForm.controls["secondaryRouteId"].value;
    if (field == "primaryRouteId") {
      this.secondaryDropdownList = this.filterList(
        this.commonList,
        primaryVal
      );

    } else if (field == "secondaryRouteId") {
      this.primaryDropdownList = this.filterList(
        this.commonList,
        secondaryVal
      );
        if(secondaryVal){
          this.retryRouteList = this.moKeywordsService.getRetryAggregatorList();
        }else if(!secondaryVal){
          this.retryRouteList = this.filterList(
            this.retryRouteList,"S"
          );
          this.retryRouteList = this.filterList(
            this.retryRouteList,"B"
          );
        }


    }
  }


  keywordExistance() {
    this.updateRequiredCase();
    this.uniqueKeyword = true;
    if (this.moKeywordForm.controls['code'].valid && this.moKeywordForm.controls['keywords'].valid) {
      this.moKeywordForm.controls['caseSensitivityFlag'].value == "Y" ? this.caseFlagExistence = true : this.caseFlagExistence = false;
      let req = {
        "caseSensitivityFlag": this.caseFlagExistence,
        "keyword": this.moKeywordForm.controls['keywords'].value,
        "code": this.moKeywordForm.controls['code'].value
      };
      this.spinner.show();
      if(this.moKeywordForm.controls['code'] || this.moKeywordForm.controls['keywords']){

      this.moKeywordsService.keywordExists(req).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            if (res['data'] == "false") {
            } else {
              if(req['keyword']){
                this.commonService.showErrorToast(req['keyword'] + ' already exist against ' + req['code']);
              }else{
                this.commonService.showErrorToast('Empty Keyword already exist against ' + req['code']);
              }
            }
          } else {
            this.uniqueKeyword = true;
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
        }
        }
      }



  populateValues(item) {
    //this.caseRequired=true;
    this.updateRequiredCase();

    this.moKeywordForm.controls['code'].setValue(item.code);
    this.moKeywordForm.controls['keywords'].setValue(item.keyword);
    if(item.keyword){
      this.caseRequired=false;
    }
    this.moKeywordForm.controls['primaryRouteId'].setValue(item.primaryRouteId);
    this.moKeywordForm.controls['secondaryRouteId'].setValue(item.secondaryRouteid);

    if(!item.secondaryRouteid){
      this.retryRouteList = this.filterList(
        this.retryRouteList,"S"
      );
      this.retryRouteList = this.filterList(
        this.retryRouteList,"B"
      );
    }
    

    this.moKeywordForm.controls['department'].setValue(item.department.id);
    this.moKeywordForm.controls['caseSensitivityFlag'].setValue(item.caseSensitivityFlag == 'True' ? 'Y' : 'N');
    this.moKeywordForm.controls['retryFlag'].setValue(item.retryFlag == "True" ? true : false)
    this.flagChanged('retryFlag', ['retryRoute', 'retryCount', 'retryIntervalArray']);
    if (this.moKeywordForm.controls['retryFlag'].value) {

      // if (item.retryRoute != 'NA') {
      //   let routeObject = this.moKeywordsService.getRetryAggregatorList();
      //   for (let x in routeObject) {
      //     if (item['retryRoute'] == routeObject[x]['aggregatorName']) {
      //       item['retryRoute'] = routeObject[x]['id'];
      //       break;
      //     }
      //   }
      // }
      this.moKeywordForm.controls['retryRoute'].setValue(item.retryRoute);
      this.moKeywordForm.controls['retryCount'].setValue(item.maxRetryCount);
      this.retryCountChanged();
      for (let x = 0; x < item.maxRetryCount; x++) {
        this.moKeywordForm.controls['retryIntervalArray']['controls'][x].controls.retryInterval.setValue(item.retryIntervals.split(',')[x])
      }
    } else {
      this.moKeywordForm.controls['retryRoute'].setValue("");
      this.moKeywordForm.controls['retryCount'].setValue("");
      let retryIntervalControl = <FormArray>(this.moKeywordForm.controls.retryIntervalArray);

      for (let i = retryIntervalControl.length - 1; i >= 0; i--) {
        retryIntervalControl.removeAt(i);
      }
    }

    this.recordId = item.id;
    this.deptValue = item.department.department;
  }

  retryCountChanged() {

    let retryCount = this.moKeywordForm.controls.retryCount.value;
    let retryIntervalControl = <FormArray>(this.moKeywordForm.controls.retryIntervalArray);

    for (let i = retryIntervalControl.length - 1; i >= 0; i--) {
      retryIntervalControl.removeAt(i);
    }

    for (let i = 0; i < retryCount; i++) {
      retryIntervalControl.push(
        this.fb.group({
          retryInterval: [
            '',
            [
              Validators.required,
              Validators.min(1),
              Validators.max(900),
              Validators.pattern(this.commonService.patterns.numberOnly)
            ]
          ]

        })
      )
    }
  }

  flagChanged(flagName, fields) {
    if (this.moKeywordForm.controls[flagName].value) {
      for (let x = 0; x < fields.length; x++)
        this.moKeywordForm.controls[fields[x]].enable();
    } else {
      for (let x = 0; x < fields.length; x++) {
        this.moKeywordForm.controls[fields[x]].disable();
      }
    }
  }

  confirmUpdateRecoed() {

    this.formSubmitted = true;
    if (this.moKeywordForm.valid && this.uniqueKeyword) {
      if (this.moKeywordForm.controls['retryFlag'].value) {
        var retryIntervalCommaSeparatedValue = this.manipulateRetryInterval();
        if (!retryIntervalCommaSeparatedValue) { return }
      }
      let req = {
        "keyword": this.moKeywordForm.controls["keywords"].value,
      }

      let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
      modalRef.componentInstance.content = 'Are you sure you want to update ' + req['keyword'] + ' MO keyword?';
      modalRef.componentInstance.leftButton = 'No';
      modalRef.componentInstance.rightButton = 'Yes';
      modalRef.result.then(
        (data: any) => {
          if (data == "yes") {
            this.updateMoKeywordForm(retryIntervalCommaSeparatedValue);
          }
        },
        (reason: any) => {

        });
      this.formSubmitted = false;
    }
  }

  manipulateRetryInterval() {
    if (this.moKeywordForm.controls.retryCount.value > 1) {
      for (let x = 0; x < this.moKeywordForm.controls.retryCount.value - 1; x++) {
        let y = x + 1;
        if (Number(this.moKeywordForm.controls.retryIntervalArray.value[x].retryInterval) > Number(this.moKeywordForm.controls.retryIntervalArray.value[y].retryInterval)) {
          this.commonService.showErrorToast("Duration of retry interval " + ++y + " should be greater than retry interval " + ++x);
          return;
        }
      }

    }

    var retryIntervalValues = [];
    for (let x = 0; x < this.moKeywordForm.controls.retryCount.value; x++) {
      retryIntervalValues.push(this.moKeywordForm.controls.retryIntervalArray.value[x].retryInterval);
    }
    return retryIntervalValues;
  }

  updateMoKeywordForm(retryIntervalCommaSeparatedValue) {
    let reqObj = {
      "code": this.moKeywordForm.controls["code"].value,
      "keyword": this.moKeywordForm.controls["keywords"].value,
      "primaryRouteId": this.moKeywordForm.controls["primaryRouteId"].value,
      "secondaryRouteId": this.moKeywordForm.controls['secondaryRouteId'].value?this.moKeywordForm.controls['secondaryRouteId'].value:null,
      "department": {
        "department": this.deptValue,
        "id": this.moKeywordForm.controls["department"].value,
        "show": 0
      },
      "caseSensitivityFlag": this.moKeywordForm.controls["caseSensitivityFlag"].value,
      "modifiedBy": this.commonService.getUserName(),
      "id": this.recordId,
      "retryIntervals": this.moKeywordForm.controls['retryFlag'].value && retryIntervalCommaSeparatedValue ? retryIntervalCommaSeparatedValue.join() : 0,
      "retryFlag": this.moKeywordForm.controls['retryFlag'].value ? 'Y' : 'N',
      "maxRetryCount": this.moKeywordForm.controls['retryFlag'].value && this.moKeywordForm.controls['retryCount'].value ? Number(this.moKeywordForm.controls['retryCount'].value) : 0,
      "retryRoute": this.moKeywordForm.controls['retryFlag'].value && this.moKeywordForm.controls['retryRoute'].value ? this.moKeywordForm.controls['retryRoute'].value : "B"
    }
    debugger;
    this.spinner.show();
    console.log(reqObj);
    this.moKeywordsService.updateMOKeywrd(reqObj).subscribe(
      res => {
        this.commonService.showSuccessToast(reqObj['keyword'] + ' MO keyword has been updated');
        this.modalRef.close();
        this.getMoKeywordList();
      });
  }


  updateRecord(updatemodal: any, record: any) {
    this.populateValues(record);
    this.getPrimarySecondary();
    this.uniqueKeyword = true;
    this.keywordPreviousValue = record.keyword;
    this.caseSensitivityFlagPreviousValue = record.caseSensitivityFlag == 'True' ? 'Y' : 'N';
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  checkKeywordUniqueness() {
    this.uniqueKeyword = true;
    console.log("this.keywordPreviousValue ", this.keywordPreviousValue)
    console.log("this.moKeywordForm.controls['keywords'].value ", this.moKeywordForm.controls['keywords'].value)
    console.log("this.caseSensitivityFlagPreviousValue ", this.caseSensitivityFlagPreviousValue)
    console.log("this.moKeywordForm.controls['caseSensitivityFlag'].value ", this.moKeywordForm.controls['caseSensitivityFlag'].value)
    if (!(this.keywordPreviousValue != this.moKeywordForm.controls['keywords'].value) && !(this.caseSensitivityFlagPreviousValue != this.moKeywordForm.controls['caseSensitivityFlag'].value))
      return;
    if (this.moKeywordForm.controls['caseSensitivityFlag'].valid && this.moKeywordForm.controls['keywords'].valid) {
      this.moKeywordForm.controls['caseSensitivityFlag'].value == "Y" ? this.caseFlagExistence = true : this.caseFlagExistence = false;
      let req = {
        "caseSensitivityFlag": this.caseFlagExistence,
        "keyword": this.moKeywordForm.controls['keywords'].value
      };
      this.spinner.show();
      this.moKeywordsService.keywordExists(req).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            if (res['data'] == "false") {
            } else {
              this.uniqueKeyword = false;
            }
          } else {
            this.uniqueKeyword = true;
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
    }
  }

  getRouteList() {
    this.spinner.show();
    this.moKeywordsService.getRouteList('true').subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.primaryDropdownList = res['data'];
          debugger;
          this.secondaryDropdownList = JSON.parse(JSON.stringify(this.primaryDropdownList));
          this.commonList = res['data']

        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.primaryDropdownList = [];
          this.secondaryDropdownList = [];
        }
      });
  }

  getPrimarySecondary() {
    this.spinner.show();
    this.moKeywordsService.getRouteList('false').subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.primaryDropdownList = res['data'];
          let primariid = this.moKeywordForm.controls["primaryRouteId"].value?
          this.moKeywordForm.controls["primaryRouteId"].value:"";
          this.secondaryDropdownList = this.filterList(
            this.primaryDropdownList,
            primariid
          );
          this.commonList = res['data'];
          //this.secondaryDropdownList = JSON.parse(JSON.stringify(this.primaryDropdownList));
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.primaryDropdownList = [];
          this.secondaryDropdownList = [];
        }
      });
  }

  getDepartmentList() {
    this.spinner.show();
    this.moKeywordsService.getDepartmentList().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.departments = res['data'];
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.departments = [];
        }
      });
  }

  getMoKeywordList() {
    let id = 0;
    this.spinner.show();
    this.moKeywordsService.getkeywordsList(id).subscribe(
      res => {

        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          debugger;
          this.moKeywordList = res["data"];
          this.moKeywordList.forEach(element => {
            if (element['caseSensitivityFlag'].toLowerCase() == 'y') {
              element['caseSensitivityFlag'] = "True";
            }
            else if (element['caseSensitivityFlag'].toLowerCase() == 'n') {
              element['caseSensitivityFlag'] = "False";
            }
            if (element['retryFlag'].toLowerCase() == 'y') {
              element['retryFlag'] = "True";
              let routeObject = this.moKeywordsService.getRetryAggregatorList();
              for (let x in routeObject) {
                if (element['retryRoute'] == routeObject[x]['id']) {
                  element['displayRetryRoute'] = routeObject[x]['aggregatorName'];
                  break;
                }
              }
            }
            else if (element['retryFlag'].toLowerCase() == 'n') {
              element['retryFlag'] = "False";
              element['displayRetryRoute'] = "NA";
            }
          });
          this.dataSource= new MatTableDataSource(this.moKeywordList) ;
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;

        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.moKeywordList = [];
        }
      });
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.showError=false;
    if(this.dataSource.filteredData.length==0){
      this.showError=true;
    }
  }


  prepareApprovalForm() {
    this.statusForm = this.formBuilder.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)
        ]
      ]
    });
  }

  confirmDeleteRecord(updatemodal: any, record: any) {
    this.formSubmitted = false;
    this.prepareApprovalForm();
    this.deleteReq = {
      "id": record['id'],
      "keyword": record['keyword']
    }
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  deleteRecord() {
    this.deleteReq['remarks'] = this.statusForm.controls['remarks'].value;
    this.formSubmitted = true;
    if (this.statusForm.valid) {
      this.spinner.show();
      this.moKeywordsService.deleteMoKeyword(this.deleteReq).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast(this.deleteReq['keyword'] + ' MO keyword has been deleted');
            this.modalRef.close();
            this.getMoKeywordList();
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
      this.formSubmitted = false;
    }
  }


  exportAs(fileType: string) {
    let data = [];
    this.moKeywordList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Username"] = element.username;
      excelDataObject["Keyword"] = element.keyword;
      excelDataObject["Primary Route"] = element.primaryRoute;
      excelDataObject["Secondary Route"] = element.secondaryRoute;
      excelDataObject["Case Sensitivity Flag"] = element.caseSensitivityFlag.toLowerCase() == 'y' ? 'True' : 'False';
      excelDataObject["Status"] = element.status == 1 ? 'Enable' : 'Disable';
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'MO-Keywords-List');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'MO-Keywords-List');
        break;
      default:
        this.excelService.exportAsExcelFile(data, 'MO-Keywords-List');
        break;
    }
  }
}
