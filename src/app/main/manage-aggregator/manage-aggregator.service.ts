import { Injectable } from "@angular/core";
import { NetworkService } from "../../common/network.service";
import { CommonService } from "../../common/common.service";

@Injectable()
export class ManageAggregatorService {
  NO_HEADER = null;
  constructor(
    private _networkService: NetworkService,
    private _commonService: CommonService
  ) { }

  // saveAggregator(data) {
  //   return this._networkService.post(
  //     "aggregator",
  //     data,
  //     this.NO_HEADER,
  //     this._commonService.bearer
  //   );
  // }

  // getAggregator(id) {
  //   return this._networkService.get(
  //     "aggregator/" + id,
  //     this.NO_HEADER,
  //     this._commonService.bearer
  //   );
  // }

  // updateAggregator(id, aggregator) {
  //   return this._networkService.put(
  //     "aggregator/" + id,
  //     aggregator,
  //     this.NO_HEADER,
  //     this._commonService.bearer
  //   );
  // }

  // updateAggregatorStatus(id, data) {
  //   return this._networkService.put(
  //     "aggregator/status/" + id,
  //     data,
  //     this.NO_HEADER,
  //     this._commonService.bearer
  //   );
  // }

  // getAllAggregatoraList() {
  //   return this._networkService.get(
  //     "aggregator",
  //     this.NO_HEADER,
  //     this._commonService.bearer
  //   );
  // }

  // getStatusList() {
  //   return this._networkService.get(
  //     "aggregator",
  //     this.NO_HEADER,
  //     this._commonService.bearer
  //   );
  // }

  // deleteAggregator(id,remarks) {
  //   return this._networkService.get(
  //     "aggregator",
  //     this.NO_HEADER,
  //     this._commonService.bearer
  //   );
  // }


  getTopologyList() {
    return [
      { id: 1, name: "Active/Active" },
      { id: 2, name: "Active/Passive" }
    ];
  }

  getAggregatorTypeList() {
    return [{ id: "D", name: "Domestic" }, { id: "I", name: "International" }];
  }

  getAggregatorProtocolList() {
    return [{ id: "HTTP", name: "HTTP" }, { id: "HTTPS", name: "HTTPS" }];
    // , { id: "SMPP", name: "SMPP" }
  }
  getAggregatorResponseList() {
    return this._networkService.get('honcho/agg/responseType', null, 'bearer');
  }
  getAllAggregatorList() {
    return this._networkService.get('honcho/agg?epoch=' + this._commonService.getTimestamp(), null, 'bearer');
  }

  createAggregator(req) {
    return this._networkService.post('honcho/agg/create', req, null, 'bearer');
  }

  deleteAggregator(req) {
    return this._networkService.post('honcho/agg/delete/' + req['aggId'] +
      '?remarks=' + req['remarks'], null, 'bearer');
  }

  updateAggregator(req) {
    return this._networkService.post('honcho/agg/update', req, null, 'bearer');
  }
  getAggregatorById(req) {
    return this._networkService.get('honcho/agg/getagg' + '?id=' + req['id'] +
      '&status=' + req['status'], null, 'bearer');
  }

  getCategoryList() {
    return this._networkService.get(
      "honcho/master/category",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }
}
