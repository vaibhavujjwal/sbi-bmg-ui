import { CanActivate, Router } from "@angular/router";
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router/src/router_state";
import { Injectable } from "@angular/core";
import { CommonService } from "../common/common.service";

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {

    if (sessionStorage.getItem("_bearerTkn")) {
      return true;
    }
    this.router.navigate(["/auth/login"], { queryParams: { returnUrl: state.url } });
    return false;
  }
}

@Injectable()
export class ModuleGuardService implements CanActivate {
  constructor(private router: Router, private commonService: CommonService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    if (sessionStorage.getItem("_userdata")) {
      let moduleInfo = route.data;
      let userData = JSON.parse(sessionStorage.getItem("_userdata"));
      let permissionList = JSON.parse(
        JSON.parse(sessionStorage.getItem("_userdata")).permissionJson
      );
      if(userData['role']=="ROLE_CAMPAIGN" && !permissionList['smsTemplateManagement']){
        permissionList['smsTemplateManagement'] = 'RW';
        permissionList['smsTemplateManagement_bulkUpload'] = 'RW';
      }
      let permission = permissionList[moduleInfo["moduleName"]];
      let isAllowed = false;

      for (let y = 0; y < moduleInfo["permissions"].length; y++) {
        if (permission == moduleInfo["permissions"][y]) {
          isAllowed = true;
          break;
        }
      }

      if (isAllowed) {
        return true;
      }
    }
    if (sessionStorage.getItem("_userdata"))
    this.commonService.showErrorToast("User does not have required permissions to access module(s).");
    return false;
  }
}
