import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageMoRouteComponent } from './manage-mo-route.component';
import { SharedModule } from "../../common/shared.modules";
import { ViewMoRouteComponent } from './view-mo-route/view-mo-route.component';
import { CreateMoRouteComponent } from './create-mo-route/create-mo-route.component';
import { RouterModule, Routes } from "@angular/router";
import { MODULE } from "../../common/common.const";
import { AuthGuardService, ModuleGuardService } from "../../guards/auth.guard";
import { ManageMoRouteService } from "./manage-mo-route.service";
import {MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule} from '@angular/material';


const routes: Routes = [
  {
    path: "",
    component: ManageMoRouteComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/manage-mo-route/view-mo-route",
        pathMatch: "full"
      },
      {
        path: "view-mo-route",
        component: ViewMoRouteComponent,
        data: { moduleName: MODULE.MO_ROUTE, permissions: ["RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "create-mo-route",
        component: CreateMoRouteComponent,
        data: { moduleName: MODULE.MO_ROUTE, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
    MatSortModule, MatTableModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ManageMoRouteComponent, ViewMoRouteComponent, CreateMoRouteComponent],
  providers: [ManageMoRouteService]
})
export class ManageMoRouteModule { }
