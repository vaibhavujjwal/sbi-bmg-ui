import { Component, OnInit } from '@angular/core';
import { ManageBulkTemplateService } from '../manage-bulk-template.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";
import { Router } from "@angular/router";

@Component({
  selector: 'app-create-bulk-template',
  templateUrl: './create-bulk-template.component.html',
  styleUrls: ['./create-bulk-template.component.scss']
})
export class CreateBulkTemplateComponent implements OnInit {
  users = [];
  smsTemplateForm : FormGroup;
  formSubmitted : boolean = false;
  loginUser : any;
  isEdit : boolean = false;
  math = Math;
  placeholder : boolean = false;
  userIDValue :any;
  errorContent : any;
  errorMsgType:any;
  maxLengthSMS:any;
  showUser : any;
  errorZero: boolean = false;
  errZeroMsg:any;
  spamKeywordsList = [];
  unicodeSupport : any;
  UnicodeError: boolean = false;
  unicodeFlag: boolean = false;
  maxlen:any;
  errorMsgSpam:any;
  spamError: boolean = false;
  messagePart:any;
  departments = [];
  errortext:any;
  errorUnicode:any;
  contentTypeVal:any;
  categoryList: Array<object> = [];

  constructor(private bulkTemplateService: ManageBulkTemplateService, private fb: FormBuilder,
    private spinner: NgxSpinnerService,private router: Router, private commonService: CommonService) { }
    ngOnInit() {
      this.getUserList();
      this.getTemlateCategoryList();
      this.initialiseForms();
      this.maxLengthSMS=160;
      this.checkUser();
      this.getSpamKeyword();
      this.checkUnicodeSupport();
    }
    getTemlateCategoryList(){
      this.spinner.show();
      this.bulkTemplateService.getCategoryList().subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.categoryList = res['data'];
          } else {
            this.categoryList = [];
          }
        });
    }

    getUserList() {
      this.spinner.show();
      this.bulkTemplateService.getAllUser().subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.users = res['data'];
          }else{
            this.users = [];
          }
        });
    }

     checkUnicodeSupport(){
      this.unicodeSupport = this.commonService.getUnicodeSupport();
      this.unicodeSupport=='Y' ? this.unicodeFlag=true : this.unicodeFlag=false;
      }


    initialiseForms() {
      this.smsTemplateForm = this.fb.group({
        contentType: ['', [Validators.required]],
        name: ['', [Validators.required, Validators.pattern(this.commonService.patterns.templateName)]],
        language: [''],
        content: ['', [Validators.required]],
        user: ['', [Validators.required]],
        placeHolder: [''],
        temlateCategory: ['', [Validators.required]],
      });
    }


  getSpamKeyword() {
       this.spinner.show();
       this.bulkTemplateService.getSpamList().subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.spamKeywordsList = res['data'];
          }else{
            this.spamKeywordsList = [];
          }
        });
  }


  checkSpamKeyword() {
    let templateContent = this.smsTemplateForm.controls["content"].value;
    templateContent = templateContent.split(" ");
    templateContent = templateContent.map(function (value) {
      return value.toUpperCase();
    });
    let matchedKeywordsList = [];
    for (let i = 0; i < this.spamKeywordsList.length; i++) {
      var keywordExist = templateContent.indexOf(this.spamKeywordsList[i].keyword.toUpperCase());
      if (keywordExist != -1) {
        matchedKeywordsList.push(this.spamKeywordsList[i].keyword);
      }
    }
    if (matchedKeywordsList.length > 0) {
      this.spamError=true;
      this.errorMsgSpam="Keyword(s) '" + matchedKeywordsList.join(",") + "' are not allowed to be used in the SMS content.";
    }else{
      this.spamError=false;
    }
    return matchedKeywordsList.length == 0 ? false : true;
  }


    checkUser(){
      this.loginUser = this.commonService.getUser();
      if(this.loginUser == '1000'){
        this.userIDValue = this.smsTemplateForm.controls["user"].value;
        this.showUser=true;
      }else{
        this.userIDValue = this.commonService.getUser();
        this.smsTemplateForm.controls['user'].setValue(this.userIDValue);
        this.showUser=false;
      }

    }


    submitForm() {
      this.checkUser();
      this.formSubmitted = true;
      if (this.smsTemplateForm.valid && !this.spamError && !this.UnicodeError && !this.errorZero) {

        let tempalatetype ='promo';
        if(this.smsTemplateForm.controls["temlateCategory"].value==1){
          tempalatetype = 'otp';
        }else if(this.smsTemplateForm.controls["temlateCategory"].value==2){
          tempalatetype = 'txn';
        }else if(this.smsTemplateForm.controls["temlateCategory"].value==3){
          tempalatetype = 'inf';
        }

        let contentBackSlash = this.smsTemplateForm.controls["content"].value.trim();
          contentBackSlash = contentBackSlash.replace(/"/g, "\&quot;");
          contentBackSlash = contentBackSlash.replace(/>/g, "&gt;");
          contentBackSlash = contentBackSlash.replace(/</g, "&lt;");
          contentBackSlash = contentBackSlash.replace(/>=/g, "&ge;");
          contentBackSlash = contentBackSlash.replace(/<=/g, "&le;");
          contentBackSlash = contentBackSlash.replace(/'/g, "&#39;");

          //let dotstarCount = contentBackSlash && contentBackSlash.match(/\.\*/g)?contentBackSlash.match(/\.\*/g).length:'';
          let dotstarCount = contentBackSlash && contentBackSlash.match(/\.\{1,30}/g)?contentBackSlash.match(/\.\{1,30}/g).length:0;
          console.log(".* count..."+dotstarCount)
        let reqObj = {
          "reqObj": {
              "name":this.smsTemplateForm.controls["name"].value.trim(),
              "content":contentBackSlash,
              "userId": this.userIDValue,
              "contentType":this.smsTemplateForm.controls["contentType"].value,
              "templateType":tempalatetype,
              "placeholderCount":dotstarCount
          }
       }
        this.spinner.show();
        this.bulkTemplateService.addTemplate(reqObj).subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusCode'] == 200) {
              this.commonService.showSuccessToast(reqObj['reqObj']['name'] + ' template has been sent to DLT User for approval');
              this.router.navigate(["/main/manage-bulk-template/view-bulk-template"]);

              this.initialiseForms();
            } else {
              this.commonService.showErrorToast(res['result']['userMsg']);
            }
            this.formSubmitted = false;
          });
      }
    }

    addPlaceholder() {
      this.placeholder = true;
    }

    appendPlaceholder() {
      let contentValue = this.smsTemplateForm.controls['content'].value;
      this.smsTemplateForm.controls['content'].setValue(contentValue + " <!" + this.smsTemplateForm.controls['placeHolder'].value + "!> ");
      this.smsTemplateForm.controls['placeHolder'].setValue("");
      this.placeholder = false;
    }

    maxLengthText(contents){
      if(this.messagePart == 1){
        this.errorZero=false;
        this.maxlen = 160;
        this.maxLengthSMS=160;
      }else if(this.messagePart > 1){
        this.errorZero=false;
        this.maxlen = 153*this.messagePart;
        contents.length>160 ? this.maxLengthSMS=153 : this.maxLengthSMS=160;
      }else{
        this.errorZero=true;
        this.errZeroMsg="Message part length is not configured. Please contact your administrator.";
      }
    }

    maxLengthUnicode(contents){
      if(this.messagePart == 1){
        this.errorZero=false;
        this.maxlen = 70;
        this.maxLengthSMS=70;
      }else if(this.messagePart > 1){
        this.errorZero=false;
        this.maxlen = 67*this.messagePart;
        contents.length>70 ? this.maxLengthSMS=67 : this.maxLengthSMS=70;
      }else{
        this.errorZero=true;
        this.errZeroMsg="Message part length is not configured. Please contact your administrator.";
      }
    }


    isDoubleByte() {
      this.messagePart = this.commonService.getMaxMessageParts();
      let contents = this.smsTemplateForm.controls['content'].value.trim();
      for (var i = 0, n = contents.length; i < n; i++) {
        if(contents.charCodeAt(i) > 255) {
          if(this.unicodeSupport=='Y'){
            this.smsTemplateForm.controls['contentType'].setValue('2');
            this.UnicodeError = false;
            this.maxLengthUnicode(contents);
          }else{
            this.errorMsgType="Enter Text, as Unicode is not supported for this user";
            this.UnicodeError = true;
          }
          break;
        }else{
            this.smsTemplateForm.controls['contentType'].setValue('1');
            this.UnicodeError = false;
            this.maxLengthText(contents);
        }
      }

    }



}
