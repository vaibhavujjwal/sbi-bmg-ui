import { Component, OnInit } from '@angular/core';
import { MODULE } from "../../common/common.const";
import { CommonService } from "../../common/common.service";

@Component({
  selector: 'app-manage-sender-id',
  templateUrl: './manage-sender-id.component.html',
  styleUrls: ['./manage-sender-id.component.scss']
})
export class ManageSenderIdComponent implements OnInit {
  MODULES = MODULE;
  constructor(public commonService: CommonService) { }

  ngOnInit() {
  }

}
