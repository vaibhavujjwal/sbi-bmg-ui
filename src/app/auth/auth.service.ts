import { Injectable } from "@angular/core";
import { NetworkService } from "../common/network.service";
import { Observable } from "rxjs";
import { CommonService } from "../common/common.service";
import { Router } from "@angular/router";
import { HttpResponse } from "@angular/common/http";

@Injectable()
export class AuthService {
  NO_HEADER = null;
  NO_AUHTORIZATION = null;
  NO_PARAMS = null;
  constructor(
    private networkService: NetworkService,
    private _commonService: CommonService,
    private router : Router
  ) {}

  validateLogin(req: any): Observable<HttpResponse<Object>> {
    return this.networkService.postLogin("login", req);
  }

  validateLoginAccount() : Observable<Object>{
    return this.networkService.get(
      "account",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  forgotPasswordLink(username) {
    return this.networkService.put("login/password/forgot/" + username);
  }

  validateToken(token) {
    return this.networkService.get("login/link/validate?link=" + token);
  }

  setPassword(req, token) {
    let basicAuthToken = btoa(req["username"] + ":" + req["password"]);
    sessionStorage.setItem("_basicTkn", basicAuthToken);
    return this.networkService.post(
      "login/password/set?link=" + token,
      this.NO_PARAMS,
      this.NO_HEADER,
      this._commonService.basic
    );
  }

  getCircle(){
    return this.networkService.getNonEncryption("master/circle");
  }

  logOut(){
    this.networkService.getLogout("logout", null , "bearer").subscribe(
      res => {
        sessionStorage.clear();
        this.router.navigate(["/auth/login"]);
      });
   }
}
