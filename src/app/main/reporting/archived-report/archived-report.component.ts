import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ReportingService } from '../reporting.service';
import { ExcelService } from "../../../common/excel.service";
import { CommonService } from "../../../common/common.service";
import * as moment from 'moment';
import { RESPONSE } from "../../../common/common.const";
import { MatTableDataSource, MatSort, MatPaginator } from '../../../../../node_modules/@angular/material';

@Component({
  selector: 'app-archived-report',
  templateUrl: './archived-report.component.html',
  styleUrls: ['./archived-report.component.scss']
})
export class ArchivedReportComponent implements OnInit {
  reportingForm: FormGroup;
  formSubmitted: boolean = false;
  reportList = [];
  searchText: string = "";
  pageLimit: number;
  groupBy: string;
  sDate: any;
  then: any;
  endDte: any;
  edate: any;
  eDate: any;
  now = moment(new Date());
  minDate: any;
  maxDate = this.now.format('YYYY-MM-DD');
  maxDate1 = this.now.format('YYYY-MM-DD');
  formattedFromDate: any;
  formattedToDate: any;
  dataSource: MatTableDataSource<ArchivedReportComponent>;
  dataSourceLength: number;
  @ViewChild('input') input: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['date', 'keyword', 'shortLongCode', 'countTotal', 'countSuccess',
    'countFailed', 'countInactive', 'countUnmatched'];

  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
    private reportingService: ReportingService,
    private excelService: ExcelService, public commonService: CommonService) { }

  ngOnInit() {
   if (this.commonService.getUserByRole() == 'ROLE_ADMIN') {
      this.displayedColumns = ['date', 'keyword','department', 'shortLongCode', 'countTotal', 'countSuccess',
      'countFailed', 'countInactive', 'countUnmatched'];
      }
    this.initializeForm();
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.setEndDAte();
    this.submitForm();
  }

  initializeForm() {
    this.reportingForm = this.fb.group({
      keyword: [''],
      startDate: [new Date().toISOString().substr(0, 10)],
      endDate: [new Date().toISOString().substr(0, 10)],
      groupBy: ['date']
    });
    this.groupBy = this.commonService.titleCase(this.reportingForm.controls['groupBy'].value);
  }

  submitForm() {
    this.formSubmitted = true;
    let fromDate = new Date(this.reportingForm.controls['startDate'].value);
    let toDate = new Date(this.reportingForm.controls['endDate'].value);
    this.formattedFromDate = moment(fromDate).format("YYYY-MM-DD");
    this.formattedToDate = moment(toDate).format("YYYY-MM-DD");
    this.reportingForm.controls['startDate'].setValue(this.formattedFromDate);
    this.reportingForm.controls['endDate'].setValue(this.formattedToDate);
    if (this.reportingForm.valid) {
      let req = {
        keyword: this.reportingForm.controls['keyword'].value,
        startDate: this.reportingForm.controls['startDate'].value,
        endDate: this.reportingForm.controls['endDate'].value,
        groupBy: this.commonService.getGroupBy("mo")
      };
      this.spinner.show();
      this.reportingService.getMOReports(req).subscribe(
        res => {
          this.spinner.hide();
          this.groupBy = this.commonService.titleCase(this.reportingForm.controls['groupBy'].value);
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.reportList = res['data'];
            this.dataSource = new MatTableDataSource(this.reportList);
            if (this.dataSource.data.length == 0) {
              this.dataSourceLength = 0;
            } else {
              this.dataSourceLength = this.dataSource.data.length;
            }
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
          } else {
            this.reportList = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length == 0) {
      this.dataSourceLength = 0;
    }
    else {
      this.dataSourceLength = this.dataSource.filteredData.length;
    }
  }

  setEndDAte() {
    this.sDate = this.reportingForm.controls['startDate'].value,
      this.then = moment(new Date(this.sDate));
    this.minDate = this.then.format('YYYY-MM-DD');
    this.reportingForm.controls['endDate'].setValue(this.sDate);
  }

  setStartDAte() {
    this.maxDate = moment(new Date()).format('YYYY-MM-DD');
  }

  restrictKeyword(event: Event) {
    event.preventDefault();
  }

  exportAs(fileType: string) {
    let data = [];
    this.reportList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Date"] = new Date(element.date).toLocaleDateString('en-GB');
      excelDataObject["Department"] = element.department;
      excelDataObject["Keyword"] = element.keyword;
      excelDataObject["Short/Long Code"] = element.shortLongCode;
      excelDataObject["Total Count"] = element.countTotal;
      excelDataObject["Success Count"] = element.countSuccess;
      excelDataObject["Failed Count"] = element.countFailed;
      excelDataObject["Inactive Count"] = element.countInactive;
      excelDataObject["Unmatched Count"] = element.countUnmatched;
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'MO-Report');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'MO-Report');
        break;
      default:
        this.excelService.exportAsCsvFile(data, 'MO-Report');
        break;
    }
  }
}

  // p: number = 1;
  // dateSorted: boolean = false;
  // departmentSorted: boolean = false;
  // shortLongCodeSorted: boolean = false;
  // keywordSorted: boolean = false;
  // totalCountSorted: boolean = false;
  // successCountSorted: boolean = false;
  // failedCountSorted: boolean = false;
  // inactiveCountSorted: boolean = false;
  // unmatchedCountSorted: boolean = false;
  // isDataAvail:boolean=false;
  
  // sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
  //   this.reportList = this.commonService.sortArray(isAssending, sortBy, this.reportList, isNumber);
  // }