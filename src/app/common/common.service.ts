import { Injectable, Injector } from "@angular/core";
import { Constants } from "./common.const";
import { Router } from "@angular/router";
import { HttpHeaders } from "@angular/common/http";
import Swal from "sweetalert2";
import { ToastrService } from 'ngx-toastr';
import * as FileSaver from "file-saver";
import { AbstractControl } from "../../../node_modules/@angular/forms";

@Injectable()
export class CommonService extends Constants {
  public httpOptions = {};

  recordsPerPage = [10, 20, 30];

  constructor(private injector: Injector, private toastr: ToastrService) {
    super();
  }

  public get router(): Router {
    return this.injector.get(Router);
  }

  seToken() {
    this.httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Basic RklGVXNlcjpGSUZVc2V"
      })
    };
  }

  getToken(endPoint: any, token?: any) {
    switch (endPoint) {
      case this.basic:
        this.httpOptions = {
          headers: new HttpHeaders({
            "Content-Type": "application/json",
            Authorization: "Basic " + token,
            observe: "response",
            responseType: "text"
          })
        };
        break;
      case this.bearer:
        this.httpOptions = {
          headers: new HttpHeaders({
            "Content-Type": "application/json",
            "X-Auth-Token": sessionStorage.getItem("_bearerTkn"),
            observe: "response",
            responseType: "text"
          })
        };
        break;
      default:

        // this.httpOptions = {
        //   headers: new HttpHeaders({
        //     Authorization: "bearer " + sessionStorage.getItem("_bearerTkn")
        //   })
        // };
        break;
    }
    return this.httpOptions;
  }

  getGroupBy(report: any) {
    let groupBy = "";
    switch (report) {
      case 'summary':
        switch (this.getRoleId()) {
          case 3:
            groupBy = "date,department,senderId,aggregator,username,campaignId,categoryId,campaignName";
            break;
          case 1:
            groupBy = "date,senderId,campaignId,categoryId,campaignName";
            break;
          case 4:
            groupBy = "date,senderId,username,campaignId,categoryId,campaignName,userId";
            break;
          default:
            groupBy = "date,department,senderId,aggregator,username,campaignId,categoryId,campaignName,userId";
            break;
        }
        break;
      case 'mo':
        switch (this.getUser()) {
          case 3:
            groupBy = "date,department,keyword,shortLongCode";
            break;
          case 1:
            groupBy = "date,department,keyword,shortLongCode";
            break;
          case 4:
            groupBy = "date,department,keyword,shortLongCode";
            break;
          default:
            groupBy = "date,department,keyword,shortLongCode";
            break;
        }
        break;
      case 'OTTSummary':
        switch (this.getUser()) {
          case 3:
            groupBy = "";
            break;
          case 1:
            groupBy = "";
            break;
          case 4:
            groupBy = "";
            break;
          default:
            groupBy = "department,username";
            break;
        }
        break;
      default:
        switch (this.getUser()) {
          case 3:
            groupBy = "date,department,senderId,aggregator,username,campaignId,categoryId,campaignName";
            break;
          case 1:
            groupBy = "date,senderId,campaignId,categoryId,campaignName";
            break;
          case 4:
            groupBy = "date,senderId,username,campaignId,categoryId,campaignName";
            break;
          default:
            groupBy = "date,department,senderId,aggregator,username,campaignId,categoryId,campaignName";
            break;
        }
        break;
    }
    return groupBy;
  }

  getUserType(pending?: any) {
    let roleId;
    if (sessionStorage && sessionStorage.getItem("_userdata")) {
      roleId = JSON.parse(sessionStorage.getItem("_userdata"))['roleId'];
    }
    switch (roleId) {
      case 2:
        return 1;
        break;
      case 4:
        return 2;
        break;
      case 5:
        return 2;
        break;
      case 3:
        if (pending) {
          return 4;
        } else {
          return 3;
        }
        break;
      case 6:       //FOR  mkt user
        if (pending) {
          return 5;
        } else {
          return 6;
        }
        break;
      default:
        break;
    }
    // switch (roleId) {
    //   case 3:
    //     return 1;
    //     break;
    //   case 1:
    //     return 2;
    //     break;
    //   case 4:
    //     if (hodPending) {
    //       return 4;
    //     } else {
    //       return 3;
    //     }
    //     break;
    //   default:
    //     break;
    // }
  }

  getRoleId() {
    let roleId;
    if (sessionStorage && sessionStorage.getItem("_userdata")) {
      roleId = JSON.parse(sessionStorage.getItem("_userdata"))['roleId'];
    }
    return roleId;
  }


  getUserByRole() {
    let loginData = JSON.parse(sessionStorage.getItem("_userdata"));
    return loginData ? loginData["role"] : "";
  }

  getDeptName() {
    let deptName = JSON.parse(sessionStorage.getItem("_userdata"));
    return deptName ? deptName["department"] : "";
  }

  getDeptID() {
    let deptID = JSON.parse(sessionStorage.getItem("_userdata"));
    return deptID ? deptID["departmentId"] : "";
  }

  getAppName() {
    let appName = JSON.parse(sessionStorage.getItem("_userdata"));
    return appName ? appName["applicationName"] : "";
  }

  getChannelId() {
    let channelId;
    if (sessionStorage && sessionStorage.getItem("_userdata")) {
      channelId = JSON.parse(sessionStorage.getItem("_userdata"))['channelId'];
    }

    return channelId;
  }

  hasReadAccess(module: any) {
    if (sessionStorage && sessionStorage.getItem("_userdata")) {
      let userData = JSON.parse(sessionStorage.getItem("_userdata"));
      let accessObj = JSON.parse(
        JSON.parse(sessionStorage.getItem("_userdata"))["permissionJson"]
      );
      if(userData['role']=="ROLE_CAMPAIGN" && !accessObj['smsTemplateManagement']){
        accessObj['smsTemplateManagement'] = 'RW';
        accessObj['smsTemplateManagement_bulkUpload'] = 'RW';
      }
      return accessObj[module] == "R" || accessObj[module] == "RW";
    }
    return false;
  }

  hasWriteAccess(module: any) {
    let userData = JSON.parse(sessionStorage.getItem("_userdata"));
    if (sessionStorage && sessionStorage.getItem("_userdata")) {
      let accessObj = JSON.parse(
        JSON.parse(sessionStorage.getItem("_userdata"))["permissionJson"]
      );
      if(userData['role']=="ROLE_CAMPAIGN" && !accessObj['smsTemplateManagement']){
        accessObj['smsTemplateManagement'] = 'RW';
        accessObj['smsTemplateManagement_bulkUpload'] = 'RW';
      }
      return accessObj[module] == "RW";
    }
    return false;
  }

  getUser() {
    let loginData = JSON.parse(sessionStorage.getItem("_userdata"));
    return loginData ? loginData["userId"] : "";
  }

  getUserName() {
    let loginData = JSON.parse(sessionStorage.getItem("_userdata"));
    return loginData ? loginData["username"] : "";
  }

  getFCM() {
    let fcmFlag = JSON.parse(sessionStorage.getItem("_userdata"));

    return fcmFlag ? fcmFlag["fcmFlag"] : "";
  }

  getCampaignUserType() {
    let campaignUserType = JSON.parse(sessionStorage.getItem("_userdata"))["category"];
    return campaignUserType ? campaignUserType : "";
  }

  getFullName() {
    let fullName = JSON.parse(sessionStorage.getItem("_userdata"))["firstName"] + " " + JSON.parse(sessionStorage.getItem("_userdata"))["lastName"];
    return (fullName && !fullName.includes('undefined')) ? " - " + fullName : "";
  }

  getUserRole() {
    let loginData = JSON.parse(sessionStorage.getItem("_userdata"));
    let loginUser = loginData ? loginData["role"] : "";
    // if (loginUser == "FUNC") {
    //   return loginData["channelName"] == "Campaign" ? "Bulk User" : "API User"
    // }
    // else {
    return loginUser = loginData ? loginData["roleDescription"] : "";
    // }
  }

  getUserRoleId() {
    let loginData = JSON.parse(sessionStorage.getItem("_userdata"));
    return loginData ? loginData["roleId"] : "";
  }

  getUnicodeSupport() {
    let loginData = JSON.parse(sessionStorage.getItem("_userdata"));
    return loginData ? loginData["unicodeSupport"] : "";
  }
  getMaxMessageParts() {
    let loginData = JSON.parse(sessionStorage.getItem("_userdata"));
    return loginData ? loginData["maxMessageParts"] : "";
  }

  getInternationalFlag() {
    let loginData = JSON.parse(sessionStorage.getItem("_userdata"));
    return loginData ? loginData["internationalRouteFlag"] : "";
  }
  camelize(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
      if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
      return index == 0 ? match.toLowerCase() : match.toUpperCase();
    });
  }

  titleCase(str) {
    let result = str.replace(/([A-Z])/g, " $1");
    return result.charAt(0).toUpperCase() + result.slice(1);
  }

  isNumber(evt) {
    evt = evt ? evt : window.event;
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  // nospaceValidator(control: AbstractControl): { [s: string]: boolean } {
  //   let re = /^\S+(?: \S+)*$/ ;
  //   if (control.value && control.value.match(re)) {
  //     return { nospace: true };
  //   }
  // }
  unicodeToText(unicode) {
    // console.log("Before editing...." + unicode);
    var appendedUnicode = unicode.match(/.{4}/g).join('\\u');
    appendedUnicode = "\\u" + appendedUnicode;
    // console.log("After editing...." + appendedUnicode);
    return eval("decodeURI('" + appendedUnicode + "')");
  }

  checkNumber(evt) {
    evt = evt ? evt : window.event;
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  checkCommaNumber(evt) {
    evt = evt ? evt : window.event;
    var charCode = evt.which ? evt.which : evt.keyCode;
    ;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 44) {
      return false;
    }
    return true;
  }

  sortArray(isAssending: boolean, sortingKey: any, data: any, isNumber?: boolean) {
    if (isNumber) {
      let sortedArray = data.sort(function (a, b) {
        if (a[sortingKey] < b[sortingKey])
          return -1 * (isAssending ? 1 : -1);
        if (a[sortingKey] > b[sortingKey])
          return 1 * (isAssending ? 1 : -1);
        return 0;
      });
      return JSON.parse(JSON.stringify(sortedArray));
    } else {
      let sortedArray = data.sort(function (a, b) {
        if (a[sortingKey].toLowerCase() < b[sortingKey].toLowerCase())
          return -1 * (isAssending ? 1 : -1);
        if (a[sortingKey].toLowerCase() > b[sortingKey].toLowerCase())
          return 1 * (isAssending ? 1 : -1);
        return 0;
      });
      return JSON.parse(JSON.stringify(sortedArray));
    }
  }

  isAuthenticated() {
    if (
      localStorage.getItem("_lgntkn") == null ||
      localStorage.getItem("_lgndata") == null
    ) {
      return false;
    } else {
      return true;
    }
  }

  getTimestamp() {
    return new Date().getTime();
  }

  showSuccessAlertMessage(message) {
    Swal.fire({
      type: "success",
      title: "Success",
      text: message
    });
  }

  showInfoAlertMessage(message) {
    Swal.fire({
      type: "info",
      title: "Alert",
      text: message
    });
  }

  showErrorAlertMessage(message) {
    Swal.fire({
      type: "error",
      title: "Error",
      text: message
    });
  }

  logOut() {
    localStorage.clear();
    this.router.navigate([""]);
  }

  showErrorToast(err?: any) {
    if (err) {
      this.toastr.error('', err);
    } else {
      this.toastr.error('', 'Someting went wrong!');
    }
  }

  showSuccessToast(successFor: any) {

    switch (successFor) {
      case 'delete':
        this.toastr.success('', 'Record is successfully deleted');
        break;
      case 'create':
        this.toastr.success('', 'Record is successfully created');
        break;
      case 'update':
        this.toastr.success('', 'Record is successfully updated');
        break;
      case 'deleteAddressBook':
        this.toastr.success('', ' Address book is successfully deleted');
        break;
      case 'updateMORoute':
        this.toastr.success('', 'MO Route is successfully updated');
        break;
      case 'updateAddressBook':
        this.toastr.success('', 'Address book is successfully updated');
        break;
      default:
        this.toastr.success('', successFor);
        break;
    }
  }

  downloadFile(data: any, fileName: string, fielType: string, fileExt: string) {
    //
    const blob = new Blob([data], { type: fielType });
    FileSaver.saveAs(data, fileName + "_" + new Date().getTime() + fileExt);
  }

}
