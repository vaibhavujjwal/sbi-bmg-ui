import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ReportingComponent } from "./reporting.component";
import { SummaryReportComponent } from "./summary-report/summary-report.component";
import { DetailedReportComponent } from "./detailed-report/detailed-report.component";
import { ArchivedReportComponent } from "./archived-report/archived-report.component";
import { MODULE } from "src/app/common/common.const";
import { MoDetailedReportComponent } from "./mo-detailed-report/mo-detailed-report.component";
import { OttReportComponent } from "./ott-report/ott-report.component";
import { AuthGuardService, ModuleGuardService } from "src/app/guards";
import { FailedSMSComponent } from "./failed-sms/failed-sms.component";
import { UmidReportComponent } from "./umid-report/umid-report.component";

export const routes: Routes = [
  {
    path: "",
    component: ReportingComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/reporting/summary-report",
        pathMatch: "full"
      },
      {
        path: "summary-report",
        component: SummaryReportComponent,
        data: { moduleName: MODULE.SUMMARY_REPORT, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "detailed-report",
        component: DetailedReportComponent,
        data: { moduleName: MODULE.DETAILED_REPORT, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "archived-report",
        component: ArchivedReportComponent,
        data: { moduleName: MODULE.MO_SUMMARY_REPORT, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "mo-detailed-report",
        component: MoDetailedReportComponent,
        data: { moduleName: MODULE.MO_DETAILED_REPORT, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "ott-report",
        component: OttReportComponent,
        data: { moduleName: MODULE.PUSH_NOTIFICATION_REPORT, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      } ,
      {
        path: "failed-sms-report",
        component: FailedSMSComponent,
        data: { moduleName: MODULE.FAILED_SMS_REPORT, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "umid-report",
        component: UmidReportComponent,
        data: { moduleName: MODULE.MO_DETAILED_REPORT, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      }

    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
