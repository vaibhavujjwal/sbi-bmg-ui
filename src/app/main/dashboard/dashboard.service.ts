import { Injectable } from '@angular/core';
import { NetworkService } from '../../common/network.service';
import { forkJoin } from 'rxjs';
import { CommonService } from '../../common/common.service';

@Injectable()
export class DashboardService {
    roleName: string;
    constructor(private _networkService: NetworkService, public commonService: CommonService) { }

    getDept() {
        this.roleName = this.commonService.getUserByRole();
        if (this.roleName == "ROLE_ADMIN") {
            return this._networkService.get('honcho/department', null, 'bearer');
        }
    }
    getAppName(req: any) {
        this.roleName = this.commonService.getUserByRole();
        if (this.roleName == "ROLE_ADMIN") {
            return this._networkService.get('honcho/user/dept/' + req, null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {
            return this._networkService.get('provost/user/dept/' + req, null, 'bearer');
        }
        else if (this.roleName == "ROLE_API") {
            return this._networkService.get('txns/user/dept/' + req, null, 'bearer');
        }
        else if (this.roleName == "ROLE_CAMPAIGN") {
            return this._networkService.get('hype/user/dept/' + req, null, 'bearer');
        }
    }

    getData(req: any) {
        // let userwiseTrafficDetails = this._networkService.get('dashboard/userwiseTrafficDetails?endDate=' + req["endDate"] + '&startDate=' + req['startDate'], null, 'bearer');

        // let generalData = this._networkService.get('dashboard/generalData?endDate=' + req["endDate"] + '&startDate=' + req['startDate'], null, 'bearer');

        // let campaignBulkTraffic = this._networkService.get('dashboard/campaignBulkTraffic?endDate=' + req["endDate"] + '&startDate=' + req['startDate']
        //     , null, 'bearer');

        // let apiTraffic = this._networkService.get('dashboard/apiTraffic?endDate=' + req["endDate"] + '&startDate=' + req['startDate']
        //     , null, 'bearer');
        let deptartmentApiTraffic, campaignBoardData;
        // if (req['isAppNotAvail'] == false) {
            deptartmentApiTraffic = this._networkService.get('dashboard/deptartmentApiTraffic?endDate=' + req["endDate"] + '&startDate=' + req['startDate'] +
                '&appName=' + req['appName'], null, 'bearer');

            campaignBoardData = this._networkService.get('dashboard/campaignBoardData?endDate=' + req["endDate"] + '&startDate=' + req['startDate'] +
                '&appName=' + req['appName'], null, 'bearer');
        // }
        let withoutApp= [deptartmentApiTraffic, campaignBoardData];

        // if (req['isAppNotAvail'] == false) {
            withoutApp.push(deptartmentApiTraffic);
            withoutApp.push(campaignBoardData);
        // }
        return forkJoin(withoutApp);

    }

    getCampTrafficTiles(req: any) {
        return this._networkService.get('dashboard/campaignBulkTraffic?endDate=' + req["endDate"] + '&startDate=' + req['startDate']
            , null, 'bearer');
    }
    getAPITranfficTiles(req: any) {
        return this._networkService.get('dashboard/apiTraffic?endDate=' + req["endDate"] + '&startDate=' + req['startDate']
            , null, 'bearer');
    }
    getUserWiseTraffic(req: any) {
        return this._networkService.get('dashboard/userwiseTrafficDetails?endDate=' + req["endDate"] + '&startDate=' + req['startDate'], null, 'bearer');
    }
    getGeneralData(req: any) {
        return this._networkService.get('dashboard/generalData?endDate=' + req["endDate"] + '&startDate=' + req['startDate'], null, 'bearer');
    }

    getUptimeData(req: any) {
        return this._networkService.get('dashboard/uptimeData?reportDate=' + req["reportDate"] + '&reportHour=' + req['report']
        , null, 'bearer');
    }
    timestampGraphData(req: any) {
        return this._networkService.get('dashboard/timestampGraphData?reportDate=' + req["reportDate"] + '&reportHour=' + req['report']
        , null, 'bearer');
    }
}
