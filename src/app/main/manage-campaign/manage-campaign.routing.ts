import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ManageCampaignComponent } from "./manage-campaign.component";
import { ScheduleCampaignComponent } from "./schedule-campaign/schedule-campaign.component";
import { PendingApprovalComponent } from "./pending-approval/pending-approval.component";
import { ViewScheduledCampaignComponent } from "./view-scheduled-campaign/view-scheduled-campaign.component";
import { AuthGuardService, ModuleGuardService } from "src/app/guards";
import { MODULE } from "src/app/common/common.const";

export const routes: Routes = [
  {
    path: "",
    component: ManageCampaignComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/manage-campaign/view-schedule-campaign",
        pathMatch: "full"
      },
      {
        path: "schedule-campaign",
        component: ScheduleCampaignComponent,
        data: { moduleName: MODULE.CAMPAIGN, permissions: ["RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "view-schedule-campaign",
        component: ViewScheduledCampaignComponent,
        data: { moduleName: MODULE.CAMPAIGN, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "pending-approval",
        component: PendingApprovalComponent,
        data: { moduleName: MODULE.CAMPAIGN, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
