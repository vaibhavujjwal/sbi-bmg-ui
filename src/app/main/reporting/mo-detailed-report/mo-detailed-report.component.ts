import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '../../../../../node_modules/@angular/forms';
import { NgxSpinnerService } from '../../../../../node_modules/ngx-spinner';
import { ReportingService } from '../reporting.service';
import { ExcelService } from '../../../common/excel.service';
import { CommonService } from '../../../common/common.service';
import * as moment from 'moment';
import { RESPONSE } from '../../../common/common.const';
import { MatTableDataSource, MatSort, MatPaginator } from '../../../../../node_modules/@angular/material';
import { MoDetailedResponse } from './mo-detailed-response';

@Component({
  selector: 'app-mo-detailed-report',
  templateUrl: './mo-detailed-report.component.html',
  styleUrls: ['./mo-detailed-report.component.scss']
})
export class MoDetailedReportComponent implements OnInit {
  reportingForm: FormGroup;
  formSubmitted: boolean = false;
  searchText: string = "";
  pageLimit: number;
  p: number = 1;
  sDate: any;
  then: any;
  endDte: any;
  edate: any;
  eDate: any;
  now = moment(new Date());
  minDate: any;
  maxDate = this.now.format('YYYY-MM-DD');
  maxDate1 = this.now.format('YYYY-MM-DD');
  formattedFromDate: any;
  formattedToDate: any;
  reportList = [];
  isDataAvail: boolean = false;
  // dateSorted: boolean = false;
  // mobileSorted: boolean = false;
  // codeSorted: boolean = false;
  // keywordSorted: boolean = false;
  // messageSorted: boolean = false;
  // deptSorted: boolean = false;
  // statusSorted: boolean = false;
  // errCodeSorted: boolean = false;
  // errTypeSorted: boolean = false;
  // errDescSorted: boolean = false;
  dataSource: MatTableDataSource<MoDetailedResponse>;
  dataSourceLength: number;
  @ViewChild('input') input: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['recTime', 'msisdn', 'shortCode', 'keyword', 'message',
    'source', 'errorCode', 'errorType', 'errorDesc'];

  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService, private reportingService: ReportingService,
    private excelService: ExcelService, public commonService: CommonService) { }

  ngOnInit() {
    if (this.commonService.getUserByRole() == 'ROLE_ADMIN' || this.commonService.getUserByRole() == 'ROLE_API') {
      this.displayedColumns[5] = 'deptName';
    }
    this.initializeForm();
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.setEndDAte();
    this.submitForm();
  }
  initializeForm() {
    this.reportingForm = this.fb.group({
      msdin: ['', [Validators.minLength(10), Validators.maxLength(16), Validators.pattern(this.commonService.patterns.phone)]],
      startDate: [new Date().toISOString().substr(0, 10)],
      endDate: [new Date().toISOString().substr(0, 10)],
      keyword: ['']
    });
  }
  setEndDAte() {
    this.sDate = this.reportingForm.controls['startDate'].value,
      this.then = moment(new Date(this.sDate));
    this.minDate = this.then.format('YYYY-MM-DD');
    this.reportingForm.controls['endDate'].setValue(this.sDate);
  }

  restrictKeyword(event: Event) {
    event.preventDefault();
  }
  setStartDAte() {
    this.maxDate = moment(new Date()).format('YYYY-MM-DD');
    // this.eDate = this.reportingForm.controls['endDate'].value,
    //   this.edate = moment(new Date(this.eDate));
    // this.maxDate = this.edate.format('YYYY-MM-DD');
  }
  submitForm() {
    this.formSubmitted = true;
    let fromDate = new Date(this.reportingForm.controls['startDate'].value);
    let toDate = new Date(this.reportingForm.controls['endDate'].value);

    this.formattedFromDate = moment(fromDate).format("YYYY-MM-DD");
    this.formattedToDate = moment(toDate).format("YYYY-MM-DD");

    this.reportingForm.controls['startDate'].setValue(this.formattedFromDate);
    this.reportingForm.controls['endDate'].setValue(this.formattedToDate);
    if (this.reportingForm.valid) {
      let req = {
        msisdn: this.reportingForm.controls['msdin'].value,
        startDate: this.reportingForm.controls['startDate'].value,
        endDate: this.reportingForm.controls['endDate'].value,
        keyword: this.reportingForm.controls['keyword'].value
      };
      this.spinner.show();

      this.reportingService.getMODetailReports(req).subscribe(
        res => {
          this.spinner.hide();
          console.log("Response....." + JSON.stringify(res));
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.reportList = res['data']['reportResponseList'];

            if (this.reportList == null) {
              this.reportList = [];
            }

            if (this.reportList) {
              this.reportList.forEach(element => {
                if (element['source'] == 'F') {
                  element['source'] = 'Fail';
                }
                else {
                  element['source'] = "Success";
                }
              });
            }
            console.log("MO Detailed Reports...." + JSON.stringify(this.reportList));
            this.dataSource = new MatTableDataSource(this.reportList);
            console.log("Datasource data...." + this.dataSource.data);
            if (this.dataSource.data.length == 0) {
              this.dataSourceLength = 0;
            } else {
              this.dataSourceLength = this.dataSource.data.length;
            }
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;

          } else {
            this.reportList = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
    }
    else {
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (this.dataSource != undefined) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
      if (this.dataSource.filteredData.length == 0) {
        this.dataSourceLength = 0;
      }
      else {
        this.dataSourceLength = this.dataSource.filteredData.length;
      }
    }

  }

  // sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
  //   this.reportList = this.commonService.sortArray(isAssending, sortBy, this.reportList, isNumber);
  // }
  exportAs(fileType: string) {
    let data = [];
    this.reportList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Date"] = new Date(element.recTime).toLocaleDateString('en-GB');
      excelDataObject["Mobile Number"] = element.msisdn;
      excelDataObject["Short Code/ Long Code"] = element.shortCode;
      excelDataObject["MO Keyword"] = element.keyword;
      excelDataObject["MO Message"] = element.message;
      excelDataObject["Department Name"] = element.deptName;
      excelDataObject["Status"] = element.source;
      excelDataObject["Error Code"] = element.errorCode;
      excelDataObject["Error Type"] = element.errorType;
      excelDataObject["Error Description"] = element.errorDesc;
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'MO-Detailed-Report');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'MO-Detailed-Report');
        break;
      default:
        this.excelService.exportAsCsvFile(data, 'MO-Detailed-Report');
        break;
    }
  }
  pdfResponse: string = '';
  showPdf() {
    let fromDate = new Date(this.reportingForm.controls['startDate'].value);
    let toDate = new Date(this.reportingForm.controls['endDate'].value);
    this.formattedFromDate = moment(fromDate).format("YYYY-MM-DD");
    this.formattedToDate = moment(toDate).format("YYYY-MM-DD");
    this.reportingForm.controls['startDate'].setValue(this.formattedFromDate);
    this.reportingForm.controls['endDate'].setValue(this.formattedToDate);
    if (this.reportingForm.valid) {
      let req = {
        msisdn: this.reportingForm.controls['msdin'].value,
        startDate: this.reportingForm.controls['startDate'].value,
        endDate: this.reportingForm.controls['endDate'].value,
        keyword: this.reportingForm.controls['keyword'].value
      };
      this.spinner.show();
      this.reportingService.generateMODetailedPDF(req).subscribe(
        res => {
          this.spinner.hide();
          console.log("PDF data...." + res);
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.pdfResponse = res['data'];
            console.log("PDF API response........" + this.pdfResponse);
            let date = new Date();
            console.log("Date for PDF....:" + moment(date).format('YYYYMMDDHHMMSS'))
            const fileName = "MO_DETAILED_REPORT" + moment(date).format('YYYYMMDDHHMMSS') + ".pdf";
            this.downloadFile(this.pdfResponse, fileName);


            // const linkSource = this.pdfResponse;
            // const downloadLink = document.createElement("a");
            // let date = new Date();
            // console.log("Date for PDF name" + moment(date).format('YYYYMMDDHHMMSS'))
            // const fileName = "MO_DETAILED_REPORT" + moment(date).format('YYYYMMDDHHMMSS') + ".pdf";
            // downloadLink.href = linkSource;
            // downloadLink.download = fileName;
            // downloadLink.click();
          } else {
            this.pdfResponse = '';
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });

    }
  }

  downloadFile(pdfBase64, fileName) {
    var res = pdfBase64.substr(28, (pdfBase64.length - 28));
    pdfBase64 = res;
    console.log("substr..." + pdfBase64);
    // IE doesn't allow using a blob object directly as link href
    // instead it is necessary to use msSaveOrOpenBlob
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      var byteCharacters = atob(pdfBase64);
      var byteNumbers = new Array(byteCharacters.length);
      for (var i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      var blob = new Blob([byteArray], { type: 'application/pdf' });
      window.navigator.msSaveOrOpenBlob(blob, fileName);
      return;
    }

    // For Other Browsers
    const linkSource = 'data:application/pdf;base64,' + pdfBase64;
    const link = document.createElement('a');
    link.href = linkSource;
    link.download = fileName;
    document.body.appendChild(link);
    link.click();
    link.remove();

    window.addEventListener("focus", function () {
      URL.revokeObjectURL(link.href);
    }, { once: true });
  }

}
