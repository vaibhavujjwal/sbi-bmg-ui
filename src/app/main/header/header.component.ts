import { Component, OnInit } from "@angular/core";
import { CommonService } from "src/app/common/common.service";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ManageUserService } from "../manage-user/manage-user.service";
import { RESPONSE } from "../../common/common.const";
import { AuthService } from "../../auth/auth.service";
import { NgbModal, NgbModalRef, NgbModalOptions, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
  providers: [ManageUserService,AuthService]
})
export class HeaderComponent implements OnInit {

  private modalRef: NgbModalRef;
  largeModalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false,
    size: "lg"
  };
  userDetails = [];
  viewPermissionList = [];
  accessPermissionViewList = [];

  closeResult = "";

  constructor(public commonService: CommonService, private router: Router,
    private spinner: NgxSpinnerService, private manageUserService: ManageUserService,
    private modalService: NgbModal, private authService : AuthService) {}

  ngOnInit() {}

  viewUser(content) {
    let userId = this.commonService.getUser();
    this.spinner.show();
    this.manageUserService.getUser(userId).subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        this.userDetails = data["data"];
        this.viewPermissionList = JSON.parse(
          this.userDetails["permissionJson"]
        );

        if (this.accessPermissionViewList.length == 0)
          this.accessPermissionViewList = this.manageUserService.getPermissionsViewList();
        this.spinner.hide();
        this.modalRef = this.modalService.open(content, this.largeModalOptions);
        this.modalRef.result.then(
          result => {
            this.closeResult = `Closed with: ${result}`;
          },
          reason => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          }
        );
      } else {
        this.commonService.showErrorToast(data['result']['userMsg']);
      }
    });
  }

  logout() {
    this.authService.logOut();
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
