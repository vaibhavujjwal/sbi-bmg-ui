import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBulkTemplateComponent } from './create-bulk-template.component';

describe('CreateBulkTemplateComponent', () => {
  let component: CreateBulkTemplateComponent;
  let fixture: ComponentFixture<CreateBulkTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBulkTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBulkTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
