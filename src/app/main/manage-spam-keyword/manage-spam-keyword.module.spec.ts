import { ManageSpamKeywordModule } from './manage-spam-keyword.module';

describe('ManageSpamKeywordModule', () => {
  let manageSpamKeywordModule: ManageSpamKeywordModule;

  beforeEach(() => {
    manageSpamKeywordModule = new ManageSpamKeywordModule();
  });

  it('should create an instance', () => {
    expect(manageSpamKeywordModule).toBeTruthy();
  });
});
