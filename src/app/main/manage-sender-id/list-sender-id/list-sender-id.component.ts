import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { SenderIdService } from '../manage-sender-id.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ExcelService } from "../../../common/excel.service";
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MODULE } from "../../../common/common.const";
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

export interface senderIDTypes {
  Actions: any;
  senderid: any;
  username:any;
  status: any;
}

@Component({
  selector: 'app-list-sender-id',
  templateUrl: './list-sender-id.component.html',
  styleUrls: ['./list-sender-id.component.scss']
})

export class ListSenderIdComponent implements OnInit {
  MODULES = MODULE;
  pageLimit: number;
  modalRef:any;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  searchText: string = "";
  p: number = 1;
  reqGetSender = {};
  senderIdList = [];
  recordName: any;
  formSubmitted: boolean = false;
  roleName: any;
  deleteReq = {};
  statusForm: FormGroup;
  displayedColumns: string[] = ['senderid',	'status'];
  displayedColumns1: string[] = ['actions',	'senderid',	'username','status'];
  displayedColumns2: string[] = ['senderid',	'username','status'];

  dataSource: MatTableDataSource<senderIDTypes>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  showError:boolean=false;


  constructor(private modalService: NgbModal, private senderIdService: SenderIdService,
     private fb: FormBuilder, private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService, private excelService: ExcelService, public commonService: CommonService) {
  }

  ngOnInit() {
    this.getSenderIdList();
    this.pageLimit = this.commonService.recordsPerPage[0];
  }

  prepareApprovalForm() {
    this.statusForm = this.formBuilder.group({
      remarks: [ "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumaric)
        ]
      ]
    });
  }

  getSenderIdList() {
    this.spinner.show();
    this.reqGetSender = {
      "roleName": this.commonService.getUserByRole()
    }
    this.senderIdService.getSenderIdListByUser(this.reqGetSender).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.senderIdList = res["data"];
          this.senderIdList.forEach(element => {
            element['status'] == 1 ? element['status'] = "Enabled" :  element['status'] = "Disabled";
           });

           this.dataSource= new MatTableDataSource(this.senderIdList) ;
           this.dataSource.sort = this.sort;
           this.dataSource.paginator = this.paginator;

          } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.senderIdList = [];
        }
      });
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.showError=false;
    if(this.dataSource.filteredData.length==0){
      this.showError=true;
    }
  }


  confirmDeleteRecord(updatemodal: any, record: any) {
    this.recordName = record.templateName;
    this.formSubmitted = false;
    this.prepareApprovalForm();
    this.deleteReq = {
      "id": record['id'],
      "senderid": record['senderid']
    }
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  deleteRecord() {
    this.deleteReq['remarks'] = this.statusForm.controls['remarks'].value;
    this.formSubmitted = true;
    if (this.statusForm.valid) {
      this.spinner.show();
      this.senderIdService.deleteSenderId(this.deleteReq).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast(this.deleteReq['senderid'] + ' sender CLI has been deleted');
            this.modalRef.close();
            this.getSenderIdList();
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
          this.formSubmitted = false;
        });
    }
  }

  changeStatusConfirmation(record: any,id) {
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    if (record['status'] == 'Enabled') {
      modalRef.componentInstance.content = 'Are you sure to disable "' + record['senderid'] + '" sender CLI?';
    } else {
      modalRef.componentInstance.content = 'Are you sure to enable "' + record['senderid'] + '" sender CLI?';
    }
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.changeStatus(record);
        } else {
          document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
        }
      },
      (reason: any) => {
        document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
      });
  }

  changeStatus(record) {
    let req = {
      "senderid": record['id'],
      "approvedBy": record['approvedBy'],
      "status": record['status'] == "Enabled" ? 2 : 1
    };
    this.spinner.show();
    this.senderIdService.updateSattus(req).subscribe(
      res => {
        this.spinner.hide();
        if (record['status'] == 'Enabled') {
          this.commonService.showSuccessToast('"' + record['senderid'] + '" sender CLI Disabled');
        } else if (record['status'] == 'Disabled') {
          this.commonService.showSuccessToast('"' + record['senderid'] + '" sender CLI Enabled');
        }
        this.getSenderIdList();
      });
  }

  exportAs(fileType: string) {
    let data = [];
    this.senderIdList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Sender ID"] = element.senderid;
      excelDataObject["Name"] = element.username;
      excelDataObject["Department"] = element.department;
      excelDataObject["Status"] = element.status == 1 ? 'Enable' : 'Disable';
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'CLI-List');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'CLI-List');
        break;
      default:
        this.excelService.exportAsExcelFile(data, 'CLI-List');
        break;
    }
  }
}
