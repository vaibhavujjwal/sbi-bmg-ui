import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewDepartmentComponent } from './view-department/view-department.component';
import { AddDepartmentComponent } from './add-department/add-department.component';
import { ManageDepartmentComponent } from './manage-department.component';
import { routing } from './manage-department.routing';
import { SharedModule } from "../../common/shared.modules";
import { ManageDepartmentService } from './manage-department.service';
import {MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule} from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    routing,
    SharedModule,
    MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
    MatSortModule, MatTableModule

  ],
  declarations: [ViewDepartmentComponent, AddDepartmentComponent, ManageDepartmentComponent],
  providers: [ManageDepartmentService]
})
export class ManageDepartmentModule { }
