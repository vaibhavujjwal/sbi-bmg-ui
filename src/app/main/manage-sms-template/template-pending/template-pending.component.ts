import { Component, OnInit, ViewChild } from '@angular/core';
import { ManageSmsTemplateService } from "../manage-sms-template.service";
import { NgxSpinnerService } from "ngx-spinner";
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NgbModal, NgbModalOptions, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { CommonService } from "../../../common/common.service";
import { RESPONSE, MODULE } from "../../../common/common.const";
import { ExcelService } from "../../../common/excel.service";
import * as moment from 'moment';
import { stat } from 'fs';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

export interface tempResTypes {
  actions: any;
  templateName: string;
  language: any;
  template: any;
  caseSensitivityFlag: any;
  maskFlag: any;
  maskText: any;
  username: string;
  creationDate: any;
  approvedBy: any;
  status: any;
}

@Component({
  selector: 'app-template-pending',
  templateUrl: './template-pending.component.html',
  styleUrls: ['./template-pending.component.scss']
})

export class TemplatePendingComponent implements OnInit {

  MODULES = MODULE;
  mainGroup: FormGroup;
  now = moment(new Date());
  maxDate = this.now.format('YYYY-MM-DD');
  dltApproveForm: FormGroup;
  templateDetail: any;
  uploadedFileId: any;
  buttonName: any;
  largeModalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false,
    size: "lg"
  };
  modalRef: any;
  approvalReq = {};
  formSubmitted: any;
  math = Math;
  changeStatusCamp: boolean = false;
  fileUploaded: boolean = false;
  filenotUploaded: boolean = false;
  fileCheck: boolean = false;
  userDetails: object;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  pageLimit: number;
  searchText: string = "";
  p: number = 1;
  closeResult: string;
  showError: boolean = false;
  requestDate: any;
  statusDate: any;
  statusForm: FormGroup;
  bulkForm: FormGroup;
  updateUserStatusEvent: string;
  // tempRes: TemplateResponse[];
  tempRes: any;
  bulkApprovalReq: any;
  dltIdFlag: boolean;
  statusForBulk: number;
  accepted: Boolean;
  isBulkBtnEnable: boolean = false;
  dltIdlist: string[];
  userRole = this.commonService.getUserByRole();
  displayedColumns: string[] = ['actions',
    'spiceTemplateid',
    'templateIdDlt',
    'templateName',
    'templateType',
    'senderId',
    'language',
    'template',
    'caseSensitivityFlag',
    'maskFlag',
    'maskText',
    'username',
    'creationDate',
    'approvedBy',
    'status'];
  dataSource: MatTableDataSource<tempResTypes>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('fileInput') fileInput: any;

  constructor(private manageSmsTemplateService: ManageSmsTemplateService, private spinner: NgxSpinnerService,
    private modalService: NgbModal, private fb: FormBuilder, private formBuilder: FormBuilder,
    public commonService: CommonService, private excelService: ExcelService) { }

  ngOnInit() {
    this.getTemplateList();
    this.dltIdFlag = true;
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.maxDate = moment(new Date()).format('YYYY-MM-DD');
  }

  getTemplateList() {
    this.accepted = false;
    let req = {
      "id": 0,
      "userRole": this.userRole
    }
    this.spinner.show();
    this.manageSmsTemplateService.getTemplateListPending(req).subscribe(
      res => {
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.tempRes = res["data"];

          let group = {};
          this.tempRes.forEach(element => {
            group[element['templateName']] = new FormControl('');
          })
          this.mainGroup = new FormGroup(group);

          if (this.tempRes.length > 0) {
            this.isBulkBtnEnable = true;
          } else {
            this.isBulkBtnEnable = false;
          }
          this.tempRes.forEach(element => {
            if (element['status'] == 10) {
              element['status'] = "DLT Approval pending";
            } else if (element['status'] == 0) {
              element['status'] = "HOD Approval pending";
            } else {
              element['status'] = "Rejected";
            }
          });
          this.tempRes.forEach(element => {
            if (!element['templateIdDlt'] || element['templateIdDlt'] == 0) {
              element['templateIdDlt'] = 'NA';
            }
          });
          this.tempRes.forEach(element => {
            if (!element['senderId']) {
              element['senderId'] = 'NA';
            }
          });

          this.tempRes.forEach(element => {
            if (element['templateType'] == 'inf') {
              element['templateType'] = 'Informational';
            } else if (element['templateType'] == 'otp') {
              element['templateType'] = 'OTP';
            } else if (element['templateType'] == 'promo') {
              element['templateType'] = 'Promotional';
            } else if (element['templateType'] == 'txn') {
              element['templateType'] = 'Transactional';
            }
          });


          this.tempRes.forEach(element => {
            element['caseSensitivityFlag'] == 'Y' ? element['caseSensitivityFlag'] = "Yes" : element['caseSensitivityFlag'] = "No";
            element['maskFlag'] == 'Y' ? element['maskFlag'] = "Yes" : element['maskFlag'] = "No";
          });

          this.tempRes.forEach(element => {
            if (element.language == 2) {
              if (element['maskText'] && element['maskText'] != "") {
                try {
                  element['maskText'] = this.commonService.unicodeToText(element['maskText']);
                }
                catch (e) {
                  console.log("comes in catch block...");
                }
              }
              try {
                element['template'] = this.commonService.unicodeToText(element['template']);
              } catch (e) {
                console.log("comes in catch block...");
              }
            }
            element['checked'] = false;
          });
          console.log("Response of pending template list....." + JSON.stringify(this.tempRes));
          this.dataSource = new MatTableDataSource(this.tempRes);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.spinner.hide();



        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.tempRes = [];
          this.isBulkBtnEnable = false;

        }
        this.spinner.hide();
      });
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.showError = false;
    if (this.dataSource.filteredData.length == 0) {
      this.showError = true;
    }
  }


  deSelectAll() {
    this.accepted = false;
    for (let i = 0; i < this.tempRes.length; i++) {
      if (this.tempRes[i].checked) {
        this.tempRes[i].checked = false;
      }
    }
  }
  dltChange() {
    this.dltApproveForm.controls['otherDLT'].setValidators([])
    if (this.dltApproveForm.controls['registeredDLT'].value == 'O') {
      this.dltApproveForm.controls['otherDLT'].setValidators([Validators.required])
    }
  }

  checkAll() {
    let final = ("" + this.p + 0);
    let index = Number(final) - 10;
    this.accepted = true;

    let tempLength = Number(this.pageLimit) + index;
    if (tempLength > this.tempRes.length) {
      tempLength = this.tempRes.length;
    }
    else {
      tempLength = tempLength;
    }
    for (let i = index; i < tempLength; i++) {
      if (this.tempRes[i].checked) {
        this.tempRes[i].checked = false;
        this.mainGroup.controls[this.tempRes[i]['templateName']].setValidators([])
        this.mainGroup.controls[this.tempRes[i]['templateName']].updateValueAndValidity();
      } else {
        this.tempRes[i].checked = true;
        this.mainGroup.controls[this.tempRes[i]['templateName']].setValidators([Validators.required, Validators.minLength(3),
        Validators.maxLength(20), Validators.pattern(this.commonService.patterns.numberOnly)])
        this.mainGroup.controls[this.tempRes[i]['templateName']].updateValueAndValidity();
      }
    }
  }

  pagechng() {
    this.accepted = false;
    for (let i = 0; i < this.tempRes.length; i++) {
      if (this.tempRes[i].checked) {
        this.tempRes[i].checked = false;
      }
    }
  }
  bulkApprovalType: String;
  //confirmationForBulkApp(formObject)
  confirmationForBulkApp(bulkApproval, type, obj, btn) {
    if (this.userRole == 'ROLE_DLT' && obj.invalid) {
      Object.keys(obj.controls).forEach((key, index) => {
        console.log(key)
        if (obj.controls[key].invalid) {
          obj.controls[key].markAsTouched();
        }
      })
      return;
    } else if (this.userRole == 'ROLE_DLT' && this.fileCheck && !this.fileUploaded) {
      this.filenotUploaded = true;
      return;
    }
    let reqobj = obj && obj['controls'] ? obj['controls'] : {};
    let templateIdlist = [];
    this.prepareApprovalForm();
    //this.updateForm(obj);
    this.changeStatusCamp = false;
    this.bulkApprovalType = type ? type : this.buttonName;
    let remarks;
    const selectedTemplates = this.tempRes.filter((template) => template['checked']);
    let result = selectedTemplates.map(a => a.id);

    if (type == "bulkApprove" || (reqobj['approvalStatus'] && reqobj['approvalStatus'].value.toLowerCase() == "approve")) {
      this.statusForBulk = 1;
      remarks = "Approve";
    } else {
      this.statusForBulk = 2;
      remarks = "Reject";
    }

    if (this.userRole == 'ROLE_DLT' && this.fileCheck && this.fileUploaded) {
      //this.fileUploaded = false;
      this.bulkApprovalReq = {
        "templateId": [], //result,
        "status": 1,
        "remarks": "",
        "approvedBy": this.commonService.getUser(),
        "dltTemplateId": '',
        "dltTelemarketer": '',
        "dltTemplateName": '',
        "dltType": '',
        "dltHeader": '',
        "dltCategory": '',
        "registeredDlt": '',
        "dltRequestedOn": null,
        "dltStatusDate": null,//"2021-01-20",
        "dltApprovalStatus": '',
        "dltStatus": '',
        "dltTemplateMessage": '',
        "dltConsentType": '',
        "dltRejectionReason": '',
        "fileId": this.uploadedFileId
      }
      this.sendCheckedTemp();
    } else if (result.length >= 1 || (this.templateDetail && this.templateDetail['id'])) {
      templateIdlist.push(this.templateDetail ? this.templateDetail['id'] : '');
      if (reqobj.hasOwnProperty()) {
        this.requestDate = new Date(reqobj['requestedOn'].value);
        this.statusDate = new Date(reqobj['statusDate'].value);
        reqobj['registeredDLT'].setValue(reqobj['registeredDLT'].value == 'O' ? reqobj['otherDLT'].value : reqobj['registeredDLT'].value);
      }
      this.bulkApprovalReq = {
        "templateId": result.length >= 1 ? result : templateIdlist, //result,
        "status": this.statusForBulk,
        "remarks": remarks,
        "approvedBy": this.commonService.getUser(),
        "dltTemplateId": selectedTemplates && selectedTemplates[0] ? selectedTemplates[0].templateIdDlt : reqobj['dltTemplateId'].value,
        // "dltTelemarketer": selectedTemplates && selectedTemplates[0]?"":reqobj['telemarketer'].value,
        "dltTelemarketer": "",
        "dltTemplateName": selectedTemplates && selectedTemplates[0] ? "" : reqobj['dltTemplateName'].value,
        "dltType": selectedTemplates && selectedTemplates[0] ? "" : reqobj['type'].value,
        "dltHeader": selectedTemplates && selectedTemplates[0] ? "" : reqobj['header'].value,
        "dltCategory": selectedTemplates && selectedTemplates[0] ? "" : reqobj['category'].value,
        "registeredDlt": selectedTemplates && selectedTemplates[0] ? "" : reqobj['registeredDLT'].value,
        "dltRequestedOn": selectedTemplates && selectedTemplates[0] ? "" : moment(this.requestDate).format("YYYY-MM-DD"),
        "dltStatusDate": selectedTemplates && selectedTemplates[0] ? "" : moment(this.statusDate).format("YYYY-MM-DD"),//"2021-01-20",
        "dltApprovalStatus": selectedTemplates && selectedTemplates[0] ? "" : reqobj['approvalStatus'].value,
        "dltStatus": selectedTemplates && selectedTemplates[0] ? "" : reqobj['status'].value,
        "dltTemplateMessage": selectedTemplates && selectedTemplates[0] ? selectedTemplates[0].template : this.templateDetail['template'],
        //"dltTemplateMessage": this.dltApproveForm && this.dltApproveForm.controls['dlttemplatemessage']?this.dltApproveForm.controls['dlttemplatemessage'].value:"", 
        "dltConsentType": selectedTemplates && selectedTemplates[0] ? "" : reqobj['consentType'].value,
        "dltRejectionReason": selectedTemplates && selectedTemplates[0] ? "" : reqobj['rejectionReason'] ? reqobj['rejectionReason'].value : "",
        "fileId": 0
      }
      if (this.userRole == 'ROLE_DLT') {
        this.sendCheckedTemp();
      } else {
        console.log("Bulk Approval Rejection req...." + JSON.stringify(this.bulkApprovalReq));
        btn &&
          btn.parentElement &&
          btn.parentElement.parentElement &&
          btn.parentElement.parentElement.blur();
        this.modalRef = this.modalService.open(bulkApproval, this.largeModalOptions);
        this.modalRef.result.then(
          result => {
            this.closeResult = `Closed with: ${result}`;
          },
          reason => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          }
        );
      }

    }
    else if (!this.fileUploaded) {
      var message = this.mainGroup.invalid ? "Please enter numeric DLT Template ID" :
        "Select template(s) to approve / reject."
      this.commonService.showErrorToast(message);
    }
  }

  bulkApprovalReject(uppLoadTemplates, type, btn) {
    console.log(type);
    this.bulkForm = this.formBuilder.group({
    });
    this.fileUploaded = false;
    this.fileCheck = true;
    // this.fileInput.nativeElement.value = '';
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(uppLoadTemplates, this.largeModalOptions);
    this.modalRef.result.then(
      result => {
        this.fileCheck = false;
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.fileCheck = false;
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }


  downloadtemp() {
    this.spinner.show();
    this.manageSmsTemplateService.downloadTEmplate()
      .subscribe(res => {
        console.log("Bulk Approval response...." + JSON.stringify(res));
        if (!res['data']) {
          this.commonService.showErrorToast("No Data Found");
          return;
        }
        var csv = res['data'].substr(29, (res['data'].length - 29));
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          var byteCharacters = atob(csv);
          var byteNumbers = new Array(byteCharacters.length);
          for (var i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
          }
          var byteArray = new Uint8Array(byteNumbers);
          var blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
          window.navigator.msSaveOrOpenBlob(blob, "Templates.xls");
          return;
        }

        // For Other Browsers
        const linkSource = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' + csv;
        const link = document.createElement('a');
        link.href = linkSource;
        link.download = "Templates.xls";
        document.body.appendChild(link);
        link.click();
        link.remove();

        window.addEventListener("focus", function () {
          URL.revokeObjectURL(link.href);
        }, { once: true });
        this.spinner.hide();
      },
        err => {
          this.commonService.showErrorToast(err['error'] ? err['error']['userMsg'] : "No Data Found");
        });
  }

  sendCheckedTemp() {
    if (this.statusForBulk == 1) {
      this.statusForm.controls.remarks.setValue("approved");
    }
    this.changeStatusCamp = true;
    if (this.statusForm.valid || (this.dltApproveForm && this.dltApproveForm.valid) || this.fileUploaded) {
      let callAPI = '';
      this.bulkApprovalReq['remarks'] = this.statusForm.controls.remarks.value;
      if (this.dltApproveForm && this.dltApproveForm.valid) {
        this.bulkApprovalReq['remarks'] = this.buttonName.toLowerCase() == 'approve' ? 'approved' : 'rejected';
      }
      this.modalRef.close();
      this.spinner.show();
      if (this.userRole == 'ROLE_DLT') {
        callAPI = 'dltBulkApproval';
        //this.bulkApprovalReq['status'] = this.bulkApprovalReq['remarks']=='approved'?0:this.bulkApprovalReq['status'];
        this.bulkApprovalReq['status'] = this.bulkApprovalReq['remarks'] == 'approved' ? 0 : this.bulkApprovalReq['status'];
        if (this.fileUploaded) {
          this.bulkApprovalReq['status'] = 0;
          this.bulkApprovalReq['remarks'] = "";
        }
      } else {
        callAPI = 'bulkApproval';
      }
      this.manageSmsTemplateService[callAPI](this.bulkApprovalReq)
        .subscribe(res => {
          console.log("Bulk Approval response...." + JSON.stringify(res));
          if (this.statusForBulk == 1 && this.bulkApprovalReq['status'] != 0) {
            this.commonService.showSuccessToast("Selected template(s) has been approved");
          } else if (this.bulkApprovalReq['status'] == 0) {
            this.commonService.showSuccessToast("Selected template(s) has been sent to HOD for approval");
          }
          else {
            this.commonService.showSuccessToast("Selected template(s) has been rejected");
          }
          this.getTemplateList();
          this.spinner.hide();
        },
          err => {
            this.commonService.showErrorToast(err['error']['userMsg']);
          });
    }

  }
  updateCampStatus() {
    if (this.updateUserStatusEvent == "Approve") {
      this.statusForm.controls.remarks.setValue("approved");
    }
    this.approvalReq['remarks'] = this.statusForm.controls.remarks.value;
    this.approvalReq['userID'] = this.commonService.getUser();
    this.changeStatusCamp = true;
    if (this.statusForm.valid) {
      this.spinner.show();
      this.manageSmsTemplateService.updateSattusApproval(this.approvalReq)
        .subscribe(res => {
          this.spinner.hide();
          this.modalRef.close();
          if (this.updateUserStatusEvent == "Approve") {
            this.commonService.showSuccessToast(this.approvalReq['templateNme'] + " template has been approved");
          } else {
            this.commonService.showSuccessToast(this.approvalReq['templateNme'] + " template has been rejected");
          }
          this.getTemplateList();
        },
          err => {
            this.commonService.showErrorToast(err['error']['userMsg']);
          });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

  prepareApprovalForm() {
    this.statusForm = this.formBuilder.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)
        ]
      ]
    });
  }

  approvalStatus(user, content, event, btn) {
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();
    this.approvalReq = {
      "id": user['id'],
      "approvedBy": "headofdept",
      "templateNme": user['templateName'],
      "status": event == 'Approve' ? 1 : 7
    };
    this.prepareApprovalForm();
    this.changeStatusCamp = false;
    this.userDetails = user;
    this.modalRef = this.modalService.open(content, this.largeModalOptions);
    this.updateUserStatusEvent = event;
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  updateForm(obj) {
    console.log(obj, this.mainGroup.controls[obj['templateName']]);
    if (!obj.checked && this.commonService.getUserByRole() == 'ROLE_DLT') {
      //this.dltIdlist.push(this.mainGroup.controls)
      this.mainGroup.controls[obj['templateName']].setValidators([Validators.required, Validators.minLength(3),
      Validators.maxLength(20), Validators.pattern(this.commonService.patterns.numberOnly)])
      this.mainGroup.controls[obj['templateName']].updateValueAndValidity();
    } else if (this.commonService.getUserByRole() == 'ROLE_DLT') {
      this.mainGroup.controls[obj['templateName']].setValidators([])
      this.mainGroup.controls[obj['templateName']].updateValueAndValidity();
    }
  }

  prepareDltForm() {
    this.dltApproveForm = this.fb.group({
      dltTemplateId: ['', [Validators.required,
      Validators.minLength(5), Validators.maxLength(25),
      Validators.pattern(this.commonService.patterns.numberOnly)
      ]
      ],
      //telemarketer: ['', [Validators.required]],
      dltTemplateName: ['', [Validators.required]],
      type: ['', [Validators.required]],
      header: ['', [Validators.required]],
      category: ['', [Validators.required]],
      registeredDLT: ['', [Validators.required]],
      otherDLT: [''],
      requestedOn: [new Date().toISOString().substr(0, 10), [Validators.required]],
      statusDate: [new Date().toISOString().substr(0, 10), [Validators.required]],
      approvalStatus: ['', [Validators.required]],
      status: ['', [Validators.required]],
      consentType: ['', [Validators.required]],
      rejectionReason: ['', [Validators.required]]
      //dlttemplatemessage : ['', [Validators.required]]
    });
  }

  dltFormSetup(updatemodal, type, obj, btn) {
    this.prepareDltForm();
    this.templateDetail = obj;
    this.buttonName = type;
    if (this.buttonName.toLowerCase() == 'approve') {
      this.dltApproveForm.controls['rejectionReason'].setValue('NA');
    }
    this.dltApproveForm.controls['header'].setValue(obj['senderId']);
    //this.dltApproveForm.controls['dlttemplatemessage'].setValue(obj['template']);
    //this.dltApproveForm.controls['dlttemplatemessage'].updateValueAndValidity();
    this.dltApproveForm.controls['approvalStatus'].setValue(type);
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  onFileSelect(files) {
    let self = this;
    const file = files[0];
    if (file) {
      const filemb = file.size / (1024 * 1024);
      const fileExt = file.name.split(".").pop();
      if (fileExt.toLowerCase() == "xls" || fileExt.toLowerCase() == "xlsx") {
        let fromDataReq = new FormData();
        fromDataReq.append("fileData", files[0]);
        fromDataReq.append("module", "5");
        fromDataReq.append("userId", this.commonService.getUser());
        fromDataReq.append("username", this.commonService.getUserName());
        this.spinner.show();
        this.manageSmsTemplateService.uploadTemplateFile(fromDataReq).subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
              this.uploadedFileId = res['data'];
              this.fileUploaded = true;
              this.commonService.showSuccessToast("File Uploaded successfully");
            } else {
              this.fileUploaded = false;
              this.commonService.showErrorToast(res['result']['userMsg']);
            }
          },
          err => {
            this.fileUploaded = false;
          });
      } else {
        this.commonService.showErrorToast("Invalid file");
        this.fileUploaded = false;
      }
    }
  }

  sampleFileDownload(fileType: string) {
    if (fileType === "txt") {
      let txtData = "TEXT\r\nSAMPLE";
      this.excelService.exportAsTxtFile(txtData, 'Sample_TXT');
    } else if (fileType === "excel") {
      let excelData;
      excelData = [
        {
          "TEMPLATE ID": "1107160988255906073",
          "TELEMARKETER": "--",
          "TEMPLATE NAME": "ePay and PG/SBePay9_new",
          "TYPE": "Service-Implicit",
          "HEADER": "SBePay,SBePay",
          "CATEGORY": "--",
          "REGISTERED DLT": "Vodafone Idea",
          "REQUESTED ON": "01/06/2021 03:05:00",
          "STATUS DATE": "01/06/2021 17:22:00",
          "APPROVAL STATUS": "Approved",
          "STATUS": "Active",
          "TEMPLATE MESSAGE": "Dear CustomerYour Challan Details :Challan Number:{#var#}",
          "CONSENT TYPE": "Implicit",
          "REJECTION REASON": "--",
          //"INTERNAL TEMPLATE ID": "232",
          //"CONTENT TYPE": "2"
        },
        {
          "TEMPLATE ID": "1107160988255906073",
          "TELEMARKETER": "--",
          "TEMPLATE NAME": "ePay and PG/SBePay9_new",
          "TYPE": "Service-Implicit",
          "HEADER": "SBePay,SBePay",
          "CATEGORY": "--",
          "REGISTERED DLT": "Vodafone Idea",
          "REQUESTED ON": "01/06/2021 03:05:00",
          "STATUS DATE": "01/06/2021 17:22:00",
          "APPROVAL STATUS": "Approved",
          "STATUS": "Active",
          "TEMPLATE MESSAGE": "Dear CustomerYour Challan Details :Challan Number:{#var#}",
          "CONSENT TYPE": "Implicit",
          "REJECTION REASON": "--",
          //"INTERNAL TEMPLATE ID": "232",
          //"CONTENT TYPE": "2"
        }
      ];

      this.excelService.exportAsExcelFile(excelData, 'Sample_Excel');
    } else if (fileType === "csv") {
      let csvData;
      csvData = [
        {
          "TEMPLATE ID": "1107160988255906073",
          "TELEMARKETER": "--",
          "TEMPLATE NAME": "ePay and PG/SBePay9_new",
          "TYPE": "Service-Implicit",
          "HEADER": "SBePay,SBePay",
          "CATEGORY": "--",
          "REGISTERED DLT": "Vodafone Idea",
          "REQUESTED ON": "01/06/2021 03:05:00",
          "STATUS DATE": "01/06/2021 03:05:00",
          "APPROVAL STATUS": "Approved",
          "STATUS": "Active",
          "TEMPLATE MESSAGE": "Dear CustomerYour Challan Details :Challan Number:{#var#}",
          "CONSENT TYPE": "Implicit",
          "REJECTION REASON": "--",
          //"INTERNAL TEMPLATE ID": "232",
          //"CONTENT TYPE": "2"
        },
        {
          "TEMPLATE ID": "1107160988255906073",
          "TELEMARKETER": "--",
          "TEMPLATE NAME": "ePay and PG/SBePay9_new",
          "TYPE": "Service-Implicit",
          "HEADER": "SBePay,SBePay",
          "CATEGORY": "--",
          "REGISTERED DLT": "Vodafone Idea",
          "REQUESTED ON": "01/06/2021 03:05:00",
          "STATUS DATE": "01/06/2021 03:05:00",
          "APPROVAL STATUS": "Approved",
          "STATUS": "Active",
          "TEMPLATE MESSAGE": "Dear CustomerYour Challan Details :Challan Number:{#var#}",
          "CONSENT TYPE": "Implicit",
          "REJECTION REASON": "--",
          //"INTERNAL TEMPLATE ID": "232",
          //"CONTENT TYPE": "2"
        }
      ];
      this.excelService.exportAsCsvFile(csvData, 'Sample_CSV');
    }
  }
  restrictKeyword(event: Event) {
    event.preventDefault();
  }

}