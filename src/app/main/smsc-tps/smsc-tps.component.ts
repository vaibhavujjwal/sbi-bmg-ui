import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common/common.service";

@Component({
  selector: 'app-smsc-tps',
  templateUrl: './smsc-tps.component.html',
  styleUrls: ['./smsc-tps.component.scss']
})
export class SmscTpsComponent implements OnInit {

  constructor( public commonService: CommonService ) { }

  ngOnInit() {
  }

}
