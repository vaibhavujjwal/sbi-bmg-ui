
import { Injectable } from '@angular/core';
import { NetworkService } from '../../common/network.service';

@Injectable()
export class AddressBookService {

  constructor(private _networkService: NetworkService) {}    

  getAddressBookList() {
    return this._networkService.get('hype/addressBook', null, 'bearer');
  }

  deleteAddressBook(req: any) {
    return this._networkService.post('hype/delete/addressBook/'+req['addressBookId']+'?remarks=' +req['remarks'], null, 'bearer');
  }

  createAddressBook(req: any) {  
    return this._networkService.post('hype/addressBook',req, null, 'bearer');
  }

  updateAddressBook(req: any) {  
    return this._networkService.post('hype/addressBook/' + req['addressBookId'], req, null, 'bearer');
  }

  downloadFile(req: any){   
    return this._networkService.getFileDownload('hype/addressBook/'+ req['addressBookId'] +'/download/' + req['type'], null, 'bearer');
  }

  uploadFile(req: any){
    return this._networkService.uploadFile('hype/master/fileUpload', req, null, 'bearer');
  }

}

