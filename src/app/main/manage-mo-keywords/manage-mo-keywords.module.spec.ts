import { ManageMoKeywordsModule } from './manage-mo-keywords.module';

describe('ManageMoKeywordsModule', () => {
  let manageMoKeywordsModule: ManageMoKeywordsModule;

  beforeEach(() => {
    manageMoKeywordsModule = new ManageMoKeywordsModule();
  });

  it('should create an instance', () => {
    expect(manageMoKeywordsModule).toBeTruthy();
  });
});
