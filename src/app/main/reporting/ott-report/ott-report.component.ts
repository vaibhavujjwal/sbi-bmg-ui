import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import * as moment from 'moment';
import { ExcelService } from "../../../common/excel.service";
import { NgxSpinnerService } from '../../../../../node_modules/ngx-spinner';
import { ReportingService } from '../reporting.service';
import { CommonService } from '../../../common/common.service';
import { RESPONSE, USER_ROLE } from "../../../common/common.const";
import { MatTableDataSource, MatSort, MatPaginator } from '../../../../../node_modules/@angular/material';
import { OTTResponse } from './ott-response';

@Component({
  selector: 'app-ott-report',
  templateUrl: './ott-report.component.html',
  styleUrls: ['./ott-report.component.scss']
})
export class OttReportComponent implements OnInit {
  USER_ROLES = USER_ROLE;
  reportingForm: FormGroup;
  formSubmitted: boolean = false;
  searchText: string = "";
  departments = [];
  reportList = [];
  pageLimit: number;
  //p: number = 1;
  groupBy: string;
  // dateSorted: boolean = false;
  // campaignIDSorted: boolean = false;
  // departmentSorted: boolean = false;
  // userIdSorted: boolean = false;
  // usernameSorted: boolean = false;
  // appNameSorted: boolean = false;
  // osNameSorted: boolean = false;
  // countTotalSorted: boolean = false;
  // countSubmittedTotalSorted: boolean = false;
  // countSubmittedSuccessSorted: boolean = false;
  // countSubmitfailSorted: boolean = false;
  sDate: any;
  then: any;
  endDte: any;
  edate: any;
  eDate: any;
  now = moment(new Date());
  minDate: any;
  maxDate = this.now.format('YYYY-MM-DD');
  maxDate1 = this.now.format('YYYY-MM-DD');
  formattedFromDate: any;
  formattedToDate: any;
  deptSettings = {};
  deptList = [];
  error: any = { isError: false, errorMessage: '' };
  dataSource: MatTableDataSource<OTTResponse>;
  dataSourceLength: number;
  @ViewChild('input') input: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['date', 'campaignId', 'department', 'userId', 'username',
    'appName', 'osName', 'countTotal','countSubmittedTotal','countSubmittedSuccess','countSubmitfail'];

  constructor(private spinner: NgxSpinnerService, private reportingService: ReportingService,
    private fb: FormBuilder, private excelService: ExcelService, public commonService: CommonService) { }

  ngOnInit() {
    this.initializeForm();
    this.pageLimit = this.commonService.recordsPerPage[0];
    if (this.commonService.getUserByRole() == "ROLE_ADMIN") {
      this.getData();
    } else {
      this.submitForm();
    }
    this.setEndDAte();
  }

  restrictKeyword(event: Event) {
    event.preventDefault();
  }

  getData() {
    this.spinner.show();
    this.reportingService.getDept().subscribe(responseList => {
      this.spinner.hide();
      if (responseList['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.departments = responseList["data"];
        console.log("Department response..." + JSON.stringify(this.departments));
        this.departments.forEach(element => {
          this.deptList.push(element['department']);
        });
        this.deptSettings = {
          singleSelection: false,
          idField: 'department',
          textField: 'department',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          itemsShowLimit: 3,
          allowSearchFilter: true,
          defaultOpen: false
        };
      } else {
        this.departments = [];
      }
      if (responseList['result']['statusDesc'] != RESPONSE.SUCCESS) {
        this.commonService.showErrorToast(responseList['result']['userMsg']);
      }
    });
    this.submitForm();
  }

  initializeForm() {
    this.reportingForm = this.fb.group({
      startDate: [new Date().toISOString().substr(0, 10)],
      endDate: [new Date().toISOString().substr(0, 10)],
      department: [''],
      user: ['', [Validators.pattern(this.commonService.patterns.alphaNumericCamp)]],
      groupBy: ['grp', [Validators.required]]
    });
    this.groupBy = this.commonService.titleCase(this.reportingForm.controls['groupBy'].value);
  }

  setEndDAte() {
    this.sDate = this.reportingForm.controls['startDate'].value,
      this.then = moment(new Date(this.sDate));
    this.minDate = this.then.format('YYYY-MM-DD');
    this.reportingForm.controls['endDate'].setValue(this.sDate);
  }

  setStartDAte() {
    this.maxDate = moment(new Date()).format('YYYY-MM-DD');
    // this.eDate = this.reportingForm.controls['endDate'].value,
    //   this.edate = moment(new Date(this.eDate));
    // this.maxDate = this.edate.format('YYYY-MM-DD');
  }
  submitForm() {
    this.formSubmitted = true;
    let fromDate = new Date(this.reportingForm.controls['startDate'].value);
    let toDate = new Date(this.reportingForm.controls['endDate'].value);
    this.formattedFromDate = moment(fromDate).format("YYYY-MM-DD");
    this.formattedToDate = moment(toDate).format("YYYY-MM-DD");
    let dept = this.reportingForm.controls['department'].value;
    console.log("Selected dept..." + JSON.stringify(dept));
    // let deptStr = dept.toString();
    let deptNames;
    if (dept.length > 0) {
      deptNames = dept.map(a => a).toString();
    }
    else {
      deptNames = this.reportingForm.controls['department'].value;
    }
    console.log("dept anemssss..." + typeof (deptNames) + "  and " + deptNames);
    this.reportingForm.controls['startDate'].setValue(this.formattedFromDate);
    this.reportingForm.controls['endDate'].setValue(this.formattedToDate);
    if (this.reportingForm.valid) {
      let req = {
        startDate: this.reportingForm.controls['startDate'].value,
        endDate: this.reportingForm.controls['endDate'].value,
        department: deptNames,
        // this.reportingForm.controls['department'].value
        username: this.reportingForm.controls['user'].value,
        groupBy: this.commonService.getGroupBy('OTTSummary')
      };
      console.log("Request...." + JSON.stringify(req));
      this.spinner.show();
      this.reportingService.getOTTReport(req).subscribe(
        res => {
          this.spinner.hide();
          this.groupBy = this.commonService.titleCase(this.reportingForm.controls['groupBy'].value);

          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.reportList = res['data'];
            this.dataSource = new MatTableDataSource(this.reportList);
            if (this.dataSource.data.length == 0) {
              this.dataSourceLength = 0;
            } else {
              this.dataSourceLength = this.dataSource.data.length;
            }
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
          } else {
            this.reportList = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length == 0) {
      this.dataSourceLength = 0;
    }
    else {
      this.dataSourceLength = this.dataSource.filteredData.length;
    }
  }

  // sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
  //   this.reportList = this.commonService.sortArray(isAssending, sortBy, this.reportList, isNumber);
  // }

  exportAs(fileType: string) {
    let data = [];
    this.reportList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Date"] = new Date(element.date).toLocaleDateString('en-GB');
      excelDataObject["Campaign ID"] = element['campaignId'];
      if (this.commonService.getUserByRole() == 'ROLE_ADMIN') {
        excelDataObject["Department Name"] = element.department;
      }
      if (this.commonService.getUserByRole() == 'ROLE_HOD' || this.commonService.getUserByRole() == 'ROLE_ADMIN') {
        excelDataObject["User ID"] = element.userId;
        excelDataObject["Username"] = element.username;
      }
      excelDataObject["App Name"] = element.appName;
      excelDataObject["OS Name"] = element.osName;
      excelDataObject["Total Count"] = element.countTotal;
      excelDataObject["Submitted Count"] = element.countSubmittedTotal;
      excelDataObject["Success Count"] = element.countSubmittedSuccess;
      excelDataObject["Failure Count"] = element.countSubmitfail;
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'OTT-Summary-Report');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'OTT-Summary-Report');
        break;
      default:
        this.excelService.exportAsCsvFile(data, 'OTT-Summary-Report');
        break;
    }
  }
}
