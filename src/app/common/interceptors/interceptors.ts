import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { ErrorHandlerInterceptor } from "./errorHandler-interceptor";
import { EncryptDecryptIntercepter } from "./encrypt-decrypt-intercepter";

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: ErrorHandlerInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: EncryptDecryptIntercepter, multi: true }
  
];
