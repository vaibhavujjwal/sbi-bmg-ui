import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../auth.service";
import { NgxSpinnerService } from "ngx-spinner";
import { CommonService } from "../../common/common.service";
import { HttpResponse } from "@angular/common/http";
import "../../../assets/aes.js";
declare var AesUtil: any;
declare var CryptoJS: any;

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  formSubmitted: boolean = false;
  returnUrl: string;
  moduleGuardPromise: Promise<Boolean>;
  isFormValid: boolean = false;
  circleData: Array<Object> = [];
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    public commonService: CommonService
  ) { }

  ngOnInit() {
    this.show = true;
    this.initialiseForms();
    this.getCircleList();
  }

  initialiseForms() {
    this.loginForm = this.fb.group({
      circle: ["", Validators.required],
      userName: ["", [Validators.required, Validators.pattern(this.commonService.patterns.loginPattern)]],
      password: ["", [Validators.required]]
    });
  }


  getCircleList() {
    this.authService.getCircle().subscribe(
      data => {
        if (data["result"]["statusDesc"] == "success") {
          this.circleData = data["data"];
        }
      }
    )
  }
  encryptUsername: any;
  encryptPassword: any;
  async login() {
    this.formSubmitted = true;
    if (this.loginForm.valid) {
      this.isFormValid = true;
      let reqFormData = new FormData();
      let appendedUsername = this.loginForm.controls["circle"].value + "/" + this.loginForm.controls["userName"].value;
      this.encryptUsername = await this.toEncrypt(appendedUsername);
      this.encryptPassword = await this.toEncrypt(this.loginForm.controls["password"].value);
      console.log(this.encryptUsername);
      reqFormData.append('username', this.encryptUsername);
      reqFormData.append('password', this.encryptPassword);
      this.spinner.show();
      console.log("FORm Data..." + JSON.stringify(reqFormData));
      this.authService.validateLogin(reqFormData).subscribe((res: HttpResponse<Object>) => {
        let authToken = res.headers.get("x-auth-token");
        if (authToken) sessionStorage.setItem("_bearerTkn", authToken);
        console.log("token... " + authToken);
        this.authService.validateLoginAccount().subscribe(
          data => {
            this.spinner.hide();
            if (this.commonService.getUserByRole() == "ROLE_CRM") {
              console.log("CRM ROLE");
              this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/main/reporting/detailed-report';
              console.log("URL IS...:" + this.returnUrl);
            }
            else if (this.commonService.getUserByRole() == "ROLE_MARKETING") {
              console.log("Marketing ROLE");
              this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/main/manage-campaign';
            }else if (this.commonService.getUserByRole() == "ROLE_DLT") {
              console.log("Marketing ROLE");
              this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/main/manage-sms-template';
            }
            else {
              console.log("NON CRM ROLE and MARKETTING");
              this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/main';
              console.log("URL ISSSSSSSSSSSSSSSSS" + this.returnUrl);
            }
            this.router.navigateByUrl(this.returnUrl);
            //this.moduleGuardPromise = this.router.navigateByUrl(this.returnUrl);
            // this.moduleGuardPromise.then(guardRes => {
            //   if (!guardRes) {
            //     if (this.commonService.getUserByRole() == "ROLE_CRM")
            //       this.router.navigateByUrl('/main/reporting/detailed-report');
            //     else
            //       this.router.navigateByUrl('/main');
            //   }
            // },
            //   error => {
            //   });
          });
      });
    }
    else {
      if (!(this.loginForm.controls.userName.value && this.loginForm.controls.password.value && this.loginForm.controls.circle.value)) {
        this.isFormValid = true;
      }
      else {
        this.commonService.showErrorToast("Invalid username or password");
        this.loginForm.reset();
        this.isFormValid = false;
      }

    }
  }

  toEncrypt(val: any) {
    var iterationCount = 1000;
    var keySize = 128;
    var aesUtil = new AesUtil(keySize, iterationCount);
    // salt , iv, passphrase, plainText
    var iv = CryptoJS.lib.WordArray.random(128 / 8).toString(CryptoJS.enc.Hex);
    var salt = CryptoJS.lib.WordArray.random(128 / 8).toString(CryptoJS.enc.Hex);
    console.log("salt....." + salt);
    console.log("iv..." + iv);
    // var encString1 = aesUtil.encrypt("8bc82ffe516ea1f1b89dcb529a201d56", "ebb7947b5feabdc0b60f2a694fa259c4", "Gr33nL!ght#D91", val);
    var encString1 = aesUtil.encrypt(salt, iv, "Gr33nL!ght#D91", val);
    var aesPassword = (iv + "::" + salt + "::" + encString1);
    var password = btoa(aesPassword);
    console.log('encrypted request ' + JSON.stringify(encString1));
    return password
  }

  show: boolean;
  hide: boolean;
  showPassword(input: any): any {
    input.type = input.type === 'password' ? 'text' : 'password';
    this.hide = true;
    this.show = false;
  }
  hidePassword(input: any): any {
    input.type = input.type === 'password' ? 'text' : 'password';
    this.hide = false;
    this.show = true;
  }
}
