import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ManageBulkTemplateComponent } from "./manage-bulk-template.component";
import { MODULE } from "src/app/common/common.const";
import { ViewBulkTemplateComponent } from "./view-bulk-template/view-bulk-template.component";
import { PendingBulkTemplateComponent } from "./pending-bulk-template/pending-bulk-template.component";
import { CreateBulkTemplateComponent } from "./create-bulk-template/create-bulk-template.component";

export const routes: Routes = [
  {
    path: "",
    component: ManageBulkTemplateComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/manage-bulk-template/view-bulk-template",
        pathMatch: "full"
      },
      {
        path: "view-bulk-template",
        component: ViewBulkTemplateComponent,
        data: { moduleName: MODULE.Bulk_TEMPLATE, permissions: ["R", "RW"] }
      },
      {
        path: "pending-bulk-template",
        component: PendingBulkTemplateComponent,
        data: { moduleName: MODULE.BULK_TEMPLATE_APPROVAL, permissions: ["R", "RW"] }
      },
      {
        path: "create-bulk-template",
        component: CreateBulkTemplateComponent,
        data: { moduleName: MODULE.BULK_TEMPLATE_BULK_UPLOAD, permissions: ["R", "RW"] }
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
