import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSmscTpsComponent } from './view-smsc-tps.component';

describe('ViewSmscTpsComponent', () => {
  let component: ViewSmscTpsComponent;
  let fixture: ComponentFixture<ViewSmscTpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSmscTpsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSmscTpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
