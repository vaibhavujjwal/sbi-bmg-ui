interface TemplateResponse
{
    id: number;
    templateName: string;
    template: string;
    status: number;
    approvedBy: string;
    user: number;
    username: string;
    department: string;
    creationDate: string;
    language: number;
    maskFlag: string;
    maskText: string;
    caseSensitivityFlag: string;
    checked: boolean;
}