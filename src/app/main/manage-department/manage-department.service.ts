import { Injectable } from '@angular/core';
import { NetworkService } from "../../common/network.service";

@Injectable()
export class ManageDepartmentService {
    constructor(private _networkService: NetworkService) { }
     createDepartment(req: any) {
          return this._networkService.post('honcho/department/create', req, null, 'bearer');
           }

     deleteDepartmentById(req: any) {
         return this._networkService.post('honcho/department/delete/' + req['deptId'] + '?remarks=' + req['remarks'], null, 'bearer');
     }
    //  getDepartmentList() {
    //      return this._networkService.get('department', null, 'bearer');
    //  }
    getAllDepartmentList(req: any) {
        return this._networkService.get('honcho/department/getdepartment/' + '?id=' + req['id'] + '&status=' + req['status'], null, 'bearer');
    }
     updateDepartmentById(req: any) {
       return this._networkService.post('honcho/department/update/' + req['deptId'] + '?department=' + req['deptName'], null, 'bearer');
    }
     updateStatusById(req: any){
       return this._networkService.post('honcho/department/update/status/' + req['deptId'] +'?status=' + req['status'], null, 'bearer');
     }

     //check put 2 param here
}
