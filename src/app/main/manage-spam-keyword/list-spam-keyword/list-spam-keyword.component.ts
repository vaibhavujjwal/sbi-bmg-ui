import { Component, OnInit,ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../../../common/common.service';
import { ManageSpamKeywordService } from '../manage-spam-keyword.service';
import { RESPONSE } from '../../../common/common.const';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';




export interface spamKeywordType {
  Actions: any;
  keyword: any;
  level: any;
  status: any;
  }


@Component({
  selector: 'app-list-spam-keyword',
  templateUrl: './list-spam-keyword.component.html',
  styleUrls: ['./list-spam-keyword.component.scss']
})
export class ListSpamKeywordComponent implements OnInit {

  spamKeywordsList = [];
  formSubmitted: boolean = false;
  deleteReq = {} ;
  statusForm: FormGroup;
  spamKeywordForm: FormGroup;
  pageLimit: number;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  searchText: string = "";
  keywordID = [];
  p: number = 1;
  isEdit: boolean = false;
  modalRef: any;
  displayedColumns: string[] = ['actions',	'keyword',
  'level', 'status'];
  dataSource: MatTableDataSource<spamKeywordType>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  showError:boolean=false;



  constructor(private modalService: NgbModal,
    private spinner: NgxSpinnerService, private fb: FormBuilder, private formBuilder: FormBuilder,
    public commonService: CommonService, private spamService: ManageSpamKeywordService) { }

  initializeForm() {
    this.spamKeywordForm = this.fb.group({
      keywords: ['', [Validators.required, Validators.pattern(this.commonService.patterns.tenCommaSeparatedKeywords)]],
      level: ['', [Validators.required]]
      });
  }

  get f() { return this.spamKeywordForm.controls; }

  ngOnInit() {
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.getSpamKeywordList();
    this.initializeForm();
  }

  getSpamKeywordList() {
    this.spinner.show();
    let req = {
      "userId": this.commonService.getUser()
    };
    this.spamService.getSpamKeywords(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.spamKeywordsList = res["data"];
          this.spamKeywordsList.forEach(element => {
            if (element['level'].toLowerCase() == 'l') {
              element['level'] = "Low";
            }
            else if(element['level'].toLowerCase() == 'm') {
              element['level'] = "Medium";
            }else if(element['level'].toLowerCase() == 'h'){
              element['level'] = "High";
            }
          });

          this.dataSource= new MatTableDataSource(this.spamKeywordsList) ;
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;

        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.spamKeywordsList = [];
        }
      });
  }



  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.showError=false;
    if(this.dataSource.filteredData.length==0){
      this.showError=true;
    }
  }

  prepareApprovalForm() {
    this.statusForm = this.formBuilder.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)
        ]
      ]
    });
  }

  confirmDeleteRecord(updatemodal: any, record: any) {
    this.formSubmitted = false;
    this.prepareApprovalForm();
    this.deleteReq = {
      "keywordId": record["id"],
      "keyywordName" : record['keyword']
    }
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
}, (reason) => {
});
}

  deleteRecord(record: any) {
    this.deleteReq['remarks'] = this.statusForm.controls['remarks'].value;
    this.formSubmitted = true;
    if(this.statusForm.valid){
      this.spinner.show();
      this.spamService.deleteSpamKeywords(this.deleteReq).subscribe(
      res => {
        this.spinner.hide();
        if(res['result']['statusDesc'] == RESPONSE.SUCCESS){
          this.commonService.showSuccessToast(this.deleteReq['keyywordName'] +" spam keyword has been deleted");
          this.modalRef.close();
          this.getSpamKeywordList();
        }else{
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
        this.formSubmitted = false;
      });
    }
  }

  populateValues(item) {
    this.spamKeywordForm.controls['keywords'].setValue(item.keyword);
    this.keywordID = item.id;
        if(item.level=='Low'){
          this.spamKeywordForm.controls['level'].setValue('l');
        }else if(item.level=='Medium'){
          this.spamKeywordForm.controls['level'].setValue('m');
        }else if(item.level=='High'){
          this.spamKeywordForm.controls['level'].setValue('h');
        }
  }

  updateRecord(updatemodal: any, record: any) {
    this.populateValues(record);
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  saveCLI() {
    this.formSubmitted = true;
    if (this.spamKeywordForm.valid) {
    }
  }

  confirmUpdateRecoed() {
    this.formSubmitted = true;
    if (this.spamKeywordForm.valid) {
        let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
        modalRef.componentInstance.content = 'Are you sure you want to update details?';
        modalRef.componentInstance.leftButton = 'No';
        modalRef.componentInstance.rightButton = 'Yes';
        modalRef.result.then(
          (data: any) => {
            if (data == "yes") {
              this.updateKeyword();
            }
          },
          (reason: any) => {
          });
          this.formSubmitted = false;
        }
        }

  updateKeyword(){
        let reqObj = {
          "keyword": this.spamKeywordForm.controls["keywords"].value,
          "level": this.spamKeywordForm.controls["level"].value,
          "keywordId":this.keywordID,
          "loginId": this.commonService.getUser()
        }
        this.spinner.show();
        this.spamService.updateSpamKeyword(reqObj).subscribe(
          res => {
            this.commonService.showSuccessToast(reqObj['keyword'] + ' spam keyword has been updated');
                    this.formSubmitted = false;
                    this.modalRef.close();
                    this.getSpamKeywordList();
          });
    }

    changeStatusConfirmation(record: any,id) {
      let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
      if(record['status']['id'] == 1){
        modalRef.componentInstance.content = 'Are you sure you want to disable '+ record['keyword'] +' spam keyword?';
      }else{
        modalRef.componentInstance.content = 'Are you sure you want to enable ' + record['keyword'] + ' spam keyword?';
      }
      modalRef.componentInstance.leftButton = 'No';
      modalRef.componentInstance.rightButton = 'Yes';
      modalRef.result.then(
        (data: any) => {
          if (data == "yes") {
            this.changeStatus(record);
          }else{
            document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
          }
        },
        (reason: any) => {
          document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
        });
    }

    changeStatus(record: any){
      let req = {
        "keywordId": record['id'],
        "statusId": record['status']['id'] == 1 ? 2 : 1,
        "loginId": this.commonService.getUser()
      };
      this.spinner.show();
      this.spamService.updateStatus(req).subscribe(
      res => {
        this.spinner.hide();
        if(record['status']['id']== 1){
          this.commonService.showSuccessToast(record['keyword'] + ' spam keyword disabled');
        }else if(record['status']['id']== 2){
          this.commonService.showSuccessToast(record['keyword'] + ' spam keyword enabled');
        }
        this.getSpamKeywordList();
      });
    }
}

