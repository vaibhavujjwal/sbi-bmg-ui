import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from '../../../common/common.service';
import { ManageDepartmentService } from '../manage-department.service';
import { RESPONSE } from "../../../common/common.const";
import { Router } from "@angular/router";

@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.scss']
})
export class AddDepartmentComponent implements OnInit {
  departmentForm: FormGroup;
  formSubmitted: boolean = false;
  private modalRef: NgbModalRef;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };

  constructor(private fb: FormBuilder, private router: Router,
    private modalService: NgbModal, private spinner: NgxSpinnerService,
    private deptService: ManageDepartmentService, private commonService: CommonService, ) { }

  ngOnInit() {
    this.initialiseForms();
  }

  initialiseForms() {
    this.departmentForm = this.fb.group({
      deptName: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
        Validators.pattern(this.commonService.patterns.alphaNumaric)]]
    });
  }

  saveDepartment(){
    this.formSubmitted = true;
    if (this.departmentForm.valid) {
      let reqObj = {
        "department": this.departmentForm.controls.deptName.value.trim(),
        "show": 1
      }
      this.spinner.show();
      this.deptService.createDepartment(reqObj).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast(reqObj['department'] +' department added succesfully');
            this.router.navigate(["/main/manage-department/view-department"]);
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        }
      );
  }
}

}
