import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewViewUsersComponent } from './new-view-users.component';

describe('NewViewUsersComponent', () => {
  let component: NewViewUsersComponent;
  let fixture: ComponentFixture<NewViewUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewViewUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewViewUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
