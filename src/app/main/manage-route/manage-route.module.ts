import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ManageRouteComponent } from "./manage-route.component";
import { AddComponent } from "./add/add.component";
import { ListComponent } from "./list/list.component";
import { SharedModule } from "../../common/shared.modules";
import { routing } from "./manage-route.routing";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import {MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule} from '@angular/material';
@NgModule({
  imports: [CommonModule, SharedModule, routing, NgbModule,
    MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
    MatSortModule, MatTableModule],
  declarations: [ManageRouteComponent, AddComponent, ListComponent]
})
export class ManageRouteModule {}
