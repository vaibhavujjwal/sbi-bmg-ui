export interface MoDetailedResponse {
    recTime: string,
    moID: string,
    msisdn: string,
    shortCode: string,
    keyword: string,
    message: string,
    binRoute: string,
    deptName: string,
    errorCode: number,
    errorType: number,
    errorDesc: string,
    logTime: string,
    source: string
}