import { Injectable } from "@angular/core";
import { NetworkService } from "../../common/network.service";
import { forkJoin } from 'rxjs';
import { CommonService } from "../../common/common.service";

@Injectable()
export class ManageCampaignService {
  constructor(private _networkService: NetworkService, public commonService: CommonService) { }
  roleName: string;

  getCampaignList(req: any) {
    this.roleName = this.commonService.getUserByRole();
    if (this.roleName == "ROLE_ADMIN") {         //ADMIN
      return this._networkService.get('honcho/campaign?type=' + req["type"] +
        '&startDate=' + req['startDate'] + '&endDate=' + req['endDate'], null, 'bearer');
    }
    else if (this.roleName == "ROLE_HOD") {    //HOD
      return this._networkService.get('provost/campaign?type=' + req["type"] +
        '&startDate=' + req['startDate'] + '&endDate=' + req['endDate'], null, 'bearer');
    }
    else if (this.roleName == "ROLE_CAMPAIGN") {      //Campaign
      return this._networkService.get('hype/campaign?type=' + req["type"] +
        '&startDate=' + req['startDate'] + '&endDate=' + req['endDate'], null, 'bearer');
    }
    else if (this.roleName == "ROLE_MARKETING") {      //Campaign
      return this._networkService.get('peddle/campaign?type=' + req["type"] +
        '&startDate=' + req['startDate'] + '&endDate=' + req['endDate'], null, 'bearer');
    }
  }

  createCampaign(req: any) {
    return this._networkService.post("hype/campaign", req, null, 'bearer');
  }

  getHeaders(req: any) {
    return this._networkService.uploadFile('hype/campaign/getHeaders', req, null, 'bearer');
  }

  scheduleCampaign(req: any) {
    return this._networkService.post('hype/campaign/' + req['campaignId'] + '/schedule', req, null, 'bearer');
  }

  getCategoryList(req: any){
    return this._networkService.get('hype/route/appdetails/'+ req , null, 'bearer');
  }

  getAddressBookList() {
    return this._networkService.get('hype/addressBook', null, 'bearer');
  }

  getPredefinedTemplates() {
    return this._networkService.get('hype/campaign/template', null, 'bearer');
  }

  getHours(req: any) {
    return this._networkService.get('hype/campaign/validHours?scheduledDate=' + req, null, 'bearer');
  }

  getData() {
    let senderId = this._networkService.get('hype/senderid/info?' + 'showAll=ENABLED', null, 'bearer');
    let spamKeywords = this._networkService.get('hype/spamkeyword?' + 'showAllFlag=ENABLED', null, 'bearer');
    return forkJoin([senderId, spamKeywords]);
  }

  cancelCampaign(req: any) {
    this.roleName = this.commonService.getUserByRole();
    if (this.roleName == "ROLE_HOD") {    //HOD
      return this._networkService.post('provost/campaign/' + req['campaignId'] + '/status/' + req['campaignStatus'], null, null, 'bearer');
    }
    else if (this.roleName == "ROLE_CAMPAIGN") {      //Campaign
      return this._networkService.post('hype/campaign/' + req['campaignId'] + '/status/' + req['campaignStatus'], null, null, 'bearer');
    }
  }

  updateSattus(req: any) {
    this.roleName = this.commonService.getUserByRole();

    if (this.roleName == "ROLE_HOD") {    //HOD
      return this._networkService.post('provost/campaign/' + req['campaignId'] + '/approve', req, null, 'bearer');
    }
    else if (this.roleName == "ROLE_MARKETING") {      //mrkting
      return this._networkService.post('peddle/campaign/' + req['campaignId'] + '/approve', req, null, 'bearer');
    }
  }

  uploadFile(req: any) {
    return this._networkService.uploadFile('master/fileUpload', req, null, 'bearer');
  }

  deleteCampaign(req: any) {
    return this._networkService.post('hype/campaign/' + req['campaignId'] + '?remarks=' + req['remarks'], null, 'bearer');
  }

  fileDownload(req) {
    this.roleName = this.commonService.getUserByRole();

    // if (this.roleName == "ROLE_ADMIN") {         //ADMIN
    //     return this._networkService.getFile('honcho/downloadFile/' + req['campaignId'] + '/' + req['type'], null, 'bearer');
    // }
    // else if (this.roleName == "ROLE_HOD") {    //HOD
    //     return this._networkService.getFile('provost/downloadFile/' + req['campaignId'] + '/' + req['type'], null, 'bearer');
    // }
    // else if (this.roleName == "ROLE_API") {    //API
    //     return this._networkService.getFile('txns/downloadFile/' + req['campaignId'] + '/' + req['type'], null, 'bearer');
    // }
    // else
    if (this.roleName == "ROLE_CAMPAIGN") {      //Campaign
      return this._networkService.getFile('hype/downloadFile/' + req['campaignId'] + '/' + req['type'], null, 'bearer');
    }
  }
  // getSpamKeywords(req: any) {
  //   return this._networkService.get('spamkeyword?userId=' + req['userId'] + '&showAllFlag=ENABLED', null, 'bearer');
  // }


  // getSenderIdListByUser(userId: any) {
  //   return this._networkService.get('senderid/info/' + userId + '?showAll=ENABLED', null, 'bearer');
  // }


  getTemplateIdList(req: any){
    return this._networkService.get('hype/get/approvedTemplates/'+ req , null, 'bearer');
  }
}
