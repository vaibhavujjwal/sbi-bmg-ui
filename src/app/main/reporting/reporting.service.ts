import { Injectable } from "@angular/core";
import { NetworkService } from '../../common/network.service';
import { forkJoin } from 'rxjs';
import { CommonService } from "../../common/common.service";

@Injectable()
export class ReportingService {
    roleName: string;
    constructor(private _networkService: NetworkService, public commonService: CommonService) { }

    ngOnInit() {
    }

    getData() {
        this.roleName = this.commonService.getUserByRole();
        let channels;
        let departments;
        let aggregators;
        if (this.roleName == "ROLE_ADMIN") {         //ADMIN
            channels = this._networkService.get('honcho/master/channel', null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {    //HOD
            channels = this._networkService.get('provost/master/channel', null, 'bearer');
        }
        if (this.roleName == "ROLE_ADMIN") {         //ADMIN
            departments = this._networkService.get('honcho/master/department', null, 'bearer');
        }
        if (this.roleName == "ROLE_HOD") {         //HOD
            departments = this._networkService.get('provost/master/department', null, 'bearer');
        }
        if (this.roleName == "ROLE_ADMIN") {         //ADMIN
            aggregators = this._networkService.get('honcho/agg?epoch=0', null, 'bearer');
        }
        let serviceArr = [channels, departments];

        if (this.roleName == "ROLE_ADMIN") {
            serviceArr.push(aggregators);
        }
        return forkJoin(serviceArr);
    }
    getDept() {
        this.roleName = this.commonService.getUserByRole();
        let departments;
        if (this.roleName == "ROLE_ADMIN") {         //ADMIN
            return departments = this._networkService.get('honcho/master/department', null, 'bearer');
        }
    }
    getOTTReport(req: any) {
        this.roleName = this.commonService.getUserByRole();

        if (this.roleName == "ROLE_ADMIN") {         //ADMIN
            return this._networkService.post('honcho/ott/summary', req, null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {    //HOD
            return this._networkService.post('provost/ott/summary', req, null, 'bearer');
        }
        else if (this.roleName == "ROLE_API") {    //API
            return this._networkService.post('txns/ott/summary', req, null, 'bearer');
        }
        else if (this.roleName == "ROLE_CAMPAIGN") {      //Campaign
            return this._networkService.post('hype/ott/summary', req, null, 'bearer');
        }
    }
    getAllChannel() {
        this.roleName = this.commonService.getUserByRole();

        if (this.roleName == "ROLE_ADMIN") {         //ADMIN
            return this._networkService.get('honcho/master/channel', null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {    //HOD
            return this._networkService.get('provost/master/channel', null, 'bearer');
        }
        else if (this.roleName == "ROLE_API") {    //API
            return this._networkService.get('txns/master/channel', null, 'bearer');
        }
        else if (this.roleName == "ROLE_CAMPAIGN") {      //Campaign
            return this._networkService.get('hype/master/channel', null, 'bearer');
        }
    }

    getAllDepartments() {
        this.roleName = this.commonService.getUserByRole();

        if (this.roleName == "ROLE_ADMIN") {         //ADMIN
            return this._networkService.get('honcho/master/department', null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {    //HOD
            return this._networkService.get('provost/master/department', null, 'bearer');
        }
        else if (this.roleName == "ROLE_API") {    //API
            return this._networkService.get('txns/master/department', null, 'bearer');
        }
        else if (this.roleName == "ROLE_CAMPAIGN") {      //Campaign
            return this._networkService.get('hype/master/department', null, 'bearer');
        }
    }

    getAggregatorList() {
        this.roleName = this.commonService.getUserByRole();

        if (this.roleName == "ROLE_ADMIN") {         //ADMIN
            return this._networkService.get('honcho/agg?epoch=0', null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {    //HOD
            return this._networkService.get('provost/agg?epoch=0', null, 'bearer');
        }
        else if (this.roleName == "ROLE_API") {    //API
            return this._networkService.get('txns/agg?epoch=0', null, 'bearer');
        }
        else if (this.roleName == "ROLE_CAMPAIGN") {      //Campaign
            return this._networkService.get('hype/agg?epoch=0', null, 'bearer');
        }
    }

    getAllReports(req: any) {
        this.roleName = this.commonService.getUserByRole();

        if (this.roleName == "ROLE_ADMIN") {         //ADMIN
            return this._networkService.post('honcho/summary/reports/' + req['channel'] + '?aggregator=' + req['aggregator'] +
                '&campaignId=' + req['campaign'] + '&endDate=' + req['endDate'] + '&startDate=' + req['startDate'] + '&department=' + req['department'] + '&userId=' + req['userId'] + '&groupBy=' + req['groupBy'] + '&senderId=' + req['sender'] + '&username=' + req['username'], null, null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {    //HOD
            return this._networkService.post('provost/summary/reports/' + req['channel'] + '?aggregator=' + req['aggregator'] +
                '&campaignId=' + req['campaign'] + '&endDate=' + req['endDate'] + '&startDate=' + req['startDate'] + '&department=' + req['department'] + '&userId=' + req['userId'] + '&groupBy=' + req['groupBy'] + '&senderId=' + req['sender'] + '&username=' + req['username'], null, null, 'bearer');
        }
        else if (this.roleName == "ROLE_API") {    //API
            return this._networkService.post('txns/summary/reports/' + req['channel'] + '?aggregator=' + req['aggregator'] +
                '&campaignId=' + req['campaign'] + '&endDate=' + req['endDate'] + '&startDate=' + req['startDate'] + '&department=' + req['department'] + '&userId=' + req['userId'] + '&groupBy=' + req['groupBy'] + '&senderId=' + req['sender'] + '&username=' + req['username'], null, null, 'bearer');
        } else if (this.roleName == "ROLE_CAMPAIGN") {      //Campaign
            return this._networkService.post('hype/summary/reports/' + req['channel'] + '?aggregator=' + req['aggregator'] +
                '&campaignId=' + req['campaign'] + '&endDate=' + req['endDate'] + '&startDate=' + req['startDate'] + '&department=' + req['department'] + '&userId=' + req['userId'] + '&groupBy=' + req['groupBy'] + '&senderId=' + req['sender'] + '&username=' + req['username'], null, null, 'bearer');
        }
    }

    getMOReports(req: any) {
        this.roleName = this.commonService.getUserByRole();

        if (this.roleName == "ROLE_ADMIN") {
            return this._networkService.get('honcho/summary/reports/mo?endDate=' + req['endDate'] +
                '&groupBy=' + req['groupBy'] + '&keyword=' + req['keyword'] + '&startDate=' + req['startDate'], null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {
            return this._networkService.get('provost/summary/reports/mo?endDate=' + req['endDate'] +
                '&groupBy=' + req['groupBy'] + '&keyword=' + req['keyword'] + '&startDate=' + req['startDate'], null, 'bearer');
        }
        else if (this.roleName == "ROLE_API") {
            return this._networkService.get('txns/summary/reports/mo?endDate=' + req['endDate'] +
                '&groupBy=' + req['groupBy'] + '&keyword=' + req['keyword'] + '&startDate=' + req['startDate'], null, 'bearer');
        }
        else if (this.roleName == "ROLE_CAMPAIGN") {
            return this._networkService.get('hype/summary/reports/mo?endDate=' + req['endDate'] +
                '&groupBy=' + req['groupBy'] + '&keyword=' + req['keyword'] + '&startDate=' + req['startDate'], null, 'bearer');
        }
    }

    getDetailReports(req: any) {
        // return this._networkService.getMG('report/'+ req['msdin'] +'?endDate='+ req['endDate'] +'&startDate='+ req['startDate'], null, 'bearer');
        return this._networkService.get('msisdn/detailed/report/' + req['msdin'] + '?endDate=' + req['endDate'] + '&startDate=' + req['startDate'], null, 'bearer');
    }

    getFailedSMSAggregatorWise(req: any) {
        return this._networkService.post('honcho/agg/summary', req, null, 'bearer');
    }

    getMODetailReports(req: any) {
        this.roleName = this.commonService.getUserByRole();

        if (this.roleName == "ROLE_ADMIN") {
            return this._networkService.post('honcho/detailed/report/mo', req, null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {
            return this._networkService.post('provost/detailed/report/mo', req, null, 'bearer');
        }
        else if (this.roleName == "ROLE_API") {
            return this._networkService.post('txns/detailed/report/mo', req, null, 'bearer');
        }
        else if (this.roleName == "ROLE_CAMPAIGN") {
            return this._networkService.post('hype/detailed/report/mo', req, null, 'bearer');
        }
    }

    fileDownload(req) {
        this.roleName = this.commonService.getUserByRole();

        if (this.roleName == "ROLE_ADMIN") {         //ADMIN
            return this._networkService.getFile('honcho/downloadFile/' + req['campaignId'] + '/' + req['type'], null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {    //HOD
            return this._networkService.getFile('provost/downloadFile/' + req['campaignId'] + '/' + req['type'], null, 'bearer');
        }
        else if (this.roleName == "ROLE_API") {    //API
            return this._networkService.getFile('txns/downloadFile/' + req['campaignId'] + '/' + req['type'], null, 'bearer');
        }
        else if (this.roleName == "ROLE_CAMPAIGN") {      //Campaign
            return this._networkService.getFile('hype/downloadFile/' + req['campaignId'] + '/' + req['type'], null, 'bearer');
        }
    }

    generateSummaryPDF(req) {
        this.roleName = this.commonService.getUserByRole();
        if (this.roleName == "ROLE_ADMIN") {
            //ADMIN
            return this._networkService.postNonEncrypt('honcho/summary/pdf/' + req['channel'] + '?aggregator=' + req['aggregator'] +
                '&campaignId=' + req['campaign'] + '&endDate=' + req['endDate'] + '&startDate=' + req['startDate'] + '&department=' + req['department'] + '&userId=' + req['userId'] + '&groupBy=' + req['groupBy'] + '&senderId=' + req['sender'] + '&username=' + req['username'], null, null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {    //HOD
            return this._networkService.postNonEncrypt('provost/summary/pdf/' + req['channel'] + '?aggregator=' + req['aggregator'] +
                '&campaignId=' + req['campaign'] + '&endDate=' + req['endDate'] + '&startDate=' + req['startDate'] + '&department=' + req['department'] + '&userId=' + req['userId'] + '&groupBy=' + req['groupBy'] + '&senderId=' + req['sender'] + '&username=' + req['username'], null, null, 'bearer');
        }
        else if (this.roleName == "ROLE_API") {    //API
            return this._networkService.postNonEncrypt('txns/summary/pdf/' + req['channel'] + '?aggregator=' + req['aggregator'] +
                '&campaignId=' + req['campaign'] + '&endDate=' + req['endDate'] + '&startDate=' + req['startDate'] + '&department=' + req['department'] + '&userId=' + req['userId'] + '&groupBy=' + req['groupBy'] + '&senderId=' + req['sender'] + '&username=' + req['username'], null, null, 'bearer');
        } else if (this.roleName == "ROLE_CAMPAIGN") {      //Campaign
            return this._networkService.postNonEncrypt('hype/summary/pdf/' + req['channel'] + '?aggregator=' + req['aggregator'] +
                '&campaignId=' + req['campaign'] + '&endDate=' + req['endDate'] + '&startDate=' + req['startDate'] + '&department=' + req['department'] + '&userId=' + req['userId'] + '&groupBy=' + req['groupBy'] + '&senderId=' + req['sender'] + '&username=' + req['username'], null, null, 'bearer');
        }
    }

    generateDLRPDF(req) {
        return this._networkService.getNonEncryption1('msisdn/detailed/pdf/' + req['msdin'] + '?endDate=' + req['endDate'] + '&startDate=' + req['startDate'], null, 'bearer');
    }
    
    generateMODetailedPDF(req) {
        this.roleName = this.commonService.getUserByRole();
        if (this.roleName == "ROLE_ADMIN") {
            return this._networkService.postNonEncrypt('honcho/detailed/reportpdf/mo', req, null, 'bearer');
        }
        else if (this.roleName == "ROLE_HOD") {
            return this._networkService.postNonEncrypt('provost/detailed/reportpdf/mo', req, null, 'bearer');
        }
        else if (this.roleName == "ROLE_API") {
            return this._networkService.postNonEncrypt('txns/detailed/reportpdf/mo', req, null, 'bearer');
        }
        else if (this.roleName == "ROLE_CAMPAIGN") {
            return this._networkService.postNonEncrypt('hype/detailed/reportpdf/mo', req, null, 'bearer');
        }
    }

    sendPushSMS(req) {
        return this._networkService.post('msisdn/push/sms', req, null, 'bearer');
    }

    downloadReport(fileName){
        this.roleName = this.commonService.getUserByRole();
        if (this.roleName == "ROLE_ADMIN"){
            return this._networkService.getFile('honcho/downloadApiFile/'+fileName,null,'bearer');
        }else if(this.roleName == "ROLE_API"){
            return this._networkService.getFile('txns/downloadApiFile/'+fileName,null,'bearer');
        }
    }
    getDetailReportsUmid(req: any) {
        return this._networkService.get('honcho/umid/dlr/report?umid=' + req['umid'] + '&date=' + req['date'], null, 'bearer');
    }
    generateDLRPDFUmid(req) {
        return this._networkService.getNonEncryption1('honcho/umid/dlr/report/pdf?umid=' + req['umid'] + '&date=' + req['date'], null, 'bearer');
    }
}