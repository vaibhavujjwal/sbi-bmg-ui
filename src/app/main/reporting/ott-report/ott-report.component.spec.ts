import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OttReportComponent } from './ott-report.component';

describe('OttReportComponent', () => {
  let component: OttReportComponent;
  let fixture: ComponentFixture<OttReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OttReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OttReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
