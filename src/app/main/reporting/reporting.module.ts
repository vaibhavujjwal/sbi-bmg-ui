import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReportingComponent} from './reporting.component';
import {SummaryReportComponent} from './summary-report/summary-report.component';
import {DetailedReportComponent} from './detailed-report/detailed-report.component';
import {ArchivedReportComponent} from './archived-report/archived-report.component';
import { routing } from './reporting.routing';
import { ReportingService } from './reporting.service';
import { SharedModule } from "../../common/shared.modules";
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatButtonModule,
  MatNativeDateModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { MoDetailedReportComponent } from './mo-detailed-report/mo-detailed-report.component';
import { OttReportComponent } from './ott-report/ott-report.component';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from '../../validators/format-datepicker';
import { FailedSMSComponent } from './failed-sms/failed-sms.component';
import { UmidReportComponent } from './umid-report/umid-report.component';

@NgModule({
  imports: [
    CommonModule,
    routing,
    SharedModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatButtonModule,
    MatNativeDateModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule
  ],
  declarations: [ReportingComponent, SummaryReportComponent, DetailedReportComponent, ArchivedReportComponent, MoDetailedReportComponent, OttReportComponent, FailedSMSComponent, UmidReportComponent],
  providers: [ReportingService
  ,
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
]
})
export class ReportingModule { }
