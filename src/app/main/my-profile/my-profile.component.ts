import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common/common.service";

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {
  constructor(public commonService: CommonService) { }

  ngOnInit() {
  }

}