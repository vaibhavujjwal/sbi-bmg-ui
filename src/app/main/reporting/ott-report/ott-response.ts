
  export interface OTTResponse {
    date: string,
    userId: number,
    username: string,
    department: string,
    campaignId: number,
    osName: string,
    appName: string,
    countTotal: number,
    countSubmittedTotal: number,
    countSubmittedSuccess: number,
    countSubmitfail: number
  }