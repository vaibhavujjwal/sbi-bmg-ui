import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RegistrationRequestsComponent } from "./registration-requests.component";
import { SharedModule } from "../../common/shared.modules";
import { routing } from "./registration-requests.routing";
import { ViewComponent } from './view/view.component';

@NgModule({
  imports: [CommonModule, SharedModule, routing],
  declarations: [
    RegistrationRequestsComponent,
    ViewComponent
  ]
})
export class RegistrationRequestsModule {}
