import { Component, OnInit } from "@angular/core";
import { RegistrationService } from "../registration.service";
import { NgxSpinnerService } from "ngx-spinner";
import { CommonService } from "src/app/common/common.service";
import { RESPONSE } from "src/app/common/common.const";
import * as FileSaver from "file-saver";
@Component({
  selector: "app-reg-home",
  templateUrl: "./reg-home.component.html",
  styleUrls: ["./reg-home.component.scss"],
  providers: [RegistrationService]
})
export class RegHomeComponent implements OnInit {
  formsList: Array<object> = [];
  constructor(
    private registrationService: RegistrationService,
    private spinner: NgxSpinnerService,
    private commonService: CommonService
  ) {}

  ngOnInit() {
    this.getFormsList();
  }

  getFormsList() {
    this.spinner.show();

    this.registrationService.getRequestFormsList().subscribe(
      data => {
        if (
          data["result"]["statusDesc"] == RESPONSE.SUCCESS &&
          data["data"].length > 0
        ) {
          this.formsList = data["data"];
        } else {
          this.formsList = [];
          this.commonService.showInfoAlertMessage("Data not found");
        }
        this.spinner.hide();
      },
      error => {
        this.formsList = [];
        this.spinner.hide();
        this.commonService.showErrorAlertMessage("Something went wrong");
      }
    );
  }

  downloadForm(type,fileTitle) {
    console.log(type, "download");
    this.spinner.show();
    this.registrationService.downloadRawFormFile(type).subscribe(data => {
      console.log(data);
      const blob = new Blob([data], { type: "application/pdf" });
      console.log(blob);
      this.spinner.hide();
      FileSaver.saveAs(data, fileTitle+".pdf");
    });
  }
}
