import { TestBed } from '@angular/core/testing';

import { ManageMoRouteService } from './manage-mo-route.service';

describe('ManageMoRouteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageMoRouteService = TestBed.get(ManageMoRouteService);
    expect(service).toBeTruthy();
  });
});
