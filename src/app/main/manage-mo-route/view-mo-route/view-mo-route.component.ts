import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { ManageMoRouteService } from '../manage-mo-route.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ExcelService } from "../../../common/excel.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from "../../../common/common.service";

import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { RESPONSE } from "../../../common/common.const";
import { MODULE } from "../../../common/common.const";


export interface moRouteTypes {
  Actions: any;
  name: number;
  topicId: string;
  protocol: any;
  host: any;
  port: any;
  uri: any;
  status: any;
}


@Component({
  selector: 'app-view-mo-route',
  templateUrl: './view-mo-route.component.html',
  styleUrls: ['./view-mo-route.component.scss']
})
export class ViewMoRouteComponent implements OnInit {
  MODULES = MODULE;
  updateMoRoute: FormGroup;
  pageLimit: number;
  deleteReq ={};
  recordId = [];
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  searchText: string = "";
  formSubmitted: boolean = false;
  showError:boolean = false;
  p: number = 1;
  moRouteList = [];
  modalRef: any;
  moRouteName: any;
  statusForm: FormGroup;
  statusCheck: any;
  displayedColumns: string[] = ['Actions','name','topicId','protocol','host','port','uri','status'];
  dataSource: MatTableDataSource<moRouteTypes>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(private modalService: NgbModal, private manageMoRouteService: ManageMoRouteService, private fb: FormBuilder , private formBuilder: FormBuilder,
     private spinner: NgxSpinnerService, private excelService: ExcelService,
     public commonService: CommonService) {
  }

  ngOnInit() {
    this.initialiseForms();
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.getMoRouteList();
  }

   prepareApprovalForm() {
    this.statusForm = this.formBuilder.group({
      remarks: ["",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)
        ]
      ]
    });
  }

  getMoRouteList(){
    this.spinner.show();
    this.manageMoRouteService.getRouteList().subscribe(res => {
      this.spinner.hide();
      if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.moRouteList = res["data"];
        this.moRouteList.forEach(element => {
          element['status']== '1' ? element['status'] = "Enabled" : element['status'] = "Disabled";
        });
        this.dataSource = new MatTableDataSource(this.moRouteList) ;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;

      } else {
        this.moRouteList = [];
        this.commonService.showErrorToast(res['result']['userMsg']);
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.showError=false;
    if(this.dataSource.filteredData.length==0){
      this.showError=true;
    }
  }


  initialiseForms() {
    this.updateMoRoute = this.fb.group({
      routeName: ['', [Validators.required , Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)]],
      protocol: ['', [Validators.required]],
      host: [''],
      port: ['', [Validators.required,Validators.max(65535),Validators.min(1024)]],
      uri: ['', [Validators.required , Validators.pattern(this.commonService.patterns.uriValidation)]],
      });
  }

  dynamicValidation(){
    this.updateMoRoute.controls.host.clearValidators();
    this.updateMoRoute.controls.host.updateValueAndValidity();
    let contents = this.updateMoRoute.controls['host'].value.trim();
    for (var i = 0, n = contents.length; i < n; i++) {
      if (contents.charCodeAt(i) >= 48 && contents.charCodeAt(i) <= 57) {
        this.updateMoRoute.controls.host.setValidators([Validators.required, Validators.minLength(5),Validators.pattern(this.commonService.patterns.ip)]);
        this.updateMoRoute.controls.host.updateValueAndValidity();
      }else{
        this.updateMoRoute.controls.host.setValidators([Validators.required,Validators.minLength(5), Validators.pattern(this.commonService.patterns.uriValidation)]);
        this.updateMoRoute.controls.host.updateValueAndValidity();
      }
    }
  }

  confirmDeleteRecord(updatemodal: any, record: any) {
        this.formSubmitted = false;
        this.prepareApprovalForm();
        this.deleteReq = {
          "recordId": record['id'],
          "recordName" : record['name']
        }
        this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
        this.modalRef.result.then((result) => {
      }, (reason) => {
    });
  }

  deleteRecord() {
    this.deleteReq['remarks'] = this.statusForm.controls['remarks'].value.trim();
    this.formSubmitted = true;
    if(this.statusForm.valid){
    this.spinner.show();
    this.manageMoRouteService.deleteMORoute(this.deleteReq).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast(this.deleteReq['recordName'] + " has been deleted");
            this.modalRef.close();
            this.getMoRouteList();
        } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
        }
        this.formSubmitted = false;
      });
    }
  }


  confirmUpdateRecoed() {
      this.formSubmitted = true;
      this.dynamicValidation();
    if (this.updateMoRoute.valid) {
        let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
        modalRef.componentInstance.content = 'Are you sure you want to update this MO Route?';
        modalRef.componentInstance.leftButton = 'No';
        modalRef.componentInstance.rightButton = 'Yes';
        modalRef.result.then(
          (data: any) => {
            if (data == "yes") {
              this.updateMoRouteForm();
            }
          },(reason: any) => {
          });
          this.formSubmitted = false;
      }
    }

  changeStatusConfirmation(record: any,id) {

    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    if (record['status'] == 'Disabled') {
      modalRef.componentInstance.content = 'Are you sure you want to enable ' + record['name'] + ' MO Route?';
    } else if (record['status'] == 'Enabled') {
      modalRef.componentInstance.content = 'Are you sure you want to disable ' + record['name'] + ' MO Route?';
    }
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.changeStatus(record);
        } else {
          document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
        }
      },
      (reason: any) => {
             document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
     });
  }

   populateValues(item) {
    this.updateMoRoute.controls['routeName'].setValue(item.name);
    this.updateMoRoute.controls['protocol'].setValue(item.protocol);
    this.updateMoRoute.controls['uri'].setValue(item.uri);
    this.updateMoRoute.controls['port'].setValue(item.port);
    this.updateMoRoute.controls['host'].setValue(item.host);
    this.recordId = item.id;
  }

  updateRecord(updatemodal: any, record: any) {
    this.populateValues(record);
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

updateMoRouteForm() {
      let reqObj = {
        "uri": this.updateMoRoute.controls["uri"].value.trim(),
        "protocol": this.updateMoRoute.controls["protocol"].value,
        "port": this.updateMoRoute.controls["port"].value,
        "name": this.updateMoRoute.controls["routeName"].value.trim(),
        "host": this.updateMoRoute.controls["host"].value,
        "id": this.recordId
      }
      this.spinner.show();
      this.manageMoRouteService.updateMORoute(reqObj).subscribe(
        res => {
          this.commonService.showSuccessToast(reqObj['name'] + ' has been updated');
          this.formSubmitted = false;
          this.modalRef.close();
          this.getMoRouteList();
        });
    }

  changeStatus(record: any) {
    let req = {
      "routeId": record['id'],
      "statusId": record['status'] == 'Enabled' ? 2 : 1
    };
    this.spinner.show();
    this.manageMoRouteService.updateSattus(req).subscribe(
      res => {
        this.spinner.hide();
        if (req['statusId'] == 1) {
          this.commonService.showSuccessToast(record['name'] + ' MO Route enabled');
        } else {
          this.commonService.showSuccessToast(record['name'] + ' MO Route disabled');
        }
        this.getMoRouteList();
      });
  }
}
