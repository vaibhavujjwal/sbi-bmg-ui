import { ManageDepartmentModule } from './manage-department.module';

describe('ManageDepartmentModule', () => {
  let manageDepartmentModule: ManageDepartmentModule;

  beforeEach(() => {
    manageDepartmentModule = new ManageDepartmentModule();
  });

  it('should create an instance', () => {
    expect(manageDepartmentModule).toBeTruthy();
  });
});
