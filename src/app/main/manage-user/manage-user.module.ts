import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ManageUserComponent } from "./manage-user.component";
import { ViewUserComponent } from "./view-user/view-user.component";
import { AddUserComponent } from "./add-user/add-user.component";
import { PendingRequestComponent } from "./pending-request/pending-request.component";
import { routing } from "./manage-user.routing";
import { SharedModule } from "../../common/shared.modules";
import { NewViewUsersComponent } from './new-view-users/new-view-users.component';
import {MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule} from '@angular/material';
import { UnlockUserComponent } from './unlock-user/unlock-user.component';

@NgModule({
  imports: [CommonModule, routing, SharedModule,
    MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
    MatSortModule, MatTableModule
],
  declarations: [
    ManageUserComponent,
    ViewUserComponent,
    AddUserComponent,
    PendingRequestComponent,
    NewViewUsersComponent,
    UnlockUserComponent
  ]
})
export class ManageUserModule {}
