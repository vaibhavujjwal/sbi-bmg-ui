import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FailedSMSComponent } from './failed-sms.component';

describe('FailedSMSComponent', () => {
  let component: FailedSMSComponent;
  let fixture: ComponentFixture<FailedSMSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FailedSMSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FailedSMSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
