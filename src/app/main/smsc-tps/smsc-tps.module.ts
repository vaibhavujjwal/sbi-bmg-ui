import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SmscTpsComponent} from './smsc-tps.component';
import {ViewSmscTpsComponent} from './view-smsc-tps/view-smsc-tps.component';
import { routing } from './smsc-tps.routing';


@NgModule({
  imports: [
    CommonModule,
    routing
  ],
  declarations: [SmscTpsComponent, ViewSmscTpsComponent]
})
export class SmscTpsModule { }



