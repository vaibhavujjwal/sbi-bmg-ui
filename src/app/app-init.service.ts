import { Injectable }  from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
 
@Injectable()
export class AppInitService {
    constructor( private spinner: NgxSpinnerService) {
    }
    Init() {
        return new Promise<void>((resolve, reject) => {
            console.log("AppInitService.init() called");
            //this.spinner.show();
            ////do your initialisation stuff here  
            setTimeout(() => {
                console.log('AppInitService Finished');
                resolve();
            }, 1000);
        });
    }
}