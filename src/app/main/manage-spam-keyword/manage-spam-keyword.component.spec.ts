import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageSpamKeywordComponent } from './manage-spam-keyword.component';

describe('ManageSpamKeywordComponent', () => {
  let component: ManageSpamKeywordComponent;
  let fixture: ComponentFixture<ManageSpamKeywordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageSpamKeywordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageSpamKeywordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
