import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  NgbModal,
  NgbModalRef,
  NgbModalOptions,
  ModalDismissReasons
} from "@ng-bootstrap/ng-bootstrap";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormArray
} from "@angular/forms";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { Router } from "@angular/router";
import { CommonService } from "../../../common/common.service";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE, MODULE } from "src/app/common/common.const";
import { ManageRouteService } from "../manage-route.service";
import { ManageUserService } from "../../manage-user/manage-user.service";
import Swal from "sweetalert2";
import { MatTableDataSource, MatSort, MatPaginator } from "../../../../../node_modules/@angular/material";
import { ListResponse } from "./list-response";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"],
  providers: [ManageRouteService, ManageUserService]
})
export class ListComponent implements OnInit {
  MODULES = MODULE;

  private modalRef: NgbModalRef;

  modalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false,
    windowClass: "modalCustomSize"
  };
  closeResult: string;

  rows = [];
  pageLimit: number;
  p: number = 1;
  table: DatatableComponent;
  searchText: string = "";

  routeForm: FormGroup;
  deleteForm: FormGroup;
  userSetting: object = {};

  userList: Array<object> = [];
  categoryList: Array<object> = [];
  routeDetails: Array<object> = [];

  userNameSorted: boolean = false;
  topologyModeSorted: boolean = false;
  retryFlagSorted: boolean = false;
  retryRouteSorted: boolean = false;
  routeTypeSorted: boolean = false;
  userTypeSorted: boolean = false;
  routeCategorySorted: boolean = false;
  appNameSorted: boolean = false;
  deleteFormSubmitted: boolean = false;
  dataSource: MatTableDataSource<ListResponse>;
  dataSourceLength: number;
  @ViewChild('input') input: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['actions','userName', 'category', 'applicationName', 'primaryRoute', 'secondaryRoute',
    'topology', 'retryFlag', 'retryRoute','routeCategoty','userType'];

  constructor(
    public commonService: CommonService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private router: Router,
    private manageRouteService: ManageRouteService,
    private manageUserService: ManageUserService
  ) { }

  ngOnInit() {
    this.categoryList = this.manageRouteService.getCategoryList();
    this.prepareRouteForm();
    this.getUserList();
    this.getRoutesList();
    this.pageLimit = this.commonService.recordsPerPage[0];
  }

  prepareRouteForm() {
    this.routeForm = this.formBuilder.group({
      "routeSearchType": [""],//["D"]
      "categorySearchType": [""],// ["Transactional"],
      "userTypeSearchType": [""],// [4],
      "userSearchType": [""]
    })
  }

  getUserList() {
    this.spinner.show();
    this.manageRouteService.getSpecificUsersList().subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        this.spinner.hide();
        this.userList = data["data"];
        // let tmpList = data["data"];
        // this.userList = tmpList.filter(function (item) {
        //   return (
        //     item["status"] != 5 &&
        //     item["status"] != 7 &&
        //     (item["roleId"] == 4 || item["roleId"] == 5)
        //   );
        // });
        this.sortTable('userName', true);
        this.userSetting = {
          singleSelection: true,
          idField: 'userId',
          textField: 'userName',
          itemsShowLimit: 3,
          allowSearchFilter: true,
          closeDropDownOnSelection: true,
          defaultOpen: false
        }
      }
    });
  }

  getRoutesList() {
    this.spinner.show();
    let req = {
      "routeType": this.routeForm.controls.routeSearchType.value ? this.routeForm.controls.routeSearchType.value : "",
      "routeCategory": this.routeForm.controls.categorySearchType.value ? this.routeForm.controls.categorySearchType.value : "",
      "userType": this.routeForm.controls.userTypeSearchType.value ? this.routeForm.controls.userTypeSearchType.value : 0,
      "userId": this.routeForm.controls.userSearchType.value[0] ? this.routeForm.controls.userSearchType.value[0].userName : "",
      "routeId": 0
    }
    this.manageRouteService.getRoutesList(req).subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        this.rows = data["data"];
        if (this.rows.length > 0) {
          this.rows.forEach(element => {
            element['retryFlag'] == 'Y' ? element['retryFlag'] = 'On' : element['retryFlag'] = 'Off';
            element['topology'] == 1 ? element['topology'] = 'Active/Active' : element['topology'] = 'Active/Passive';
          });
          //this.sortTable('modifiedDate', false, true);
        }
        this.dataSource = new MatTableDataSource(this.rows);
        if (this.dataSource.data.length == 0) {
          this.dataSourceLength = 0;
        } else {
          this.dataSourceLength = this.dataSource.data.length;
        }
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
      this.spinner.hide();
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length == 0) {
      this.dataSourceLength = 0;
    }
    else {
      this.dataSourceLength = this.dataSource.filteredData.length;
    }
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
    if(isAssending){
      this.userList = this.commonService.sortArray(isAssending, sortBy, this.userList, isNumber);
    }else
    this.rows = this.commonService.sortArray(isAssending, sortBy, this.rows, isNumber);
  }

  searchTypeChanged() {
  }

  prepareDeleteUserForm() {
    this.deleteForm = this.formBuilder.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)
        ]
      ]
    });
  }

  deleteUser() {
    this.deleteFormSubmitted = true;

    if (this.deleteForm.valid) {
      Swal.fire({
        title: "Are you sure?",
        text: "Do you really want to delete this Aggregator Route?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ok, Proceed"
      }).then(result => {
        if (result.value) {
          this.spinner.show();
          let routeId = this.routeDetails["id"];
          let remarks = this.deleteForm.controls.remarks.value.trim();
          this.manageRouteService
            .deleteRoute(routeId, remarks)
            .subscribe(data => {
              this.spinner.hide();
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.commonService.showSuccessToast('Aggregator route has been deleted');
                this.modalRef.close();
                this.routeDetails = [];
                this.getRoutesList();
              } else {
                this.commonService.showErrorToast(data['result']['userMsg']);
              }
            });
        }
      });
    }
  }

  viewDeleteRouteModal(route, content, btn) {
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();

    this.prepareDeleteUserForm();
    this.deleteFormSubmitted = false;
    this.routeDetails = route;
    this.modalRef = this.modalService.open(content, this.modalOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}