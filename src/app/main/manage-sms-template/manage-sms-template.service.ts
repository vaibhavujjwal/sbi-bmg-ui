import { Injectable } from '@angular/core';
import { NetworkService } from "../../common/network.service";

@Injectable()
export class ManageSmsTemplateService {

    constructor(private _networkService: NetworkService) { }

    getTemplateList(req: any) {
      if(req['roleName']=='ROLE_ADMIN'){
        return this._networkService.get('honcho/template/all/all', null, 'bearer');
      }else if(req['roleName']=='ROLE_HOD'){
        return this._networkService.get('provost/template/all/all', null, 'bearer');
      }else if(req['roleName']=='ROLE_API'){
        return this._networkService.get('txns/template/all/all', null, 'bearer');
      }else if(req['roleName']=='ROLE_DLT'){
        return this._networkService.get('dlt/template/all/all', null, 'bearer');
      }else if(req['roleName']=='ROLE_CAMPAIGN'){
        return this._networkService.get('hype/template/all/all', null, 'bearer');
      }
     }

    getTemplateListPending(req: any){
          if(req['userRole']=='ROLE_DLT'){
            return this._networkService.get('dlt/template/all/10', null, 'bearer');
          }
          return this._networkService.get('provost/template/all/0', null, 'bearer');
    }

    deleteTemplate(req: any) {
        return this._networkService.post('txns/template/' + req['templateId'] + '?remarks=' + req['remarks'], null, 'bearer');
    }

    deleteTemplateCamp(req: any) {
      return this._networkService.post('hype/template/' + req['templateId'] + '?remarks=' + req['remarks'], null, 'bearer');
  }


    updateSattus(req: any) {
        return this._networkService.post('provost/template/approve/' + req["id"] + '?status=' + req["status"], null, null, 'bearer');
    }

    updateSattusDlt(req: any) {
      return this._networkService.post('dlt/template/approve/' + req["id"] + '?status=' + req["status"], null, null, 'bearer');
  }

    updateSattusApproval(req: any){
        return this._networkService.post('provost/template/approve/' + req["id"] + '?status=' + req["status"]+"&remarks="+req['remarks']+"&approvedBy="+req['userID'], null, null, 'bearer');
    }

    updateAPITemplate(req:any){
      return this._networkService.post('txns/template/update/' + req["templateID"],req , null, 'bearer');
    }

    updateAPITemplateCamp(req:any){
      return this._networkService.post('hype/template/update/' + req["templateID"],req , null, 'bearer');
    }

    getAllUser() {
        return this._networkService.get('txns/user?role=4', null, 'bearer');
    }

    getAllUserCamp() {
      return this._networkService.get('hype/user?role=5', null, 'bearer');
    }

    getSenderId() {
      return this._networkService.get('txns/senderid/info?' + 'showAll=ENABLED', null, 'bearer');
    }
    getSenderIdDlt() {
      return this._networkService.get('dlt/senderid/info?' + 'showAll=ENABLED', null, 'bearer');
    }

    getSenderIdCamp() {
      return this._networkService.get('hype/senderid/info?' + 'showAll=ENABLED', null, 'bearer');
    }

    getLanguage() {
        return this._networkService.get('master/language', null, 'bearer');
    }

    addTemplate(req: any) {
        return this._networkService.post('txns/template/info' ,
            req['reqObj'], null, 'bearer');
    }

    addTemplateCamp(req: any) {
      return this._networkService.post('hype/template/info' ,
          req['reqObj'], null, 'bearer');
  }

    uploadFile(req: any) {
        return this._networkService.uploadFile('txns/master/fileUpload', req, null, 'bearer');
    }

    uploadFileCamp(req: any) {
      return this._networkService.uploadFile('hype/master/fileUpload', req, null, 'bearer');
  }

    uploadTemplateFile(req: any) {
      return this._networkService.uploadFile('dlt/master/fileUpload', req, null, 'bearer');
  }

    saveBulkSmsTemplates(req: any) {
        return this._networkService.post('txns/template/bulkUpload' , req, null, 'bearer');
    }

    saveBulkSmsTemplatesCamp(req: any) {
      return this._networkService.post('hype/template/bulkUpload' , req, null, 'bearer');
  }

    bulkApproval(req: any){
      return this._networkService.post('provost/template/bulkApprove' , req, null, 'bearer');
    }
    
    dltBulkApproval(req: any){
      return this._networkService.post('dlt/template/bulkApprove' , req, null, 'bearer');
    }
    getCategoryList(req) {
       if(req=='ROLE_HOD'){
        return this._networkService.get("provost/master/category",null,'bearer');
      }else if(req=='ROLE_API'){
      return this._networkService.get("txns/master/category",null,'bearer');
    }else if(req=='ROLE_CAMPAIGN'){
      return this._networkService.get("hype/master/category",null,'bearer');
    }
  }
  downloadTEmplate(){
    return this._networkService.get("dlt/download/pending/templates",null,'bearer');
  }

  dupchekCamp(req:any){
    return this._networkService.get("hype/template/validate/name/"+req,null,'bearer');
  }

  dupchekApi(req:any){
    return this._networkService.get("txns/template/validate/name/"+req,null,'bearer');
  }
}
