import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from "../../../common/common.service";
import { ManageSmsTemplateService } from "../manage-sms-template.service";
import { RESPONSE } from "../../../common/common.const";
import { ExcelService } from "../../../common/excel.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";


@Component({
  selector: 'app-bulk-upload-template',
  templateUrl: './bulk-upload-template.component.html',
  styleUrls: ['./bulk-upload-template.component.scss']
})
export class BulkUploadTemplateComponent implements OnInit {

  fileUploaded: boolean = false;
  uploadedFileId: any;
  users = [];
  formSubmitted: boolean = false;
  bulkForm: FormGroup;
  loginUser :any;
  showUser:any;
  userIDValue:any;
  categoryList: Array<object> = [];
  @ViewChild('fileInput') fileInput: any;

  constructor(private spinner: NgxSpinnerService, public commonService: CommonService,
    private manageSmsTemplateService: ManageSmsTemplateService, private router: Router,private excelService: ExcelService,
    private fb: FormBuilder , private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getUserList();
    this.checkUser();
    this.initialiseForm();
  }

  initialiseForm() {
    this.bulkForm = this.fb.group({
      contentType: ['', [Validators.required]],
      userId: [''],
      caseSensitivityFlag : ['' , [Validators.required]]
    });
    this.fileUploaded = false;
    this.fileInput.nativeElement.value = '';
  }

  getUserList() {
    this.spinner.show();
    let getUserFunction = 'getAllUser';
    if(this.commonService.getUserByRole()=='ROLE_CAMPAIGN'){
      getUserFunction = 'getAllUserCamp';
    }
    this.manageSmsTemplateService[getUserFunction]().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.users = res['data'];
        }else{
          this.users = [];
        }
      });
  }

  checkUser(){
    this.loginUser = this.commonService.getUser();
    if(this.loginUser == '1000'){
      this.userIDValue = this.bulkForm.controls["userId"].value;
      this.showUser=true;
    }else{
      this.userIDValue = this.commonService.getUser();
      this.showUser=false;
    }
}

  onFileSelect(files) {
    let self = this;
    const file = files[0];
    if (file) {
      const filemb = file.size / (1024 * 1024);
      const fileExt = file.name.split(".").pop();
      if (fileExt.toLowerCase() == "xlsx" || fileExt.toLowerCase() == "xls") {
        let fromDataReq = new FormData();
        fromDataReq.append("fileData", files[0]);
        fromDataReq.append("module", "4");
        fromDataReq.append("userId", this.commonService.getUser());
        fromDataReq.append("username", this.commonService.getUserName());
        this.spinner.show();
        let uploadFile = 'uploadFile';
        if(this.commonService.getUserByRole()=='ROLE_CAMPAIGN'){
            uploadFile = 'uploadFileCamp';
          }
        this.manageSmsTemplateService[uploadFile](fromDataReq).subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
              this.uploadedFileId = res['data'];
              this.fileUploaded = true;
              this.commonService.showSuccessToast("File Uploaded successfully");
            } else {
              this.fileUploaded = false;
              this.commonService.showErrorToast(res['result']['userMsg']);
            }
          },
          err => {
            this.fileUploaded = false;
          });
      } else {
        this.commonService.showErrorToast("Invalid file");
        this.fileUploaded = false;
      }
    }
  }

  submitForm() {
    this.formSubmitted = true;
    if (this.bulkForm.valid && this.fileUploaded) {
      let req = {
        "fileId": this.uploadedFileId,
        "smsContentType":this.bulkForm.controls['contentType'].value,
        "caseSensitivityFlag":this.bulkForm.controls["caseSensitivityFlag"].value,
        "userId": this.userIDValue,
        "language": this.bulkForm.controls["contentType"].value
      }
      let bulkploadTemplate = 'saveBulkSmsTemplates';
      if(this.commonService.getUserByRole()=='ROLE_CAMPAIGN'){
        bulkploadTemplate = 'saveBulkSmsTemplatesCamp';
      }
      this.spinner.show();
      this.manageSmsTemplateService[bulkploadTemplate](req).subscribe(
        res => {
          this.spinner.hide();
          console.log("response..."+JSON.stringify(res));
          if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
            if(res['data']['successRecordCount']>0){
              this.formSubmitted = false;
              this.initialiseForm();
              this.commonService.showSuccessToast('Template created successfully.' 
              +'\n'+ 'Total Records: ' +res['data']['totalRecordCount']
              +', \n'+ 'Valid Records: ' +res['data']['successRecordCount']
              +', \n'+ 'Invalid Records: '+res['data']['invalidRecordCount'] +'.');
             
              this.router.navigate(["/main/manage-sms-template/approved-sms-template"]); 
            }else{
              this.commonService.showErrorToast('There was no valid record.'
              +'\n'+ 'Total Records: ' +res['data']['totalRecordCount']
              +', \n'+ 'Valid Records: ' +res['data']['successRecordCount']
              +', \n'+ 'Invalid Records: '+res['data']['invalidRecordCount'] +'.');
            }
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
    }
  }

  sampleFileDownload(fileType: string) {
    if (fileType === "txt") {
      let txtData = "TEXT\r\nSAMPLE";
      this.excelService.exportAsTxtFile(txtData, 'Sample_TXT');
    } else if (fileType === "excel") {
      let excelData;
        excelData = [
          {
            "Name": "Demo",
            "Template": "Hi, message Tempalte is this",
            "Mask Flag": "Y",
            "Mask Text": "hi meXXXX with mask flag",
            "Template Type":"inf",
            "Sender ID":'xyzd#abcdf'
          },
          {
            "Name": "Demo",
            "Template": "Hi, message Tempalte is this",
            "Mask Flag": "Y",
            "Mask Text": "hi meXXXX with mask flag",
            "Template Type":"inf",
            "Sender ID":'xyzd#abcdf'
          }
        ];

      this.excelService.exportAsExcelFile(excelData, 'Sample_Excel');
    } else if (fileType === "csv") {
      let csvData;
        csvData = [
          {
            "Name": "Demo",
            "Template": "Hi, message Tempalte is this",
            "Mask Flag": "Y",
            "Mask Text": "hi meXXXX with mask flag",
            "Template Type":"inf",
            "Sender ID":'xyzd#abcdf'
          },
          {
            "Name": "Demo",
            "Template": "Hi, message Tempalte is this",
            "Mask Flag": "Y",
            "Mask Text": "hi meXXXX with mask flag",
            "Template Type":"inf",
            "Sender ID":'xyzd#abcdf'
          }
        ];
      this.excelService.exportAsCsvFile(csvData, 'Sample_CSV');
    }
  }

}
