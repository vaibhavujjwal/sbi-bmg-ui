import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from "../../../common/common.service";
import { SenderIdService } from "../manage-sender-id.service";
import { RESPONSE } from "../../../common/common.const";
import { ExcelService } from "../../../common/excel.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-upload-bulk-sender-id',
  templateUrl: './upload-bulk-sender-id.component.html',
  styleUrls: ['./upload-bulk-sender-id.component.scss']
})

export class UploadBulkSenderIdComponent implements OnInit {
  fileUploaded: boolean = false;
  uploadedFileId: any;
  users = [];
  formSubmitted: boolean = false;
  selectedUser = "";
  userSetting={};
  userSearchType : number = 1;

  constructor(private spinner: NgxSpinnerService, public commonService: CommonService,
    private senderIdService: SenderIdService,private router: Router, private excelService: ExcelService) { }

  ngOnInit() {
    this.getUserList();
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
    this.users = this.commonService.sortArray(isAssending, sortBy, this.users, isNumber );
  }

  getUserList() {
    this.spinner.show()
    this.senderIdService.getUsersList(this.userSearchType).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.users =res["data"].filter(
                x => x["roleId"] == 4 || x["roleId"] == 5
          );
          this.sortTable('userName', true);
          this.userSetting = {
            singleSelection: true,
            idField: 'userId',
            textField: 'userName',
            itemsShowLimit: 3,
            allowSearchFilter: true,
            defaultOpen: false
          }

        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.users = [];
        }
      }
    );
  }

  @ViewChild('fileInput') fileInput: any;
  onFileSelect(files) {
    let self = this;
    const file = files[0];
    if (file) {
      const filemb = file.size / (1024 * 1024);
      const fileExt = file.name.split(".").pop();
      if (fileExt.toLowerCase() == "xlsx" || fileExt.toLowerCase() == "xls") {
        let fromDataReq = new FormData();
        fromDataReq.append("fileData", files[0]);
        fromDataReq.append("module", "3");
        fromDataReq.append("userId", this.commonService.getUser());
        fromDataReq.append("username ", this.commonService.getUserName());
        this.spinner.show();
        this.senderIdService.uploadFile(fromDataReq).subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
              this.uploadedFileId = res['data'];
              this.fileUploaded = true;
              this.commonService.showSuccessToast("File Uploaded successfully");
            } else {
              this.fileUploaded = false;
              this.commonService.showErrorToast(res['result']['userMsg']);
            }
          },
          err => {
            this.fileUploaded = false;
          });
      } else {
        this.commonService.showErrorToast("Invalid file");
        this.fileUploaded = false;
      }
    }
  }

  submitForm() {
    this.formSubmitted = true;
    debugger;

    if (this.fileUploaded && this.selectedUser) {
      var userArray=this.selectedUser;
      for(let i=0;i<userArray.length;i++){
             var userID = userArray[i]['userId'];
      }

    let req = {
        "fileId": this.uploadedFileId,
        "refId": 1000,
        "userId": userID
      }
      this.spinner.show();
      this.senderIdService.saveBulkSenderId(req).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
            this.formSubmitted = false;
            this.resetForm();
            this.commonService.showSuccessToast('CLI created successfully');
            this.router.navigate(["/main/manage-sender-id/list-sender-id"]);
       } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
    }
  }

  resetForm(){
    this.fileUploaded = false;
    this.selectedUser = "";
    this.fileInput.nativeElement.value = '';
  }

  sampleFileDownload(fileType: string) {
    if (fileType === "txt") {
      let txtData = "TEXT\r\nSAMPLE";
      this.excelService.exportAsTxtFile(txtData, 'Sample_TXT');
    } else if (fileType === "excel") {
      let excelData = [
          {
            "SenderId": "123123"
          },
          {
            "SenderId": "adsfads"
          }
      ];
  this.excelService.exportAsExcelFile(excelData, 'Sample_Excel');
    }else if (fileType === "csv") {
      let csvData = [
          {
            "SenderId": "123123"
          },
          {
            "SenderId": "adsfads"
          }
        ];
      this.excelService.exportAsCsvFile(csvData, 'Sample_CSV');
    }
  }
}
