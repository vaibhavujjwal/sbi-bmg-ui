import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageMoRouteComponent } from './manage-mo-route.component';

describe('ManageMoRouteComponent', () => {
  let component: ManageMoRouteComponent;
  let fixture: ComponentFixture<ManageMoRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageMoRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageMoRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
