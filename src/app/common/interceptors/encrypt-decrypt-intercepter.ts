import { Injectable } from "@angular/core";
import { tap, map } from "rxjs/operators";
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
} from "@angular/common/http";
import { Observable } from "rxjs/Observable";

declare var AesUtil: any;
@Injectable()
export class EncryptDecryptIntercepter implements HttpInterceptor {
    reqBody: string;
    isNotEncrypted: any;
    constructor() { }
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        this.isNotEncrypted = request.url.includes('/v2/');         // For non encrypted request, skip following if (&& !this.isNotEncrypted)
        if (request.body != null && !this.isNotEncrypted) {
            console.log('processing request... ', request.body);
            var str = JSON.stringify(request.body);
            //var str1 = str.replace(/\\/g, '');
            var str1 =str;
            if (str1.startsWith("\"")) {
                this.reqBody = str1.substring(1, str1.length - 1)
            } else {    
                this.reqBody = str1;
            }
            console.log('now request body... ', this.reqBody);
            var iterationCount = 1000;
            var keySize = 128;
            var aesUtil = new AesUtil(keySize, iterationCount);
            var encString1 = aesUtil.encrypt("8bc82ffe516ea1f1b89dcb529a201d56", "ebb7947b5feabdc0b60f2a694fa259c4", "Gr33nL!ght#D91", this.reqBody);
            var encString = "{\"mnbvcxzlkjhgfdsapoiuytrewq\":\"" + encString1 + "\"}";
            console.log('encrypted request body... ' + JSON.stringify(encString));
        }
        console.log('requested url... ', request.url);
        let updatedRequest = request;
        if (!(request.url.endsWith("/login") || request.url.endsWith("/campaign/getHeaders") || request.url.endsWith("/master/fileUpload") || (request.url.includes("hype/addressBook") && request.url.includes("/download")))) {
            console.log('its not a login URL ');
            updatedRequest = request.clone({
                //headers: request.headers.set("Authorization", "Some-dummyCode")
                body: encString
            });
        }
        
        console.log("Before making api call : ", updatedRequest);
        return next.handle(updatedRequest).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log("api call success... ", event);
                    console.log('event.body... ', event.body);
                    if (event.body != null) {
                        if (event.body.qwertyuiopasdfghjklzxcvbnm != undefined) {
                            var iterationCount = 1000;
                            var keySize = 128;
                            var aesUtil = new AesUtil(keySize, iterationCount);
                            var ciphertext = aesUtil.decrypt("8bc82ffe516ea1f1b89dcb529a201d56", "ebb7947b5feabdc0b60f2a694fa259c4", "Gr33nL!ght#D91", event.body.qwertyuiopasdfghjklzxcvbnm);
                            console.log('ciphertext is...' + JSON.stringify(JSON.parse(ciphertext)['data']));
                            if (event.url.endsWith("/sbi/config/v1/account"))
                                sessionStorage.setItem('_userdata', JSON.stringify(JSON.parse(ciphertext)['data']))
                            event = event.clone({
                                body: JSON.parse(ciphertext)
                            });
                        }
                    }
                }
                return event;
            })
        );
    }
}



// import { Injectable } from "@angular/core";
// import { tap, map } from "rxjs/operators";
// import {
//     HttpRequest,
//     HttpHandler,
//     HttpEvent,
//     HttpInterceptor,
//     HttpResponse,
//     HttpErrorResponse
// } from "@angular/common/http";
// import { Observable } from "rxjs/Observable";

// declare var AesUtil: any;
// @Injectable()
// export class EncryptDecryptIntercepter implements HttpInterceptor {
//     reqBody: string;
//     constructor() { }
//     intercept(
//         request: HttpRequest<any>,
//         next: HttpHandler
//     ): Observable<HttpEvent<any>> {
//         if (request.body != null) {
//             console.log('processing request... ', request.body);
//             var str = JSON.stringify(request.body);
//             var str1 = str.replace(/\\/g, '');
//             if (str1.startsWith("\"")) {
//                 this.reqBody = str1.substring(1, str1.length - 1)
//             } else {
//                 this.reqBody = str1;
//             }
//             console.log('now request body... ', this.reqBody);
//             var iterationCount = 1000;
//             var keySize = 128;
//             var aesUtil = new AesUtil(keySize, iterationCount);
//             var encString1 = aesUtil.encrypt("8bc82ffe516ea1f1b89dcb529a201d56", "ebb7947b5feabdc0b60f2a694fa259c4", "Gr33nL!ght#D91", this.reqBody);
//             var encString = "{\"mnbvcxzlkjhgfdsapoiuytrewq\":\"" + encString1 + "\"}";
//             console.log('encrypted request body... ' + JSON.stringify(encString));
//         }
//         console.log('requested url... ', request.url);
//         let updatedRequest = request;
//         if (!request.url.endsWith("/login")) {
//             console.log('its not a login URL');
//             updatedRequest = request.clone({
//                 //headers: request.headers.set("Authorization", "Some-dummyCode")
//                 body: encString
//             });
//         }
        
//         console.log("Before making api call : ", updatedRequest);
//         return next.handle(updatedRequest).pipe(
//             map((event: HttpEvent<any>) => {
//                 if (event instanceof HttpResponse) {
//                     console.log("api call success... ", event);
//                     console.log('event.body... ', event.body);
//                     if (event.body != null) {
//                         if (event.body.qwertyuiopasdfghjklzxcvbnm != undefined) {
//                             var iterationCount = 1000;
//                             var keySize = 128;
//                             var aesUtil = new AesUtil(keySize, iterationCount);
//                             var ciphertext = aesUtil.decrypt("8bc82ffe516ea1f1b89dcb529a201d56", "ebb7947b5feabdc0b60f2a694fa259c4", "Gr33nL!ght#D91", event.body.qwertyuiopasdfghjklzxcvbnm);
//                             console.log('ciphertext is...' + JSON.stringify(JSON.parse(ciphertext)['data']));
//                             if (event.url.endsWith("/sbi/config/v1/account"))
//                                 sessionStorage.setItem('_userdata', JSON.stringify(JSON.parse(ciphertext)['data']))
//                             event = event.clone({
//                                 body: JSON.parse(ciphertext)
//                             });
//                         }
//                     }
//                 }
//                 return event;
//             })
//         );
//     }
// }

 // catchError((error, caught) => {
            //     if (error != null || error != undefined) {
            //         var iterationCount = 1000;
            //         var keySize = 128;
            //         var aesUtil = new AesUtil(keySize, iterationCount);
            //         console.log("error... ", error.error);
            //         console.log("error.qwertyuiopasdfghjklzxcvbnm... ", error.error.qwertyuiopasdfghjklzxcvbnm);
            //         var ciphertext = aesUtil.decrypt("8bc82ffe516ea1f1b89dcb529a201d56", "ebb7947b5feabdc0b60f2a694fa259c4", "Gr33nL!ght#D91", error.error.qwertyuiopasdfghjklzxcvbnm);
            //         console.log('ciphertext of error is...' + JSON.stringify(JSON.parse(ciphertext)));
            //         this.spinner.hide();
            //         if (JSON.parse(ciphertext)['result'] && JSON.parse(ciphertext)['result']['userMsg']) {
            //           this.commonService.showErrorToast(JSON.parse(ciphertext)['result']['userMsg']);
            //           if (JSON.parse(ciphertext)['result']['userMsg'].toLowerCase() == "Unauthorized access".toLowerCase()) {
            //             sessionStorage.clear();
            //             this.router.navigate([""]);
            //           }
            //         }
            //       }
            //     return empty();
            //   })