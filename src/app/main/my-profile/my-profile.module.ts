import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { MyProfileComponent } from "./my-profile.component";
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../../common/shared.modules";

const routes: Routes = [
  {
    path: "",
    component: MyProfileComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/my-profile/view-profile",
        pathMatch: "full"
      },
      {
        path: "view-profile",
        component: ViewProfileComponent
      }
    ]
  }
]

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MyProfileComponent, ViewProfileComponent]
})
export class MyProfileModule { }
