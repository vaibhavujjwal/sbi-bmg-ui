import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
// import { ChartOptions, ChartType, ChartDataSets } from '../../../../node_modules/@types/chart.js';
import { ChartDataSets,ChartOptions, ChartType } from 'chart.js';
import { Label } from '../../../../node_modules/ng2-charts';
import { DashboardService } from "./dashboard.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { RESPONSE } from "../../common/common.const";
import { CommonService } from "../../common/common.service";
import { debug } from 'util';
import * as pluginDataLabels from '../../../../node_modules/chartjs-plugin-datalabels';
import { FormGroup, FormBuilder, Validators } from '../../../../node_modules/@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private dashboardService: DashboardService,
    private spinner: NgxSpinnerService,
    public commonService: CommonService,
    private fb: FormBuilder, ) { }
  
  @ViewChild('selectedCategory') selectedCategory: ElementRef;
  userWiseTrafficList = [];
  hours = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
  timestampGraph = [];
  uptimeData = [];
  yesterday:any;
  yesterday1:any;
  generalDataList = [];
  campaignBoardList = [];
  deptApiTrafficList = [];
  campaignBulkTrafficList: any;
  uptoday:any;
  timeStampToday:any;
  isYester: boolean=false;
  istoday:boolean=true;
  tisYester: boolean=false;
  tistoday:boolean=true;
  apiTrafficList: any;
  requestParam: any;
  totalCampTile: number;
  totalSMSCampTile: number;
  successSMSCampTile: number;
  pendingSMSCampTile: number;
  failedSMSCampTile: number;
  totalSMSTile: number;
  successSMSTile: number;
  pendingSMSTile: number;
  failedSMSTile: number;
  deptList = [];
  appList = [];
  public pieChartData: number[];
  department: any;
  applicationName: string;
  selectedHour:any;
  upselectedHour:any;
  startDate: any;
  endDate: any;
  isTodayClass: boolean;
  isWeekClass: boolean;
  isMonthClass: boolean;
  isYearClass: boolean;
  isAppNotAvail: boolean = false;
  barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{
      scaleLabel: {
        display: true,
        labelString: 'Minutes'
      }
    }], yAxes: [{
      scaleLabel: {
        display: true,
        labelString: 'Count'
      },ticks: {
        beginAtZero: true
      }
    }] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
};

barChartOptions1: ChartOptions = {
  responsive: true,
  // We use these empty structures as placeholders for dynamic theming.
  scales: { xAxes: [{
    scaleLabel: {
      display: true,
      labelString: 'Seconds'
    }
  }], yAxes: [{
    scaleLabel: {
      display: true,
      labelString: 'Count'
    },ticks: {
      beginAtZero: true
    }
  }] },
  plugins: {
    datalabels: {
      anchor: 'end',
      align: 'end',
    }
  }
};

barChartLabels: Label[] = [
 '05', '10', '15', '20',
 '25', '30', '35', '40',
 '45', '50', '55','60'];

barChartLabels1: Label[] = [
  '<=01', '1<10', '10<60',
  '60<120','>120'];
barChartType: ChartType = 'line';
barChartLegend = true;
barChartPlugins = [pluginDataLabels];

barChartData: ChartDataSets[] = [
  { data: [],
    label: 'OTP>2(min)' },
  { data: [], 
    label: 'TXN>5(min)' }
];

barChartData1: ChartDataSets[] = [
  { data: [],
    label: 'OTP' },
  { data: [], 
    label: 'TXN' }
];

  ngOnInit() {
    this.getTodayWeekMonthYearDate('today');
    if(this.commonService.getUserByRole() == "ROLE_ADMIN"){
      let dte = new Date();
      this.selectedHour = dte.getHours();
      this.upselectedHour = dte.getHours();
      this.uptoday = dte.toISOString().substr(0, 10);
      this.timeStampToday = this.uptoday;
      this.yesterday = this.timeStampToday;
      this.yesterday1 = this.timeStampToday;
      this.getUptimeData(this.uptoday);
      this.timestampGraphData(this.uptoday);
    }
  }

  timestampGraphData(today: any){
    let req = {
      reportDate:today,
      report:this.selectedHour
    }
    this.spinner.show();
    this.dashboardService.timestampGraphData(req).subscribe(responseList => {
      this.spinner.hide();
      if (responseList['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.timestampGraph = responseList["data"];
        this.barChartData1['0']['data'] = this.timestampGraph['otpDataList'];
        this.barChartData1['1']['data'] = this.timestampGraph['txnDataList'];
        console.log(this.barChartData1);
      } else {
        this.timestampGraph = [];
      }
    },err => {
      this.commonService.showErrorToast(err);
    });
  }

  getUptimeData(today: any){
    let req = {
      reportDate:today,
      report:this.upselectedHour
    }
    this.spinner.show();
    this.dashboardService.getUptimeData(req).subscribe(responseList => {
      this.spinner.hide();
      if (responseList['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.uptimeData = responseList["data"];
        this.barChartData['0']['data'] = this.uptimeData['otpDataList'];
        this.barChartData['1']['data'] = this.uptimeData['txnDataList'];
        console.log(this.barChartData);
      } else {
        this.uptimeData = [];
      }
    },err => {
      this.commonService.showErrorToast(err);
    });
  }

  changeDate(obj:any,graph:any){
    if(graph=='UPTIME'){
      let dte = new Date();
      dte.setDate(dte.getDate() - obj);
      let selectDate = dte.toISOString().substr(0, 10);
      this.yesterday1 = selectDate;
      this.istoday = obj==1?false:true;
      this.isYester = obj==1?true:false;
      this.getUptimeData(selectDate);
    }else if(graph=='TIMESTAMP'){
      let dte = new Date();
      dte.setDate(dte.getDate() - obj);
      let selectDate = dte.toISOString().substr(0, 10);
      this.yesterday = selectDate;
      this.tistoday = obj==1?false:true;
      this.tisYester = obj==1?true:false;
      this.timestampGraphData(selectDate);
    }
  }


  getTodayWeekMonthYearDate(dataFor: string) {
    var curr = new Date;
    var currYear = curr.getFullYear();

    if (dataFor == "today") {
      this.startDate = new Date().toISOString().substr(0, 10);
      this.endDate = new Date().toISOString().substr(0, 10);
      this.isTodayClass = true;
      this.isWeekClass = false;
      this.isMonthClass = false;
      this.isYearClass = false;
    } else if (dataFor == "week") {
      var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
      var last = first + 6; // last day is the first day + 6
      this.startDate = new Date(curr.setDate(first)).toISOString().substr(0, 10);
      this.endDate = new Date().toISOString().substr(0, 10);
      this.isTodayClass = false;
      this.isWeekClass = true;
      this.isMonthClass = false;
      this.isYearClass = false;
    } else if (dataFor == "month") {
      var currMonth = curr.getMonth() + 1;
      let newCurrMonth;
      if (currMonth < 10) {
        newCurrMonth = "0" + currMonth;
        this.startDate = currYear + "-" + newCurrMonth + "-" + "01";
      }
      else {
        this.startDate = currYear + "-" + currMonth + "-" + "01";
      }
      this.endDate = new Date().toISOString().substr(0, 10);
      this.isTodayClass = false;
      this.isWeekClass = false;
      this.isMonthClass = true;
      this.isYearClass = false;
    } else if (dataFor == "year") {
      this.startDate = currYear + "-" + "01-01";
      this.endDate = new Date().toISOString().substr(0, 10);
      this.isTodayClass = false;
      this.isWeekClass = false;
      this.isMonthClass = false;
      this.isYearClass = true;
    }
    if (this.commonService.getUserByRole() == "ROLE_ADMIN") {
      this.getDeptartments();
    } else if (this.commonService.getUserByRole() == "ROLE_HOD") {
      this.toGetAllHODApplications(this.commonService.getDeptID());
    } else {
      this.applicationName = this.commonService.getAppName();
      this.toGetApp(this.commonService.getAppName());
    }
    this.getCampaignTrafficTiles();
    this.getAPITrafficTiles();
    this.getUserWiseTraffic();
    this.getGeneralData();
  }

  getDeptartments() {
    this.spinner.show();
    this.dashboardService.getDept().subscribe(responseList => {
      this.spinner.hide();
      console.log("Departments.... " + JSON.stringify(responseList));
      if (responseList['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.deptList = responseList["data"];
        this.toGetAllApplications(this.deptList[1]);
        this.department = this.deptList[1];
      }
    })
  }
  toGetAllHODApplications(id: number) {
    this.spinner.show();
    this.dashboardService.getAppName(id).subscribe(responseList => {
      this.spinner.hide();
      if (responseList['result']['statusDesc'] == RESPONSE.SUCCESS) {
        if (responseList["data"].length > 0) {
          this.appList = responseList["data"];
          this.applicationName = this.appList[0].applicationName;
          this.toGetApp(this.appList[0].applicationName);
        }
        else {
          this.applicationName = "";
          this.isAppNotAvail = true;
          this.deptApiTrafficList = [];
          this.campaignBoardList = [];
          this.getData();
        }
      }
    })
  }
  toGetAllApplications(event: any) {
    this.spinner.show();
    this.dashboardService.getAppName(event.id).subscribe(responseList => {
      this.spinner.hide();
      if (responseList['result']['statusDesc'] == RESPONSE.SUCCESS) {
        if (responseList["data"].length > 0) {
          this.appList = responseList["data"];
          this.isAppNotAvail = false;
          this.applicationName = this.appList[0].applicationName;
          this.toGetApp(this.appList[0].applicationName);
        }
        else {
          this.applicationName = "";
          this.isAppNotAvail = true;
          this.deptApiTrafficList = [];
          this.campaignBoardList = [];
          this.getData();
        }
      }
    })
  }

  toGetApp(name: string) {
    this.campaignBoardList = [];
    this.deptApiTrafficList = [];
    this.applicationName = name;
    this.getData();
  }

  getData() {
    this.requestParam = {
      "startDate": this.startDate,
      "endDate": this.endDate,
      "appName": this.applicationName,
      "isAppNotAvail": this.isAppNotAvail
    };
    this.spinner.show();
    this.dashboardService.getData(this.requestParam).subscribe(responseList => {
      this.spinner.hide();
      // if (responseList[0]['result']['statusDesc'] == RESPONSE.SUCCESS) {
      //   this.userWiseTrafficList = responseList[0]["data"];
      //   console.log("userwiseTrafficDetails data response...." + JSON.stringify(this.userWiseTrafficList));
      //   console.log("selected Category...." + this.selectedCategory);
      // } else {
      //   this.userWiseTrafficList = [];
      // }

      // if (responseList[1]['result']['statusDesc'] == RESPONSE.SUCCESS) {
      //   this.generalDataList = responseList[1]["data"];
      //   console.log("generalData data response...." + JSON.stringify(this.generalDataList));
      //   this.pieChartData = [this.generalDataList['campaignSuccessPercent'], this.generalDataList['campaignFailurePercent']];
      // } else {
      //   this.generalDataList = [];
      // }
      // if (responseList[2]['result']['statusDesc'] == RESPONSE.SUCCESS) {
      //   this.campaignBulkTrafficList = responseList[2]['data'];
      //   console.log("campaignBulkTrafficList data response...." + JSON.stringify(this.campaignBulkTrafficList));
      //   if (this.campaignBulkTrafficList) {
      //     this.totalCampTile = this.campaignBulkTrafficList.map(a => a.totalSuccessCampaign);
      //     this.totalSMSCampTile = this.campaignBulkTrafficList.map(a => a.smsSent);
      //     this.successSMSCampTile = this.campaignBulkTrafficList.map(a => a.successSms);
      //     this.pendingSMSCampTile = this.campaignBulkTrafficList.map(a => a.pendingSms);
      //     this.failedSMSCampTile = this.campaignBulkTrafficList.map(a => a.failedSms);
      //   }
      // } else {
      //   this.campaignBulkTrafficList = []
      // }

      // if (responseList[3]['result']['statusDesc'] == RESPONSE.SUCCESS) {
      //   this.apiTrafficList = responseList[3]['data'];
      //   console.log("apiTrafficList data response...." + JSON.stringify(this.apiTrafficList));
      //   this.totalSMSTile = this.apiTrafficList.map(a => a.sentSms);
      //   this.successSMSTile = this.apiTrafficList.map(a => a.successSms);
      //   this.pendingSMSTile = this.apiTrafficList.map(a => a.pendingSms);
      //   this.failedSMSTile = this.apiTrafficList.map(a => a.failedSms);
      // } else {
      //   this.apiTrafficList = [];
      // }
      if (this.isAppNotAvail == false) {
        if (responseList[0]['result']['statusDesc'] == RESPONSE.SUCCESS) {
          console.log("deptartmentApiTraffic data response...." + JSON.stringify(responseList[0]['data']));
          responseList[0]['data'].forEach(element => {
            let tileObj = {
              "lineChart": [],
              "barChart": [],
              "users": [],
              "hours": [],
              "deptID": '',
              "deptName": ''
            };
            let getArray = element.userWiseHourlyTps[0]['tpsInfo'];
            let allHoursWithTPS = this.getRemainingHours(getArray);
            tileObj['deptID'] = element['deptId'];
            tileObj['deptName'] = element['deptName'];
            let dataArrayLine = allHoursWithTPS.map(b => b.tps);
            let labelArrayLine = element.userWiseHourlyTps.map(a => a.username);
            // for (let i = 0; i < dataArrayLine.length; i++) {
            let objt = {};
            objt['data'] = dataArrayLine;
            objt['label'] = labelArrayLine;
            objt['fill'] = false;
            tileObj['lineChart'].push(objt);
            console.log("deptartmentApiTraffic LINE CHART...." + JSON.stringify(tileObj['lineChart']));
            // }
            let dataArrayBarTotal = element.userWiseSmsStatus.map(a => a.totalSmsSent);
            let obj = {};
            obj['data'] = dataArrayBarTotal;
            obj['label'] = "Total";
            tileObj['barChart'].push(obj);
            let dataArrayBarSuccess = element.userWiseSmsStatus.map(a => a.success);
            obj = {};
            obj['data'] = dataArrayBarSuccess;
            obj['label'] = "Success";
            tileObj['barChart'].push(obj);
            let dataArrayBarFailed = element.userWiseSmsStatus.map(a => a.fail);
            obj = {};
            obj['data'] = dataArrayBarFailed;
            obj['label'] = "Failed";
            tileObj['barChart'].push(obj);
            let dataArrayBarPending = element.userWiseSmsStatus.map(a => a.pending);
            obj = {};
            obj['data'] = dataArrayBarPending;
            obj['label'] = "Pending";
            tileObj['barChart'].push(obj);
            let lebelArrayBar = element.userWiseSmsStatus.map(a => a.username);
            tileObj['users'] = lebelArrayBar;
            // let lebelArrayLine = element.userWiseSmsStatus.map(a => a.username);
            tileObj['hours'] = allHoursWithTPS.map(b => b.hour);
            tileObj['hours'] = [].concat.apply([], tileObj['hours']);
            tileObj['hours'] = Array.from(new Set(tileObj['hours']));
            tileObj['hours'].sort(function (a, b) { return a - b });
            this.deptApiTrafficList.push(tileObj);
            console.log("FINAL OBJ......" + JSON.stringify(this.deptApiTrafficList));
          });
        } else {
          this.deptApiTrafficList = [];
        }


        if (responseList[1]['result']['statusDesc'] == RESPONSE.SUCCESS) {
          console.log("campaignBulkTraffic data response...." + JSON.stringify(responseList[1]['data']));
          responseList[1]['data'].forEach(element => {
            element.campaignWiseTpsGraph = this.getRemainingHours(element.campaignWiseTpsGraph);
            let tileObj = {
              "campaignId": 0,
              "campaignName": '',
              "status": '',
              "departmentName": '',
              "departmentUser": '',
              "senderId": '',
              "startTime": '',
              "endTime": '',
              "totalSmsCount": 0,
              "dndCount": 0,
              "smsSent": 0,
              "smsSuccess": 0,
              "smsFailed": 0,
              "smsPending": 0,
              "currentTps": 0,
              "maxTps": 0,
              "hours": [],
              "date": [],
              "lineChart": []
            };
            tileObj['campaignId'] = element['campaignId'];
            tileObj['campaignName'] = element['campaignName'];
            tileObj['status'] = element['status'];
            tileObj['departmentName'] = element['departmentName'];
            tileObj['departmentUser'] = element['departmentUser'];
            tileObj['senderId'] = element['senderId'];
            tileObj['startTime'] = element['startTime'];
            tileObj['endTime'] = element['endTime'];
            tileObj['totalSmsCount'] = element['totalSmsCount'];
            tileObj['dndCount'] = element['dndCount'];
            tileObj['smsSent'] = element['smsSent'];
            tileObj['smsSuccess'] = element['smsSuccess'];
            tileObj['smsFailed'] = element['smsFailed'];
            tileObj['smsPending'] = element['smsPending'];
            tileObj['currentTps'] = element['currentTps'];
            tileObj['maxTps'] = element['maxTps'];
            tileObj['date'] = element.campaignWiseTpsGraph.map(a => a.date);
            tileObj['hours'] = element.campaignWiseTpsGraph.map(a => a.hour);
            tileObj['hours'] = [].concat.apply([], tileObj['hours']);
            tileObj['hours'] = Array.from(new Set(tileObj['hours']));
            tileObj['hours'].sort(function (a, b) { return a - b });

            let dataArrayLine = element.campaignWiseTpsGraph.map(a => a.tps);
            for (let i = 0; i < dataArrayLine.length; i++) {
              let obj = {};
              obj['data'] = dataArrayLine[i];
              obj['label'] = tileObj['campaignName'];
              obj['fill'] = false
              tileObj['lineChart'].push(obj);
            }
            this.campaignBoardList.push(tileObj);
          })
        } else {
          this.campaignBoardList = [];
        }
      }
      if (this.isAppNotAvail == false) {
        if (responseList[0]['result']['statusDesc'] != RESPONSE.SUCCESS && responseList[1]['result']['statusDesc'] != RESPONSE.SUCCESS) {
          // this.commonService.showErrorToast(responseList[0]['result']['userMsg'] + " and " + responseList[1]['result']['userMsg']);
        }
        else if (responseList[0]['result']['statusDesc'] != RESPONSE.SUCCESS) {
          // this.commonService.showErrorToast(responseList[0]['result']['userMsg']);
        }
        else if (responseList[1]['result']['statusDesc'] != RESPONSE.SUCCESS) {
          // this.commonService.showErrorToast(responseList[1]['result']['userMsg']);
        }
        // else if (responseList[2]['result']['statusDesc'] != RESPONSE.SUCCESS) {
        //   this.commonService.showErrorToast(responseList[2]['result']['userMsg']);
        // }
        // else if (responseList[3]['result']['statusDesc'] != RESPONSE.SUCCESS) {
        //   this.commonService.showErrorToast(responseList[3]['result']['userMsg']);
        // }
        // else if (responseList[4]['result']['statusDesc'] != RESPONSE.SUCCESS) {
        //   this.commonService.showErrorToast(responseList[4]['result']['userMsg']);
        // }
        // else if (responseList[5]['result']['statusDesc'] != RESPONSE.SUCCESS) {
        //   this.commonService.showErrorToast(responseList[5]['result']['userMsg']);
        // }
      }
      // else {
      //   if (responseList[0]['result']['statusDesc'] != RESPONSE.SUCCESS && responseList[1]['result']['statusDesc'] != RESPONSE.SUCCESS
      //     && responseList[2]['result']['statusDesc'] != RESPONSE.SUCCESS && responseList[3]['result']['statusDesc'] != RESPONSE.SUCCESS) {
      //     this.commonService.showErrorToast(responseList[0]['result']['userMsg'] + " , " + responseList[1]['result']['userMsg'] + " , " +
      //       responseList[2]['result']['userMsg'] + " and " + responseList[3]['result']['userMsg']);
      //   }
      //   else if (responseList[0]['result']['statusDesc'] != RESPONSE.SUCCESS) {
      //     this.commonService.showErrorToast(responseList[0]['result']['userMsg']);
      //   }
      //   else if (responseList[1]['result']['statusDesc'] != RESPONSE.SUCCESS) {
      //     this.commonService.showErrorToast(responseList[1]['result']['userMsg']);
      //   }
      //   else if (responseList[2]['result']['statusDesc'] != RESPONSE.SUCCESS) {
      //     this.commonService.showErrorToast(responseList[2]['result']['userMsg']);
      //   }
      //   else if (responseList[3]['result']['statusDesc'] != RESPONSE.SUCCESS) {
      //     this.commonService.showErrorToast(responseList[3]['result']['userMsg']);
      //   }
      // }
    })
  }
  // PIE CHART
  public pieChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = ['Success', 'Fail'];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartColors = [
    {
      backgroundColor: ['#66bb6a', '#e57373'],
    },
  ];
  public campaignWiseOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    maintainAspectRatio: false,
    spanGaps: true,
    legend: { display: false },
    scales: {
      xAxes: [{
        scaleLabel: { display: true, labelString: 'Hours', fontSize: 16, fontColor: '#8f3e92' },
        ticks: {
          autoSkip: false,
        }
      }],
      yAxes: [
        {
          scaleLabel: { display: true, labelString: 'TPS', fontSize: 16, fontColor: 'rgba(0,191,165,1)' },
          ticks: { min: 0, beginAtZero: true, fontSize: 13 }
        },
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public campaignWiseColors: Array<any> = [
    {
      backgroundColor: '#f9bd63',
      borderColor: '#ffa726',
      pointBackgroundColor: '#ffa726',
      pointBorderColor: '#ffa726',
      pointHoverBackgroundColor: '#ffa726',
      pointHoverBorderColor: '#ffa726'
    }
  ];
  public campaignWiseLegend = true;
  public campaignWiseType = 'line';
  public userwiseSMSOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales:
    {
      yAxes:
        [
          {
            scaleLabel: { display: true, labelString: 'Count', fontSize: 16, fontColor: '#66bb6a' },
            position: 'left', id: '1', type: 'linear',
            ticks: { min: 0, beginAtZero: true, fontSize: 13 }
          }
        ],
      xAxes:
        [
          {
            scaleLabel: { display: true, labelString: 'Users', fontSize: 16, fontColor: '#e57373' },
          }
        ]
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public userwiseSMSColors: Array<any> = [
    {
      backgroundColor: '#f29fbb',
      borderColor: '#f48fb1',
      pointBackgroundColor: '#f48fb1',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#f48fb1'
    },
    {
      backgroundColor: '#88d88c',
      borderColor: '#66bb6a',
      pointBackgroundColor: '#66bb6a',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#66bb6a'
    },
    {
      backgroundColor: '#f78f8f',
      borderColor: '#e57373',
      pointBackgroundColor: '#e57373',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#e57373'
    },
    {
      backgroundColor: '#fcd864',
      borderColor: '#ffd54f',
      pointBackgroundColor: '#ffd54f',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#ffd54f'
    }
  ];
  public userwiseSMSType: ChartType = 'bar';
  public userwiseSMSLegend = true;

  // LINE GRAPH FOR API TRAFFIC USERWISE
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    maintainAspectRatio: false,
    spanGaps: true,
    scales: {
      xAxes: [{
        scaleLabel: { display: true, labelString: 'Hours', fontSize: 16, fontColor: '#8f3e92' },
        ticks: {
          autoSkip: false,
        }
      }],
      yAxes: [
        {
          scaleLabel: { display: true, labelString: 'TPS', fontSize: 16, fontColor: 'rgba(0,191,165,1)' },
          ticks: { min: 0, beginAtZero: true, fontSize: 13 }
        },
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(171,234,225,0.5)',
      borderColor: '#00B0FB	',
      pointBackgroundColor: '#00B0FB',
      pointBorderColor: '#00B0FB',
      pointHoverBackgroundColor: '#00B0FB',
      pointHoverBorderColor: '#00B0FB'
    },
    {
      backgroundColor: 'rgba(171,234,225,0.5)',
      borderColor: '#FF9C00',
      pointBackgroundColor: '#FF9C00',
      pointBorderColor: '#FF9C00',
      pointHoverBackgroundColor: '#FF9C00',
      pointHoverBorderColor: '#FF9C00'
    },
    {
      backgroundColor: 'rgba(171,234,225,0.5)',
      borderColor: '#009754',
      pointBackgroundColor: '#009754',
      pointBorderColor: '#009754',
      pointHoverBackgroundColor: '#009754',
      pointHoverBorderColor: '#009754'
    }
  ];

  public lineChartLegend = true;
  public lineChartType = 'line';

  campaignWiseTpsGraph = [
    { "tps": 4, "hour": 12, "date": "2019-04-29" },
    { "tps": 4, "hour": 10, "date": "2019-04-29" },
    { "tps": 2, "hour": 19, "date": "2019-04-29" },
    { "tps": 4, "hour": 23, "date": "2019-04-29" },
    { "tps": 4, "hour": 25, "date": "2019-04-29" },
    { "tps": 2, "hour": 18, "date": "2019-04-29" }
  ]

  campaignWiseTpsGraph2 = [];
  min: any;
  max: any;

  getRemainingHours(arrayOfTps: any) {
    this.campaignWiseTpsGraph2 = [];
    if (arrayOfTps.length > 0) {
      this.min = arrayOfTps[0].hour;
      this.max = arrayOfTps[0].hour;
    }

    for (let i = 1; i < arrayOfTps.length; i++) {
      if (arrayOfTps[i].hour < this.min) {
        this.min = arrayOfTps[i].hour
      }

      if (arrayOfTps[i].hour > this.max) {
        this.max = arrayOfTps[i].hour
      }
    }

    arrayOfTps.sort(function (a, b) {
      return a.hour - b.hour
    })

    for (let k = 0, j = this.min; j <= this.max; j++ , k++) {
      let index = -1
      var val = j
      index = arrayOfTps.findIndex(function (item, i) {
        return item.hour === val
      })

      if (index === -1) {
        this.campaignWiseTpsGraph2[k] = { "tps": 0, "hour": j }

      }
      else {
        this.campaignWiseTpsGraph2[k] = arrayOfTps[index]
      }
    }
    // print 
    for (let i = 0; i < this.campaignWiseTpsGraph2.length; i++) {
      console.log(this.campaignWiseTpsGraph2[i])
    }
    console.log("Array of tps..." + this.campaignWiseTpsGraph2);
    return this.campaignWiseTpsGraph2;
  }

  isuserWiseTrafficListNotAvail: boolean = false;

  getCampaignTrafficTiles() {
    this.campaignBulkTrafficList = "";
    this.requestParam = {
      "startDate": this.startDate,
      "endDate": this.endDate
    };
    this.spinner.show();
    this.dashboardService.getCampTrafficTiles(this.requestParam).subscribe(responseList => {
      this.spinner.hide();
      if (responseList['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.campaignBulkTrafficList = responseList['data'];
        console.log("campaignBulkTrafficList data response...." + JSON.stringify(this.campaignBulkTrafficList));
        if (this.campaignBulkTrafficList) {
          this.totalCampTile = this.campaignBulkTrafficList.map(a => a.totalSuccessCampaign);
          this.totalSMSCampTile = this.campaignBulkTrafficList.map(a => a.smsSent);
          this.successSMSCampTile = this.campaignBulkTrafficList.map(a => a.successSms);
          this.pendingSMSCampTile = this.campaignBulkTrafficList.map(a => a.pendingSms);
          this.failedSMSCampTile = this.campaignBulkTrafficList.map(a => a.failedSms);
        }
      } else {
        this.campaignBulkTrafficList = []
      }
    })
  }

  getAPITrafficTiles() {
    this.apiTrafficList = "";
    this.requestParam = {
      "startDate": this.startDate,
      "endDate": this.endDate
    };
    this.spinner.show();
    this.dashboardService.getAPITranfficTiles(this.requestParam).subscribe(responseList => {
      this.spinner.hide();
      if (responseList['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.apiTrafficList = responseList['data'];
        console.log("apiTrafficList data response...." + JSON.stringify(this.apiTrafficList));
        this.totalSMSTile = this.apiTrafficList.map(a => a.sentSms);
        this.successSMSTile = this.apiTrafficList.map(a => a.successSms);
        this.pendingSMSTile = this.apiTrafficList.map(a => a.pendingSms);
        this.failedSMSTile = this.apiTrafficList.map(a => a.failedSms);
      } else {
        this.apiTrafficList = [];
      }
    })
  }
  getUserWiseTraffic() {
    this.isuserWiseTrafficListNotAvail = false;
    this.userWiseTrafficList = [];
    this.requestParam = {
      "startDate": this.startDate,
      "endDate": this.endDate
    };
    this.spinner.show();
    this.dashboardService.getUserWiseTraffic(this.requestParam).subscribe(responseList => {
      this.spinner.hide();
      if (responseList['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.userWiseTrafficList = responseList["data"];
        if (this.userWiseTrafficList.length == 0) {
          this.isuserWiseTrafficListNotAvail = true;
        }
        console.log("userwiseTrafficDetails data response...." + JSON.stringify(this.userWiseTrafficList));
        console.log("selected Category...." + this.selectedCategory);
      } else {
        this.userWiseTrafficList = [];
      }
    })
  }
  isGeneralDataAvail: boolean = false;
  getGeneralData() {
    this.generalDataList.length = 0;
    this.isGeneralDataAvail = false;
    this.requestParam = {
      "startDate": this.startDate,
      "endDate": this.endDate
    };
    this.spinner.show();
    this.dashboardService.getGeneralData(this.requestParam).subscribe(responseList => {
      this.spinner.hide();
      if (responseList['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.isGeneralDataAvail = true;
        this.generalDataList = responseList["data"];
        console.log("generalData data response...." + JSON.stringify(this.generalDataList));
        this.pieChartData = [this.generalDataList['campaignSuccessPercent'], this.generalDataList['campaignFailurePercent']];
      } else {
        this.generalDataList = [];
      }
    })
  }
}
