import { Component, OnInit } from '@angular/core';
import { ManageSmsTemplateService } from '../manage-sms-template.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";
import { Router } from "@angular/router";


@Component({
  selector: 'app-create-sms-template',
  templateUrl: './create-sms-template.component.html',
  styleUrls: ['./create-sms-template.component.scss']
})
export class CreateSmsTemplateComponent implements OnInit {

  users = [];
  senderIdList = [];
  senderIdListSetting = {};
  selectedSenderId: any;
  uniqueTemp: any;
  smsTemplateForm: FormGroup;
  formSubmitted: boolean = false;
  duplicateCheck: any;
  loginUser: any;
  isEdit: boolean = false;
  math = Math;
  placeholder: boolean = false;
  userIDValue: any;
  errorMsgType: any;
  errorZero: boolean = false;
  errZeroMsg: any;
  showUser: any;
  messagePart: any;
  unicodeSupport: any;
  UnicodeError: boolean = false;
  unicodeFlag: boolean = false;
  maxlen: any;
  errorUnicode: any;
  errortext: any;
  departments = [];
  categoryList: Array<object> = [];
  categoryListForCampUser: Array<object> = [];
  maxLengthSMS: any;
  numberOfSms: any;
  contentTypeVal: any;

  constructor(private smsTemplateService: ManageSmsTemplateService, private fb: FormBuilder,
    private spinner: NgxSpinnerService, private router: Router, private commonService: CommonService) { }

  ngOnInit() {
    this.getUserList();
    this.getTemlateCategoryList();
    this.getSenderIdList();
    this.initialiseForms();
    this.checkUser();
    this.errorUnicode = false;
    this.checkUnicodeSupport();
    this.maxLengthSMS = 160;
  }

  getUserList() {
    this.spinner.show();
    let getUserFunction = 'getAllUser';
    if (this.commonService.getUserByRole() == 'ROLE_CAMPAIGN') {
      getUserFunction = 'getAllUserCamp';
    }
    this.smsTemplateService[getUserFunction]().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.users = res['data'];
        } else {
          this.users = [];
        }
      });
  }
  getTemlateCategoryList() {
    this.spinner.show();
    this.smsTemplateService.getCategoryList(this.commonService.getUserByRole()).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.categoryList = res['data'];
          if(this.commonService.getUserByRole() == 'ROLE_CAMPAIGN'){
            this.categoryListForCampUser = this.categoryList.filter(
              x => x["id"] == 3 || x["id"] == 4
            );
            this.categoryList = this.categoryListForCampUser;
          }

        } else {
          this.categoryList = [];
        }
      });
  }

  getSenderIdList() {
    this.spinner.show();
    let getSenderId = 'getSenderId';
    if (this.commonService.getUserByRole() == 'ROLE_CAMPAIGN') {
      getSenderId = 'getSenderIdCamp';
    }
    this.smsTemplateService[getSenderId]().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.senderIdList = res['data'];
        } else {
          this.senderIdList = [];
        }
      });
  }

  initialiseForms() {
    this.smsTemplateForm = this.fb.group({
      contentType: ['', [Validators.required]],
      name: ['', [Validators.required, Validators.pattern(this.commonService.patterns.templateName)]],
      language: [''],
      maskFlag: ['', [Validators.required]],
      maskText: [''],
      content: ['', [Validators.required]],
      user: ['', [Validators.required]],
      caseSensitivityFlag: ['', [Validators.required]],
      temlateCategory: ['', [Validators.required]],
      placeHolder: [''],
      senderId: ['', [Validators.required]]
    });
    this.senderIdListSetting = {
      singleSelection: false,
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      allowSearchFilter: true,
      closeDropDownOnSelection: false,
      idField: 'senderid',
      textField: 'senderid'
    };
  }

  maskFlagChanged(event: any) {
    if (event == 'Y') {
      this.smsTemplateForm.controls['maskText'].setValidators(Validators.required);
      this.smsTemplateForm.controls['maskText'].updateValueAndValidity();
    } else {
      this.smsTemplateForm.controls['maskText'].clearValidators();
      this.smsTemplateForm.controls['maskText'].updateValueAndValidity();
    }
  }

  checkUser() {
    this.loginUser = this.commonService.getUser();
    if (this.loginUser == '1000') {
      this.userIDValue = this.smsTemplateForm.controls["user"].value;
      this.showUser = true;
    } else {
      this.userIDValue = this.commonService.getUser();
      this.smsTemplateForm.controls['user'].setValue(this.userIDValue);
      this.showUser = false;
    }

  }
  checkUnicodeSupport() {
    this.unicodeSupport = this.commonService.getUnicodeSupport();
    if (this.unicodeSupport == 'Y') {
      this.unicodeFlag = true;
    } else {
      this.unicodeFlag = false;
    }
  }

  matches: any;
  isValidContent: boolean = true;

  duplicatecheck() {
    if (!this.smsTemplateForm.controls['name'].value) {
      return;
    }
    this.uniqueTemp = true;
    this.duplicateCheck = '';
    let req = this.smsTemplateForm.controls['name'].value;
    let dupcheck = 'dupchekApi';
    if (this.commonService.getUserByRole() == 'ROLE_CAMPAIGN') {
      dupcheck = 'dupchekCamp';
    }
    // this.spinner.show();
    this.smsTemplateService[dupcheck](req.trim()).subscribe(
      res => {
        this.spinner.hide();
        this.uniqueTemp = res['data'];
        if (this.uniqueTemp) {
          this.duplicateCheck = 'D';
        }
      });
  }
  submitForm() {
    this.checkUser();
    this.formSubmitted = true;
    //let tempDuplicatecheck = this.smsTemplateForm.controls['name'].value?this.duplicatecheck(this.smsTemplateForm.controls['name'].value):true;
    // console.log('duplicate check...'+tempDuplicatecheck);
    if (this.smsTemplateForm.controls['content'].value.indexOf('  ') > -1) {
      return;
    }
    if (this.uniqueTemp && this.duplicateCheck == 'D' && this.smsTemplateForm.valid && !this.UnicodeError && !this.errorZero) {
      let contentBackSlash = this.smsTemplateForm.controls["content"].value.trim();
      contentBackSlash = contentBackSlash.replace(/"/g, "\&quot;");
      contentBackSlash = contentBackSlash.replace(/>/g, "&gt;");
      contentBackSlash = contentBackSlash.replace(/</g, "&lt;");
      contentBackSlash = contentBackSlash.replace(/>=/g, "&ge;");
      contentBackSlash = contentBackSlash.replace(/<=/g, "&le;");
      contentBackSlash = contentBackSlash.replace(/'/g, "&#39;");

      if (this.smsTemplateForm.controls["maskText"].value) {
        var contentBackSlashMask = this.smsTemplateForm.controls["maskText"].value.trim();
        contentBackSlashMask = contentBackSlashMask.replace(/"/g, "\&quot;");
        contentBackSlashMask = contentBackSlashMask.replace(/>/g, "&gt;");
        contentBackSlashMask = contentBackSlashMask.replace(/</g, "&lt;");
        contentBackSlashMask = contentBackSlashMask.replace(/>=/g, "&ge;");
        contentBackSlashMask = contentBackSlashMask.replace(/<=/g, "&le;");
        contentBackSlashMask = contentBackSlashMask.replace(/'/g, "&#39;");
      }

      this.matches = contentBackSlash.match(/\\U|\\u|\\L|\\l|\\N/g);
      if (this.matches != null) {
        this.isValidContent = false
      } else {
        this.isValidContent = true;
      }
      if (this.isValidContent) {
        let tempalatetype = 'promo';
        if (this.smsTemplateForm.controls["temlateCategory"].value == 1) {
          tempalatetype = 'otp';
        } else if (this.smsTemplateForm.controls["temlateCategory"].value == 2) {
          tempalatetype = 'txn';
        } else if (this.smsTemplateForm.controls["temlateCategory"].value == 3) {
          tempalatetype = 'inf';
        }
        let templateContentCheck = this.validationCheck(contentBackSlash);

        if (!templateContentCheck['validtmplate']) {
          this.commonService.showErrorToast("Invalid dynamic value placeholder");
          return;
        }


        let senderIdlist = this.smsTemplateForm.controls["senderId"].value;
        let senderIds = ""
        senderIdlist.forEach(element => {
          senderIds = senderIds + "," + (element['senderid']);
        });
        let sendercli = senderIds.substring(1);
        console.log(sendercli);
        let reqObj = {
          "reqObj": {
            "language": this.smsTemplateForm.controls["contentType"].value,
            "maskFlag": this.smsTemplateForm.controls["maskFlag"].value,
            "maskText": contentBackSlashMask,
            "template": contentBackSlash,
            "caseSensitivityFlag": this.smsTemplateForm.controls["caseSensitivityFlag"].value,
            "templateName": this.smsTemplateForm.controls["name"].value.trim(),
            "userId": this.userIDValue,
            "templateType": tempalatetype.trim(),
            "placeholderCount": templateContentCheck['count'],
            "senderId": sendercli
          }
        }
        this.spinner.show();
        let addTemplateFunction = 'addTemplate';
        if (this.commonService.getUserByRole() == 'ROLE_CAMPAIGN') {
          addTemplateFunction = 'addTemplateCamp';
        }
        this.smsTemplateService[addTemplateFunction](reqObj).subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusCode'] == 200) {
              let user = 'HOD';
              if (!res['data']) {
                user = 'DLT user';
              }
              this.commonService.showSuccessToast(reqObj['reqObj']['templateName'] + ' template has been sent to ' + user + ' for approval');
              this.router.navigate(["/main/manage-sms-template/approved-sms-template"]);
              this.initialiseForms();
            } else {
              this.commonService.showErrorToast(res['result']['userMsg']);
            }
            this.formSubmitted = false;
          });
      }

    }
  }

  addPlaceholder() {
    this.placeholder = true;
  }

  appendPlaceholder() {
    let contentValue = this.smsTemplateForm.controls['content'].value;
    this.smsTemplateForm.controls['content'].setValue(contentValue + " <!" + this.smsTemplateForm.controls['placeHolder'].value + "!> ");
    this.smsTemplateForm.controls['placeHolder'].setValue("");
    this.placeholder = false;
  }

  maxLengthText(contents) {
    if (this.messagePart == 1) {
      this.errorZero = false;
      this.maxlen = 160;
      this.maxLengthSMS = 160;
    } else if (this.messagePart > 1) {
      this.errorZero = false;
      this.maxlen = 153 * this.messagePart;
      contents.length > 160 ? this.maxLengthSMS = 153 : this.maxLengthSMS = 160;
    } else {
      this.errorZero = true;
      this.errZeroMsg = "Message part length is not configured. Please contact your administrator.";
    }
  }

  maxLengthUnicode(contents) {
    if (this.messagePart == 1) {
      this.errorZero = false;
      this.maxlen = 70;
      this.maxLengthSMS = 70;
    } else if (this.messagePart > 1) {
      this.errorZero = false;
      this.maxlen = 67 * this.messagePart;
      contents.length > 70 ? this.maxLengthSMS = 67 : this.maxLengthSMS = 70;
    } else {
      this.errorZero = true;
      this.errZeroMsg = "Message part length is not configured. Please contact your administrator.";
    }
  }


  isDoubleByte() {
    this.messagePart = this.commonService.getMaxMessageParts();
    let contents = this.smsTemplateForm.controls['content'].value.trim();
    for (var i = 0, n = contents.length; i < n; i++) {
      if (contents.charCodeAt(i) > 255) {
        if (this.unicodeSupport == 'Y') {
          this.smsTemplateForm.controls['contentType'].setValue('2');
          this.UnicodeError = false;
          this.maxLengthUnicode(contents);
        } else {
          this.errorMsgType = "Enter Text, as Unicode is not supported for this user";
          this.UnicodeError = true;
        }
        break;
      } else {
        this.smsTemplateForm.controls['contentType'].setValue('1');
        this.UnicodeError = false;
        this.maxLengthText(contents);
      }
    }

  }
  validationCheck(contentBackSlash: any) {
    console.log("content...." + contentBackSlash);
    let dotstarCountval = contentBackSlash && contentBackSlash.match(/\.\*/g) ? contentBackSlash.match(/\.\*/g).length : 0;
    if (dotstarCountval >= 1) {
      this.commonService.showErrorToast("Template content with .* is not allowed");
      return { validtmplate: false, count: 0 };
    }

    let dotstarCount = contentBackSlash && contentBackSlash.match(/\.\{1,30}/g) ? contentBackSlash.match(/\.\{1,30}/g).length : 0;
    console.log(".* count..." + dotstarCount)

    let textContent = contentBackSlash;
    textContent = textContent.trim();
    textContent = textContent.replace(/\r?\n|\r/g, "");
    textContent = textContent.replace(/\s/g, '');
    if (textContent.indexOf(".\{") == 0 && textContent.length <= '7') {
      return { validtmplate: false, count: 0 };
    }
    let braceCheck = textContent.match(/{([^}]+)}/g);
    let correctValid = textContent.match(/\.\{1,30}/g);
    let only_brace = textContent.match(/\{/g);
    let back_brace = textContent.match(/\}/g);
    let only_bracelength = textContent.match(/\{/g)?textContent.match(/\{/g).length:0;
    let back_braceLength = textContent.match(/\}/g)?textContent.match(/\}/g).length:0;

    if (only_brace == null && back_brace == null) {
      return { validtmplate: true, count: 0 };
    } else if (only_bracelength != back_braceLength) {
      return { validtmplate: false, count: 0 };
    }

    if (only_brace) {
      if (correctValid && (only_brace.length != correctValid.length)) {
        return { validtmplate: false, count: 0 };
      } else if (!correctValid) {
        return { validtmplate: false, count: 0 };
      }
    } else if (braceCheck) {
      if (correctValid && correctValid.length != braceCheck.length) {
        return { validtmplate: false, count: 0 };
      } else if (!correctValid) {
        return { validtmplate: false, count: 0 };
      }
    }
    let dotcurlet = contentBackSlash && contentBackSlash.match(/\.\{/g) ? contentBackSlash.match(/\.\{/g).length : 0;
    console.log(".* count..." + dotcurlet)
    if (dotcurlet > 1) {
      let result = contentBackSlash.match(/{([^}]+)}/g)
      for (let i = 0; i < dotcurlet; i++) {
        let onethirtyCount = result[i].match(/1,30/g) ? result[i].match(/1,30/g).length : 0;
        if (onethirtyCount < 1) {
          return { validtmplate: false, count: 0 };
        }
      }
    } else if (dotcurlet == 1 && dotstarCount < 1) {
      return { validtmplate: false, count: 0 };
    }
    return { validtmplate: true, count: dotstarCount };
  }
}