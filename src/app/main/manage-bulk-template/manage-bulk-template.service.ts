import { Injectable } from "@angular/core";
import { NetworkService } from "../../common/network.service";
import { CommonService } from "../../common/common.service";

@Injectable()
export class ManageBulkTemplateService {
  NO_HEADER = null;
  constructor(
    private _networkService: NetworkService,
    private _commonService: CommonService
  ) {}


  getTemplateList(req: any) {
    if(req['roleName']=='ROLE_ADMIN'){
      return this._networkService.get('honcho/templatebulk/all/all', null, 'bearer');
    }else if(req['roleName']=='ROLE_HOD'){
      return this._networkService.get('provost/templatebulk/all/all', null, 'bearer');
    }else if(req['roleName']=='ROLE_CAMPAIGN'){
      return this._networkService.get('hype/templatebulk/all/all', null, 'bearer');
    }else if(req['roleName']=='ROLE_DLT'){
      return this._networkService.get('dlt/templatebulk/all/all', null, 'bearer');
    }
  }

getSpamList(){
return  this._networkService.get('hype/spamkeyword?' + 'showAllFlag=ENABLED', null, 'bearer');

}


getTemplateListPending(req: any){
    if(req['userRole']=='ROLE_DLT'){
      return this._networkService.get('dlt/templatebulk/all/10', null, 'bearer');
     }
      return this._networkService.get('provost/templatebulk/all/0' , null, 'bearer');
}

deleteTemplate(req: any) {
     return this._networkService.post('hype/templatebulk/delete/' + req['templateId'] + '?remarks=' + req['remarks'] , null, 'bearer');
}


updateSattus(req: any) {
  if(req['status']==7){
    return this._networkService.post('provost/templatebulk/approve/' + req["id"] + '?status=' + req["status"]+"&remarks="+req['remarks'] +"&dltId="+req['dltId'], null, null, 'bearer');
  }else if(req['status']==1) {
    return this._networkService.post('provost/templatebulk/approve/' + req["id"] + '?status=' + req["status"]+"&remarks="+req['remarks'] +"&dltId="+req['dltId'], null, null, 'bearer');
  }else {
    return this._networkService.post('provost/templatebulk/approve/' + req["id"] + '?status=' + req["status"]+"&remarks="+req['remarks'] +"&dltId="+req['dltId'], null, null, 'bearer');
  }
}
updateDltBUlkTemplate(req: any){
  return this._networkService.post('dlt/templatebulk/approve/' + req["id"] + '?status=' + req["status"]+"&remarks="+req['remarks'] +"&dltId="+req['dltId'], null, null, 'bearer');
}
updateHodTemplate(req: any){
  return this._networkService.post('provost/templatebulk/bulkApprove' , req, null, 'bearer');
}
updateDltTemplate(req: any){
  return this._networkService.post('dlt/templatebulk/bulkApprove' , req, null, 'bearer');
}

updateAPITemplate(req:any){
  return this._networkService.post('hype/templatebulk/update/' + req["templateID"],req , null, 'bearer');
}

getAllUser() {
    return this._networkService.get('hype/user?role=5', null, 'bearer');
}

getLanguage() {
    return this._networkService.get('master/language', null, 'bearer');
}

addTemplate(req: any) {
  debugger;
    return this._networkService.post('hype/templatebulk/add' ,
        req['reqObj'], null, 'bearer');
}

uploadFile(req: any) {
    return this._networkService.uploadFile('master/fileUpload', req, null, 'bearer');
}

saveBulkSmsTemplates(req: any) {
    return this._networkService.post('template/bulkUpload?userId=' + req['loginId'], req, null, 'bearer');
}
getCategoryList(){
       return this._networkService.get("hype/master/category",null,'bearer');
}
}
