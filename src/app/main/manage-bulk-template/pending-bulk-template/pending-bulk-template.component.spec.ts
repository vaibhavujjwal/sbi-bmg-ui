import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingBulkTemplateComponent } from './pending-bulk-template.component';

describe('PendingBulkTemplateComponent', () => {
  let component: PendingBulkTemplateComponent;
  let fixture: ComponentFixture<PendingBulkTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingBulkTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingBulkTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
