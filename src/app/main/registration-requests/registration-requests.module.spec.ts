import { RegistrationRequestsModule } from './registration-requests.module';

describe('RegistrationRequestsModule', () => {
  let registrationRequestsModule: RegistrationRequestsModule;

  beforeEach(() => {
    registrationRequestsModule = new RegistrationRequestsModule();
  });

  it('should create an instance', () => {
    expect(registrationRequestsModule).toBeTruthy();
  });
});
