import { Component, OnInit } from '@angular/core';
import { CommonService } from "src/app/common/common.service";
// import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ManageUserService } from "../../manage-user/manage-user.service";
import { RESPONSE } from "../../../common/common.const";
import { NgbModal, NgbModalRef, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { MyProfileService } from '../my-profile.service';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.scss'],
  providers: [ManageUserService, MyProfileService]
})
export class ViewProfileComponent implements OnInit {
  private modalRef: NgbModalRef;
  largeModalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false,
    size: "lg"
  };
  userDetails: any;
  viewPermissionList = [];
  accessPermissionViewList = [];
  closeResult = "";

  constructor(public commonService: CommonService, private spinner: NgxSpinnerService,
    private manageUserService: ManageUserService, private profileService: MyProfileService) { }
  ngOnInit() {
    this.viewProfile();
  }
  hod: any;
  finalApp: any;
  categories: any;
  appCategory: any;
  viewProfile() {
    this.spinner.show();
    this.profileService.getProfile().subscribe(res => {
      this.spinner.hide();
      this.userDetails = res["data"];
      this.hod = this.userDetails['hod'].substring(1, this.userDetails['hod'].length - 1);
      this.hod = this.hod.split(',');
      this.finalApp = this.userDetails['finalApprover'].substring(1, this.userDetails['finalApprover'].length - 1);
      this.finalApp = this.finalApp.split(',');
      this.categories = this.userDetails['categoryAppNameList'].map(a => a.categoryId).map(b => b.category);
      console.log("MY PROFILE..." + JSON.stringify(this.userDetails));
      this.viewPermissionList = JSON.parse(
        this.userDetails["permissionJson"]
      );
      if (this.accessPermissionViewList.length == 0) {
        this.accessPermissionViewList = this.manageUserService.getPermissionsViewList();
      }
    });
  }
}