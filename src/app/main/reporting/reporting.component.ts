import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common/common.service';
import { MODULE } from "../../common/common.const";
import { ReportingService } from "./reporting.service";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
// import { NgxSpinner } from '../../../../node_modules/ngx-spinner/lib/ngx-spinner.enum';

@Component({
  selector: 'app-reporting',
  templateUrl: './reporting.component.html',
  styleUrls: ['./reporting.component.scss']
})
export class ReportingComponent implements OnInit {

  MODULES = MODULE;

  constructor(public commonService: CommonService, public reportingService: ReportingService, public formBuilder: FormBuilder, public spinner: NgxSpinnerService) { }

  reportingForm: FormGroup;
  formSubmitted: boolean = false;
  ngOnInit() {
    if (this.commonService.getUserByRole() == 'ROLE_CRM')
      this.initializeForm();
  }

  initializeForm() {
    this.reportingForm = this.formBuilder.group({
      mobileNumber: ["", [Validators.required, Validators.maxLength(10), Validators.pattern(this.commonService.patterns.indianMobileNumberWithLength)]]
    })
  }

  sendSMS() {
    this.formSubmitted = true;
    if (this.reportingForm.valid) {
      this.spinner.show();
      let req = {
        "msisdn": "91" + this.reportingForm.controls.mobileNumber.value
      }
      this.reportingService.sendPushSMS(req).subscribe(res => {
        this.spinner.hide();
        this.commonService.showSuccessToast(res['result']['userMsg']);
      });
    }
  }

}
