import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonService } from '../../../common/common.service';
import { FormBuilder, FormGroup, Validators } from '../../../../../node_modules/@angular/forms';
import { NgbModal, NgbModalOptions, NgbModalRef, ModalDismissReasons } from '../../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { ManageUserService } from '../manage-user.service';
import { NgxSpinnerService } from '../../../../../node_modules/ngx-spinner';
import { Router } from '../../../../../node_modules/@angular/router';
import { RESPONSE, MODULE } from '../../../common/common.const';
import Swal from "sweetalert2";
import { DatatableComponent } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Result } from './result';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { ResultDataSourceService } from './result-data-source.service';
import { tap } from '../../../../../node_modules/rxjs/operators';
import { merge } from 'rxjs';

@Component({
  selector: 'app-new-view-users',
  templateUrl: './new-view-users.component.html',
  styleUrls: ['./new-view-users.component.scss'],
  providers: [ManageUserService]
})
export class NewViewUsersComponent implements OnInit {

  constructor(
    public commonService: CommonService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private manageUserService: ManageUserService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  // searchText: string = "";
  userNameSorted: boolean = false;
  roleSorted: boolean = false;
  statusDescSorted: boolean = false;
  firstNameSorted: boolean = false;
  lastNameSorted: boolean = false;
  emailSorted: boolean = false;
  mobileNumberSorted: boolean = false;
  departmentSorted: boolean = false;
  departmentHodSorted: boolean = false;
  finalApproverSorted: boolean = false;
  // applicationNameSorted: boolean = false;
   creatadBySorted: boolean = false;
   createdOnSorted: boolean = false;
  // total: number;
  MODULES = MODULE;
  private modalRef: NgbModalRef;
  largeModalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false,
    size: "lg"
  };
  modalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  userSearchType: number = 1;
  userDetails: object;
  accessPermissionViewList: Array<object> = [];
  viewPermissionList: object = {};
  viewSearchForm: FormGroup;
  statusFormSubmitted: boolean = false;
  deleteFormSubmitted: boolean = false;
  updateUserStatusEvent: boolean;
  statusForm: FormGroup;
  deleteForm: FormGroup;
  statusList: Array<object> = [];
  pageLimit: number;
  table: DatatableComponent;
  formSubmitted: boolean = false;
  isSearchHit: boolean = false;
  result: Result;
  dataSource: ResultDataSourceService;
  displayedColumns = ['actions', "userName", "statusDesc", "role", "firstName", "lastName", "email", "mobileNumber",
    "department", "hod", "finalApprover", "creatadBy", "createdOn"];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.initializeForm();
    this.RenderDataTable();
  }

  initializeForm() {
    this.viewSearchForm = this.fb.group({
      pageLimit: [this.commonService.recordsPerPage[0], [Validators.required]],
      userSearchType: ['1', [Validators.required]],
      searchBy: ['', [Validators.required]],
      textToSearch: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(70)]]
    });
  }

  RenderDataTable() {
    let req = {
      "type": this.viewSearchForm.controls['userSearchType'].value,
      "pageNo": 1,
      "pageSize": this.paginator.pageSize?this.paginator.pageSize:10
    }
    this.dataSource = new ResultDataSourceService(this.manageUserService, this.spinner);
    this.dataSource.loadUsers(req);
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
    console.log("SORTED DATA...."+JSON.stringify(this.dataSource.response));
    this.dataSource.response = this.commonService.sortArray(isAssending, sortBy, this.dataSource.response, isNumber);
    this.dataSource.lessonsSubject.next(this.dataSource.response);

  }

  get searchInfoForm() {
    let x = <FormGroup>this.viewSearchForm;
    return x.controls;
  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        tap(() => this.loadResultsPage())
      )
      .subscribe();
  }

  loadResultsPage() {
    let req = {
      "type": this.viewSearchForm.controls['userSearchType'].value,
      "pageNo": this.paginator.pageIndex + 1,
      "pageSize": this.paginator.pageSize
    }
    this.dataSource.loadUsers(req);
  }
  getSearchRecords() {
    this.isSearchHit = false;
    this.formSubmitted = true;
    let req = {
      "type": this.viewSearchForm.controls['userSearchType'].value,
      "pageNo": 1,
      "pageSize": this.paginator.pageSize,
      "searchBy": this.viewSearchForm.controls['searchBy'].value,
      "searchFor": this.viewSearchForm.controls['textToSearch'].value
    }
    if (this.viewSearchForm.valid) {
      this.dataSource = new ResultDataSourceService(this.manageUserService, this.spinner);
      this.dataSource.getUser(req);
      this.isSearchHit = true;
    }
  }

  userSearchTypeChanged() {
    this.formSubmitted = false;
    this.isSearchHit = false;
    this.viewSearchForm.controls['textToSearch'].setValue("");
    this.viewSearchForm.controls['searchBy'].setValue("");
    this.paginator.pageIndex = 0;
    this.RenderDataTable();
  }

  editLink(id) {
    this.router.navigate(["/main/manage-user/add-user?userId=" + id]);
  }

  prepareUpdateStatusForm() {
    this.statusForm = this.formBuilder.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)
        ]
      ]
    });
  }
  indexOfRow: string;

  viewUpdateStatusModal(user, content, event, id) {
    this.userDetails = user;
    this.indexOfRow = id;
    console.log("index of row...." + this.indexOfRow);
    this.statusFormSubmitted = false;
    this.prepareUpdateStatusForm();
    this.updateUserStatusEvent = event;
    //this.loadStatusList();
    this.modalRef = this.modalService.open(content, this.modalOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
        if (!this.closeResult.includes(undefined)) {
          document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
        }
      },
      reason => {
        if (reason == "Cross click")
          document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  loadStatusList() {
    if (this.statusList.length == 0) {
      this.spinner.show();
      this.manageUserService.getStatusList().subscribe(data => {
        if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
          let tempList = data["data"];
          tempList = tempList.filter(
            x => x["id"] != this.userDetails["status"]
          );
          tempList = tempList.filter(x => x["id"] != 0);
          tempList = tempList.filter(x => x["id"] != 4);
          this.statusList = tempList;
        }
        this.spinner.hide();
      });
    }
  }

  updateUserStatus(record) {
    this.statusFormSubmitted = true;
    if (this.statusForm.valid) {
      this.spinner.show();
      let finalObj = {
        loginId: 0,
        status: this.updateUserStatusEvent ? 1 : 2,
        remarks: this.statusForm.controls.remarks.value,
        userId: this.userDetails["userId"]
      };
      this.manageUserService
        .updateUserStatus(finalObj)
        .subscribe(data => {
          this.spinner.hide();
          console.log("response of update status...." + JSON.stringify(data));
          if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            this.updateUserStatusEvent ? this.commonService.showSuccessToast(this.userDetails['userName'] + " User Enabled") : this.commonService.showSuccessToast(this.userDetails['userName'] + " User Disabled");
            this.RenderDataTable();
          } else {
            if (this.updateUserStatusEvent) {
              this.updateUserStatusEvent = false;
              this.userDetails['status'] = 2;
              document.getElementById(this.indexOfRow)['checked'] = false;
            } else {
              this.updateUserStatusEvent = true;
              this.userDetails['status'] = 1;
              document.getElementById(this.indexOfRow)['checked'] = true;
            }
            this.commonService.showErrorToast(data['result']['userMsg']);
          }
          this.modalRef.close();
        }
          ,
          error => {
            if (this.updateUserStatusEvent) {
              this.updateUserStatusEvent = false;
              this.userDetails['status'] = 2;
              document.getElementById(this.indexOfRow)['checked'] = false;
              this.RenderDataTable();
            } else {
              this.updateUserStatusEvent = true;
              this.userDetails['status'] = 1;
              document.getElementById(this.indexOfRow)['checked'] = true;
              this.RenderDataTable();
            }
          });
    }
  }

  prepareDeleteUserForm() {
    this.deleteForm = this.formBuilder.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)
        ]
      ]
    });
  }

  viewDeleteUserModal(user, content, btn) {
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();

    this.prepareDeleteUserForm();
    this.deleteFormSubmitted = false;
    this.userDetails = user;
    this.modalRef = this.modalService.open(content, this.modalOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  deleteUser() {
    this.deleteFormSubmitted = true;

    if (this.deleteForm.valid) {
      Swal.fire({
        title: "Are you sure?",
        text: "Do you really want to delete this user?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ok, Proceed"
      }).then(result => {
        if (result.value) {
          this.spinner.show();
          let userId = this.userDetails["userId"];
          let remarks = this.deleteForm.controls.remarks.value.trim();
          let loginId = 0;
          this.manageUserService
            .deleteUser(userId, loginId, remarks)
            .subscribe(data => {
              this.spinner.hide();
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.commonService.showSuccessToast(this.userDetails["userName"] + ' user  has been deleted');
                this.modalRef.close();
                this.RenderDataTable();
              } else {
                this.commonService.showErrorToast(data['result']['userMsg']);
              }
            });
        }
      });
    }
  }

  viewUser(userId, content) {
    this.spinner.show();
    this.manageUserService.getUser(userId).subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        this.userDetails = data["data"];
        console.log("user data....."+JSON.stringify(this.userDetails));
this.userDetails['hod']= this.userDetails['hod'].substring(1, this.userDetails['hod'].length - 1);
this.userDetails['finalApprover']= this.userDetails['finalApprover'].substring(1, this.userDetails['finalApprover'].length - 1);

        this.viewPermissionList = JSON.parse(
          this.userDetails["permissionJson"]
        );
        if (this.userDetails["userDlrCallback"] && this.userDetails["userDlrCallback"]["retryFlag"] == 'Y')
          this.userDetails["userDlrCallback"]['retryDetails'] = this.userDetails["userDlrCallback"]['retryDetails'].split(',');

        if (this.accessPermissionViewList.length == 0)
          this.accessPermissionViewList = this.manageUserService.getPermissionsViewList();

        this.spinner.hide();
        this.modalRef = this.modalService.open(content, this.largeModalOptions);
        this.modalRef.result.then(
          result => {
            this.closeResult = `Closed with: ${result}`;
          },
          reason => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          }
        );
      } else {
        this.commonService.showErrorToast(data['result']['userMsg']);
      }
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
