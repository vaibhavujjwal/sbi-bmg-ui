import { Injectable } from '@angular/core';
import { NetworkService } from "../../common/network.service";

@Injectable()
export class ManageSpamKeywordService {
    constructor(private _networkService: NetworkService) { }
    createSpamKeywords(req: any) {
         return this._networkService.post('honcho/spamkeyword', req, null, 'bearer');
        // return this._networkService.post('spamkeyword', req, null, 'bearer');
    }
    deleteSpamKeywords(req: any) {
        return this._networkService.post('honcho/delete/spamkeyword/' + req['keywordId']+ '?remarks=' + req['remarks'], null, 'bearer');
    }
    getSpamKeywords(req: any) {
        return this._networkService.get('honcho/spamkeyword?'+'showAllFlag=all', null, 'bearer');
    }
    updateSpamKeyword(req: any) {
      return this._networkService.post('honcho/spamkeyword/'+req.keywordId,req, null, 'bearer');

    }
    updateStatus(req: any){
      return this._networkService.post('honcho/spamkeyword/'+req.keywordId+'/status/'+req.statusId, null, 'bearer');

    }
    getsensitiveFlag(req: any){
      return this._networkService.get('honcho/spamkeyword/validate/keyword/'+req+'?id=' + '0', null, 'bearer');
     }

}
