

export interface Result {
    userId: number,
    userName: string,
    firstName: string,
    lastName: string,
    email: string,
    mobileNumber: string,
    status: number,
    statusDesc: string,
    departmentId: number,
    department: string,
    roleId: number,
    role: string,
    internationalRouteFlag: string,
    creatadBy: string,
    createdOn: string,
    hod: string,
    finalApprover: string,
    showMsg: string
}