import { Component, OnInit, ViewChild } from '@angular/core';
import { ManageSmsTemplateService } from "../manage-sms-template.service";
import { NgxSpinnerService } from "ngx-spinner";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { CommonService } from "../../../common/common.service";
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { RESPONSE, MODULE } from "../../../common/common.const";
import { ExcelService } from "../../../common/excel.service";
import { TitleCasePipe } from '@angular/common';
import { first } from 'rxjs/operators';


export interface templateListTypes {
  Actions: any;
  templateName: string;
  language: any;
  template: any;
  caseSensitivityFlag: any;
  maskFlag: any;
  maskText: any;
  username: string;
  creationDate: any;
  approvedBy: any;
  status: any;
}

@Component({
  selector: 'app-approved-sms-template',
  templateUrl: './approved-sms-template.component.html',
  styleUrls: ['./approved-sms-template.component.scss']
})

export class ApprovedSmsTemplateComponent implements OnInit {

  MODULES = MODULE;
  modalRef: any;
  templateList = [];
  senderIdList = [];
  selectedSenderId:Array<object> = [];
  formSubmitted: boolean = false;
  math = Math;
  deleteReq = {};
  templateID: any;
  recordName: any;
  maxLengthSMS: any;
  statusForm: FormGroup;
  statusShow: any;
  smsTemplateForm: FormGroup;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  pageLimit: number;
  senderIdListSetting = {};
  searchText: string = "";
  roleName:string=this.commonService.getUserByRole();
  p: number = 1;
  errorMsgType: any;
  messagePart: any;
  errorZero: boolean = false;
  errZeroMsg: any;
  errortext: any;
  errorUnicode: any;
  contentTypeVal: any;
  unicodeSupport: any;
  UnicodeError: boolean = false;
  showError:boolean=false;
  unicodeFlag: boolean = false;
  maxlen: any;
  categoryList: Array<object> = [];
  displayedColumns: string[] = ['actions',
  'spiceTemplateid',
  'templateIdDlt',
  'templateName',
  'senderId',
  'templateType',
  'language',
  'template',
  'caseSensitivityFlag',
  'maskFlag',
  'maskText',
  'username',
  'creationDate',
  'approvedBy',
  'status'];
  displayedColumns1: string[] = [
  'spiceTemplateid', 
  'templateIdDlt',
  'templateName',  
  'senderId',
  'templateType',
  'language',
  'template',
  'caseSensitivityFlag',
  'maskFlag',
  'maskText',
  'username',
  'creationDate',
  'approvedBy',
  'status'];

  dataSource: MatTableDataSource<templateListTypes>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private manageSmsTemplateService: ManageSmsTemplateService, private spinner: NgxSpinnerService,
    private modalService: NgbModal, private fb: FormBuilder, private formBuilder: FormBuilder,
    public commonService: CommonService, private excelService: ExcelService, private titlecasePipe:TitleCasePipe) { }

  ngOnInit() {
    this.getTemplateList();
    if(this.commonService.getUserByRole()=='ROLE_API' || 
    this.commonService.getUserByRole()=='ROLE_CAMPAIGN'){
      this.getSenderIdList();
    }
    this.checkUnicodeSupport();
    this.initialiseForms();
    this.maxLengthSMS = 160;
    this.pageLimit = this.commonService.recordsPerPage[0];
  }

  getTemlateCategoryList(){
    this.spinner.show();
    this.manageSmsTemplateService.getCategoryList(this.commonService.getUserByRole()).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.categoryList = res['data'];
        } else {
          this.categoryList = [];
        }
      });
  }

  initialiseForms() {
    this.smsTemplateForm = this.fb.group({
      contentType: ['', [Validators.required]],
      name: ['', [Validators.required, Validators.pattern(this.commonService.patterns.templateName)]],
      language: [''],
      maskFlag: ['', [Validators.required]],
      maskText: [''],
      content: ['', [Validators.required]],
      user: [''],
      caseSensitivityFlag: ['', [Validators.required]],
      placeHolder: [''],
      temlateCategory:['', [Validators.required]],
      senderId:['', [Validators.required]]
    });
    this.senderIdListSetting = {
      singleSelection: false,
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      allowSearchFilter: true,
      closeDropDownOnSelection: false,
      idField: 'senderid',
      textField: 'senderid'
    };
  }

  getTemplateList() {
    let req = {
          "id": 0,
          "channelId": this.commonService.getChannelId(),
          "roleName": this.commonService.getUserByRole()
    }
    this.spinner.show();
    this.manageSmsTemplateService.getTemplateList(req).subscribe(
    res => {
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.templateList = res["data"];
          this.templateList.forEach(element => {
            if (element['language'] == '2') {
              if (element['maskText'] && element['maskText'] != "") {
                try {
                  element['maskText'] = this.commonService.unicodeToText(element['maskText']);
                }
                catch (e) {
                  console.log("comes in catch block...");
                }
              }
              try {
                element['template'] = this.commonService.unicodeToText(element['template']);
              }
              catch (e) {
                console.log("comes in catch block...");
              }
            }
          });
          this.templateList.forEach(element => {
            if (element['status'] == 1) {
              element['status'] = 'Enabled';
            } else if (element['status'] == 2) {
              element['status'] = 'Disabled';
            } else if (element['status'] == 0) {
              element['status'] = 'HOD Approval pending';
            } else if (element['status'] == 7) {
              element['status'] = 'Rejected';
            }else if(element['status'] == 10){
              element['status'] = "DLT Approval pending";
            }
          });

          this.templateList.forEach(element => {
            if (!element['templateIdDlt'] || element['templateIdDlt']==0) {
              element['templateIdDlt'] = 'NA';
            }
          });

          this.templateList.forEach(element => {
            if (!element['senderId']) {
              element['senderId'] = 'NA';
            }
          });

          this.templateList.forEach(element => {
            if (element['templateType']=='inf') {
              element['templateType'] = 'Informational';
            }else if (element['templateType']=='otp') {
              element['templateType'] = 'OTP';
            }else if (element['templateType']=='promo') {
              element['templateType'] = 'Promotional';
            }else if (element['templateType']=='txn') {
              element['templateType'] = 'Transactional';
            }
          });

          this.templateList.forEach(element => {
            element['language'] == 1 ? element['language'] = "Text" : element['language'] = "Unicode";
            element['caseSensitivityFlag'] == 'Y' ? element['caseSensitivityFlag'] = "Yes" : element['caseSensitivityFlag'] = "No";
            element['maskFlag'] == 'Y' ? element['maskFlag'] = "Yes" : element['maskFlag'] = "No";
          });
          this.dataSource= new MatTableDataSource(this.templateList) ;
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.templateList = [];
        }
        this.spinner.hide();
      });
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.showError=false;
    if(this.dataSource.filteredData.length==0){
      this.showError=true;
    }
  }


  checkUnicodeSupport() {
    this.unicodeSupport = this.commonService.getUnicodeSupport();
    this.unicodeSupport == 'Y' ? this.unicodeFlag = true : this.unicodeFlag = false;
  }

  confirmDeleteRecord(updatemodal: any, record: any) {
    this.formSubmitted = false;
    this.recordName = record.templateName;
    this.prepareApprovalForm();
    this.deleteReq = {
      "templateId": record['id'],
    }
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  prepareApprovalForm() {
    this.statusForm = this.formBuilder.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)

        ]
      ]
    });
  }
  getSenderIdList(){
    this.spinner.show();
    let getSenderId = 'getSenderId';
    if(this.commonService.getUserByRole()=='ROLE_CAMPAIGN'){
      getSenderId = 'getSenderIdCamp';
    }else if(this.commonService.getUserByRole()=='ROLE_DLT'){
      getSenderId = "getSenderIdDlt";
    }
    this.manageSmsTemplateService[getSenderId]().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.senderIdList = res['data'];
          } else {
          this.senderIdList = [];
        }
      });
  }
  updateRecord(updatemodal: any, record: any) {
    if(this.commonService.getUnicodeSupport() == 'N' && record['language'].toLowerCase()!='text'){
      this.commonService.showErrorToast("Your are not allow to edit the Template with Unicode content");
      return;
    }
    this.selectedSenderId = [];
    this.getTemlateCategoryList();
    this.populateValues(record);
   // this.getSenderIdList();
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  maskFlagChanged(event: any) {
    if (event == 'Y') {
      this.smsTemplateForm.controls['maskText'].setValidators(Validators.required);
      this.smsTemplateForm.controls['maskText'].updateValueAndValidity();
    } else {
      this.smsTemplateForm.controls['maskText'].clearValidators();
      this.smsTemplateForm.controls['maskText'].updateValueAndValidity();
    }
  }

  populateValues(item) {
    if (item.language == 'Text') {
      this.smsTemplateForm.controls['contentType'].setValue('1');
    } else {
      this.smsTemplateForm.controls['contentType'].setValue('2');
    }
    this.smsTemplateForm.controls['name'].setValue(item.templateName);
    if (item.maskFlag == 'Yes') {
      this.smsTemplateForm.controls['maskFlag'].setValue('Y');
    } else if (item.maskFlag == 'No') {
      this.smsTemplateForm.controls['maskFlag'].setValue('N');
    }
    this.smsTemplateForm.controls['maskText'].setValue(item.maskText);
    this.smsTemplateForm.controls['content'].setValue(item.template);
    if (item.caseSensitivityFlag == 'Yes') {
      this.smsTemplateForm.controls['caseSensitivityFlag'].setValue('Y');
    } else if (item.caseSensitivityFlag == 'No') {
      this.smsTemplateForm.controls['caseSensitivityFlag'].setValue('N');
    }
    let sender = item['senderId'].split(',');
    let selectedSenderIDs = [];
    for(var x =0;x<sender.length;x++){
      selectedSenderIDs = this.senderIdList.filter((senderIdList) => senderIdList['senderid'] ==sender[x]);
      this.selectedSenderId.push(selectedSenderIDs.length>=1?{'senderid':selectedSenderIDs[0].senderid}:{});
      //this.selectedSenderId.push(selectedSenderIDs[0]);
    }
    if(selectedSenderIDs.length<1){
      this.selectedSenderId=[];
    }
    this.smsTemplateForm.controls['senderId'].setValue(this.selectedSenderId);
    
    let categoryprefile = item.templateType;
    if(item.templateType!="OTP"){
      categoryprefile = this.titlecasePipe.transform(item.templateType)?
      this.titlecasePipe.transform(item.templateType):'';
    }
    this.smsTemplateForm.controls['temlateCategory'].setValue(categoryprefile);
    this.templateID = item.id;
    this.isDoubleByte();
  }

  confirmStatusRecord(record: any, id) {
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    if (record['status'] == 'Disabled') {
      modalRef.componentInstance.content = 'Are you sure you want to enable ' + record['templateName'] + ' Template?';
    } else if (record['status'] == 'Enabled') {
      modalRef.componentInstance.content = 'Are you sure you want to disable ' + record['templateName'] + ' Template?';
    }
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.changeStatus(record);
        } else {
          document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
        }
      },
      (reason: any) => {
        document.getElementById(id)['checked'] = !document.getElementById(id)['checked'];
      });
  }

  confirmUpdateRecoed() {
    this.formSubmitted = true;
    if (this.smsTemplateForm.valid && !this.UnicodeError && !this.errorZero) {
      this.checkUnicodeSupport();
      let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
      modalRef.componentInstance.content = 'Are you sure you want to update this template?';
      modalRef.componentInstance.leftButton = 'No';
      modalRef.componentInstance.rightButton = 'Yes';
      modalRef.result.then(
        (data: any) => {
          if (data == "yes") {
            this.updateApiTemplateForm();
          }
        },
        (reason: any) => {
        });
      this.formSubmitted = false;
    }
  }


  maxLengthText(contents) {
    if (this.messagePart == 1) {
      this.errorZero = false;
      this.maxlen = 160;
      this.maxLengthSMS = 160;
    } else if (this.messagePart > 1) {
      this.errorZero = false;
      this.maxlen = 153 * this.messagePart;
      contents.length > 160 ? this.maxLengthSMS = 153 : this.maxLengthSMS = 160;
    } else {
      this.errorZero = true;
      this.errZeroMsg = "Message part length is not configured. Please contact your administrator.";
    }
  }

  maxLengthUnicode(contents) {
    if (this.messagePart == 1) {
      this.errorZero = false;
      this.maxlen = 70;
      this.maxLengthSMS = 70;
    } else if (this.messagePart > 1) {
      this.errorZero = false;
      this.maxlen = 67 * this.messagePart;
      contents.length > 70 ? this.maxLengthSMS = 67 : this.maxLengthSMS = 70;
    } else {
      this.errorZero = true;
      this.errZeroMsg = "Message part length is not configured. Please contact your administrator.";
    }
  }

  isDoubleByte() {
    this.messagePart = this.commonService.getMaxMessageParts();
    let contents = this.smsTemplateForm.controls['content'].value.trim();
    for (var i = 0, n = contents.length; i < n; i++) {
      if (contents.charCodeAt(i) > 255) {
        if (this.unicodeSupport == 'Y') {
          this.smsTemplateForm.controls['contentType'].setValue('2');
          this.UnicodeError = false;
          this.maxLengthUnicode(contents);
        } else {
          this.errorMsgType = "Enter Text, as Unicode is not supported for this user";
          this.UnicodeError = true;
        }
        break;
      } else {
        this.smsTemplateForm.controls['contentType'].setValue('1');
        this.UnicodeError = false;
        this.maxLengthText(contents);
      }
    }
  }

  matches: any;
  isValidContent: boolean = true;

  updateApiTemplateForm() {
    let contentBackSlash = this.smsTemplateForm.controls["content"].value.trim();
    contentBackSlash = contentBackSlash.replace(/"/g, "\&quot;");
    contentBackSlash = contentBackSlash.replace(/>/g, "&gt;");
    contentBackSlash = contentBackSlash.replace(/</g, "&lt;");
    contentBackSlash = contentBackSlash.replace(/>=/g, "&ge;");
    contentBackSlash = contentBackSlash.replace(/<=/g, "&le;");
    contentBackSlash = contentBackSlash.replace(/'/g, "&#39;");
    if (this.smsTemplateForm.controls["maskText"].value) {
          var contentBackSlashMask = this.smsTemplateForm.controls["maskText"].value.trim();
          contentBackSlashMask = contentBackSlashMask.replace(/"/g, "\&quot;");
          contentBackSlashMask = contentBackSlashMask.replace(/>/g, "&gt;");
          contentBackSlashMask = contentBackSlashMask.replace(/</g, "&lt;");
          contentBackSlashMask = contentBackSlashMask.replace(/>=/g, "&ge;");
          contentBackSlashMask = contentBackSlashMask.replace(/<=/g, "&le;");
          contentBackSlashMask = contentBackSlashMask.replace(/'/g, "&#39;");
    }
    this.matches = contentBackSlash.match(/\\U|\\u|\\L|\\l|\\N/g);
    if (this.matches != null) {
      this.isValidContent = false
    } else {
      this.isValidContent = true;
    }
    let dotstarCount = contentBackSlash && contentBackSlash.match(/\.\*/g)?contentBackSlash.match(/\.\*/g).length:'';
    console.log(".* count..."+dotstarCount)
    let senderIdlist = this.smsTemplateForm.controls["senderId"].value;
    let senderIds = ""
    senderIdlist.forEach(element => {
      senderIds = senderIds+","+(element['senderid']);
    });
    let sendercli = senderIds.substring(1);
    console.log(sendercli);
    if (this.isValidContent) {
      let reqObj = {
        "smsContentType": this.smsTemplateForm.controls["contentType"].value,
        "templateName": this.smsTemplateForm.controls["name"].value.trim(),
        "maskFlag": this.smsTemplateForm.controls["maskFlag"].value,
        "maskText": contentBackSlashMask,
        "templateContent": contentBackSlash,
        "caseSensitivityFlag": this.smsTemplateForm.controls["caseSensitivityFlag"].value,
        "templateID": this.templateID,
        "templateType": this.smsTemplateForm.controls["temlateCategory"].value,
        "placeholderCount":dotstarCount,
        "senderId":sendercli
      }
      this.spinner.show();
      let update = "updateAPITemplate";
      if(this.roleName=='ROLE_CAMPAIGN'){
        update = "updateAPITemplateCamp";
      }
      this.manageSmsTemplateService[update](reqObj).subscribe(
        res => {
          this.commonService.showSuccessToast('Your template has been updated successfully');
          this.formSubmitted = false;
          this.modalRef.close();
          this.getTemplateList();
        });
      this.formSubmitted = false;
    }
  }

  deleteRecord() {
    this.deleteReq['remarks'] = this.statusForm.controls['remarks'].value;
    this.formSubmitted = true;
    if (this.statusForm.valid) {
      let deletefunction = 'deleteTemplate';
      if(this.roleName=='ROLE_CAMPAIGN'){
         deletefunction = 'deleteTemplateCamp';
      }

      this.spinner.show();
      this.manageSmsTemplateService[deletefunction](this.deleteReq).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast(this.recordName + ' template has been deleted');
            this.modalRef.close();
            this.getTemplateList();
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
          this.formSubmitted = false;
        });
    }
  }


  changeStatus(record: any) {
    let req = {
      "id": record['id'],
      "approvedBy": record['approvedBy'],
      "status": record['status'] == 'Enabled' ? 2 : 1
    };
    this.spinner.show();
    let updatestatus = "updateSattus";
    if(this.roleName=='ROLE_DLT'){
      updatestatus = "updateSattusDlt";
    }
    this.manageSmsTemplateService[updatestatus](req).subscribe(
      res => {
        this.spinner.hide();
        if (req['status'] == 1) {
          this.commonService.showSuccessToast(record['templateName'] + ' template enabled');
        } else {
          this.commonService.showSuccessToast(record['templateName'] + ' template disabled');
        }
        this.getTemplateList();
      });

  }


  exportAs(fileType: string) {
    let data = [];
    this.templateList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Template Name"] = element.templateName;
      excelDataObject["Template Content Type"] = element.templateContentType;
      excelDataObject["Template  Text"] = element.template;
      excelDataObject["Assigned To"] = element.username;
      excelDataObject["Created By"] = element.createdBy;
      excelDataObject["Approved By"] = element.approvedBy;
      excelDataObject["Status"] = element.status == 'Enabled' ? 'Enable' : 'Disable';
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'SMS-Template-List');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'SMS-Template-List');
        break;
      default:
        this.excelService.exportAsExcelFile(data, 'SMS-Template-List');
        break;
    }
  }
}
