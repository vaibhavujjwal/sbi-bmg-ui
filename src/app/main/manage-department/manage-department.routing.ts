import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ManageDepartmentComponent } from "./manage-department.component";
import { AddDepartmentComponent } from "./add-department/add-department.component";
import { ViewDepartmentComponent } from './view-department/view-department.component';
import { MODULE } from "src/app/common/common.const";

export const routes: Routes = [
  {
    path: "",
    component: ManageDepartmentComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/manage-department/view-department",
        pathMatch: "full"
      },
      {
        path: "view-department",
        component: ViewDepartmentComponent,
        data: { moduleName: MODULE.DEPARTMENT, permissions: ["R", "RW"] }
      },
      {
        path: "add-department",
        component: AddDepartmentComponent,
        data: { moduleName: MODULE.DEPARTMENT, permissions: ["RW"] }
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
