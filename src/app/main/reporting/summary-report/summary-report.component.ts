import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { ReportingService } from "../reporting.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ExcelService } from "../../../common/excel.service";
import { CommonService } from "../../../common/common.service";
import * as moment from 'moment';
import { RESPONSE, USER_ROLE } from "../../../common/common.const";
import { MatTableDataSource, MatPaginator, MatSort } from '../../../../../node_modules/@angular/material';
import { SummaryResponse } from './summart-response';

@Component({
  selector: 'app-summary-report',
  templateUrl: './summary-report.component.html',
  styleUrls: ['./summary-report.component.scss']
})
export class SummaryReportComponent implements OnInit {

  USER_ROLES = USER_ROLE;
  reportingForm: FormGroup;
  formSubmitted: boolean = false;
  searchText: string = "";
  channels = [];
  aggregators = [];
  departments = [];
  reportList = [];
  pdfResponse: string;
  pageLimit: number;
  // p: number = 1;
  groupBy: string;
  // dateSorted: boolean = false;
  // senderIdSorted: boolean = false;
  // departmentSorted: boolean = false;
  // aggregatorSorted: boolean = false;
  // usernameSorted: boolean = false;
  // channelSorted: boolean = false;
  // campNameSorted: boolean = false;
  // prioritySorted: boolean = false;
  // campaignIdSorted: boolean = false;
  // totalCountSorted: boolean = false;
  // submittedCountSorted: boolean = false;
  // deliveredCountSorted: boolean = false;
  // countRetriedSorted: boolean = false;
  // failedCountSorted: boolean = false;
  // expiredCountSorted: boolean = false;
  // submitFailCountSorted: boolean = false;
  // blockedCountSorted: boolean = false;
  // otherCountSorted: boolean = false;
  // invalidCountSorted: boolean = false;
  // dndCountSorted: boolean = false;
  // countSubmittedSorted: boolean = false;
  // countPendingSorted: boolean = false;
  sDate: any;
  then: any;
  endDte: any;
  edate: any;
  eDate: any;
  now = moment(new Date());
  minDate: any;
  maxDate = this.now.format('YYYY-MM-DD');
  maxDate1 = this.now.format('YYYY-MM-DD');
  formattedFromDate: any;
  formattedToDate: any;
  deptSettings = {};
  channelSetting = {};
  aggregatorSetting = {};
  deptList = [];
  error: any = { isError: false, errorMessage: '' };

  dataSource: MatTableDataSource<SummaryResponse>;
  dataSourceLength: number;
  @ViewChild('input') input: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /*displayedColumns: string[] = ['date', 'channel', 'campaignId', 'campaignName',
    'senderId', 'categoryId', 'countTotal', 'countSubmitted', 'countPending', 'countDelivered', 'countRetried', 'countFailed',
    'countExpired', 'countDnd'];*/

    displayedColumns: string[] = ['date', 'channel', 'campaignId', 'campaignName',
    'senderId', 'categoryId','totalCount', 'submitted', 'delivered', 'deliveryFailed',
    'submitFailed', 'blocked','countDnd','totalFailed','countPending'];

  constructor(private spinner: NgxSpinnerService, private reportingService: ReportingService,
    private fb: FormBuilder, private excelService: ExcelService, public commonService: CommonService) { }

  ngOnInit() {
    if (this.commonService.getUserByRole() == 'ROLE_ADMIN') {

      this.displayedColumns = ['date', 'channel', 'campaignId', 'campaignName',
    'department', 'username', 'senderId','aggregator','totalCount', 'submitted', 'delivered', 'deliveryFailed',
    'submitFailed', 'blocked','countDnd','totalFailed','countPending'];
      /*this.displayedColumns[4] = 'department';
      this.displayedColumns[5] = 'username';
      this.displayedColumns[6] = 'senderId';
      this.displayedColumns[7] = 'aggregator';*/
    }
    if (this.commonService.getUserByRole() == 'ROLE_HOD') {
      this.displayedColumns[4] = 'username';
    }
    if (this.commonService.getUserByRole() == 'ROLE_CAMPAIGN') {
      this.displayedColumns[4] = 'aggregator';
    }

    this.initializeForm();
    this.pageLimit = this.commonService.recordsPerPage[0];
    if (this.commonService.getUserByRole() == "ROLE_ADMIN" || this.commonService.getUserByRole() == "ROLE_HOD") {
      this.getData();
    } else {
      this.submitForm();

    }
    this.setEndDAte();
  }

  restrictKeyword(event: Event) {
    event.preventDefault();
  }

  getData() {
    this.spinner.show();
    this.reportingService.getData().subscribe(responseList => {
      this.spinner.hide();
      if (responseList[0]['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.channels = responseList[0]["data"];
        this.channelSetting = {
          singleSelection: false,
          idField: 'channelName',
          textField: 'channelName',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          itemsShowLimit: 3,
          allowSearchFilter: true,
          defaultOpen: false
        };
      } else {
        this.channels = [];
      }
      if (responseList[1]['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.departments = responseList[1]["data"];
        this.departments.forEach(element => {
          this.deptList.push(element['department']);
        });
        this.deptSettings = {
          singleSelection: false,
          idField: 'department',
          textField: 'department',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          itemsShowLimit: 3,
          allowSearchFilter: true,
          defaultOpen: false
        };
      } else {
        this.departments = [];
      }
      if (responseList.length > 2 && responseList[2]['result']['statusDesc'] == RESPONSE.SUCCESS) {
        this.aggregators = responseList[2]["data"];
        this.aggregatorSetting = {
          singleSelection: false,
          idField: 'aggregatorName',
          textField: 'aggregatorName',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          itemsShowLimit: 3,
          allowSearchFilter: true,
          defaultOpen: false
        };
      } else {
        this.aggregators = [];
      }

      if (responseList[0]['result']['statusDesc'] != RESPONSE.SUCCESS && responseList[1]['result']['statusDesc'] != RESPONSE.SUCCESS) {
        if (responseList.length > 2 && responseList[2]['result']['statusDesc'] != RESPONSE.SUCCESS) {
          this.commonService.showErrorToast(responseList[0]['result']['userMsg'] + ", " + responseList[1]['result']['userMsg'] + " and " +
            responseList[2]['result']['userMsg']);
        } else {
          this.commonService.showErrorToast(responseList[0]['result']['userMsg'] + " and " + responseList[1]['result']['userMsg']);
        }
      } else if (responseList[0]['result']['statusDesc'] != RESPONSE.SUCCESS) {
        if (responseList.length > 2 && responseList[2]['result']['statusDesc'] != RESPONSE.SUCCESS) {
          this.commonService.showErrorToast(responseList[0]['result']['userMsg'] + " and " + responseList[2]['result']['userMsg']);
        } else {
          this.commonService.showErrorToast(responseList[0]['result']['userMsg']);
        }
      } else if (responseList[1]['result']['statusDesc'] != RESPONSE.SUCCESS) {
        if (responseList.length > 2 && responseList[2]['result']['statusDesc'] != RESPONSE.SUCCESS) {
          this.commonService.showErrorToast(responseList[1]['result']['userMsg'] + " and " + responseList[2]['result']['userMsg']);
        } else {
          this.commonService.showErrorToast(responseList[1]['result']['userMsg']);
        }
      }
      if (responseList[0]['result']['statusDesc'] == RESPONSE.SUCCESS && responseList[1]['result']['statusDesc'] == RESPONSE.SUCCESS) {
        if (this.commonService.getUserByRole() == 'ROLE_ADMIN') {
          if (responseList.length > 2 && responseList[2]['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.submitForm();
          }
        } else {
          this.submitForm();
        }
      }
    });
  }

  initializeForm() {
    this.reportingForm = this.fb.group({
      startDate: [new Date().toISOString().substr(0, 10)],
      endDate: [new Date().toISOString().substr(0, 10)],
      channel: [],
      department: [''],
      aggregator: [''],
      campaign: ['', [Validators.pattern(this.commonService.patterns.numberOnly)]],
      sender: ['', [Validators.minLength(4), Validators.maxLength(16), Validators.pattern(this.commonService.patterns.cliPatternForSingle)]],
      user: ['', [Validators.minLength(5), Validators.maxLength(20), Validators.pattern(this.commonService.patterns.alphaNumericPFID)]],
      groupBy: ['senderId', [Validators.required]]
    });
    this.groupBy = this.commonService.titleCase(this.reportingForm.controls['groupBy'].value);
  }

  setEndDAte() {
    this.sDate = this.reportingForm.controls['startDate'].value,
      this.then = moment(new Date(this.sDate));
    this.minDate = this.then.format('YYYY-MM-DD');
    this.reportingForm.controls['endDate'].setValue(this.sDate);
  }

  setStartDAte() {
    this.maxDate = moment(new Date()).format('YYYY-MM-DD');
    // this.eDate = this.reportingForm.controls['endDate'].value,
    //   this.edate = moment(new Date(this.eDate));
    // this.maxDate = this.edate.format('YYYY-MM-DD');
  }

  submitForm() {
    this.formSubmitted = true;
    let fromDate = new Date(this.reportingForm.controls['startDate'].value);
    let toDate = new Date(this.reportingForm.controls['endDate'].value);
    this.formattedFromDate = moment(fromDate).format("YYYY-MM-DD");
    this.formattedToDate = moment(toDate).format("YYYY-MM-DD");
    let dept = this.reportingForm.controls['department'].value;
    let deptStr = dept.toString();
    let deptNames, aggNames, channelNames;
    if (dept.length > 0) {
      // deptNames = dept.map(a => a);
      deptNames = dept.map(a => a.department);
    }
    else {
      deptNames = this.reportingForm.controls['department'].value;
    }
    let agg = this.reportingForm.controls['aggregator'].value;
    if (agg.length > 0) {
      // aggNames = agg.map(a => a);
      aggNames = agg.map(a => a.aggregatorName);
    } else {
      aggNames = this.reportingForm.controls['aggregator'].value;
    }
    let channel = this.reportingForm.controls['channel'].value;
    if (channel !== null) {
      // aggNames = agg.map(a => a);
      channelNames = channel.map(a => a.channelName);
    } else {
      channelNames = this.reportingForm.controls['channel'].value;
    }
    this.reportingForm.controls['startDate'].setValue(this.formattedFromDate);
    this.reportingForm.controls['endDate'].setValue(this.formattedToDate);
    if (this.reportingForm.valid) {
      let req = {
        startDate: this.reportingForm.controls['startDate'].value,
        endDate: this.reportingForm.controls['endDate'].value,
        channel: channelNames,
        // this.reportingForm.controls['channel'].value,
        department:deptNames.toString(),
        //  this.reportingForm.controls['department'].value,
        // this.reportingForm.controls['aggregator'].value
        // deptNames,aggNames
        aggregator:aggNames.toString() ,
        campaign: this.reportingForm.controls['campaign'].value,
        sender: this.reportingForm.controls['sender'].value,
        username: this.reportingForm.controls['user'].value,
        userId: this.commonService.getUser(),
        groupBy: this.commonService.getGroupBy('summary')
      };
      console.log("Summary report req...." + JSON.stringify(req));
      this.spinner.show();
      this.reportingService.getAllReports(req).subscribe(
        res => {
          this.spinner.hide();
          this.groupBy = this.commonService.titleCase(this.reportingForm.controls['groupBy'].value);
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.reportList = res['data'];
            if (this.reportList) {
              this.reportList.forEach(element => {
                switch (element['categoryId']) {
                  case 1:
                    element['categoryId'] = "OTP";
                    break;
                  case 2:
                    element['categoryId'] = "Transaction";
                    break;
                  case 3:
                    element['categoryId'] = "Informational";
                    break;
                  case 4:
                    element['categoryId'] = "Promotional";
                    break;
                  default:
                    element['categoryId'] = "NA";
                }
              })
            }
            this.dataSource = new MatTableDataSource(this.reportList);
            if (this.dataSource.data.length == 0) {
              this.dataSourceLength = 0;
            } else {
              this.dataSourceLength = this.dataSource.data.length;
            }
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;

          } else {
            this.reportList = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length == 0) {
      this.dataSourceLength = 0;
    }
    else {
      this.dataSourceLength = this.dataSource.filteredData.length;
    }
  }
  getFile(id: number, name: string, status: number) {
    let req = {
      campaignId: id,
      type: status
    }
    this.spinner.show();
    this.reportingService.fileDownload(req).subscribe(
      res => {
        this.spinner.hide();
        if (res) {
          let fileName;
          if (status == 1) {
            fileName = id + "_Success";
          }
          else {
            fileName = id + "_Fail";
          }
          this.commonService.downloadFile(res, fileName, "application/csv", ".csv")
        } else {
          this.commonService.showErrorToast("No data found");
        }
      });
  }
  // sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
  //   this.reportList = this.commonService.sortArray(isAssending, sortBy, this.reportList, isNumber);
  // }

  exportAs(fileType: string) {
    let data = [];
    this.reportList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Submitted On"] = new Date(element.date).toLocaleDateString('en-GB');
      excelDataObject["Channel"] = element['channel'];
      excelDataObject["Campaign ID"] = element.campaignId;
      excelDataObject["Campaign Name"] = element.campaignName;
      if (this.commonService.getUserByRole() == 'ROLE_ADMIN') {
        excelDataObject["Department Name"] = element.department;
      }
      if (this.commonService.getUserByRole() == 'ROLE_ADMIN' || this.commonService.getUserByRole() == 'ROLE_HOD') {
        excelDataObject["Application Name"] = element.username;
      }
      if (this.commonService.getUserByRole() == 'ROLE_ADMIN' || this.commonService.getUserByRole() == 'ROLE_CAMPAIGN') {
        excelDataObject["Aggregator Name"] = element.aggregator;
      }
      excelDataObject["Sender CLI"] = element.senderId;
      excelDataObject["Priority"] = element.categoryId;
      excelDataObject["Total SMS"] = element.countTotal;
      excelDataObject["Count Submitted"] = element.countSubmitted;
      excelDataObject["Count Pending"] = element.countPending;
      excelDataObject["Successful SMS"] = element.countDelivered;
      excelDataObject["SMS Retried"] = element.countRetried;
      excelDataObject["Failed SMS"] = element.countFailed;
      excelDataObject["Delivery Expired"] = element.countExpired;
      excelDataObject["DND Count"] = element.countDnd;
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'Summary-Report');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'Summary-Report');
        break;
      default:
        this.excelService.exportAsCsvFile(data, 'Summary-Report');
        break;
    }
  }
  showPdf() {
    let fromDate = new Date(this.reportingForm.controls['startDate'].value);
    let toDate = new Date(this.reportingForm.controls['endDate'].value);
    this.formattedFromDate = moment(fromDate).format("YYYY-MM-DD");
    this.formattedToDate = moment(toDate).format("YYYY-MM-DD");
    let dept = this.reportingForm.controls['department'].value;
    let deptStr = dept.toString();
    let deptNames, aggNames;
    if (dept.length > 0) {
      deptNames = dept.map(a => a);
    }
    else {
      deptNames = this.reportingForm.controls['department'].value;
    }
    let agg = this.reportingForm.controls['aggregator'].value;
    if (agg.length > 0) {
      aggNames = agg.map(a => a);
    } else {
      aggNames = this.reportingForm.controls['aggregator'].value;
    }
    this.reportingForm.controls['startDate'].setValue(this.formattedFromDate);
    this.reportingForm.controls['endDate'].setValue(this.formattedToDate);
    if (this.reportingForm.valid) {
      let req = {
        startDate: this.reportingForm.controls['startDate'].value,
        endDate: this.reportingForm.controls['endDate'].value,
        channel: this.reportingForm.controls['channel'].value,
        department: this.reportingForm.controls['department'].value,
        aggregator: this.reportingForm.controls['aggregator'].value,
        campaign: this.reportingForm.controls['campaign'].value,
        sender: this.reportingForm.controls['sender'].value,
        username: this.reportingForm.controls['user'].value,
        userId: this.commonService.getUser(),
        groupBy: this.commonService.getGroupBy('summary')
      };
      console.log("Summary report PDF req...." + JSON.stringify(req));
      this.spinner.show();
      this.reportingService.generateSummaryPDF(req).subscribe(
        res => {
          this.spinner.hide();
          this.groupBy = this.commonService.titleCase(this.reportingForm.controls['groupBy'].value);
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.pdfResponse = res['data'];
            console.log("PDF API response........" + this.pdfResponse);
            const linkSource = this.pdfResponse;
            const downloadLink = document.createElement("a");
            let date = new Date();
            console.log("Date for PDF name" + moment(date).format('YYYYMMDDHHMMSS'))
            const fileName = "SUMMARY_REPORT_" + moment(date).format('YYYYMMDDHHMMSS') + ".pdf";
            downloadLink.href = linkSource;
            downloadLink.download = fileName;
            downloadLink.click();
          } else {
            this.pdfResponse = '';
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });

    }
  }
  downloadFile(status,obj){
    console.log(JSON.stringify(obj));
    if(obj.campaignId>0){
      let id = status=='F'?2:1;
      this.getFile(obj.campaignId,obj.campaignName,id);
    }else if(obj.campaignId==0){
      if(this.commonService.getUserByRole()=="ROLE_ADMIN" || this.commonService.getUserByRole()=="ROLE_API"){
        let date = obj?obj.date.split('-')[0]+""+obj.date.split('-')[1]+""+obj.date.split('-')[2]:null;
        let fileName = status+"-"+obj.userId+"-"+obj.username+"-"+obj.senderId+"-"+date;
        console.log(fileName);
        this.spinner.show();
        this.reportingService.downloadReport(fileName).subscribe(
          res => {
            this.spinner.hide();
            if (res) {
              console.log(res);
              this.commonService.downloadFile(res, fileName, "application/csv", ".csv")
            } else {
              this.commonService.showErrorToast("No data found");
            }
          });
      }
    }
  }
}
