
import { Injectable } from '@angular/core';
import { NetworkService } from '../../common/network.service';
import { CommonService } from '../../common/common.service'

@Injectable()
export class SenderIdService {

  constructor(private _networkService: NetworkService, private _commonService : CommonService) { }

  getSenderIdList(req: any) {
    return this._networkService.get('senderid/info/' + req['userId'] + '&showAll=all', null, 'bearer');
  }

  getAllUsers() {
    return this._networkService.get('user', null, 'bearer');
  }

  getAllAccountTypes() {
    return this._networkService.get('types', null, 'bearer');
  }

  saveSenderId(req: any) {
    return this._networkService.post('honcho/senderid/info' ,req ,null, 'bearer');
  }
  getUniqueFlag(req : any){
    return this._networkService.get('honcho/senderid/validate/cli/' + req.senderID  , null, 'bearer');
  }

  getPendingSenderIdList(id: any) {
    return this._networkService.get('senderid/info/' + id + '/pending', null, 'bearer');
  }

  deleteSenderId(req: any) {
    return this._networkService.post('honcho/senderid/' + req['id'] + '?remarks='+ req['remarks'], null, 'bearer');
  }

  updateSattus(req: any) {
    return this._networkService.post('honcho/senderid/approve/' + req["senderid"] + '?status=' + req["status"], null, null, 'bearer');
  }

    getSenderIdListByUser(req:any){
      if(req['roleName'] == 'ROLE_ADMIN'){
        return this._networkService.get('honcho/senderid/info/' + '?showAll=all', null, 'bearer');
      }else if(req['roleName'] == 'ROLE_HOD'){
        return this._networkService.get('provost/senderid/info/' + '?showAll=all', null, 'bearer');
      }else if(req['roleName'] == 'ROLE_CAMPAIGN'){
        return this._networkService.get('hype/senderid/info/' + '?showAll=all', null, 'bearer');
      }else if(req['roleName'] == 'ROLE_API'){
        return this._networkService.get('txns/senderid/info/' + '?showAll=all', null, 'bearer');
      }
    }



  uploadFile(req: any) {
    return this._networkService.uploadFile('honcho/master/fileUpload', req, null, 'bearer');
  }

  saveBulkSenderId(req: any) {
    return this._networkService.post('honcho/senderid/bulkUpload', req, null, 'bearer');
  }

  getUsersList(type) {
    return this._networkService.get(
      "honcho/user/list/" + type ,
      null,
      "bearer"
    );
  }

}

